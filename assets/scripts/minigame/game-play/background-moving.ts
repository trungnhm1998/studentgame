const { ccclass, property } = cc._decorator;

@ccclass
export default class BackgroundMoving extends cc.Component {

  _listBackgroundNode: cc.Node[] = [];
  _isMoveActived: boolean = false;
  _movingVelocity: number = 0;
  _posY: number = 0;
  // LIFE-CYCLE CALLBACKS:

  // onLoad () {}

  start() {
    this._listBackgroundNode = new Array<cc.Node>();
    this._listBackgroundNode = this.node.children;
    this._posY = 0; // this._listBackgroundNode[0].height/2;
  }

  onEnable() {
    this._isMoveActived = false;
  }

  startMoving(velocity: number) {
    for (let idx = 0; idx < this._listBackgroundNode.length; idx++) {
      this._listBackgroundNode[idx].setPosition(new cc.Vec2(idx * this._listBackgroundNode[idx].width, this._posY));
      cc.log("startMoving: ", this._listBackgroundNode[idx].position.x + ":" + this._listBackgroundNode[idx].position.y);
    }
    this._isMoveActived = true;
    this._movingVelocity = velocity;
  }

  stopMoving() {
    this._isMoveActived = false;
  }

  update(dt) {
    if (this._isMoveActived) {
      // let newDistanceMoved = this._movingVelocity * dt;
      for (let idx = 0; idx < this._listBackgroundNode.length; idx++) {
        this._listBackgroundNode[idx].setPosition(cc.v2(cc.misc.lerp(this._listBackgroundNode[idx].position.x, this._listBackgroundNode[idx].position.x - 10, dt * this._movingVelocity), this._posY));
        if (this._listBackgroundNode[idx].position.x < -this._listBackgroundNode[idx].width) {
          let preIdx = idx - 1;
          if (preIdx < 0) { preIdx = this._listBackgroundNode.length - 1; }
          this._listBackgroundNode[idx].setPosition(new cc.Vec2(this._listBackgroundNode[preIdx].position.x + this._listBackgroundNode[idx].width, this._posY));
        }
      }
    }
  }
}
