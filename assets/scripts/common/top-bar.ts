import { ACE } from "./../core/global-info";
import { SERVER_EVENT, STORAGE_KEY } from "../core/constant";
import AccountManager from "../managers/account-manager";
import LoginService from "../login/login-service";
import PopupTutorial from "./tutorial";
import { NoticeDialogType } from "../dialogs/notice-dialog";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

export enum TutorialType {
  Map = 0,
  School,
  Shop,
  StudentHouse,
}

const TOP_BAR_ATLAS_PREFIX = "common/top-ui/top_ui_4_0";

@ccclass
export default class TopBar extends cc.Component {
  @property(cc.Sprite)
  portraitImage: cc.Sprite = null;

  @property(cc.Label)
  nameString: cc.Label = null;

  @property(cc.Label)
  starString: cc.Label = null;

  @property(cc.Label)
  coinString: cc.Label = null;

  @property(cc.Node)
  buttonsGroup: cc.Node = null;

  @property([PopupTutorial])
  popupTutorials: PopupTutorial[] = [];

  @property([cc.SpriteFrame])
  avatars: cc.SpriteFrame[] = [];

  @property(cc.SpriteFrame)
  audioOffSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  audioOnSF: cc.SpriteFrame = null;

  @property(cc.Sprite)
  settingButton: cc.Sprite = null;

  @property({
    visible: false,
  })

  mapCallback: Function = null;
  prevSceneNames: string[] = ["login"];
  storyOptions = null;
  flag: boolean = true;

  onLoad() {
  }

  start() {
    this.init();
  }

  init() {
    this.changePortraitImage();
  }

  addStory(options) {
    this.storyOptions = options;
  }

  updateUserInfo() {
    const {
      studentFirstName,
      studentLastName,
      stars,
      coins,
    } = AccountManager.getInstance();
    const name = `${studentFirstName} ${studentLastName}`;
    this.changeName(name);
    this.updateStar(stars);
    this.updateCoin(coins);
    this.changePortraitImage();
  }

  positionButtons() {
    // Positions and activiates the various big buttons according to the scene.
    const backButton: cc.Node = this.buttonsGroup.getChildByName("backButton");
    const powerButton: cc.Node = this.buttonsGroup.getChildByName("powerButton");
    const guideButton: cc.Node = this.buttonsGroup.getChildByName("guideButton");
    const sceneName = ACE.sceneManager.currentScene.name;
    const buttons = [backButton, powerButton, guideButton];
    let buttonOrder: cc.Node[];
    let numButtons: number;

    if (sceneName === "map") {
      buttonOrder = [powerButton, guideButton];
      numButtons = 2;
    } else if (sceneName === "school" || sceneName === "character-shop" || sceneName === "student-house") {
      buttonOrder = [backButton, guideButton];
      numButtons = 2;
    } else if (sceneName === "minigame-menu" || sceneName == "math-room") {
      buttonOrder = [backButton];
      numButtons = 1;
    } else {
      buttonOrder = [backButton, powerButton, guideButton];
      numButtons = 3;
    }

    // insactive
    for (let i = 0; i < buttons.length; i++) {
      buttons[i].active = false;
    }
    // active and sort
    for (let i = 0; i < buttonOrder.length; i++) {
      buttonOrder[i].active = true;
      buttonOrder[i].x = i * 65;
    }
  }

  backButtonClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    const curScene = ACE.sceneManager.currentScene.name;
    let prevScene = "";
    if (curScene === "math-room") {
      prevScene = "school";
    } else if (curScene === "school"
      || curScene === "character-shop"
      || curScene === "student-house"
      || curScene === "minigame-menu"
    ) {
      prevScene = "map";
    } else if (curScene === "select-level") {
      prevScene = "minigame-menu";
    }
    cc.log("Back button on clicked");
    cc.log(ACE.sceneManager.sceneStack);
    ACE.sceneManager.pushScene(prevScene);
  }

  powerButtonClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    // insert code to log out user here.
    ACE.dialogManager.showNoticeDialog("Do you really want to logout?", NoticeDialogType.YES_NO, () => {
      LoginService.logout();
    });
  }

  guideButtonClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    const sceneName = ACE.sceneManager.currentScene.name;
    let tutorialType: TutorialType;
    switch (sceneName) {
      case "map":
        tutorialType = TutorialType.Map;
        break;
      case "character-shop":
        tutorialType = TutorialType.Shop;
        break;
      case "school":
        tutorialType = TutorialType.School;
        break;
      case "student-house":
        tutorialType = TutorialType.StudentHouse;
        break;
    }
    this.showTutorial(tutorialType);
  }

  toggleSound() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    // Opens and closes the settings menu.
    let toggleValue = ACE.resManager.getStorage(STORAGE_KEY.IS_AUDIO_OFF);
    toggleValue = !toggleValue as boolean;
    ACE.resManager.saveStorage(STORAGE_KEY.IS_AUDIO_OFF, toggleValue);
    this.settingButton.spriteFrame = toggleValue ? this.audioOnSF : this.audioOffSF;
    const volume = toggleValue ? 1.0 : 0.0;
    AudioManager.getInstance().setAudioVolume(volume, volume);
  }

  volumeButtonClick() {
  }

  hide() {
    this.node.active = false;
  }

  show() {
    this.node.active = true;
  }

  // Changes the protrait image.
  changePortraitImage() {
    // add in gender check here.
    const studentGender = AccountManager.getInstance().gender;
    if (studentGender) {
      this.portraitImage.spriteFrame = this.avatars[0];
    } else {
      this.portraitImage.spriteFrame = this.avatars[1];
    }
  }

  // changes the name string.
  changeName(name: string) {
    this.nameString.string = name;
  }

  updateStar(value: number) {
    this.starString.string = value.toLocaleString("en");
  }

  updateCoin(value: number) {
    this.coinString.string = value.toLocaleString("en");
  }

  updateCoinStar() {
    LoginService.profile(AccountManager.getInstance().userName);
  }

  showTutorial(tutorialIndex: number, closeTutorialCallback?: Function) {
    if (this.mapCallback != null && tutorialIndex == TutorialType.Map) {
      this.mapCallback();
    } else {
      this.popupTutorials[tutorialIndex].node.active = true;
      this.popupTutorials[tutorialIndex].closeTutorialCallback = closeTutorialCallback;
    }
  }

  toggleButtonGroup(isShow = true) {
    this.buttonsGroup.active = isShow;
  }
}
