import GameScene from "../../common/game-scene";
import { ACE } from "../../core/global-info";
import { SERVER_EVENT, GAME_EVENT, STORAGE_KEY } from "../../core/constant";
import LoginService from "../login-service";
import AccountManager from "../../managers/account-manager";
import SchoolService from "../../school/school-service";
import { NoticeDialogType } from "../../dialogs/notice-dialog";
import PetAva from "./pet-ava";
import PetFlyEffect from "./pet-fly-effect";
import RevealEggEffect from "./reveal-egg-effect";
import NodeUtils from "../../core/utils/node-utils";
import PetSubmitGroup from "./pet-submit-group";
import CharacterComponent from "../../common/character-component";
import AudioManager from "../../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ChoosePet extends GameScene {
  @property(cc.Node)
  background: cc.Node = null;

  @property(cc.Node)
  panelMask: cc.Node = null;

  @property(cc.Node)
  petPanel: cc.Node = null;

  @property(cc.Node)
  petPanelEndPos: cc.Node = null;

  @property(cc.Sprite)
  petShadow: cc.Sprite = null;

  @property(cc.Label)
  petName: cc.Label = null;

  @property([cc.Node])
  petAvas: cc.Node[] = [];

  @property(PetFlyEffect)
  petFlyEffect: PetFlyEffect = null;

  @property(cc.Node)
  eggNode: cc.Node = null;

  @property(cc.Node)
  effectGroup: cc.Node = null;

  @property(cc.Node)
  blankPetNode: cc.Node = null;

  @property(cc.Node)
  petGroup: cc.Node = null;

  @property(PetSubmitGroup)
  submitGroup: PetSubmitGroup = null;

  @property(cc.Node)
  effectLayer: cc.Node = null;

  @property(cc.Node)
  showdownLayer: cc.Node = null;

  @property(CharacterComponent)
  character: CharacterComponent = null;

  @property(sp.Skeleton)
  ufo: sp.Skeleton = null;

  @property(cc.Sprite)
  petShowdown: cc.Sprite = null;

  @property([cc.Node])
  characterParticles: cc.Node[] = [];

  @property([cc.Node])
  petParticles: cc.Node[] = [];

  _selectedPetSpriteFrame: cc.SpriteFrame = null;
  _petSelection: number = -1;
  _isPanelActive = false;
  _petData: any = {
    1: "s_draco",
    2: "s_haya",
    3: "s_moru",
    4: "s_shoo",
    5: "s_umic",
  };

  onLoad() {
    super.onLoad();
    this.submitGroup._submitCallback = (nickname) => {
      ACE.gameSocket.emit("finishSelectionCharacterPet", {
        nickname,
        petId: this._petSelection
      });
    };
    this.petAvas.forEach((petAva) => {
      petAva.getComponent(PetAva).choosePetRef = this;
    });
  }

  onEnter() {
    super.onEnter();
    this.panelMask.active = false;
    this.petPanel.active = false;
    this.petFlyEffect.node.active = false;
    this.submitGroup.node.active = false;
    this.petPanel.y = cc.winSize.height + this.petPanel.height;
    this.showdownLayer.active = false;
    [...this.characterParticles, ...this.petParticles].forEach((pNode) => {
      pNode.active = false;
    });
    ACE.gameSocket.emit("getPetsForSelection");
    const { renderOrder, renderItems } = this.passData;
    this.character.setRenderOrder(renderOrder);
    this.character.setRenderItems(renderItems);
    AudioManager.getInstance().playAudio("npc_appearing");
    AudioManager.getInstance().playAudio("star4");
    ACE.fairyDialog.show([
      "Every adventurer needs a companion to join them on their journey! So, lets choose your pet!!",
    ]);
  }

  start() {

  }

  registerEvents() {
    // finishSelectionCharacterPet(nickname, petId) 
    ACE.gameSocket.on("getPetsForSelection", (data) => {
      cc.log("getPetsForSelection", data);
    });
    ACE.gameSocket.on("finishSelectionCharacterPet", (data) => {
      cc.log("finishSelectionCharacterPet", data);
      AccountManager.getInstance().profile.petId = this._petSelection;
      this.onFinishSelectionPet(data);
    });
    // ACE.gameSocket.on("finishSelection", (data) => {
    //   this.onFinishSelection(data);
    // });
    ACE.gameSocket.on(SERVER_EVENT.PROFILE, (data) => {
      this.onProfile(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.GET_ASSIGNMENT_FOR_MATH_HOUSE, (data) => {
      this.onGetAssignment(data);
    });

    // ACE.gameSocket.on("finishSelectionPet", (data) => {
    //   this.onFinishSelectionPet(data);
    // });
  }

  onResize() {
    super.onResize();
  }

  onError(data) {
    super.onError(data);
    const errorStr = "System can not create new character, please try again!";
    if (data.error && data.error == errorStr) {
      ACE.sceneManager.pushScene("choose-character", {}, true);
    }
  }

  onFinishSelection(data) {
    if (ACE.sceneManager.currentScene == this) {
      ACE.gameSocket.emit("finishSelectionPet", {
        // nickname: this.inputName.string,
        // petId: this.petScript.getPetIndex() + 1,
        // petId: 8,
      });
      // AccountManager.getInstance().profile.petId = this.petScript.getPetIndex() + 1;
    }
  }

  onProfile(data) {
    if (ACE.sceneManager.currentScene == this) {
      AccountManager.getInstance().profile = data;
      ACE.topBar.updateUserInfo();
    }
  }

  onGetAssignment(data) {
    if (ACE.sceneManager.currentScene == this) {
      ACE.resManager.saveStorage(STORAGE_KEY.CHARACTER_EXIST, { Exist: AccountManager.getInstance().isCharacterExist });
      ACE.sceneManager.pushScene("map");
    }
  }

  onFinishSelectionPet(data) {
    if (ACE.sceneManager.currentScene == this) {
      if (data == true) {
        LoginService.profile(AccountManager.getInstance().userName);
        this.submitGroup.node.runAction(cc.fadeOut(0.5));
        this.submitGroup.node.active = false;
        this.effectGroup.runAction(cc.fadeOut(0.3));
        this.showdownLayer.active = true;
        this.character.node.opacity = 0;
        this.petShowdown.node.opacity = 0;
        this.petShowdown.spriteFrame = this.eggNode.getComponent(RevealEggEffect).pet.spriteFrame;

        this.node.runAction(cc.sequence(
          cc.delayTime(1.45),
          cc.callFunc(() => {
            this.characterParticles.forEach((particle) => {
              particle.active = true;
            });
          }),
          cc.delayTime(0.35),
          cc.callFunc(() => {
            AudioManager.getInstance().playAudio("character_appearing");
            this.character.node.runAction(cc.fadeIn(0.2));
            this.showdownLayer.getChildByName("effect-behind").active = true;
            this.showdownLayer.getChildByName("effect-infront").active = true;
          }),
          cc.delayTime(0.2),
          cc.callFunc(() => {
            this.petParticles.forEach((particle) => {
              particle.active = true;
            });
          }),
          cc.delayTime(0.3),
          cc.callFunc(() => {
            AudioManager.getInstance().playAudio("character_appearing");
            this.petShowdown.node.runAction(cc.fadeIn(0.3));
          }),
        ));
        this.showdownLayer.runAction(cc.sequence(
          cc.fadeIn(0.5),
          cc.delayTime(6),
          cc.callFunc(() => {
            AudioManager.getInstance().playAudio("npc_appearing");
            AudioManager.getInstance().playAudio("star6");
            ACE.fairyDialog.show([
              "And we're all set! Are you ready to enter the world of S.T.A.R. Maths Online??",
            ]);
          }),
          cc.delayTime(4.5),
          cc.callFunc(() => {
            SchoolService.enrolmentList();
            ACE.dialogManager.showLoading();
            this.submitGroup.node.active = false;
          }),
        ));
      } else {
        ACE.dialogManager.showNoticeDialog("Something went wrong.", NoticeDialogType.OK);
      }
    }
  }

  showPetInfo(event: cc.Event.EventTouch, petId: number) {
    if (this.panelMask.active) { return; }
    AudioManager.getInstance().playAudio("CHOOSE_CHAR_SOUND");
    cc.log("ChoosePet::showPetInfo", petId);
    this.panelMask.active = true;
    this.petPanel.active = true;
    cc.loader.loadRes(`images/choose-pet/${this._petData[petId]}`, cc.SpriteFrame, (error, spriteFrame) => {
      if (error) { return; }
      this._petSelection = petId;
      this.petShadow.spriteFrame = spriteFrame;
      this.petName.string = (this._petData[petId] as string).replace("s_", "").toUpperCase();
      this.petPanel.runAction(cc.sequence(
        cc.moveTo(0.5, cc.v2(0, this.petPanelEndPos.y)).easing(cc.easeBackOut()),
        cc.callFunc(() => {
          NodeUtils.findNodeByName(this.petPanel, "mask").active = true;
          this._isPanelActive = true;
          cc.log("cc.winSize.height", cc.winSize.height, this.petPanel.y);
        }),
      ));
    });
  }

  confirmPet(event: cc.Event.EventTouch) {
    if (!this._isPanelActive) { return; }
    if (this._petSelection == -1) { return; }
    this.panelMask.active = true;
    NodeUtils.preventMultipleClick((event.target as cc.Node).getComponent(cc.Button));
    this.closePetInfo(null, true).then(() => {
      if (this._petSelection) {
        const petNode = this.petAvas[this._petSelection - 1];
        const petAva = petNode.getComponent(PetAva);
        const petSprite = petNode.getComponent(cc.Sprite);
        cc.loader.loadRes(`images/choose-pet/${(this._petData[this._petSelection] as string).replace("s_", "f_")}`, cc.SpriteFrame, (error, spriteFrame) => {
          const worldPetNode = petNode.parent.convertToWorldSpaceAR(petNode.getPosition());
          const worldEggNode = this.eggNode.parent.convertToWorldSpaceAR(this.eggNode.getPosition());
          petAva.hideInfo();

          // draw order
          this.blankPetNode.active = true;
          this.blankPetNode.zIndex = 1;
          const localPetNodePos = this.petFlyEffect.node.parent.convertToNodeSpaceAR(worldPetNode);
          this.blankPetNode.setPosition(localPetNodePos);
          this.petFlyEffect.node.setPosition(localPetNodePos);
          this.petFlyEffect.miniPetSprite.spriteFrame = spriteFrame;
          this.petFlyEffect.node.zIndex = 2;
          this.petFlyEffect.node.setScale(2.5);
          this.eggNode.parent = this.effectGroup;
          this.eggNode.setPosition(this.effectGroup.convertToNodeSpaceAR(worldEggNode));
          this.eggNode.zIndex = 3;
          petNode.parent = this.effectGroup;
          petNode.zIndex = 1;
          this.effectGroup.sortAllChildren();

          this.node.runAction(
            cc.sequence(
              cc.delayTime(1),
              cc.callFunc(() => {
                AudioManager.getInstance().playAudio("pet_going_into_the_egg");
              }),
              cc.delayTime(1),
              cc.callFunc(() => {
                AudioManager.getInstance().playAudio("egg_shaking_cracking");
              }),
            ),
          ),
          // effect action
          petNode.runAction(cc.sequence(
            cc.scaleTo(0.4, 1.1).easing(cc.easeOut(1.0)),
            cc.spawn(
              cc.fadeOut(0.5),
              cc.rotateTo(0.5, 10),
              cc.callFunc(() => {
                this.petFlyEffect.node.opacity = 0;
                this.petFlyEffect.node.active = true;
                this.petFlyEffect.node.runAction(cc.fadeIn(0.5));
              }),
            ),
            cc.spawn(
              // cc.rotateTo(0.5, 0),
              cc.fadeOut(0.2),
              cc.callFunc(() => {
                this.petFlyEffect.playEffect(worldPetNode, worldEggNode).then(() => {
                  this.petFlyEffect.node.active = false;
                  this.panelMask.runAction(cc.fadeTo(2.0, 230));
                  let eggEffect = this.eggNode.getComponent(RevealEggEffect);
                  eggEffect._petId = this._petSelection;
                  this.blankPetNode.parent = this.petGroup;
                  eggEffect.playEffect().then(() => {
                    AudioManager.getInstance().playAudio("pet_appearing");
                    this.submitGroup.node.active = true;
                    this.submitGroup.node.opacity = 0;
                    this.submitGroup.node.runAction(cc.fadeIn(1.5));
                    AudioManager.getInstance().playAudio("npc_appearing");
                    AudioManager.getInstance().playAudio("star5");
                    ACE.fairyDialog.show([
                      "Congratulations! Your pet is ready to travel with you! Now let's give your pet a name!!",
                    ]);
                  });
                });
              }),
            ),
          ));
        });
      }
    });
  }

  closePetInfo(event, isConfirm = false) {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    if (!this._isPanelActive) { return; }
    if (!isConfirm) {
      this._petSelection = -1;
    }
    return new Promise((resolve, reject) => {
      this.petPanel.runAction(
        cc.sequence(
          cc.moveTo(0.5, cc.v2(0, cc.winSize.height + this.petPanel.height)).easing(cc.easeBackIn()),
          cc.callFunc(() => {
            NodeUtils.findNodeByName(this.petPanel, "mask").active = false;
            this.panelMask.active = isConfirm;
            this._isPanelActive = false;
            resolve();
          }),
        ));
    });
  }
}
