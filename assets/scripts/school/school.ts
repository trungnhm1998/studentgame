import { IHomework } from "./../models/homework-model";
import { SchoolType } from "./math-room";
import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";
import SchoolService, { NotiObject, NotiType } from "./school-service";
import SchoolManager from "../managers/school-manager";
import AccountManager from "../managers/account-manager";
import Leaderboard from "../Leaderboard/leaderboard";
import { TutorialType } from "../common/top-bar";
import IConcept from "../models/concept";
import SchoolItems from "./school-items";
import CharacterComponent from "../common/character-component";
import NodeUtils from "../core/utils/node-utils";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class School extends GameScene {
  @property(CharacterComponent)
  character: CharacterComponent = null;

  @property(SchoolItems)
  schoolItems: SchoolItems = null;

  @property(cc.Button)
  leaderboardButton: cc.Button = null;

  @property(Leaderboard)
  leaderboard: Leaderboard = null;

  hasData: boolean;
  hasCalled: boolean;

  onLoad() {
    super.onLoad();
    this.schoolItems.itemClickCallback = (name) => this.onChosenRoom(name);
  }

  onResize() {
    this.node.getChildByName("schoolBG").scale = this.getScaleRatio();
  }

  onEnter() {
    super.onEnter();
    ACE.webviewManager.preloadWebview();
    ACE.topBar.show();
    ACE.topBar.positionButtons();
    this.schoolItems.setupItems();
    this.character.autoUpdate(false);
    const mapTutorialState = AccountManager.getInstance()._gameStateTutorial.get("mathHouse");
    if (mapTutorialState && !mapTutorialState.isClosed) {
      ACE.topBar.showTutorial(TutorialType.School);
    }
    const strText = `Welcome ${AccountManager.getInstance().studentFirstName} ${
      AccountManager.getInstance().studentLastName
      } to your Maths House!`;
    SchoolService.notificationObjects.push(
      new NotiObject(strText, NotiType.NORMAL),
    );
    this.hasData = false;
    this.hasCalled = false;
    AudioManager.getInstance().playAudio("MATH_HOUSE", true, true);
  }

  onVisible() {
    super.onVisible();
    if (!ACE.webviewManager.openingWebview && !AccountManager.getInstance().gettingSchoolData) {
      ACE.dialogManager.hideLoading();
    }
  }

  onGetAssignmentForMathHouse(data) {
    super.onGetAssignmentForMathHouse(data);
    this.schoolItems.setupItems();
    cc.log("School::onGetAssignmentForMathHouse", ACE.webviewManager.openingWebview);
    if (!ACE.webviewManager.openingWebview) {
      ACE.dialogManager.hideLoading();
    }
  }

  registerEvents() {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_TOPIC, (data) => this.onLoadTopics(data));
    ACE.gameSocket.on(SERVER_EVENT.LOAD_HOMEWORK, (homeworkData) =>
      cc.log("Homework", homeworkData),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_NAPLAN, (naplanData) =>
      cc.log("Naplan", naplanData),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_REVISION, (revisionData) =>
      cc.log("Revision", revisionData),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_ASSIGNMENT, (assignmentData) =>
      cc.log("Assignment", assignmentData),
    );
    ACE.gameSocket.on(SERVER_EVENT.FOCUS_FIRST_PAGE, (data) =>
      this.onLoadFocusFirstPage(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_FOCUS_CENTER, (data) => {
      this.onLoadFocusCenter(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.IMPROVEMENT_FIRST_PAGE, (data) =>
      this.onLoadImprovementFirstPage(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_IMPROVEMENT, (data) => {
      this.onLoadImprovement(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_ASSIGNMENT_FOR_MATH_HOUSE, (data) => {
      this.onGetAssignmentForMathHouse(data);
    });
  }

  update() {
    if (!this.hasData) {
      if (
        SchoolManager.getInstance().improvementData &&
        SchoolManager.getInstance().homeworkData &&
        SchoolManager.getInstance().naplanData
      ) {
        this.hasData = true;
      }
    }

    if (this.hasData && !this.hasCalled) {
      this.hasCalled = true;
      this.setupNotiSpeechText();
    }
  }

  leaderboardBtnOnClick() {
    NodeUtils.preventMultipleClick(this.leaderboardButton);
    this.leaderboard.showPopup();
  }

  setupNotiSpeechText() {
    let strText = "";
    const studentName = `${AccountManager.getInstance().studentFirstName} ${
      AccountManager.getInstance().studentLastName
      }`;
    if (SchoolManager.getInstance().homeworkData.state !== "disabled") {
      if (SchoolManager.getInstance().homeworkData.Counter === 1) {
        // homework begin
        if (SchoolManager.getInstance().homeworkData.NumberQaAttempt === 0) {
          // T1
          strText =
            "Hi there " +
            studentName +
            "! We have a new homework challenge waiting for us! Let’s go there and earn some coins!";
          SchoolService.notificationObjects.push(
            new NotiObject(strText, NotiType.HOMEWORK),
          );
        } else {
          if (SchoolManager.getInstance().homeworkData.Finished === true) {
            if (
              SchoolManager.getInstance().homeworkData.Percentage >= 70 &&
              SchoolManager.getInstance().homeworkData.Percentage < 85
            ) {
              // T3
              strText =
                "Great work on your previous homework attempt " +
                studentName +
                "! Let’s go back there and get a higher score for more bonus coins and stars!";
              SchoolService.notificationObjects.push(
                new NotiObject(strText, NotiType.HOMEWORK),
              );
            } else if (
              SchoolManager.getInstance().homeworkData.Percentage >= 85
            ) {
              // T3
              strText =
                "Amazing work " +
                studentName +
                "! You are brilliant! I’ve set some harder questions in your homework if you are ready for challenge!";
              SchoolService.notificationObjects.push(
                new NotiObject(strText, NotiType.HOMEWORK),
              );
            } else {
              if (
                SchoolManager.getInstance().improvementData.state !== "disabled"
              ) {
                // T5
                strText =
                  "Nice try " +
                  studentName +
                  " on your previous attempt. I've placed some new concepts in the Improvement Centre. Would you like to go there now?";
                SchoolService.notificationObjects.push(
                  new NotiObject(strText, NotiType.IMPROVEMENT),
                );
              } else {
                // T4
                strText =
                  "Good effort on the previous attempt " +
                  studentName +
                  ". Let’s give the homework another go, I believe you can do better this time!";
                SchoolService.notificationObjects.push(
                  new NotiObject(strText, NotiType.IMPROVEMENT),
                );
              }
            }
          } else {
            // T2
            const numQaLeft =
              SchoolManager.getInstance().homeworkData.NumberOfQa -
              SchoolManager.getInstance().homeworkData.NumberQaAttempt;
            strText =
              "Welcome back " +
              studentName +
              ", we still have " +
              numQaLeft +
              " questions left in our homework. Lets finish it to earn some bonus coins & stars!";
            SchoolService.notificationObjects.push(
              new NotiObject(strText, NotiType.HOMEWORK),
            );
          }
        }
      } else {
        // homework is process
        if (SchoolManager.getInstance().homeworkData.Finished === true) {
          if (
            SchoolManager.getInstance().homeworkData.Percentage >= 70 &&
            SchoolManager.getInstance().homeworkData.Percentage < 85
          ) {
            // T6
            strText =
              "Solid results again " +
              studentName +
              "! Let’s do more attempts and receive all the bonus coins!";
            SchoolService.notificationObjects.push(
              new NotiObject(strText, NotiType.HOMEWORK),
            );
          } else if (
            SchoolManager.getInstance().homeworkData.Percentage >= 85
          ) {
            // T6
            strText =
              "You’ve done it again " +
              studentName +
              "! Just outstanding! Let’s do attempts and receive heaps of bonus coins!";
            SchoolService.notificationObjects.push(
              new NotiObject(strText, NotiType.HOMEWORK),
            );
          } else {
            if (
              SchoolManager.getInstance().improvementData.state !== "disabled"
            ) {
              // T5
              strText =
                "Nice try " +
                studentName +
                " on your previous attempt. I've placed some new concepts in the Improvement Centre. Would you like to go there now?";
              SchoolService.notificationObjects.push(
                new NotiObject(strText, NotiType.IMPROVEMENT),
              );

              SchoolService.scene = "Improvement";
            } else {
              // T4
              strText =
                "Good effort on the previous attempt " +
                studentName +
                ". Let’s give the homework another go, I believe you can do better this time!";
              SchoolService.notificationObjects.push(
                new NotiObject(strText, NotiType.IMPROVEMENT),
              );
            }
          }
        } else {
          // T7
          const numQaLeft =
            SchoolManager.getInstance().homeworkData.NumberOfQa -
            SchoolManager.getInstance().homeworkData.NumberQaAttempt;
          let txtAttempt = "";
          if (SchoolManager.getInstance().homeworkData.Counter === 2) {
            txtAttempt = "second";
          } else if (SchoolManager.getInstance().homeworkData.Counter === 3) {
            txtAttempt = "third";
          } else if (SchoolManager.getInstance().homeworkData.Counter === 4) {
            txtAttempt = "fourth";
          } else if (SchoolManager.getInstance().homeworkData.Counter === 5) {
            txtAttempt = "fifth";
          } else if (SchoolManager.getInstance().homeworkData.Counter === 6) {
            txtAttempt = "sixth";
          } else if (SchoolManager.getInstance().homeworkData.Counter === 7) {
            txtAttempt = "seventh";
          } else if (SchoolManager.getInstance().homeworkData.Counter === 8) {
            txtAttempt = "eighth";
          } else {
            txtAttempt = SchoolManager.getInstance().homeworkData.Counter.toString();
          }
          strText =
            "Welcome back " +
            studentName +
            ", there are " +
            numQaLeft +
            " questions left on our " +
            txtAttempt +
            " attempt. Let’s finish it to earn some coins!";
          SchoolService.notificationObjects.push(
            new NotiObject(strText, NotiType.HOMEWORK),
          );
        }
      }
    }

    if (SchoolManager.getInstance().naplanData.state !== "disabled") {
      SchoolService.scene = "Naplan";
      if (SchoolManager.getInstance().naplanData.Counter === 1) {
        // naplan begin
        if (SchoolManager.getInstance().naplanData.NumberQaAttempt === 0) {
          // T1
          strText =
            "You have the NAPLAN module unlocked for you. Would you like to practice your NAPLAN skills?";
          SchoolService.notificationObjects.push(
            new NotiObject(strText, NotiType.NAPLAN),
          );
        } else {
          const numQaLeft =
            SchoolManager.getInstance().naplanData.NumberOfQa -
            SchoolManager.getInstance().naplanData.NumberQaAttempt;
          strText =
            "We still have " +
            numQaLeft +
            " of questions left in your NAPLAN attempt. Would you like to complete it?";
          SchoolService.notificationObjects.push(
            new NotiObject(strText, NotiType.NAPLAN),
          );
        }
      }
      if (SchoolManager.getInstance().naplanData.Finished === true) {
        strText =
          "You've scored " +
          SchoolManager.getInstance().naplanData.Percentage +
          "% on your previous NAPLAN attempt. Would you like to beat that score?";
        SchoolService.notifications.push(strText);
        SchoolService.notificationObjects.push(
          new NotiObject(strText, NotiType.NAPLAN),
        );
      }
    }
    SchoolService.notiText = strText;
  }

  // make sure r name are one of [revision|practice|naplan|improvement|homework|focus|assessment]
  onChosenRoom(itemName: string) {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    SchoolManager.getInstance().roomName = itemName;
    switch (itemName) {
      case "revision":
        ACE.webviewManager.openQa("revision");
        break;
      case "practice":
        SchoolService.loadTopics(0);
        break;
      case "naplan":
        ACE.webviewManager.openQa("naplan");
        break;
      case "improvement":
        SchoolService.loadImprovement();
        break;
      case "homework":
        ACE.webviewManager.openQa("homework");
        break;
      case "focus":
        SchoolService.loadFocusCenter();
        break;
      case "assessment":
        ACE.webviewManager.openQa("assignment");
        break;
    }
  }

  /**
   *
   * @param event
   * @param eventType name whatvever you want but it must be string type always
   */
  onPetNotiCallback(event) {
    // if (SchoolService.scene != "" && SchoolService.scene != NotiType.NORMAL)
    if (SchoolService.currentNotfication != null) {
      this.onChosenRoom(SchoolService.currentNotfication.type);
    }
  }

  onLoadTopics(listTopics) {
    SchoolManager.getInstance().listTopics = listTopics;
    ACE.sceneManager.pushScene("math-room", { schoolType: SchoolType.Practice });
  }

  onLoadFocusFirstPage(focusAllData: IConcept[]) {
    ACE.sceneManager.pushScene("math-room", { schoolType: SchoolType.FocusCenter, data: focusAllData });
  }

  onLoadFocusCenter(focusAllData: IConcept[]) {
    SchoolManager.getInstance().listConcepts = focusAllData;
    if (focusAllData.length > 9) {
      SchoolService.loadFocusFirstPage();
    } else {
      ACE.sceneManager.pushScene("math-room", { schoolType: SchoolType.FocusCenter, data: focusAllData });
    }
  }

  onLoadImprovementFirstPage(improvementAllData) {
    ACE.sceneManager.pushScene("math-room", { schoolType: SchoolType.Improvement, data: improvementAllData });
  }

  onLoadImprovement(improvementAllData: IConcept[]) {
    SchoolManager.getInstance().listConcepts = improvementAllData;
    if (improvementAllData.length > 9) {
      SchoolService.loadImprovementFirstPage();
    } else {
      ACE.sceneManager.pushScene("math-room", { schoolType: SchoolType.Improvement, data: improvementAllData });
    }
  }
}
