import { Config } from './../config';
import HotUpdate, { HotUpdateCode } from "../loading/hot-update";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CheckUpdate extends cc.Component {
  onLoad() {
    cc.game.addPersistRootNode(this.node);
    if (cc.sys.isNative) {
      this.schedule(this.checkUpdate, Config.checkUpdateInterval);
      // }, Config.checkUpdateInterval);
    }
  }

  checkUpdate(t) {
    if (!HotUpdate._instance) {
      cc.log("HotUpdateInstance is null");
      return;
    }
    cc.log(`Check Update interval in map interval[${t}], is updating [${HotUpdate._instance._updating}]`);
    if (!HotUpdate._instance._updating) {
      HotUpdate._instance.init(null, true).then((code: HotUpdateCode) => {
        if (code == HotUpdateCode.ERROR) {
          cc.log("EntryLoading::HotUpdate:error");
        } else {
          cc.log("EntryLoading::HotUpdate:Update successful");
        }
      });
    } else {
      cc.log("Map::checkUpdate::Updating");
    }
  }

  onDestroy() {
    this.unschedule(this.checkUpdate);
  }
}
