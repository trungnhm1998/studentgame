import { ACE } from "./global-info";
import { STORAGE_KEY } from "./constant";

export default class AudioManager {
  public static getInstance(): AudioManager {
    if (!this._instance) {
      this._instance = new AudioManager();
    }
    return this._instance;
  }
  private static _instance: AudioManager;
  _maxAudioInstance: number = -1;
  _effectVolume: number = 1.0;
  _musicVolume: number = 1.0;
  _musicsId = [];
  _soundsId = [];
  _currentMusicPath: string = "";

  constructor() {
    this._maxAudioInstance = cc.audioEngine.getMaxAudioInstance();
    // TODO: Split sound and effect instead of toggle value
    const toggleValue = ACE.resManager.getStorage(STORAGE_KEY.IS_AUDIO_OFF);
    if (toggleValue != null) {
      const volume = toggleValue ? 1.0 : 0.0;
      this.setAudioVolume(volume, volume);
    }
  }

  /**
   * @returns audioId
   * @param fileName
   * @param isMusic
   * @param loop
   */
  playAudio(fileName: string, isMusic = false, loop = false): number {
    if (this._musicsId.length + this._soundsId.length == this._maxAudioInstance) { return; }
    let audioPath = "";
    // if (fileName.substring(fileName.length - 5, fileName.length - 1) != ".mp3") {
    //   audioPath = `/audios/${isMusic ? "musics/" : "effects/"}${fileName}.mp3`;
    // } else {
    //   audioPath = `/audios/${isMusic ? "musics/" : "effects/"}${fileName}`;
    // }

    if (fileName.includes(".mp3")) {
      audioPath = `/audios/${isMusic ? "musics/" : "effects/"}${fileName.replace(".mp3", "")}`;
    } else {
      audioPath = `/audios/${isMusic ? "musics/" : "effects/"}${fileName}`;
    }

    return this.playAudioWithPath(audioPath, isMusic, loop);
  }

  playAudioWithPath(path: string, isMusic = false, loop = false): number {
    if (isMusic) {
      if (this._currentMusicPath == path) { return; }
      this._currentMusicPath = path;
    }
    if (this._musicsId.length + this._soundsId.length == this._maxAudioInstance) { return; }
    const audioPath = path;
    let audioId = null;
    cc.loader.loadRes(audioPath, (err, clip) => {
      if (err != null) { return; }

      if (isMusic) {
        this.stopAllMusic();
      }

      if (isMusic) {
        audioId = cc.audioEngine.playMusic(clip, loop);
        this._musicsId.push(audioId);
      } else {
        audioId = cc.audioEngine.playEffect(clip, loop);
        this._soundsId.push(audioId);
      }
      // cc.log("AudioEngine audioId: " + audioId + " state " + cc.audioEngine.getState(audioId), cc.audioEngine.AudioState);
      // cc.audioEngine.is

      cc.audioEngine.setFinishCallback(audioId, this.stopAudio.bind(this, audioId, isMusic));
      return audioId;
    });

  }

  preloadAudio(paths: string[], isMusic = false): Promise<void> {
    return new Promise((resolve, reject) => {
      paths.forEach((path, idx) => {
        path = `/audios/${isMusic ? "musics/" : "effects/"}${path}`;
        cc.loader.loadRes(path, (err, clip) => {
          if (err) {
            cc.log("Failed to preload Audio with path", path);
            return;
          }
          if (idx == paths.length - 1) {
            resolve();
          }
        });
      });
    });
  }

  stopAudio(id: number, isMusic = false) {
    let idx = -1;
    if (isMusic) {
      idx = this._musicsId.indexOf(id);
    } else {
      idx = this._soundsId.indexOf(id);
    }

    if (idx > -1) {
      if (isMusic) {
        this._musicsId.splice(idx, 1);
      } else {
        this._soundsId.splice(idx, 1);
      }
    }
  }

  stopAllMusic() {
    for (const musicId of this._musicsId) {
      cc.audioEngine.stop(musicId);
    }
    this._musicsId = [];
    this._currentMusicPath = null;
  }

  get effectVolume(): number {
    return this._effectVolume;
  }

  set effectVolume(volume: number) {
    cc.audioEngine.setEffectsVolume(volume);
    this._effectVolume = volume;
  }

  get musicVolume(): number {
    return this._musicVolume;
  }

  set musicVolume(volume: number) {
    cc.audioEngine.setMusicVolume(volume);
    this._musicVolume = volume;
  }

  setAudioVolume(music: number, effect: number) {
    this.musicVolume = music;
    this.effectVolume = effect;
  }

  /**
   * Web only
   */
  resumeContext() {
    if (cc.sys.isBrowser && !CC_JSB) {
      cc.sys.__audioSupport.context.resume().then(() => {
        cc.log("audio context resumse");
      }).catch(() => {
        cc.log("audio context failed to resumse");
      })
    }
  }
}
