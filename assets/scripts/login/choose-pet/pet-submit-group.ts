import { ACE } from "../../core/global-info";
import { NoticeDialogType } from "../../dialogs/notice-dialog";
import AudioManager from "../../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PetSubmitGroup extends cc.Component {
  @property(cc.EditBox)
  petName: cc.EditBox = null;

  @property(cc.Button)
  submitButton: cc.Button = null;

  _submitCallback: Function = () => {};

  onSubmit() {
    if (this._submitCallback) {
      AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
      if (this.petName.string == "") {
        ACE.dialogManager.showNoticeDialog("Please give your pet a nickname!", NoticeDialogType.OK);
      } else {
        this._submitCallback(this.petName.string);
      }
    }
  }
}
