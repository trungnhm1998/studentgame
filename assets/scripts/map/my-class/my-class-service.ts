import { ACE } from "../../core/global-info";
import { SERVER_EVENT } from "../../core/constant";

export default class MyClassService {
  static sendGift(ID: number, FullName: string) {
    ACE.gameSocket.emit(SERVER_EVENT.SEND_GIFT, {
      studentChecks: [
        {
          ID,
          FullName,
        },
      ],
    });
  }

  static getNumberGifts() {
    ACE.gameSocket.emit(SERVER_EVENT.GET_NUMBER_OF_GIFTS, {
    });
  }

  static loadFriendList() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_FRIEND_LIST, {
    });
  }
}
