const { ccclass } = cc._decorator;

@ccclass
export default class StudentItemInShop {
  Coins: number;
  ID: number;
  IsBasic: boolean;
  IsDefault: boolean;
  IsUse: number; // : 0 / 1 :
  IsLocked: number; // buy or not,
  Name: string;
  RoomItemGroupL1ID: number; // : use for call try room,
  Stars: number;
  UrlIcon: string;
  UrlImage: string;
}
