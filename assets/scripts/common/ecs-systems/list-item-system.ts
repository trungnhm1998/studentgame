import System from "../../core/ecs/system";
import Entity from "../../core/ecs/entity";
import ListItemComponent from "../ecs-components/list-item-component";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ListItemSystem extends System {
  _filter = ["ListItemComponent"];

  onLoad() {
    super.onLoad();
  }

  onEntitiesUpdate(entity: Entity) {
    const { name, itemRef } = entity.getECSComponent(ListItemComponent)._data;
    const { index, parent } = itemRef;
    if (entity) {
      this.node.emit("update-list-item", entity);
    }
  }
}
