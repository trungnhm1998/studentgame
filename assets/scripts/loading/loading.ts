const { ccclass, property } = cc._decorator;

@ccclass
export default class Loading extends cc.Component {

  @property(cc.Label)
  info: cc.Label = null;

  @property(cc.ProgressBar)
  progressBar: cc.ProgressBar = null;

  _totalOfBoost = [];
  _totalOfOthersScene = [];

  init() {
    let scenes = ["login"];
    this.info.string = "Creating the magical island...";
    this.progressBar.progress = 0;
    // boost 0.0 -> 0.2
    cc.director.preloadScene("boost", (count, total, item) => {
      this.progressBar.progress = this.scale(count / total, 0.0, 1.0, 0.0, 0.2);
    }, (err, asset) => {
      Promise.all([
        this.newGameLoader(`/scenes/${scenes[0]}`, () => this.info.string = "Calling your companion...", 0.2, 1.0),
        // this.gameLoader(scenes.map((path) => `/scenes/${path}`), () => this.info.string = "Calling your companion..."),
      ]).then(() => {
        cc.director.loadScene("boost");
      });
      cc.log("Loaded asset");
    });
  }

  scale(num, in_min, in_max, out_min, out_max): number {
    return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  }

  newGameLoader(path: string, callback: Function, min, max) {
    callback();
    return new Promise((resolve, reject) => {
      cc.loader.loadRes((path as string), (count, total, item) => {
        this.progressBar.progress = this.scale(count / total, 0.0, 1.0, min, max);
      }, (err, res) => {
        if (err) {
          cc.log("Error while loading", path);
        }
        resolve();
      });
    });
  }

  gameLoader(path: string | string[], callback): Promise<void> {
    callback();
    this.progressBar.progress = 0;
    return new Promise((resolve, reject) => {
      if (path.length > 1) {
        cc.loader.loadResArray((path as string[]), (count, total, item) => {
          cc.log(`${item.id} count at ${count} / ${total}`);
          this._totalOfOthersScene.push(total);
          let finalTotal = this._totalOfBoost.length + this._totalOfOthersScene.length;
          this.progressBar.progress = count / (finalTotal - 186); // finalTotal = 511, too long so i minus 150 = ~361, 361 - (361 * 0.9) = ~86 is the 0.9 progress in boost scene
        }, (err, res) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      } else {
        cc.loader.loadRes((path as string), (count, total, item) => {
          cc.log(`${item.id} count at ${count} / ${total}`);
          // this.progressBar.progress = count / total;
          this._totalOfOthersScene.push(total);
          let finalTotal = this._totalOfBoost.length + this._totalOfOthersScene.length;
          this.progressBar.progress = count / finalTotal;
        }, (err, res) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      }
    });
  }
}
