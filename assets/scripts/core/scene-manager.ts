import { ACE } from "./global-info";
import GameScene from "../common/game-scene";
import NodeUtils from "./utils/node-utils";
const { ccclass, property } = cc._decorator;

const DEFAULT_SCENE_DIR = "scenes";

export default class SceneManager {
  static _instance: SceneManager = null;
  static getInstance(): SceneManager {
    if (SceneManager._instance == null) {
      SceneManager._instance = new SceneManager();
    }
    return SceneManager._instance;
  }

  // if a scene already loaded before get from this
  // or preloaded beforehand
  preloadedSceneDictionary: { [sceneName: string]: GameScene } = {};
  sceneStack: GameScene[] = [];
  previousScene: GameScene = null;
  currentScene: GameScene = null;
  // this should be null always unless you want to change new scene this variable
  // will change to that scene and back to null after done changes scene
  nextScene: GameScene = null;
  // the actual UI/Node will be the child of this sceneNodeContainer
  // latest child/scene will have the largest zIndex
  sceneNodeContainer: cc.Node;
  sceneDirPath: string = DEFAULT_SCENE_DIR;
  temp: string = "";

  constructor() {
    this.sceneStack = [];
    this.previousScene = null;
    this.currentScene = null;
    this.nextScene = null;
    this.preloadedSceneDictionary = {};
  }

  clearStack() {
    this.sceneStack = [];
    this.previousScene = null;
  }

  popScene(gameScene: (GameScene | string)) {
    if (typeof gameScene == "string" && this.preloadedSceneDictionary[gameScene]) {
      const sceneNode = this.preloadedSceneDictionary[gameScene].node;
      if (sceneNode) {
        this.preloadedSceneDictionary[gameScene] = undefined;
        sceneNode.getComponent(GameScene).onCleanup();
        sceneNode.removeFromParent(true);
        cc.log("SceneManager::popScene", gameScene);
      }
    }
  }

  /**
   * to push new screen and show it
   * @param gameScene object or name of the prefab
   * @param passData use to pass data from one scene to another
   * @param clearStack to clear the stack and have only 1 screen
   */
  pushScene(gameScene: (GameScene | string), passData?: any, clearStack: boolean = false, forceReload = false) {
    cc.log("SceneManager::pushScene", gameScene);
    if (typeof gameScene == "string") {
      // already loaded return it
      ACE.dialogManager.showLoading();
      this.preloadScene(gameScene, -1, forceReload).then(() => {
        this.nextScene = this.preloadedSceneDictionary[gameScene];
        this.nextScene.name = gameScene;
        this.nextScene.passData = passData;
        this.switchScene();
        if (clearStack) {
          this.sceneStack = [this.currentScene];
          let sceneCount = 0;
          while (this.sceneNodeContainer.childrenCount > 1) {
            const sceneNode: cc.Node = this.sceneNodeContainer.children[sceneCount];
            if (sceneNode) {
              if (sceneNode.name != this.currentScene.name) {
                this.preloadedSceneDictionary[sceneNode.name] = undefined;
                sceneNode.getComponent(GameScene).onCleanup();
                sceneNode.removeFromParent(true);
              } else {
                sceneCount++;
              }
            }
          }
        }
      });
    }
  }

  /**
   * preloadScene but not yet show it
   * just add it in to the memory/game to use later
   * @param sceneName this must be correct with the prefab scene you create
   * @param sceneIndex the greater this is the rendering order it will be
   */
  preloadScene(sceneName: string, sceneIndex = -1, forceReload = false): Promise<void> {
    return new Promise((resolve, reject) => {
      const scene = this.preloadedSceneDictionary[sceneName];
      if (scene != null && scene != undefined && !forceReload) {
        scene.node.opacity = 0;
        resolve();
      } else {
        if (forceReload) {
          if (scene) {
            cc.log("SceneMananger::preloadScene::forceReload for scene ", scene.name);
            scene.onCleanup();
            scene.node.removeFromParent();
          }
        }
        cc.loader.loadRes(
          `${this.sceneDirPath}/${sceneName}`,
          (error, prefab) => {
            if (error) {
              cc.log("SceneManager::preloadScene failed with error.", error);
              reject();
              return;
            }
            const sceneNode: cc.Node = cc.instantiate(prefab);
            sceneNode.opacity = 0;
            const gameSceneComponent: GameScene = sceneNode.getComponent(GameScene);
            this.preloadedSceneDictionary[sceneName] = gameSceneComponent;
            sceneNode.zIndex = sceneIndex != -1 ? sceneIndex : this.sceneStack.length;
            this.sceneStack.push(gameSceneComponent);
            sceneNode.active = false;
            this.sceneNodeContainer.addChild(sceneNode);
            this.sceneNodeContainer.sortAllChildren(); // for zIndex and rendering order
            resolve();
          },
        );
      }
    });
  }

  private switchScene() {
    if (this.nextScene) {
      // trying to push new scene
      // first time this.currentScene will be null
      if (this.currentScene) {
        // leave scene animation
        this.currentScene.onInvisible();
        this.currentScene.node.opacity = 0;
        this.currentScene.node.active = false;
        this.currentScene.onLeave();
        cc.log("@@@@@@@@ test scenestack @@@@@@@@", this.currentScene.name);
      }

      this.previousScene = this.currentScene;
      this.currentScene = this.nextScene; // crucial step
      this.nextScene = null;

      NodeUtils.updateWidgetAlignment(this.currentScene.node);

      this.currentScene.node.active = true;
      this.currentScene.node.opacity = 0;
      ACE.dialogManager.hideLoading();
      this.currentScene.onResize();
      this.currentScene.onEnter(); // I usually init scene UI position in onter if onResize called later the position would be wrong
      // some change scene effect here
      this.currentScene.node.opacity = 255;
      this.currentScene.onVisible();
    }
  }
}
