import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CharacterShopService extends cc.Component {
  static loadShopCharacterTags(callback: (data) => any) {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_SHOP_TAG, callback);
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_SHOP_TAG);
  }

  static loadTotalPage(tagId, callback: (data) => any) {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_TOTAL_PAGE, callback);
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_TOTAL_PAGE, {
      shopTagId: tagId,
    });
  }

  static loadShopItems(tagId, callback: (data) => any) {
    //
    ACE.gameSocket.on(SERVER_EVENT.LOAD_CURRENT_PAGE, callback);

    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CURRENT_PAGE, {
      shopTagId: tagId,
    });
  }

  static loadNextPage(callback: (data) => any) {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_NEXT_PAGE, callback);
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_NEXT_PAGE);
  }
  static loadPrePage(callback: (data) => any) {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_PRE_PAGE, callback);
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_PRE_PAGE);
  }

  // static tryChracterItem(tagId, itemId) {
  //   //for first page
  //   let NUM_ITEM_PAGE = 16;
  //   let page = Math.floor(itemId / NUM_ITEM_PAGE);

  //   ACE.gameSocket.emit(SERVER_EVENT.LOAD_CURRENT_PAGE, {
  //     shopTagId: tagId,
  //   });

  //   ACE.gameSocket.on(SERVER_EVENT.LOAD_CURRENT_PAGE, data => {
  //     ACE.gameSocket.off(SERVER_EVENT.LOAD_CURRENT_PAGE);
  //     //
  //     if (page == 0) {
  //       this.tryCharacterItemByIdx(itemId);
  //     } else {
  //       this.nextToPage(page - 1, (data) => {
  //         ACE.gameSocket.off(SERVER_EVENT.LOAD_NEXT_PAGE);
  //         this.tryCharacterItemByIdx(itemId % NUM_ITEM_PAGE);
  //       });
  //     }
  //   });
  // }

  // static nextToPage(numPageRemain, callback: (data) => any) {
  //   this.nextPageForTryItem(data => {
  //     let remainPage = numPageRemain - 1;
  //     if (remainPage <= 0) {
  //       callback();
  //     } else {
  //       this.nextToPage(remainPage, callback);
  //     }
  //   });
  // }

  // static nextPageForTryItem(callback: (data) => any) {
  //   ACE.gameSocket.emit(SERVER_EVENT.LOAD_NEXT_PAGE, {
  //   });

  //   ACE.gameSocket.on(SERVER_EVENT.LOAD_NEXT_PAGE, callback);
  // }

  static tryCharacterItemByIdx(itemId) {
    ACE.gameSocket.emit(SERVER_EVENT.TRY_ITEM_PRODUCT, {
      index: itemId.toString(),
    });
  }

  static equipedCharacterItemByIdx(itemId) {
    ACE.gameSocket.emit(SERVER_EVENT.EQUIP_CHARACTER_ITEM, {
      index: itemId.toString(),
    });
  }

  static hiddenCharacterItemByIdx(itemId) {
    ACE.gameSocket.emit(SERVER_EVENT.HIDE_CHARACTER_ITEM, {
      index: itemId.toString(),
    });
  }

  static buyItemBy(itemId, isByStar) {
    cc.log("Call buy in service: ", itemId);
    let keyBuyItem = SERVER_EVENT.BUY_ITEM_BY_COIN;
    if (isByStar) {
      keyBuyItem = SERVER_EVENT.BUY_ITEM_BY_STAR;
    }
    ACE.gameSocket.emit(keyBuyItem, {
      index: itemId.toString(),
    });
  }
}
