const {ccclass, property} = cc._decorator;

@ccclass
export default class LabelFollowSkeletonMoving extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    nameBone: string = 'board';

    @property(sp.Skeleton)
    skeletonData: sp.Skeleton = null

    @property
    offsetX = 0;

    board: any = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    // start () {
        
    // }

    update (dt) {
        this.board = this.skeletonData.findBone(this.nameBone);
        if(this.board)
        {
            this.skeletonData.updateWorldTransform();
            this.label.node.setPosition(this.board.worldX + this.offsetX, this.board.worldY)
        }
        
    }
}
