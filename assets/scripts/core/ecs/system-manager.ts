import System from "./system";
import Entity from "./entity";

export default class SystemManager {
  static _instance: SystemManager = null;
  static getInstance(): SystemManager {
    if (SystemManager._instance == null) {
      SystemManager._instance = new SystemManager();
    }
    return SystemManager._instance;
  }

  systems: Map<string, System>;
  constructor() {
    this.systems = new Map();
  }

  addSystem(system: System) {
    if (!this.systems.has(system._uuidv4)) {
      this.systems.set(system._uuidv4, system);
    }
  }

  notifyAddEntity(entity: Entity) {
    this.systems.forEach((system) => {
      system.updateSingleEntityRef(entity);
    });
  }

  onEntityDelete(entity: Entity) {
    this.systems.forEach((system) => {
      system.onEntityDelete(entity);
    });
  }

  notifyEntityUpdate(entity: Entity) {
    this.systems.forEach((system) => {
      system.updateSingleEntityRef(entity);
    });
  }
}
