import System from "../../core/ecs/system";
import Entity from "../../core/ecs/entity";
import DragComponent from "../ecs-components/drag-component";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DragSystem extends System {
  static pointerDrag: Entity = null;
  _filter = ["DragComponent"];

  @property(cc.Node)
  dragLayer: cc.Node = null;
  onLoad() {
    super.onLoad();
  }

  onEntitiesUpdate(entity: Entity) {
    // cc.log(`DragSystem::onEntitiesUpdate::Entity Size [${this._entities.size}]`);
    this.addEntityDragEvent(entity);
  }

  addEntityDragEvent(entity: Entity) {
    const entityNode = entity.node;
    entity.getECSComponent(DragComponent)._data.originalParent = entityNode.parent;
    entity.getECSComponent(DragComponent)._data.dragLayer = this.dragLayer;
    entityNode.on(cc.Node.EventType.TOUCH_START, (event: cc.Event.EventTouch) => {
      const dragComp = entity.getECSComponent(DragComponent)._data;
      dragComp.originalPosition = cc.v2(entityNode.getPosition().x, entityNode.getPosition().y);
      dragComp.touchId = event.getID();
      entityNode.parent = this.dragLayer;
      DragSystem.pointerDrag = entity;
    });

    entityNode.on(cc.Node.EventType.TOUCH_MOVE, (event: cc.Event.EventTouch) => {
      const dragComp = entity.getECSComponent(DragComponent)._data;
      if (dragComp.dragLayer && dragComp.touchId == event.getID()) {
        DragSystem.pointerDrag = entity;
        entityNode.x += event.getDeltaX() / 1.7;
        entityNode.y += event.getDeltaY() / 1.7;
      }
    });

    entityNode.on(cc.Node.EventType.TOUCH_CANCEL, (event: cc.Event.EventTouch) => {
      const dragComp = entity.getECSComponent(DragComponent)._data;
      entityNode.setPosition(dragComp.originalPosition);
    });
    // entityNode.on(cc.Node.EventType.TOUCH_END, (event: cc.Event.EventTouch) => {
    // });
  }
}
