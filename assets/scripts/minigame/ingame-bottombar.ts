import GamePlayController from "./gameplay-controller";
import MathQuestion from "./math-logic/stage/math-question";

const { ccclass, property } = cc._decorator;

@ccclass
export default class IngameBottomBar extends cc.Component {

  @property(cc.Label)
  answerLabel: cc.Label = null;

  @property(cc.Label)
  answerA: cc.Label = null;

  @property(cc.Label)
  answerB: cc.Label = null;

  @property(cc.Label)
  answerC: cc.Label = null;

  @property(cc.Label)
  answerD: cc.Label = null;

  _mathQuestionCurrent: MathQuestion = null;
  _isEnableAnswer: boolean = false;
  _numA: number = 0;
  _numB: number = 0;
  _numC: number = 0;
  _numD: number = 0;
  _answer: any;

  onEnable() {
    this.onClearAnswer();
    this._isEnableAnswer = true;
  }

  generateAnswer(result = GamePlayController.getInstance().getMathQuestionCurrent().getMathResult()) {
    const answers = ["A", "B", "C", "D"];
    let pickedAnswer = answers[Math.floor(Math.random() * answers.length)];
    switch (pickedAnswer) {
      case "A":
        this._numA = result;
        this._numB = (result + 1);
        this._numC = (result + 2);
        this._numD = (result + 3);
        break;
      case "B":
        this._numA = (result == 0 ? result + 3 : result - 1);
        this._numB = result;
        this._numC = (result + 2);
        this._numD = (result + 4);
        break;
      case "C":
        this._numA = (result == 0 ? result + 2 : result - 2);
        this._numB = (result == 0 ? result + 1 : result - 1);
        this._numC = result;
        this._numD = (result == 0 ? result + 3 : result + 1);
        break;
      case "D":
        this._numA = (result == 0 ? result + 3 : result - 3);
        this._numB = (result == 0 ? result + 2 : result - 2);
        this._numC = (result == 0 ? result + 1 : result - 1);
        this._numD = result;
        break;
      default:
        break;
    }
    let tmp = [this._numA, this._numB, this._numC, this._numD];
    tmp = tmp.sort((a, b) => { return a - b; });
    this._numA = tmp[0];
    this._numB = tmp[1];
    this._numC = tmp[2];
    this._numD = tmp[3];
    this.renderAnswer(this._numA, this._numB, this._numC, this._numD);
  }

  renderAnswer(answerA, answerB, answerC, answerD) {
    this.answerA.string = answerA.toString();
    this.answerB.string = answerB.toString();
    this.answerC.string = answerC.toString();
    this.answerD.string = answerD.toString();
  }

  onChosenAnswer(event, answer) {
    switch (answer) {
      case "A":
        this._answer = this._numA;
        break;
      case "B":
        this._answer = this._numB;
        break;
      case "C":
        this._answer = this._numC;
        break;
      case "D":
        this._answer = this._numD;
        break;
      default:
        break;
    }
    this.answerLabel.string = this._answer.toString();
    this.onCheckAnswer(this._answer);
  }

  onClearAnswer() {
    this.answerLabel.string = "";
  }

  onCheckAnswer(numberAnswer: number) {
    let ansLenght = GamePlayController.getInstance().getMathQuestionCurrent().getMathResult().toString().length;
    this._isEnableAnswer = false;
    if (this.answerLabel.string.length >= ansLenght) {
      // check here
      GamePlayController.getInstance().onAnswerQuestion(numberAnswer);
      this.schedule(function() {
        this.onClearAnswer();
        this._isEnableAnswer = true;
      }, 0, 0, 0.5);
    } else {
      this._isEnableAnswer = true;
    }
  }

  onResetForNewQuestion() {
    this.onClearAnswer();
    this._isEnableAnswer = true;
  }
}
