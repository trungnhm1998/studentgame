import { DIALOG_TYPE } from "./../core/constant";
import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import { SERVER_EVENT, STORAGE_KEY } from "../core/constant";
import LoginService from "./login-service";
import AccountManager, { IGameStateTutorial } from "../managers/account-manager";
import IProfile from "../models/profile";
import { IServerResponse } from "../core/interfaces/server-response";
import SchoolService from "../school/school-service";
import LoadingDialog from "../dialogs/loading-dialog";
import NodeUtils from "../core/utils/node-utils";
import { NoticeDialogType } from "../dialogs/notice-dialog";
import AudioManager from "../core/audio-manager";
import SocketIOProtocol from "../core/networks/SocketIOProtocol";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Login extends GameScene {
  @property(cc.EditBox)
  usernameInput: cc.EditBox = null;

  @property(cc.EditBox)
  passwordInput: cc.EditBox = null;

  @property(cc.Button)
  loginButton: cc.Button = null;

  @property(cc.Toggle)
  rememberBox: cc.Toggle = null;

  @property(cc.Node)
  loginFailGroup: cc.Node = null;

  @property({
    visible: false,
  })
  profile: IProfile = null;

  @property(cc.Node)
  backgroundNode: cc.Node = null;

  assignmentDataLoaded: boolean = false;
  checkCharExistsDone: boolean = false;
  rememberInfo: { username?: string, password?: string, lastState?: string } = {};

  onLoad() {
    super.onLoad();
  }

  onResize() {
    this.backgroundNode.scaleX = cc.winSize.width / this.backgroundNode.width;
    this.backgroundNode.scaleY = cc.winSize.height / this.backgroundNode.height;
  }

  start() {
  }

  onEnter() {
    super.onEnter();
    // preload Map and School
    let urls = ["scenes/map", "scenes/school"];
    cc.loader.loadResArray(urls, (err, res) => {
      cc.log("@@@@@@ Island loaded @@@@@@", err, res);
    });
    AudioManager.getInstance().stopAllMusic();
    ACE.topBar.hide();
    this.rememberInfo = ACE.resManager.getStorage(STORAGE_KEY.REMEMBER_INFO);
    this.checkAutoLogin(this.passData);
    this.loginButton.interactable = false;
    this.passwordInput.string = "";
    this.assignmentDataLoaded = false;
    this.checkCharExistsDone = false;
    if (this.rememberInfo) {
      this.rememberBox.uncheck();
      if (this.rememberInfo.username != "") {
        this.usernameInput.string = this.rememberInfo.username;
        this.rememberBox.check();
      } else {
        this.usernameInput.string = "";
      }
    } else {
      this.rememberInfo = {
        username: "",
      };
    }
  }

  checkAutoLogin(passData) {
    cc.log("Login::checkAutoLogin", passData);
    if (!passData) {
      return;
    }
    const { lastState, username, password, accessToken } = passData;
    if (lastState && username && password && accessToken) {
      this.usernameInput.string = username;
      this.rememberInfo = {
        username,
        password,
        lastState,
      };
      let data = ACE.resManager.getStorage(STORAGE_KEY.CHARACTER_EXIST);
      if (data && data.Exist) {
        LoginService.login(username, password);
      }
    }
  }

  onLeave() {
    super.onLeave();
  }

  registerEvents() {
    ACE.gameSocket.on(SERVER_EVENT.ASSET_DATA, (data) => {
      ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(10);
      // on loaded assetData
      ACE.resManager.assets = data;
      LoginService.checkCharacterExists(this.usernameInput.string);
    });
    ACE.gameSocket.on(SERVER_EVENT.LOGIN, (data) => this.onLogin(data));
    ACE.gameSocket.on(SERVER_EVENT.PROFILE, (data) => this.onProfile(data));
    ACE.gameSocket.on(SERVER_EVENT.CHECK_CHARACTER_EXISTS, (data) =>
      this.checkCharExists(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.SAVE_ACCESS_TOKEN, (data) => {
      cc.log("On Save AccessToken", data);
    })
    ACE.gameSocket.on(SERVER_EVENT.LOAD_STUDENT_GAME_TUTORIAL, (data: IGameStateTutorial) => {
      if (data.GameState == "map") {
        LoginService.loadTutorial("characterShop-new");
      } else if (data.GameState == "characterShop-new") {
        LoginService.loadTutorial("studentHouse-new");
      } else if (data.GameState == "studentHouse-new") {
        LoginService.loadTutorial("mathHouse");
      }
      AccountManager.getInstance().gameStateTutorial = data;
    });
    super.registerEvents();
  }

  onProfile(data: IProfile) {
    this.profile = data;
    AccountManager.getInstance().profile = { ...data };
    ACE.topBar.updateUserInfo();
    if (ACE.sceneManager.currentScene == this) {
      ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(15);
    }
    this.saveAccessToken();
  }

  onInputEnded(editbox: cc.EditBox) {
    if (editbox.node.name == "editbox-username") {
      const rememberInfo = ACE.resManager.getStorage(STORAGE_KEY.REMEMBER_INFO);
      if (rememberInfo && rememberInfo.isChecked) {
        if (rememberInfo.username == editbox.string) {
          this.rememberBox.check();
        }
      }
      this.passwordInput.focus();
    }
  }

  onEditingReturn(editbox: cc.EditBox) {
    cc.log("onEditingReturn", editbox.node.name, editbox.returnType);
    if (editbox.node.name == "editbox-username" && editbox.returnType == cc.EditBox.KeyboardReturnType.NEXT) { // 5
      editbox.blur();
      this.passwordInput.focus();
    } else if (editbox.node.name == "editbox-password") { // 0
      if (editbox.string != "") {
        this.onLoginButton();
      }
    }
  }

  onPasswordReturn(editbox: cc.EditBox) {
    if (editbox.string != "") {
      this.onLoginButton();
    }
  }

  checkCharExists(data: { YearGrade: number, Exist: boolean }) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(25);
    ACE.resManager.saveStorage(STORAGE_KEY.CHARACTER_EXIST, data);
    AccountManager.getInstance().isCharacterExist = data.Exist;
    AccountManager.getInstance().profile.yearGrade = data.YearGrade;
    if (AccountManager.getInstance().isCharacterExist) {
      LoginService.profile(this.usernameInput.string);
    } else {
      this.loadOnboardingAsset();
    }
    this.checkCharExistsDone = true;
    SchoolService.enrolmentList(); // this will triggers the onGetAssignmentForMathHouse below
  }

  onLogin(accessToken: string) {
    AudioManager.getInstance().resumeContext();
    NodeUtils.preventMultipleClick(this.loginButton);
    if (this.rememberBox.isChecked) {
      this.rememberInfo.username = this.usernameInput.string;
      const dataToBeSave = {
        accessToken,
        username: this.rememberInfo.username,
        password: this.passwordInput.string != "" ? this.passwordInput.string : this.rememberInfo.password,
      };
      // only save if user can get in to map scene
      ACE.resManager.saveStorage(STORAGE_KEY.ACCESS_TOKEN, dataToBeSave);
      ACE.resManager.saveStorage(STORAGE_KEY.REMEMBER_INFO, this.rememberInfo);
    } else {
      ACE.resManager.removeStorage(STORAGE_KEY.ACCESS_TOKEN);
      ACE.resManager.removeStorage(STORAGE_KEY.REMEMBER_INFO);
    }
    ACE.dialogManager.showLoading();
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(0);
    if ((ACE.gameSocket._clientProtocol as SocketIOProtocol).accessToken != undefined) {
      (ACE.gameSocket._clientProtocol as SocketIOProtocol).accessToken = accessToken;
    }
    AccountManager.getInstance().justLogin = true;
    AccountManager.getInstance().userName = this.usernameInput.string;
    AccountManager.getInstance().accessToken = accessToken;
    // get assetData for game
    LoginService.loadAssetsData();
    LoginService.loadTutorial("map");
  }

  onGetAssignmentForMathHouse(data) {
    super.onGetAssignmentForMathHouse(data);
    if (ACE.sceneManager.currentScene.name != "login") { return; }
    this.assignmentDataLoaded = true;
    if (this.checkCharExistsDone) {
      if (AccountManager.getInstance().isCharacterExist) {
        let sceneToPush = "map";
        if (this.passData && this.passData.lastState) {
          const lastState = this.passData.lastState;
          if (lastState != "login") {
            if (lastState == "math-room") {
              sceneToPush = "school";
            } else if (!lastState) {
              sceneToPush = "map";
            } else {
              sceneToPush = lastState;
            }
          }
        }
        // console.time("login");
        ACE.sceneManager.pushScene(sceneToPush);
      } else {
        ACE.sceneManager.pushScene("intro");
      }
    }
  }

  onError(data: IServerResponse) {
    super.onError(data);
    if (data.error != "") {
      ACE.dialogManager.showNoticeDialog(data.error, NoticeDialogType.OK);
    }
    // if (data.error == "User not found or password does not match") {
    //   this.loginFailGroup.active = true;
    // } else if (data.error == "It seems that your username or password is incorrect. Please check access details and try again.") {
    // }
  }

  onLoginButton() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    NodeUtils.preventMultipleClick(this.loginButton);
    LoginService.login(this.usernameInput.string, this.passwordInput.string);
  }

  inputLimits(enter) {
    if (this.usernameInput.string.length > 1) {
      this.loginButton.interactable = true;
    } else {
      this.loginButton.interactable = false;
    }
  }

  closeLoginFailMessage() {
    this.loginFailGroup.active = false;
  }

  loadOnboardingAsset() {
    let audios = ["ITEM_CLICK_SOUND", "BUTTON_CLICK_SOUND", "CHOOSE_ITEM_SOUND", "book_appearing",
      "book_opening", "book_shaking", "character_appearing", "egg_shaking_cracking", "logo_appearing",
      "npc_appearing", "pet_appearing", "pet_going_into_the_egg", "stage_where_the_character_appears",
      "star1", "star2", "star3", "star4", "star5", "star6", "star7"];
    AudioManager.getInstance().preloadAudio(audios);
  }

  saveAccessToken() {
    const accessToken = AccountManager.getInstance().accessToken;
    const userId = AccountManager.getInstance().profile.userId;
    const username = AccountManager.getInstance().userName;
    const data = {
      accessToken, userId, username
    }
    ACE.gameSocket.emit(SERVER_EVENT.SAVE_ACCESS_TOKEN, {
      accessToken,
      userId,
      username
    });
  }
}
