// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
const BASE_SPEED = 65//80;
const BASE_BG_SIZE_W = 1920;
@ccclass
export default class NewMoving extends cc.Component {
    @property
    isBG = true;

    @property
    speed: number = 6;

    @property
    offsetX: number = -BASE_BG_SIZE_W;

    @property
    OFFSET_PX: number = 5;

    posX = 0;

    @property([cc.Node])
    NodeBGs: cc.Node[] = [];

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }
    @property
    startPosX: number = 0;

    first = 0;
    last = 0;
    step = 1;

    speedFollowDesign = 0.5;

    onEnable() {
        this.step = 0;
        this.first = 0;
        this.last = this.NodeBGs.length - 1;

        if(!this.isBG){
            cc.log(
                "$$$$$$$$$$$$$$$$$$$$$$$$$$$ this.node.width  "  + this.node.width 
            );
            // this.startPosX = this.node.width/2;
            // this.offsetX = - this.node.width - this.node.width/2;
            this.node.setPosition(this.startPosX,this.node.position.y);
        }
        else{
            // this.NodeBGs.forEach(item => {
            //     item.setPosition(this.startPosX, item.position.y);
            // });
            
            if(this.NodeBGs.length > 0)
            {
                var width = this.NodeBGs[this.first].width - this.OFFSET_PX;
                this.NodeBGs[this.first].setPosition(this.startPosX - width, this.NodeBGs[this.first].position.y)
                this.NodeBGs[this.first + 1].setPosition(this.startPosX, this.NodeBGs[this.first].position.y)
                this.NodeBGs[this.last].setPosition(this.startPosX + width, this.NodeBGs[this.first].position.y)
            }
        }

        this.posX = this.startPosX;
        this.node.setPosition(this.startPosX,this.node.position.y);
    }

    update (dt) {
        // cc.log("Value change :: " + this.node.position.x
        //     + "\ndt: " + dt    
        // );
        // if(!IngameLogic.getInstance()._isPause)
        {
            if(!this.isBG)
            {
                if(this.posX <= this.offsetX)
                    this.posX = this.startPosX;
            }
            
            var speedDifficult = this.speed * this.speedFollowDesign;//IngameLogic.getInstance().speedFollowDesign;
            this.posX -= dt *  BASE_SPEED * speedDifficult;
            this.node.setPosition(this.posX,this.node.position.y);
            // this.node.position.x -= dt * this.speed;

            if(this.isBG && this.NodeBGs.length > 0 && this.posX <= - this.NodeBGs[this.first].width* this.step)
            {
                this.step ++;
                var posLast = this.NodeBGs[this.last].position.x + this.NodeBGs[this.last].width - this.OFFSET_PX; //-OFFSET_PX because image have 1px tranparent...
                // cc.log("PostLast ::::::::: ++++++++++++++++++++ " + posLast)
                this.NodeBGs[this.first].setPosition(posLast,this.NodeBGs[this.first].position.y);
                this.last = this.first;
                this.first ++;
                if(this.first > this.NodeBGs.length - 1) this.first = 0;
            }
        }
    }

    onKeyDown (event) {
        {
            var macro = cc.macro;
            
            switch(event.keyCode) {
                case macro.KEY.d:
                    this.speedFollowDesign += 0.5;
                    break;
                case macro.KEY.a:
                    this.speedFollowDesign -= 0.5;
                    break;
            }
        }
    }

}
