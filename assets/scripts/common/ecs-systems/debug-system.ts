import System from "../../core/ecs/system";
import Entity from "../../core/ecs/entity";
import PositionComponent from "../ecs-components/position-component";
import DebugComponent from "../ecs-components/debug-component";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DebugSystem extends System {
  _filter = ["DebugComponent", "PositionComponent"];

  @property(cc.SpriteFrame)
  debugSprite: cc.SpriteFrame = null;

  @property(cc.Node)
  debugLayer: cc.Node = null;

  onLoad() {
    super.onLoad();
  }

  onEntitiesUpdate(entity: Entity) {
    const { position, size, anchor } = entity.getECSComponent(PositionComponent)._data;
    const { d_name, id } = entity.getECSComponent(DebugComponent)._data;

    const debugNode = new cc.Node();
    const debugTextNode = new cc.Node();

    const textComp = debugTextNode.addComponent(cc.Label);
    textComp.string = id && d_name ? `${id}-${d_name}` : `${entity.node.name}`;
    textComp.fontSize = 10;
    debugTextNode.color = cc.Color.BLACK;
    // debugNode.addChild(debugTextNode);
    // add pink background colour so I could tell where the sprite would draw to
    debugNode.color = new cc.Color().fromHEX("#E20BFF");
    debugNode.opacity = 120;
    debugNode.addComponent(cc.Sprite).spriteFrame = this.debugSprite;
    debugNode.width = size.width == 0 ? 10 : size.width;
    debugNode.height = size.height == 0 ? 10 : size.height;
    debugNode.anchorX = debugTextNode.anchorX = anchor.x;
    debugNode.anchorY = debugTextNode.anchorY = anchor.y == 0 ? 1 : anchor.y;
    // debugNode.setPosition(position);

    entity.node.addChild(debugNode, 6, "debug-node");
    entity.node.addChild(debugTextNode, 8, "debug-text-node");
    entity.node.sortAllChildren();
    debugNode.on(cc.Node.EventType.MOUSE_MOVE, () => {
      let currentPosition = debugTextNode.parent.convertToWorldSpaceAR(debugTextNode.getPosition());
      debugTextNode.color = cc.Color.RED;
      textComp.fontSize = 35;
      debugTextNode.parent = this.debugLayer;
      debugTextNode.setPosition(this.debugLayer.convertToNodeSpaceAR(currentPosition));
    });

    debugNode.on(cc.Node.EventType.MOUSE_LEAVE, () => {
      debugTextNode.color = cc.Color.BLACK;
      textComp.fontSize = 10;
      debugTextNode.parent = entity.node;
      debugTextNode.setPosition(0, 0);
    });
  }
}
