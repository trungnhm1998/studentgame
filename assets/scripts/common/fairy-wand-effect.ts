const { ccclass, property } = cc._decorator;

@ccclass
export default class FairyWandEffect extends cc.Component {
  @property(cc.String)
  boneName: string = "light2";

  @property(cc.ParticleSystem)
  wandDust: cc.ParticleSystem = null;

  _wandNode: cc.Node = null;
  _skeletonComp: sp.Skeleton = null;
  onLoad() {
    // this.wandDust.active = false;
    if (!CC_JSB) {
      this._skeletonComp = this.getComponent(sp.Skeleton);
      let attachUtil = this._skeletonComp.attachUtil;
      attachUtil.generateAllAttachedNodes();
      let boneNodes = attachUtil.getAttachedNodes(this.boneName);
      this._wandNode = boneNodes[0];
      cc.log(this._wandNode, this._wandNode.parent.convertToWorldSpaceAR(this._wandNode.getPosition()));
      this.wandDust.node.parent = this._wandNode.parent;
    } else {
      this.wandDust.node.removeFromParent();
    }
    // NodeUtils.setNodePositionAtTarget(this.wandDust.node, this._wandNode);
  }
}
