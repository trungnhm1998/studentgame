const { ccclass } = cc._decorator;

@ccclass
export default class StudentItemUsing {
  ID: number;
  Name: string;
  UrlImage: string;
  RoomItemGroupL1ID: number;
  RoomItemGroupL2ID: number;
  RoomID: number;
}
