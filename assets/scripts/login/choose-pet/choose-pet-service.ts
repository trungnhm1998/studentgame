import { ACE } from "../../core/global-info";
import { SERVER_EVENT } from "../../core/constant";
import AccountManager from "../../managers/account-manager";

export default class ChoosePetService {
  static finishSelection(nickname: string, petId: number) {
    ACE.gameSocket.emit(SERVER_EVENT.FINISH_SELECTION, {
      nickname
    });
  }
}
