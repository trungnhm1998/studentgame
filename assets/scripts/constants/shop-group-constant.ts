let SHOP_GROUP_ID = [
  "FACE",
  "HAIR",
  "SKIN",
  "CLOTHES",
  "SHOES",
  "FULLSET",
  "ACC",
  "BRACELET",
  "NECKLACE"
];

export { SHOP_GROUP_ID };
