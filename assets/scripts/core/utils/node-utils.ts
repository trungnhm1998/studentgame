export default class NodeUtils {
  static updateWidgetAlignment(node: cc.Node) {
    for (const child of node.children) {
      const widget: cc.Widget = child.getComponent(cc.Widget);
      if (widget) {
        widget.updateAlignment();
      }
      NodeUtils.updateWidgetAlignment(child);
    }
  }

  static findNodeByName(parentNode: cc.Node, nodeName: string, requireActive = false): cc.Node {
    if (requireActive && parentNode.active == false) {
      return;
    }
    if (parentNode && parentNode.name == nodeName) { return parentNode; }
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < parentNode.children.length; i++) {
      const res = NodeUtils.findNodeByName(parentNode.children[i], nodeName, requireActive);
      if (res) { return res; }
    }
  }

  static preventMultipleClick(btn: cc.Button, time = 0.3) {
    btn.interactable = false;
    btn.scheduleOnce(() => { btn.interactable = true; }, time);
  }

  /**
   * only work for node has anchor 0.5 0.5
   * @param node Node to set
   * @param target target position
   */
  static setNodePositionAtTarget(node: cc.Node, target: cc.Node) {
    const worldPos = target.parent.convertToWorldSpaceAR(target.getPosition());
    const localPos = node.parent.convertToNodeSpaceAR(worldPos);
    node.setPosition(localPos);
  }

  static getLocalPosition(localNode: cc.Node, targetNode: cc.Node) {
    return localNode.parent.convertToNodeSpaceAR(targetNode.parent.convertToWorldSpaceAR(targetNode.getPosition()));
  }

  static phaserColorToHex(phaserColourCode: number) {
    // tslint:disable-next-line: no-bitwise
    return "#" + ("00000" + (phaserColourCode | 0).toString(16)).substr(-6);
  }

  static getCurrentSceneName() {
    let sceneName: string;
    let _sceneInfos = cc.game._sceneInfos;
    for (let i = 0; i < _sceneInfos.length; i++) {
      if (_sceneInfos[i].uuid == cc.director._scene._id) {
        sceneName = _sceneInfos[i].url;
        sceneName = sceneName.substring(sceneName.lastIndexOf("/") + 1).match(/[^\.]+/)[0];
      }
    }
    return sceneName;
  }
}
