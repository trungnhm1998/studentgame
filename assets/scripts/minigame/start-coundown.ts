import GamePlayController from "./gameplay-controller";
import IngameLogic from "./game-play/ingame-logic";

const { ccclass, property } = cc._decorator;

@ccclass
export default class StartCoundown extends cc.Component {

  @property(cc.Node)
  numberNode: cc.Node = null;
  @property([cc.SpriteFrame])
  numSprites: cc.SpriteFrame[] = [];
  @property(cc.Animation)
  animOnInitCounter: cc.Animation = null;

  _isCoundowning: boolean = false;
  _timeCountedBefore: number = 0;
  _gameTime: number = 0;
  _count: number = 0;
  _endCoundownCallback: () => void;

  start() {}

  startCoundown(callback: () => void) {
    this.node.active = true;
    this.animOnInitCounter.play();
    this._isCoundowning = true;
    this._count = 0;
    this._endCoundownCallback = callback;
    this.numberNode.active = false;
  }

  update(dt) {
    this._gameTime += dt;
    if (this._isCoundowning && this._timeCountedBefore < this._gameTime - 1.0) {
      // check
      if (this._count >= this.numSprites.length) {
        this._count = 0;
        this._isCoundowning = false;
        this.node.active = false;

        if (this._endCoundownCallback != null) {
          this._endCoundownCallback();
        }
      }
      this._timeCountedBefore = this._gameTime;
      if (this.numberNode.active == false) {
        this.numberNode.active = true;
      }
      this.numberNode.getComponent(cc.Sprite).spriteFrame = this.numSprites[this._count];
      this.numberNode.getComponent(cc.Animation).play();
      GamePlayController.getInstance().startCameraShakeEffect();
      this._count++;

    }

  }
}
