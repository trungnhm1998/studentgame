export interface IQATemplate {
  Name: string;
  resolver: IResolver;
  version: number;
  design: IDesign[];
  answers: string[];
  answerShapes: any[];
  explains: IExplain[];
}

export interface IDesign {
  version?: number;
  qaName?: string;
  qaType?: number;
  configure?: string;
  full_letters?: IFullLetters;
  incorrect?: ICorrect;
  correct?: ICorrect;
  selected?: ISelected;
  graphicId?: number;
  scale?: number;
  objPrivate?: IObjPrivate;
  x?: number;
  y?: number;
  w?: number;
  h?: number;
  name?: string;
  id?: number;
}

export interface ICorrect {
  image: IImage;
  rect: IRect;
}

export interface IImage {
  link: string;
  key: string;
  position: string;
  anchor: number[];
  scale: number;
}

export interface IRect {
  radius: number;
  stroke: number;
  border: number;
}

export interface IFullLetters {
  anchor: IAnchor;
  x: number;
  y: number;
  scale: number;
}

export interface IAnchor {
  x: number;
  y: number;
}

export interface IObjPrivate {
  keyImage?: string;
  linkImage?: string;
  preload?: boolean;
  Click: IClick;
  DragDrop: IClick;
  multiDragDrop: IClick;
  anchorX?: number;
  anchorY?: number;
  style?: IStyle;
  text?: string;
  centerX?: number;
  centerY?: number;
  lineWidth?: number;
  lineColor?: number;
  radius?: number;
  action?: IAction;
}

export interface IClick {
}

export interface IAction {
  name: string;
  type?: string;
  x?: number;
  y?: number;
  anchor?: IAnchor;
  dir?: string;
  spacing?: number;
  itemRef?: IItemRef;
  multiple?: boolean;
  toggle?: boolean;
  keyboard?: string;
  dataType?: string;
  maxLength?: number;
  resultPosition?: string;
  tabOrder?: number;
  fontSize?: number;
}

export interface IItemRef {
  align: string;
  parent: string;
  index: number;
  anchor: string;
  count: number;
}

export interface IStyle {
  font: string;
  fontStyle: string;
  fontVariant: string;
  fontWeight: string;
  fontSize: number;
  lineHeight: number;
  lineSpacing: number;
  backgroundColor: null;
  fill: string;
  align: string;
  boundsAlignH: string;
  boundsAlignV: string;
  stroke: string;
  strokeThickness: number;
  wordWrap: boolean;
  wordWrapWidth: number;
  maxLines: number;
  tabs: number;
  width?: number;
  height?: number;
  textcolor?: string;
  fontFamily?: string;
  textAlign?: string;
  borderRadius?: number;
  borderStyle?: string;
  borderWidth?: number;
  borderColor?: string;
  hidden?: boolean;
  maxLength?: number;
  autofocus?: boolean;
}

export interface ISelected {
  rect: IRect;
}

export interface IExplain {
  html: string;
}

export interface IResolver {
  generator: string;
  logId: string;
}
