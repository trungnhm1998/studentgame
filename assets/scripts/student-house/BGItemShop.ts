const { ccclass, property } = cc._decorator;

@ccclass
export default class BGItemShop extends cc.Component {

  @property([cc.SpriteFrame])
  typeSpriteFrame: cc.SpriteFrame[] = [];

  pickRandomlySpriteFrame(): cc.SpriteFrame {
    let sprite: cc.SpriteFrame = null;
    if (this.typeSpriteFrame.length > 0) {
      let rand = Math.random(); // 0 - 1
      let totalItem = this.typeSpriteFrame.length;
      let randIndex = Math.floor(rand * totalItem);
      sprite = this.typeSpriteFrame[randIndex];
    }
    return sprite;
  }
}
