const { ccclass, property } = cc._decorator;

import StudentItemUsing from "./Model/StudentItemUsing";
import StudentRoomItemGroupL1 from "./Model/StudentRoomItemGroupL1";
import StudentHouseManager from "./StudentHouseManager";
import { RESOURCE_PATH } from "../constants/resource-constant";

@ccclass
export default class ShowRoomManager extends cc.Component {

  listItemUsing: StudentItemUsing[] = [];
  listStudentRoomItemGroupL1: StudentRoomItemGroupL1[] = [];

  @property([cc.Node])
  public DataItems: cc.Node[] = [];

  tryApplyItemInRoom(data) {
    let itemUsing: StudentItemUsing = data;
    this.updateElement(itemUsing.RoomItemGroupL1ID, itemUsing.UrlImage);
  }

  loadRoomItemUsing(data) {
    // alway reset for correct
    this.listItemUsing = [];

    // set Data
    data.forEach((item) => {
      this.listItemUsing.push(item);
    });

    // Hide all item first.
    this.DataItems.forEach((item) => {
      item.active = false;
    });

    // Preload Image first
    this.preloadImage();
  }

  loadAllItemGroupL1(data) {
    // alway reset
    this.listStudentRoomItemGroupL1 = [];

    // set data
    data.forEach((item) => {
      this.listStudentRoomItemGroupL1.push(item);
    });
  }

  onEnable() {
    this.DataItems.forEach((element, id) => {
      // floor & wall alway show.
      if (id != 0 && id != 1) {
        element.active = false;
      }
    });
  }

  onDisable() {
    cc.log("DISABLE %%%%%%%%%%%%%%%%%%%", this.name);
  }

  // This for fix bug update images delay when loading image from server....
  preloadImage() {
    let self = this;

    let urlImageList: string[] = [];

    this.listItemUsing.forEach((item) => {
      urlImageList.push(RESOURCE_PATH.DATA_ASSETS_SERVER + item.UrlImage);
    });

    // after this, it will cache the list image, will help load faster then
    cc.loader.load(urlImageList,
      (err, texture) => {
        if (err) {
          cc.error(err.message || err);
          return;
        }
        // after finish Load
        StudentHouseManager.getInstance().onLoadingHouseContentFinish();

        self.listItemUsing.forEach((itemUsing) => {
          self.updateElement(itemUsing.RoomItemGroupL1ID, itemUsing.UrlImage);
        });
      },
    );
  }

  updateElement(itemInGroup_ID, urlImage: string) {
    // DataItems here should same / mapping with data of listStudentRoomItemGroupL1.
    // If not, should be fix in cocos data.
    this.listStudentRoomItemGroupL1.forEach((item, id) => {
      if (itemInGroup_ID == item.ID && this.DataItems != null) {
        let self = this;
        this.DataItems[id].active = true;

        cc.loader.load(
          RESOURCE_PATH.DATA_ASSETS_SERVER + urlImage,
          (err, texture) => {
            if (err) {
              cc.error(err.message || err);
              return;
            }
            let iconSp = new cc.SpriteFrame(texture);
            self.DataItems[id].getComponent(cc.Sprite).spriteFrame = iconSp;
          });
      }
    });
  }
}
