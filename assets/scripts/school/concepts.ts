import IConcept from "../models/concept";
import SchoolManager from "../managers/school-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ConceptComponent extends cc.Component {
  @property(cc.Label)
  title: cc.Label = null;

  @property([cc.Node])
  stars: cc.Node[] = [];

  @property(cc.SpriteFrame)
  noStarSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  wholeStarSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  halfStarSF: cc.SpriteFrame = null;

  data: IConcept;
  onConceptCallback: Function = null;

  onLoad() {}

  onEnable() {

  }

  loadConcept(data: IConcept) {
    this.data = data;

    // update UI based on data
    this.title.string = this.data.Description;
    this.setupStars();
  }

  setupStars() {
    // setup star based on ProgessStar
    // clean up first
    for (let i = 0; i < this.stars.length; i++) {
      const starNode = this.stars[i];
      starNode.removeAllChildren();
      starNode.getComponent(cc.Sprite).spriteFrame = this.noStarSF;
    }
    // we can either add a sprite as it child or change the spriteframe
    // I'll change the spriteframe
    let starToAdd = this.data.ProgressStar;
    let index = 0;
    while (starToAdd > 0) {
      // priorites whole star first
      if (starToAdd - 1 >= 0) {
        this.stars[index].getComponent(
          cc.Sprite
        ).spriteFrame = this.wholeStarSF;
        starToAdd -= 1;
        index++;
      } else if (starToAdd - 0.5 >= 0) {
        this.stars[index].getComponent(cc.Sprite).spriteFrame = this.halfStarSF;
        starToAdd -= 0.5;
        index++;
      }
    }
  }

  onConceptPressed() {
    if (this.onConceptCallback != null) {
      this.onConceptCallback();
    }
  }
}
