const CHARACTER_GIRL_WEARING_GROUP_ID = {
  HAT: 42,
  BAG: 43,
  MEDAL1: 44,
  MEDAL2: 45,
  SWORD: 46,
  GLOVE1: 47,
  CLOTHES: 48,
  HAIR: 49,
  FACE: 50,
  GLOVE2: 51,
  SHOES: 52,
  SKIN: 53,
  WINGS: 54,
  TAIL: 55

  /*
      0: {ID: 55, Name: "Tail (female, large)", PosX: 0, PosY: 0, PosZ: 13, …}
1: {ID: 54, Name: "Wings (female, large)", PosX: 0, PosY: 0, PosZ: 12, …}
2: {ID: 53, Name: "Skin (female, large)", PosX: 0, PosY: 0, PosZ: 11, …}
3: {ID: 52, Name: "Shoes (female, large)", PosX: 7, PosY: 275, PosZ: 10, …}
4: {ID: 51, Name: "Glove 2 (female, large)", PosX: 0, PosY: 0, PosZ: 9, …}
5: {ID: 50, Name: "Face (female, large)", PosX: 5, PosY: -223, PosZ: 8, …}
6: {ID: 49, Name: "Hair (female, large)", PosX: 0, PosY: 0, PosZ: 7, …}
7: {ID: 48, Name: "Clothes (female, large)", PosX: 0, PosY: 0, PosZ: 6, …}
8: {ID: 47, Name: "Glove 1 (female, large)", PosX: 0, PosY: 0, PosZ: 5, …}
9: {ID: 46, Name: "Sword (female, large)", PosX: 0, PosY: 0, PosZ: 4, …}
10: {ID: 45, Name: "Medal 2 (female, large)", PosX: 0, PosY: 0, PosZ: 3, …}
11: {ID: 44, Name: "Medal 1 (female, large)", PosX: 0, PosY: 0, PosZ: 2, …}
12: {ID: 43, Name: "Bag (female, large)", PosX: 0, PosY: 0, PosZ: 1, …}
13: {ID: 42, Name: "Hat (female, large)", PosX: 0, PosY: 0, PosZ: 0, …}
        */
};
const CHARACTER_GIRL_KID_WEARING_GROUP_ID = {
  HAT: 28,
  BAG: 29,
  MEDAL1: 30,
  MEDAL2: 31,
  SWORD: 32,
  GLOVE1: 33,
  CLOTHES: 34,
  HAIR: 35,
  FACE: 36,
  GLOVE2: 37,
  SHOES: 38,
  SKIN: 39,
  WINGS: 40,
  TAIL: 41
};

const CHARACTER_BOY_WEARING_GROUP_ID = {
  HAT: 14,
  BAG: 15,
  MEDAL1: 16,
  MEDAL2: 17,
  SWORD: 18,
  GLOVE1: 19,
  CLOTHES: 20,
  HAIR: 21,
  FACE: 22,
  GLOVE2: 23,
  SHOES: 24,
  SKIN: 25,
  WINGS: 26,
  TAIL: 27
};

const CHARACTER_BOY_KID_WEARING_GROUP_ID = {
  HAT: 0,
  BAG: 1,
  MEDAL1: 2,
  MEDAL2: 3,
  SWORD: 4,
  GLOVE1: 5,
  CLOTHES: 6,
  HAIR: 7,
  FACE: 8,
  GLOVE2: 9,
  SHOES: 10,
  SKIN: 11,
  WINGS: 12,
  TAIL: 13

  /*
0: {ID: 13, Name: "Tail (male, small)", PosX: 0, PosY: 0, PosZ: 13, …}
1: {ID: 12, Name: "Wings (male, small)", PosX: 0, PosY: 0, PosZ: 12, …}
2: {ID: 11, Name: "Skin (male, small)", PosX: 0, PosY: 0, PosZ: 11, …}
3: {ID: 10, Name: "Shoes (male, small)", PosX: -3, PosY: 285, PosZ: 10, …}
4: {ID: 9, Name: "Glove 2 (male, small)", PosX: 0, PosY: 0, PosZ: 9, …}
5: {ID: 8, Name: "Face (male, small)", PosX: 4, PosY: -204, PosZ: 8, …}
6: {ID: 7, Name: "Hair (male, small)", PosX: 0, PosY: 0, PosZ: 7, …}
7: {ID: 6, Name: "Clothes (male, small)", PosX: 0, PosY: 0, PosZ: 6, …}
8: {ID: 5, Name: "Glove 1 (male, small)", PosX: 0, PosY: 0, PosZ: 5, …}
9: {ID: 4, Name: "Sword (male, small)", PosX: 0, PosY: 0, PosZ: 4, …}
10: {ID: 3, Name: "Medal 2 (male, small)", PosX: 0, PosY: 0, PosZ: 3, …}
11: {ID: 2, Name: "Medal 1 (male, small)", PosX: 0, PosY: 0, PosZ: 2, …}
12: {ID: 1, Name: "Bag (male, small)", PosX: 0, PosY: 0, PosZ: 1, …}
13: {ID: 0, Name: "Hat (male, small)", PosX: 0, PosY: 0, PosZ: 0, …}
          */
};

export {
  CHARACTER_GIRL_WEARING_GROUP_ID,
  CHARACTER_BOY_WEARING_GROUP_ID,
  CHARACTER_GIRL_KID_WEARING_GROUP_ID,
  CHARACTER_BOY_KID_WEARING_GROUP_ID
};
