const { ccclass, property } = cc._decorator;

@ccclass
export default class StudentRoomState {
  CreatedOn: string;
  GameState: string;
  Id: string;
  Step: number;
  StudentID: number;
  UpdatedOn: string;
  isClosed: boolean;
}
