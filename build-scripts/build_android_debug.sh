#!/usr/bin/env bash

# CONSTANT GLOBAL
PROJECT_DIR=$(pwd)
COCOS_CREATOR_EXECUTE=/Applications/CocosCreator.app/Contents/MacOS/CocosCreator
ANDROID_STUDIO_DIR="$PROJECT_DIR/build/jsb-default/frameworks/runtime-src/proj.android-studio"

# Build props
platform="android"
startScene="2d2f792f-a40c-49bb-a189-ed176a246e49"
title="ACE_StarMathsOnline"
mergeStartScene="false"
androidStudio="true"
debug="false"
useDebugKeystore="true"
# appABIs="['armeabi-v7a', 'x86']"
appABIs="['armeabi-v7a']"
packageName="com.starmathsonline.student"
apiLevel="android-29"
orientation="{'landscapeLeft': true, 'landscapeRight': true}"
template="default"
md5Cache="false"
encryptJs="true"
xxteaKey="cab41512-5847-4f"
zipCompressJs="true"
excludeScenes="['479df138-4821-465f-aff3-5849939d33bb', '4e219f4e-c994-45ae-9489-5be302c63f23']"
buildPath="$PROJECT_DIR/build"

APK_NAME="$title-debug.apk"
APK_PATH="$ANDROID_STUDIO_DIR/app/build/outputs/apk/debug/$APK_NAME"

finalBuildFlag="platform=$platform;startScene=$startScene;title=$title;mergeStartScene=$mergeStartScene;androidStudio=$androidStudio;debug=$debug;useDebugKeystore=$useDebugKeystore;appABIs=$appABIs;orientation=$orientation;template=$template;md5Cache=$md5Cache;encryptJs=$encryptJs;xxteaKey=$xxteaKey;zipCompressJs=$zipCompressJs;buildPath=$buildPath;excludeScenes=$excludeScenes;"

# rm -r -f $PROJECT_DIR/build

$COCOS_CREATOR_EXECUTE --path $PROJECT_DIR --build "$finalBuildFlag"

# compile native project
cd $ANDROID_STUDIO_DIR
./gradlew assembleDebug

# copy apk to build dir
mkdir -p "$PROJECT_DIR/bin/" && cp $APK_PATH "$PROJECT_DIR/bin/$APK_NAME"

# install apk to devices
# adb install -r -d $PROJECT_DIR/bin/$APK_NAME
# echo $finalBuildFlag
