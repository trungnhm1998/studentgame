import System from "../../core/ecs/system";
import Entity from "../../core/ecs/entity";
import LabelComponent from "../ecs-components/label-component";

const { ccclass, property } = cc._decorator;

export const COLOUR = {
  black: "#000000",
};
const WordWrapOffset = 100;

@ccclass
export default class LabelSystem extends System {
  _filter = ["LabelComponent"];

  @property(cc.SpriteFrame)
  debugSprite: cc.SpriteFrame = null;

  onLoad() {
    super.onLoad();
  }

  addDebugDraw(entity: Entity, size: cc.Size) {
    // const { x, y, anchor } = entity.getECSComponent(ListComponent)._data;
    entity.node.color = new cc.Color().fromHEX("#FF7D44");
    entity.node.addComponent(cc.Sprite).spriteFrame = this.debugSprite;
    entity.node.width = size.width;
    entity.node.height = size.height;
  }

  onEntitiesUpdate(entity: Entity) {
    const {
      text,
      fill,
      fontSize,
      lineHeight,
      lineSpacing,
      align,
      stroke,
      wordWrap,
      wordWrapWidth,
      font,
      name,
    } = entity.getECSComponent(LabelComponent)._data;
    let labelNode = new cc.Node();
    let labelComp = labelNode.addComponent(cc.Label);
    cc.loader.loadRes(`fonts/${font}`, (error, fnt: cc.TTFFont) => {
      labelComp.font = fnt;
      // for some case fill will be a hex colour
      labelNode.color = new cc.Color().fromHEX(COLOUR[fill] ? COLOUR[fill] : fill);
      // labelNode.width = fontSize * text.length;
      // cc.log(`before label [${name}] width [${labelNode.width}]`);
      // labelNode.width = wordWrapWidth - WordWrapOffset;
      labelComp.verticalAlign = cc.Label.VerticalAlign.TOP;
      labelComp.fontSize = fontSize;
      labelComp.overflow = cc.Label.Overflow.NONE;
      labelComp.horizontalAlign = this.getAlignment(align);
      labelComp.lineHeight = fontSize;
      // cc.log(`label [${name}] width [${labelNode.getContentSize().width}] [${labelNode.getBoundingBox().size}]`);
      labelComp.string = text;
      entity.node.addChild(labelNode, 3, "label-node");
      entity.node.sortAllChildren();
      this.scheduleOnce(() => {
        const contentSize = labelNode.getContentSize();
        labelNode.height = contentSize.height;
        if (contentSize.width >= wordWrapWidth - WordWrapOffset) {
          labelNode.width = wordWrapWidth - WordWrapOffset;
          labelComp.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
          labelComp.enableWrapText = wordWrap;
        }
        this.addDebugDraw(entity, cc.size(labelNode.width, labelNode.height));
      });
    });
  }

  getAlignment(jsonAlign: string) {
    switch (jsonAlign) {
      case "center":
        return cc.Label.HorizontalAlign.CENTER;
    }
    return cc.Label.HorizontalAlign.CENTER;
  }
}
