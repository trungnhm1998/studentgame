import { IListItemComponent } from './../common/ecs-components/list-item-component';
import { IQATemplate, IDesign } from "../../Scene/qa-template-interface";
import Entity from "../core/ecs/entity";
import DebugComponent, { IDebugComponent } from "../common/ecs-components/debug-component";
import PositionComponent, { IPositionComponent } from "../common/ecs-components/position-component";
import ImageComponent, { IImageComponent } from "../common/ecs-components/image-component";
import EntityManager from "../core/ecs/entity-manager";
import DragComponent, { IDragComponent } from "../common/ecs-components/drag-component";
import DropComponent, { IDropComponent } from "../common/ecs-components/drop-component";
import LabelComponent, { ILabelComponent } from "../common/ecs-components/label-component";
import ListComponent, { IListComponent } from "../common/ecs-components/list-component";
import RectComponent from "../common/ecs-components/rect-component";
import ListItemComponent from "../common/ecs-components/list-item-component";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QARendererV2 extends cc.Component {
  @property(cc.Prefab)
  entityPrefab: cc.Prefab = null;

  _entities: Entity[] = [];

  cleanEntities() {
    while (this._entities.length > 0) {
      const entity = this._entities.pop();
      EntityManager.getInstance().destroy(entity);
    }
    this._entities = [];
  }

  renderQA(template: IQATemplate) {
    this.cleanEntities();
    const { design } = template;
    let renderOrder = 0;
    design.forEach((object) => {
      const { graphicId, objPrivate } = object;
      if (graphicId) {
        if (objPrivate && objPrivate.preload) {
        } else {
          const entityNode = new cc.Node("qa-object");
          const entityComp = entityNode.addComponent(Entity);
          this._entities.push(entityComp);
          entityComp.init();

          entityComp.addECSComponent(PositionComponent, {
            position: cc.v2(object.x, object.y),
            size: cc.size(object.w, object.h),
            anchor: cc.v2(objPrivate.anchorX, objPrivate.anchorY),
          } as IPositionComponent);
          this.addGraphicComponent(object, entityComp);
          this.addActionComponent(object, entityComp);

          if (objPrivate.action == undefined || !objPrivate.action.itemRef) {
            this.node.addChild(entityNode, renderOrder++);
          }
        }
      }
    });
    this.node.sortAllChildren();
    // cc.log(`Entities size [${this._entities.length}]`);
  }

  addGraphicComponent(object: IDesign, entity: Entity) {
    const { graphicId, objPrivate } = object;
    switch (graphicId) {
      case 1:
        entity.addECSComponent(ImageComponent, {
          keyImage: objPrivate.keyImage,
          linkImage: objPrivate.linkImage,
          scale: object.scale,
        } as IImageComponent);
        break;
      case 2:
        entity.addECSComponent(RectComponent);
        break;
      case 3:
        // if a label and has item ref it just a defined of the style for input?
        if (objPrivate.action && objPrivate.action.type && objPrivate.action.type == "input") {

        } else {
          entity.addECSComponent(LabelComponent, {
            ...objPrivate.style,
            text: objPrivate.text,
            name: object.name,
          } as ILabelComponent);
        }
        break;
      default:
        break;
    }
    if (graphicId == 1 || graphicId == 3) {
      // entity.addECSComponent(DebugComponent, {
      //   d_name: object.name,
      //   id: object.id,
      // } as IDebugComponent);
    }
  }

  addActionComponent(object: IDesign, entity: Entity) {
    const { objPrivate } = object;
    if (!objPrivate || !objPrivate.action) {
      return;
    }
    const { type, itemRef } = objPrivate.action;
    switch (type) {
      case "drag":
        entity.addECSComponent(DragComponent, {
          touchId: -1,
          originalParent: this.node,
          ...objPrivate.action,
        } as IDragComponent);
        break;
      case "drop":
        entity.addECSComponent(DropComponent, {
          touchId: -1,
          size: cc.size(object.w, object.h),
          ...objPrivate.action,
        } as IDropComponent);
        break;
      case "list":
        entity.addECSComponent(ListComponent, {
          ...objPrivate.action,
        } as IListComponent);
        break;
    }

    if (itemRef && itemRef.parent) {
      entity.addECSComponent(ListItemComponent, {
        ...objPrivate.action,
      } as IListItemComponent);
    }
  }
}
