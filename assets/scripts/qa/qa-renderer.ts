import { IQA, IDesign, IAction, ConfigState } from "../models/qa";
import { Graphic, ActionType, Server } from "../constants/qa.constants";
import QaNode from "./qa-node";
import QaImage from "./qa-image";
import QaDropZone from "./qa-drop-zone";
import QaDrag from "./qa-drag";
import QaRect from "./qa-rect";
// import QaLabel from "./qa-label";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QARenderer extends cc.Component {

  @property(cc.JsonAsset)
  qaJson: cc.JsonAsset = null;

  // @property(cc.Prefab)
  // qaGraphicPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  qaRectPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  qaObjectPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  qaLabelPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  qaImagePrefab: cc.Prefab = null;
  // @property(cc.Label)
  // mouseDebug: cc.Label = null;

  @property(cc.Node)
  uiNode: cc.Node = null;

  // multiple layer node for render on top
  @property(cc.Node)
  dragUiNode: cc.Node = null;

  @property(cc.Button)
  closeExplainButton: cc.Button = null;

  @property(cc.WebView)
  explainWebview: cc.WebView = null;


  static _instance: QARenderer = null;

  static getInstance(): QARenderer {
    if (this._instance == null)
      this._instance = new QARenderer();
    return this._instance;
  }

  // // [qa_id]: { qa_object }
  // _qaObjects = {};

  // _qaLists = {};
  // _qaDragContainer = {};
  // _qaDropContainer = [];

  // tempPadding = 15;

  _scaleRatio: number = 1;
  _QAData: IQA;
  _clickGroup: QaNode[] = [];
  _clickValue: any = null;
  _configs: any = {};
  _actionGroup: Map<string, QaNode> = new Map<string, QaNode>();
  _qaNodeDic: Map<string, cc.Node> = new Map<string, cc.Node>();
  _dropzones: Map<string, cc.Node> = new Map<string, cc.Node>();
  _qaActionNodeDic: Map<string, cc.Node> = new Map<string, cc.Node>();

  // _loadedImages: { [imageLink: string]: cc.SpriteFrame } = {};

  public get scaleRatio(): number {
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    return uiRatio;
  }

  onLoad() {
    cc.view.setResizeCallback(() => this.onResize())
    QARenderer.getInstance()._clickGroup = [];
    // for debugging
    if (this.qaJson) {
      this.setupUI(this.qaJson.json);
    }
  }

  onResize() {
    cc.log('QaRenderer::onResize');
    cc.log(`cc.winSize [${cc.winSize.width}-${cc.winSize.height}]`)
    let designRatio = 1920 / 1080;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this._scaleRatio = uiRatio;
    this.explainWebview.node.scale = uiRatio;
    // this.ui.scale = uiRatio;
  }

  start() {
  }

  onEnable() {

  }

  getAnswer(): any {
    // based on current rendering QA return corresponding answer
    return this._clickValue;
  }

  setupUI(QAData: IQA) {
    this.clear();
    QARenderer.getInstance()._QAData = QAData;
    for (let i = 0; i < QARenderer.getInstance()._QAData.design.length; i++) {
      const qaDesign = QARenderer.getInstance()._QAData.design[i];
      if (qaDesign.graphicId) {
        // we can render this
        this.renderGraphicObject(qaDesign);
      }

      if (qaDesign.configure) {
        QARenderer.getInstance()._configs[qaDesign.configure] = qaDesign;
      }
    }
    cc.log(QARenderer.getInstance()._configs);
  }

  renderGraphicObject(qaDesign: IDesign) {
    if (!qaDesign.graphicId) return;

    let qaNode: cc.Node = null;

    switch (qaDesign.graphicId) {
      case Graphic.IMAGE:
        qaNode = cc.instantiate(this.qaImagePrefab);
        // qaNode.addComponent(QaImage);
        break;
      case Graphic.RECT:
        qaNode = cc.instantiate(this.qaRectPrefab);
        break;
      case Graphic.TEXT:
        qaNode = cc.instantiate(this.qaLabelPrefab);
        // qaNode.addComponent(QaLabel);
        break;
      default:
        break;
    }
    if (qaNode != null) {
      let qaNodeComponent: QaNode = qaNode.getComponent(QaNode);
      qaNodeComponent.init(qaDesign);
      QARenderer.getInstance()._qaNodeDic.set(qaDesign.name, qaNode);
      this.uiNode.addChild(qaNode);
      if (qaNodeComponent.hasAction()) {
        QARenderer.getInstance()._qaActionNodeDic.set(qaNodeComponent.getActionName(), qaNode);
        if (!QARenderer.getInstance()._actionGroup.has(qaNodeComponent.getActionName())) {
          QARenderer.getInstance()._actionGroup.set(qaNodeComponent.getActionName(), qaNodeComponent);
          this.addNodeActionEvent(qaNode, qaDesign);
        } else {
          // add the item to the group
          let qaNodeGroup = QARenderer.getInstance()._actionGroup.get(qaNodeComponent.getActionName());
          let uiNode = qaNodeGroup.node.getChildByName('ui');
          let world = qaNode.parent.convertToWorldSpaceAR(qaNode.getPosition());
          qaNode.parent = uiNode;
          qaNode.setPosition(uiNode.convertToNodeSpaceAR(world));
        }
      }
    }
  }

  addNodeActionEvent(qaNode: cc.Node, qaDesign: IDesign) {
    const { objPrivate } = qaDesign;
    const { action } = objPrivate;
    if (!objPrivate) return;
    if (!action) return;

    if (action.type == ActionType.CLICK) {
      let qaNodecomponent: QaNode = qaNode.getComponent(QaNode);
      qaNodecomponent.addClickEvent((value) => this.onNodeClick(value));
      QARenderer.getInstance()._clickGroup.push(qaNodecomponent);
    } else if (action.type == ActionType.DROP) {
      let dropZone = qaNode.addComponent(QaDropZone);
      QARenderer.getInstance()._dropzones.set(qaDesign.objPrivate.action.name, qaNode);
      cc.log('Add dropzone', qaNode.getBoundingBoxToWorld());
    } else if (action.type == ActionType.DRAG) {
      let dragComponent = qaNode.addComponent(QaDrag);
      qaNode.parent = this.dragUiNode; // use this node to render on top of other ui
    }
  }

  showExplain() {
    cc.log('zindex', this.explainWebview.node.parent.children);
    this.explainWebview.url = 'https://qam.starmathsonline.com.au/Home/Explanation?qaid=1573540978847'; // TODO: Update with QA Id
    this.explainWebview.node.active = true;
    this.closeExplainButton.node.active = true;
  }

  hideExplain() {
    this.explainWebview.node.active = false;
    this.closeExplainButton.node.active = false;
  }

  onNodeClick(value: string) {
    cc.log('QARenderer:onNodeClick', value);
    this._clickValue = value;
    for (let i = 0; i < QARenderer.getInstance()._clickGroup.length; i++) {
      const clickNode = QARenderer.getInstance()._clickGroup[i];
      if (clickNode._QAObject.objPrivate.action.value != value) {
        clickNode.deselect();
      }
    }
  }

  clear() {
    QARenderer.getInstance()._clickGroup = [];
    this.uiNode.removeAllChildren();
  }
}