import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class FriendHouseService {
  static LoadFriendRoom(friendId: number = 0) {
    if (friendId > 0) {
      this.openRoomFriend(friendId);
    }
    this.intFriendRoom();
  }

  static openRoomFriend(friendId: number = 0, room: string = "BedRoom") {

    ACE.gameSocket.emit(SERVER_EVENT.OPEN_ROOM, {
      roomName: room,
      studentId: friendId,
      isFriend: true,
    });
  }

  static intFriendRoom() {
    ACE.gameSocket.emit(SERVER_EVENT.INIT_ROOM);
    this.loadRoomComponents();
  }

  static loadRoomComponents() {
    ACE.gameSocket.emit(SERVER_EVENT.GET_WHOSE_HOUSE);
    ACE.gameSocket.emit(SERVER_EVENT.GET_CURRENT_FRIEND);
  }

  static nextHouseFriend() {
    ACE.gameSocket.emit(SERVER_EVENT.GET_NEXT_FRIEND);
  }

  static preHouseFriend() {
    ACE.gameSocket.emit(SERVER_EVENT.GET_PRE_FRIEND);
  }

  static showLogEvent() {
    ACE.gameSocket.on(SERVER_EVENT.GET_WHOSE_HOUSE, (data) => {
      cc.log("GET_WHOSE_HOUSE:", data);
    });
    ACE.gameSocket.on(SERVER_EVENT.LOAD_STUDENT_ROOMS, (data) => {
      cc.log("LOAD_STUDENT_ROOMS:", data);
    });
    ACE.gameSocket.on(SERVER_EVENT.LOAD_ALL_ROOM_ITEM_GROUP_L1, (data) => {
      cc.log("LOAD_ALL_ROOM_ITEM_GROUP_L1:", data);
    });
    ACE.gameSocket.on(SERVER_EVENT.LOAD_ROOM_ITEM_USING, (data) => {
      cc.log("LOAD_ROOM_ITEM_USING:", data);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_CURRENT_FRIEND, (data) => {
      cc.log("GET_CURRENT_FRIEND:", data);
    });
  }
}
