import Component, { IComponent } from "../../core/ecs/component";

const {ccclass, property} = cc._decorator;

export interface IImageComponent extends IComponent {
  keyImage: string;
  linkImage: string;
  scale?: number;
}

@ccclass
export default class ImageComponent extends Component {
  _c_name = "ImageComponent";
  _data: IImageComponent;
}
