// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class SwapLayerTrail extends cc.Component {
  @property(cc.Node)
  front: cc.Node = null;

  @property(cc.Node)
  back: cc.Node = null;

  swap() {
    if (this.node.parent == this.front) {
      this.node.parent = this.back;
    } else {
      this.node.parent = this.front;
    }
  }
}
