import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Intro extends GameScene {
  @property(cc.Node)
  witch: cc.Node = null;

  @property(cc.Node)
  bubbleChat: cc.Node = null;

  @property(sp.Skeleton)
  book: sp.Skeleton = null;

  onLoad() {
    super.onLoad();
  }

  registerEvents() {
  }

  onEnter() {
    this.witch.opacity = 0;
    this.bubbleChat.active = false;
    let audioId = null;
    this.node.runAction(cc.sequence(
      cc.callFunc(() => {
        audioId = AudioManager.getInstance().playAudio("book_appearing");
        this.book.addAnimation(0, "animation", false);
      }),
      cc.delayTime(1.8),
      cc.callFunc(() => {
        cc.audioEngine.stop(audioId);
        audioId = AudioManager.getInstance().playAudio("book_shaking");
      }),
      cc.delayTime(0.4),
      cc.callFunc(() => {
        cc.audioEngine.stop(audioId);
        audioId = AudioManager.getInstance().playAudio("book_opening");
      }),
      cc.delayTime(0.7),
      cc.callFunc(() => {
        cc.audioEngine.stop(audioId);
        audioId = AudioManager.getInstance().playAudio("logo_appearing");
      }),
      cc.delayTime(1.4),
      cc.callFunc(() => {
        cc.audioEngine.stop(audioId);
        audioId = AudioManager.getInstance().playAudio("npc_appearing");
      }),
    )),
    this.book.setCompleteListener(() => {
      cc.log("Done playing book");
      cc.audioEngine.stop(audioId);
      audioId = AudioManager.getInstance().playAudio("star1");
      this.witch.runAction(
        cc.sequence(
          cc.fadeIn(0.6),
          cc.callFunc(() => {
            this.bubbleChat.active = true;
          }),
          // play witch text hear
          cc.delayTime(7),
          cc.callFunc(() => {
            ACE.sceneManager.pushScene("choose-character");
          }),
        ),
      );
    });
  }
}
