const { ccclass, property } = cc._decorator;

export default class StateRange {
  min: number = 0;
  max: number = 0;
  upto: number = 0;
  constructor(min, max, upto: number = 0) {
    this.min = min;
    this.max = max;
    this.upto = upto;
  }
}
