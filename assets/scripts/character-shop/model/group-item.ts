const { ccclass, property } = cc._decorator;

@ccclass("GroupItem")
export default class GroupItem {
  @property(cc.String)
  itemName: string = "";
  @property([cc.SpriteFrame])
  iconSFs: cc.SpriteFrame[] = [];
}
