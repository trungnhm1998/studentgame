import NodeUtils from "../core/utils/node-utils";

const { ccclass, property } = cc._decorator;

const BORDER_COLOR = '#FFFFFF';
const BORDER_HIGHLIGHT = '#0EFFFC';

@ccclass
export default class MapItem extends cc.Component {
  border: cc.Node = null;
  tag: cc.Node = null;

  onLoad() {
    this.border = NodeUtils.findNodeByName(this.node, 'border');
    this.tag = NodeUtils.findNodeByName(this.node, 'tag');
    this.border.color = new cc.Color().fromHEX(BORDER_HIGHLIGHT);
    this.border.active = false;
    this.tag.active = true;
    this.setupEvents();
  }

  zoom() {
    this.node.stopAllActions();
    this.node.runAction(cc.sequence(
      cc.scaleTo(0.5, 1.1).easing(cc.easeBounceOut()),
      cc.callFunc(() => {
        this.node.setScale(1.1);
      })
    ));
  }

  unzoom() {
    this.node.stopAllActions();
    this.node.runAction(cc.sequence(
      cc.scaleTo(0.5, 1).easing(cc.easeBounceOut()),
      cc.callFunc(() => {
        this.node.setScale(1);
      })
    ));
  }

  setupEvents() {
    this.node.on(cc.Node.EventType.TOUCH_START, (event) => {
      this.zoom();
      this.border.active = true;
    });

    this.node.on(cc.Node.EventType.TOUCH_CANCEL, (event) => {
      this.unzoom();
      this.border.active = false;
    })

    this.node.on(cc.Node.EventType.TOUCH_END, (event) => {
      this.unzoom();
      this.border.active = false;
    })

    this.node.on(cc.Node.EventType.MOUSE_LEAVE, (event) => {
      this.unzoom();
      this.border.active = false;
    })

    this.node.on(cc.Node.EventType.MOUSE_ENTER, (event) => {
      this.zoom();
      this.border.active = true;
    })
  }
}