import ItemNode from "./item-node";
import { ItemState } from "../constants/qa.constants";
import PopupConfirm from "./popup-confirm";
import PopupCongratulation from "./popup-congratulation";
import CharacterShopService from "./character-shop-service";
import { RESOURCE_PATH } from "../constants/resource-constant";
import ProductItem from "../models/product-item";
import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";
import AudioManager from "../core/audio-manager";
import CharacterService from "../Character/character-service";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemShop extends cc.Component {
  @property(cc.Node)
  shopContents: cc.Node = null;
  @property(cc.Node)
  loadingItemNode: cc.Node = null;
  @property(cc.Node)
  scrollBarNode: cc.Node = null;
  @property(cc.Node)
  barNode: cc.Node = null;
  @property(cc.Float)
  topBarScroll: number = 0;
  @property(cc.Float)
  bottomBarScroll: number = 0;

  @property(cc.Prefab)
  itemNodePrefab: cc.Prefab = null;

  @property(PopupConfirm)
  popupConfirm: PopupConfirm = null;
  @property(PopupCongratulation)
  popupCongratulation: PopupCongratulation = null;

  _listItemNode: ItemNode[] = null;
  _listProductItem: ProductItem[] = new Array<ProductItem>();
  // _listSpriteIconItem: cc.SpriteFrame[] = null;
  _itemSelecting: number = -1;
  _curentGroupID: number = -1;
  _totalPage: number = 0;
  _curentPage: number = 0;
  _currentGroupID: number = 0;
  _idItemBuying: number = -1;
  _isBuyingByStars: boolean = false;

  //
  // called when change group item
  //
  start() {
    ACE.gameSocket.on(SERVER_EVENT.BUY_ITEM_BY_STAR, (data) =>
      this.onBuyItemResponse(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.BUY_ITEM_BY_COIN, (data) =>
      this.onBuyItemResponse(data),
    );

    ACE.gameSocket.on(SERVER_EVENT.HIDE_CHARACTER_ITEM, (data) => {
      CharacterService.loadCharaterUsing();
    });
    // this.initItemSize();
  }

  onChangeGroupItem(groupId: number) {
    this._itemSelecting = -1;
    this._listProductItem = new Array<ProductItem>();
    this.loadingItemNode.active = true;
    this._totalPage = 0;
    this.scrollBarNode.active = false;

    this._curentGroupID = groupId;
    const self = this;
    CharacterShopService.loadTotalPage(groupId + 1, (data) => {
      self._totalPage = data;
      self._curentPage = 1;
      if (self._totalPage > 1) {
        self.scrollBarNode.active = true;
        self.barNode.position = cc.v2(
          this.barNode.position.x,
          self.topBarScroll,
        );
      }
      CharacterShopService.loadShopItems(self._curentGroupID + 1, (itemData) => {
        self.onShopItemResponce(itemData);
      });
    });
    if (this.shopContents != null) {
      this.shopContents.removeAllChildren();
    }
  }

  onEnable() {
    this.initItemSize();
  }

  initItemSize(){
    cc.log("Item Shop", "W:" + this.shopContents.getComponent(cc.Layout).cellSize.width + 
    " H: " + this.shopContents.getComponent(cc.Layout).cellSize.height,
    );
    this.shopContents.getComponent(cc.Layout).cellSize  = new cc.Size(
      (this.shopContents.width - 25) / 4.0,
      (this.shopContents.height - 25) / 4.0);
  }

  onShopItemResponce(data) {
    // cc.log("onShopItemResponce", data);
    if (data != null) {
      this._listProductItem = data;
    }
    // For load all item
    // if (this._curentPage < this._totalPage) {
    //   CharacterShopService.loadNextPage(data => {
    //     this.onShopItemResponce(data);
    //   });
    //   this._curentPage++;
    // } else {
    //   this.createItems();
    // }
    this.createItems();
  }

  createItems() {
    if (this.shopContents != null) {
      this.shopContents.removeAllChildren();
    } else {
      this.shopContents = cc.find(
        "ItemsScrollView/view/contentItems",
        this.node,
      );
    }
    this._itemSelecting = -1;
    this._listItemNode = new Array<ItemNode>();

    const prePath = RESOURCE_PATH.DATA_ASSETS_SERVER;
    // this._listSpriteIconItem = new Array<cc.SpriteFrame>();
    const self = this;
    let itemIdx = 0;
    this._listProductItem.forEach((produceItem) => {
      // self._listSpriteIconItem.push(icon);

      const nodeInst = cc.instantiate(self.itemNodePrefab);
      nodeInst.parent = self.shopContents;
      nodeInst.setPosition(0, 0);
      const itemObj = nodeInst.getComponent("item-node");
      self._listItemNode.push(itemObj);
      itemObj.initItem(produceItem, itemIdx, self);
      itemIdx++;
      if (itemIdx >= self._listProductItem.length - 1) {
        self.loadingItemNode.active = false;
      }
    });
    // this.initItemSize();
  }

  callChangeCharacterSet(itemID: number, isEquip: boolean) {
    if (isEquip) {
      CharacterShopService.equipedCharacterItemByIdx(itemID);
    } else {
      CharacterShopService.tryCharacterItemByIdx(itemID);
    }
  }

  onItemSelectedCallback(itemID: number) {
    if (itemID != this._itemSelecting) {
      if (this._itemSelecting != -1) {
        this._listItemNode[this._itemSelecting].setItemUnSelected();
      }
      this._itemSelecting = itemID;
      this.callChangeCharacterSet(itemID, false);
    }
  }

  onItemBuyCallback(itemID: number, isByStar: boolean) {
    this._idItemBuying = itemID;
    this._isBuyingByStars = isByStar;
    const id = itemID;
    let buyBy = "coins";
    if (isByStar) { buyBy = "stars"; }
    const self = this;
    this.popupConfirm.showPopup(
      "Are you sure want to buy item " +
      this._listProductItem[id].Name +
      " with " +
      buyBy +
      "?",
      () => {
        // confirm to buy
        // check can buy
        const idItemWillBuy = id;
        cc.log("Buying item:", idItemWillBuy);
        CharacterShopService.buyItemBy(idItemWillBuy, isByStar);
      },
    );
  }

  onBuyItemResponse(data) {
    if (data == 1) {
      this.onBuyItemSuccessed();
    } else {
      this.onBuyItemFailed();
    }
  }

  onBuyItemSuccessed() {
    CharacterShopService.loadShopItems(this._curentGroupID + 1, (data) => {
      this.onShopItemResponce(data);
    });
    const mess = "Congratulation! you have purchased your item successfully";
    this.popupCongratulation.onShowPopup(
      mess,
      this._listProductItem[this._idItemBuying].AssetImageUrl,
      () => {
        this.onItemUseCallback(this._idItemBuying);
      },
    );
    this._listItemNode[this._idItemBuying].setStateItem(
      ItemState.AVAILIBLE_EQUIP,
    );
  }

  onBuyItemFailed() {
    let txtCurency = "coins";
    if (this._isBuyingByStars) { txtCurency = "stars"; }
    const message =
      "You don't have enough " +
      txtCurency +
      ' to buy "' +
      this._listProductItem[this._idItemBuying].Name +
      '"';
    this.popupConfirm.showPopup(message, null, true);
  }

  onItemUseCallback(itemID: number) {
    this._listItemNode.forEach((itemNode) => {
      if (itemNode.getState() == ItemState.EQUIPPED) {
        itemNode.setStateItem(ItemState.AVAILIBLE_EQUIP);
      }
    });
    this._listItemNode[itemID].setStateItem(ItemState.EQUIPPED);
    if (this._itemSelecting != -1) {
      this._listItemNode[this._itemSelecting].setItemUnSelected();
    }
    // this.onItemSelectedCallback(itemID);
    this.callChangeCharacterSet(itemID, true);
  }

  onItemHideCallback(itemID: number) {
    // this._listItemNode[itemID].setHideItem();
    this._listItemNode[itemID].setStateItem(ItemState.AVAILIBLE_EQUIP);
    CharacterShopService.hiddenCharacterItemByIdx(itemID);
  }

  btnNextPageOnClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    if (this._curentPage < this._totalPage) {
      this._curentPage++;
      this._listProductItem = new Array<ProductItem>();
      this.loadingItemNode.active = true;
      CharacterShopService.loadNextPage((data) => {
        this.onShopItemResponce(data);
      });

      const newPosY =
        this.topBarScroll -
        ((this.topBarScroll + Math.abs(this.bottomBarScroll)) /
          this._totalPage) *
        this._curentPage;

      // this.barNode.runAction(cc.sequence(cc.moveBy(10, -Math.abs(newPosY), 0)));
      this.barNode.position = cc.v2(this.barNode.position.x, newPosY);
    }
  }

  btnPrePageOnClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    if (this._curentPage > 1) {
      this._curentPage--;
      this._listProductItem = new Array<ProductItem>();
      this.loadingItemNode.active = true;
      CharacterShopService.loadPrePage((data) => {
        this.onShopItemResponce(data);
      });
      const newPosY =
        this.topBarScroll -
        ((this.topBarScroll + Math.abs(this.bottomBarScroll)) /
          this._totalPage) *
        (this._curentPage - 1);
      this.barNode.position = cc.v2(this.barNode.position.x, newPosY);
      // this.barNode.runAction(
      //   cc.sequence(cc.moveBy(100, +Math.abs(newPosY), 0))
      // );
    }
  }
}
