const { ccclass, property } = cc._decorator;

import StudentItemInShop from "./Model/StudentItemInShop";
import StudentRoomItemGroupL1 from "./Model/StudentRoomItemGroupL1";
import StudentItemMenuShopNode from "./StudentItemMenuShopNode";
import StudentItemShopNode from "./StudentItemShopNode";
import StudenHouseManager from "./StudentHouseManager";
import { RESOURCE_PATH } from "../constants/resource-constant";

@ccclass
export default class StudentRoomShopManage extends cc.Component {
  static _instance: StudentRoomShopManage = null;

  static setInstance(object: StudentRoomShopManage) {
    this._instance = object;
  }

  static getInstance() {
    if (this._instance == null) { this._instance = new StudentRoomShopManage(); }
    return this._instance;
  }

  @property(cc.Node)
  public studentRoomItemGroupL1Node: cc.Node = null;

  @property(cc.Node)
  public studentItemInShop: cc.Node = null;

  @property(cc.Node)
  public FurnitureNode: cc.Node = null;

  @property(cc.Node)
  public DecoNode: cc.Node = null;

  @property(cc.SpriteFrame)
  public SpriteFrameFurnitureNormal: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  public SpriteFrameFurnitureSelect: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  public SpriteFrameDecoNormal: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  public SpriteFrameDecoSelect: cc.SpriteFrame = null;

  @property(cc.Node)
  public Loading: cc.Node = null;

  listsStudentRoomItemGroupL1Node: cc.Node[] = [];
  listStudentItemInShopNode: cc.Node[] = [];

  public listStudentRoomItemGroupL1: StudentRoomItemGroupL1[] = [];
  public listStudentItemInShop: StudentItemInShop[] = [];

  previousTab = 0;
  currentTab = 0;

  currentGroup = 1; // Furniture: 1, Deco: 2

  setCurrentTab(tab: number) {
    this.previousTab = this.currentTab;
    this.currentTab = tab;
    this.listsStudentRoomItemGroupL1Node[this.previousTab].getChildByName("BG").active = false;
    this.listsStudentRoomItemGroupL1Node[tab].getChildByName("BG").active = true;
  }

  getTabFollowMenuItem(ID: number): number {
    let res = 0;
    this.listStudentRoomItemGroupL1.forEach((item, id) => {
      if (item.ID == ID) {
        this.setCurrentTab(id);
        return id;
      }
    });
    return res;
  }

  getIndexItemFollowID(ID: number): number {
    let res = -1;
    cc.log(this.listStudentItemInShop);
    this.listStudentItemInShop.forEach((item, id) => {
      if (item.ID == ID) {
        cc.log("@@@@@@@@@ BINGO :::: " + id);
        res = id;
        return id;
      }
    });
    return res;
  }

  start() {
    StudentRoomShopManage.setInstance(this);
  }

  SelectGroupItem(group: number) {
    if (group == 1) {
      this.FurnitureNode.getComponent(cc.Sprite).spriteFrame = this.SpriteFrameFurnitureSelect;
      this.DecoNode.getComponent(cc.Sprite).spriteFrame = this.SpriteFrameDecoNormal;
    } else {
      this.FurnitureNode.getComponent(cc.Sprite).spriteFrame = this.SpriteFrameFurnitureNormal;
      this.DecoNode.getComponent(cc.Sprite).spriteFrame = this.SpriteFrameDecoSelect;
    }
  }

  onInitMenu(data) {
    // Update Group Deco / Furniture Image Sprite.
    this.SelectGroupItem(this.currentGroup);

    // alway reset for correct
    this.listsStudentRoomItemGroupL1Node.forEach((element) => {
      element.removeFromParent(true);
    });
    this.listStudentRoomItemGroupL1 = [];
    this.listsStudentRoomItemGroupL1Node = [];
    this.studentRoomItemGroupL1Node.active = true;

    // set Data
    data.forEach((item, id) => {
      this.listStudentRoomItemGroupL1.push(item);
      let node = cc.instantiate(this.studentRoomItemGroupL1Node);
      node.parent = this.studentRoomItemGroupL1Node.parent;
      node.setPosition(node.x, -(node.y + id * (node.height + 5)));
      node.getComponent(StudentItemMenuShopNode).init(item);
      this.listsStudentRoomItemGroupL1Node.push(node);
    });
    this.setCurrentTab(0);
    this.studentRoomItemGroupL1Node.active = false;
  }

  preloadImage(data) {
    let self = this;

    let urlImageList: string[] = [];

    data.forEach((item) => {
      urlImageList.push(RESOURCE_PATH.DATA_ASSETS_SERVER + item.UrlIcon);
    });

    // after this, it will cache the list image, will help load faster then
    cc.loader.load(urlImageList,
      (err, texture) => {
        if (err) {
          cc.error(err.message || err);
          return;
        }

        data.forEach((item, id) => {
          self.listStudentItemInShop.push(item);

          let node = cc.instantiate(self.studentItemInShop);
          node.parent = self.studentItemInShop.parent;
          node.setPosition(node.x, -(node.y + id * (node.height + 5)));
          // update for BG / icon / equip / star label / coin label
          node.getComponent(StudentItemShopNode).initProp(item);
          self.listStudentItemInShopNode.push(node);
        });

        self.studentItemInShop.active = false;

        self.Loading.active = false;
      },
    );
  }

  onInitShopItems(data) {
    let jsonData = JSON.stringify(data);
    if (jsonData.startsWith("[")) {
      // alway reset for correct
      this.listStudentItemInShopNode.forEach((element) => {
        element.removeFromParent(true);
      });
      this.listStudentItemInShop = [];
      this.listStudentItemInShopNode = [];
      this.studentItemInShop.active = true;

      // Start Animation loading item in shop
      this.Loading.active = true;

      // Preload image
      this.preloadImage(data);
    } else {
      // JSON Object
    }
  }

  buttonAction(event, action) {
    let grpID = this.listStudentItemInShop[0].RoomItemGroupL1ID;
    switch (action) {
      case "selectFurniture":
        this.currentGroup = 1;
        // Update Group Deco / Furniture Image Sprite.
        this.SelectGroupItem(this.currentGroup);
        StudenHouseManager.getInstance().SelectGroupShop(this.currentGroup);
        break;
      case "selectDeco":
        this.currentGroup = 2;
        // Update Group Deco / Furniture Image Sprite.
        this.SelectGroupItem(this.currentGroup);
        StudenHouseManager.getInstance().SelectGroupShop(this.currentGroup);
        break;
      case "loadNext":
        cc.log("Next");

        StudenHouseManager.getInstance().LoadNextItems(grpID);
        break;
      case "loadPrevious":
        StudenHouseManager.getInstance().LoadPreviousItems(grpID);
        cc.log("Previous");
        break;
    }
  }
}
