export default interface ClientProtocol {
  connect(url: string, onCloseCallback: Function, onConnectCallback: Function): Promise<any>;
  on(eventName: string, callback: Function): void;
  off(eventName: string, fn?: Function): void;
  emit(eventName: string, args: any): void;
  _eventCallbacks: { [eventName: string]: Function[] };
  _socket: any;
}