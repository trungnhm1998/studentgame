import Component, { IComponent } from "../../core/ecs/component";

const { ccclass, property } = cc._decorator;

export interface IListComponent extends IComponent {
  name: string;
  type: string;
  x: number;
  y: number;
  anchor: Anchor;
  dir: string; // "horizontal" or "vertical"?
  spacing: number;
}

export interface Anchor {
  x: number;
  y: number;
}

@ccclass
export default class ListComponent extends Component {
  _c_name: string = "ListComponent";
  _data: IListComponent;
}
