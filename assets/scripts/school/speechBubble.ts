import SchoolService, { NotiObject, NotiType } from "./school-service";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SpeechBubble extends cc.Component {
  @property(cc.Node)
  btnCallbackContainer: cc.Node = null;

  @property(cc.Label)
  notiLabel: cc.Label = null;

  intervalTime: number = 10000;
  currentIntervalTime: number = 0;
  inSchool: boolean = true; // to determine if we are in School scene
  inQuestionsRoom: boolean = true;

  onLoad() {
    // this.updatePosition();
  }

  start() {
    this.updateRandomSpeechFromNotiObjects(SchoolService.notificationObjects);
  }

  // Call this function to update speechText.
  updateRandomSpeechFromNotiObjects(list: NotiObject[]) {
    const randomNotiObject = list[Math.round(Math.random() * (list.length - 1))];
    this.notiLabel.string =
      randomNotiObject.notificationText;
    if (randomNotiObject.type != NotiType.NORMAL) {
      SchoolService.currentNotfication = randomNotiObject;
      SchoolService.scene = `${randomNotiObject.type}`;
      this.btnCallbackContainer.active = true;
    } else {
      this.btnCallbackContainer.active = false;
    }
  }

  setSpeech(text: string) {
    this.notiLabel.string = text;
  }

  // Updates position based on params or parent.
  updatePosition(newX?, newY?) {
    if (typeof newX !== "undefined" && typeof newY !== "undefined") {
      this.node.x = newX;
      this.node.y = newY;
    } else {
      this.node.y =
        this.node.parent.height / 2 - this.node.children[0].height / 5;
      this.node.x = this.node.parent.x;
      cc.log(this.node.y);
    }
  }

  update(deltaTime: number) {
    if (this.inSchool) {
      this.currentIntervalTime += deltaTime * 1000;
      if (this.currentIntervalTime >= this.intervalTime) {
        this.currentIntervalTime = 0;
        if (SchoolService.notiText && SchoolService.notiText != "") {
          this.updateRandomSpeechFromNotiObjects(
            SchoolService.notificationObjects,
          );
        }
      }
    } else {
      this.notiLabel.string = SchoolService.notiText;
    }
  }
}
