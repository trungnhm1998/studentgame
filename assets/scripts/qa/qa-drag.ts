import QARenderer from "./qa-renderer";
import QaDropZone from "./qa-drop-zone";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QaDrag extends cc.Component {
  _originalPosition: cc.Vec2 = cc.v2();
  _touchId: number;

  onLoad() {
    this._originalPosition.set(this.node.getPosition());
    this.node.on(cc.Node.EventType.TOUCH_START, (event: cc.Event.EventTouch) => this.onTouchStart(event));
    this.node.on(cc.Node.EventType.TOUCH_MOVE, (event: cc.Event.EventTouch) => this.onTouchMove(event));
    this.node.on(cc.Node.EventType.TOUCH_END, (event: cc.Event.EventTouch) => this.onTouchEnd(event));
    this.node.on(cc.Node.EventType.TOUCH_CANCEL, (event: cc.Event.EventTouch) => this.onTouchEnd(event));
  }

  onTouchEnd(event: cc.Event.EventTouch): any {
    if (this._touchId == event.getID()) {
      this._touchId = null;
      // check if intersect with any drop zone?
      // which one have the lagrest intersects section
      let isIntersect = false;
      let intersectRectKeys: string[] = [];
      QARenderer.getInstance()._dropzones.forEach((dropzoneNode, key) => {
        cc.log(`[${key}] isIntersect [${dropzoneNode.getBoundingBox().intersects(this.node.getBoundingBox())}]`, dropzoneNode.getPosition(), this.node.getAnchorPoint());
        if (dropzoneNode.getBoundingBox().intersects(this.node.getBoundingBox())) {
          intersectRectKeys.push(key);
        }
      })

      if (intersectRectKeys.length != 0) {
        isIntersect = true;
        // intersect with some dropzone
        if (intersectRectKeys.length == 1) {
          // but only one
          let dropzoneNode: cc.Node = QARenderer.getInstance()._dropzones.get(intersectRectKeys[0]);
          let dragzoneComponent = dropzoneNode.getComponent(QaDropZone);
          dragzoneComponent.setDragObject(this);
        } else {
          // TODO: Check the largest intersect and choose that
        }
      }

      if (!isIntersect)
        this.node.setPosition(this._originalPosition);
    }
  }

  onTouchMove(event: cc.Event.EventTouch): any {
    cc.log(`QaDrag::onTouchMove touchId[${this._touchId}] event[${event.getID()}]`);
    if (this._touchId != event.getID()) {
      this.node.setPosition(this._originalPosition);
      return;
    };
    let delta = event.getDelta();
    // divined 1.3 because of scale factor
    this.node.x += delta.x / 1.3;
    this.node.y += delta.y / 1.3;
  }

  onTouchStart(event: cc.Event.EventTouch): any {
    cc.log('QaDrag::onTouchStart', event);
    this._touchId = event.getID();
  }

  resetDropzone() {
    this.node.setPosition(this._originalPosition);
  }
}
