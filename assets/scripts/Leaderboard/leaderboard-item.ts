import LeaderboardModel from "../models/leaderboard-model";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LeaderboardItem extends cc.Component {
  @property(cc.Label)
  rankLb: cc.Label = null;
  @property(cc.Label)
  nameLb: cc.Label = null;
  @property(cc.Node)
  nameNode: cc.Node = null;
  @property(cc.Sprite)
  iconRank: cc.Sprite = null;
  @property(cc.Sprite)
  iconAvatar: cc.Sprite = null;
  @property(cc.Label)
  rankValue: cc.Label = null;

  @property([cc.SpriteFrame])
  iconAvatarSprite: cc.SpriteFrame[] = [];
  @property([cc.SpriteFrame])
  iconRankSprite: cc.SpriteFrame[] = [];
  @property
  colorNameNormal: cc.Color = cc.Color.BLACK;
  @property
  colorNameIsMe: cc.Color = cc.Color.RED;

  _leaderboard: LeaderboardModel = null;
  _RANK_GIFT_GOLD = 3;
  _RANK_GIFT_SILVER = 10;
  _RANK_GIFT_CU = 300;

  initItem(leaderboard: LeaderboardModel, rank: number) {
    this.rankLb.string = rank + "";
    this._leaderboard = leaderboard;
    this.nameLb.string = leaderboard.FullName;
    this.rankValue.string = leaderboard.Value + "";
    var iconRankIdx = 0;
    if (rank <= this._RANK_GIFT_GOLD) {
      iconRankIdx = 0;
    } else if (rank <= this._RANK_GIFT_SILVER) {
      iconRankIdx = 1;
    } else {
      iconRankIdx = 2;
    }
    this.iconRank.spriteFrame = this.iconRankSprite[iconRankIdx];
    if (leaderboard.Gender) {
      this.iconAvatar.spriteFrame = this.iconAvatarSprite[0];
    } else {
      this.iconAvatar.spriteFrame = this.iconAvatarSprite[1];
    }

    if (leaderboard.isMe == 0) {
      this.nameNode.color = this.colorNameNormal;
    } else {
      this.nameNode.color = this.colorNameIsMe;
    }
  }
}
