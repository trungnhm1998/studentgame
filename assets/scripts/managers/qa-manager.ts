import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

export default class QAManager {
  static _instance: QAManager = null;
  static getInstance() {
    if (!this._instance) {
      this._instance = new QAManager();
    }
    return this._instance;
  }

  current() {
    switch (ACE.sceneManager.currentScene.name) {
      case 'practice':
        ACE.gameSocket.emit(SERVER_EVENT.CURRENT_QUESTION_PRACTISE_BLOCK, {});
        break;
      default:
        break;
    }
    // if ($game.state.current === "naplan") {
    //   return app.api.currentQuestionNaplan();
    // } else if ($game.state.current === "homework") {
    //     return app.api.currentQuestionHomework();
    // } else if ($game.state.current === "revision") {
    //     return app.api.currentQuestionRevision();
    // } else if ($game.state.current === "assignment") {
    //     return app.api.currentQuestionAssignment();
    // } else if ($game.state.current === "improvement") {
    //     return app.api.currentQuestionImpQaTemplate();
    // } else if ($game.state.current === "focusCenter") {
    //     return app.api.currentQuestionFocQatemplate();
    // } else if ($game.state.current === "practiseCenter") {
    //     return app.api.currentQuestionPractiseBlock();
    // } else if ($game.state.current === "testingConcept") {
    //     return app.api.currentQuestionPractiseBlock();
    // }
  }

  submit(answer: any) {
    if (answer == '' || answer == null || answer == undefined) {
      cc.log('QaManager::submit::Try to submit nil value;')
      return;
    }
    switch (ACE.sceneManager.currentScene.name) {
      case 'practice':
        ACE.gameSocket.emit(SERVER_EVENT.SUBMIT_ANSWER_PRACTISE, {
          answer
        });
        break;
      default:
        break;
    }
  }
}