import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CharacterService extends cc.Component {
  static loadCharacterInfo(callback: () => any) {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_CHAR_INFO, callback);
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CHAR_INFO);
  }

  static loadCharacter(callback: () => any) {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_CHARACTER_BASIC_USING, callback);
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CHARACTER_BASIC_USING);
  }

  static checkExistCharacter(callback: () => any) {
    ACE.gameSocket.on(SERVER_EVENT.CHECK_EXIST_CHARACTER, callback);
    ACE.gameSocket.emit(SERVER_EVENT.CHECK_EXIST_CHARACTER);
  }

  static loadCharaterUsing() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CHARACTER_USING);
  }

  // TODO: remove this for new character component
  static loadCharacterOfFriendUsing() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_FRIEND_CHARACTER_USING);
  }
}
