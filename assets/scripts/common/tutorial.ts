import LoginService from "../login/login-service";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupTutorial extends cc.Component {
  @property([cc.Node])
  tutorialContents: cc.Node[] = [];

  @property(cc.String)
  gameState: string = '';

  @property(cc.Node)
  next: cc.Node = null;

  idxTutorial: number = 0;
  closeTutorialCallback: Function;

  onEnable() {
    this.idxTutorial = 0;
    this.tutorialContents.forEach(tutorialNode => {
      tutorialNode.active = false;
    });
    this.tutorialContents[this.idxTutorial].active = true;

  }

  onLoad() {
    this.next.runAction(cc.sequence(
      cc.fadeTo(0.5, 255),
      cc.fadeTo(1, 0)
    ).repeatForever())
  }

  nextBtnOnClicked() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.idxTutorial++;
    if (this.idxTutorial >= this.tutorialContents.length) {
      if (this.closeTutorialCallback) {
        this.closeTutorialCallback();
      }
      this.node.active = false;
      LoginService.saveTutorial(this.gameState);
    } else {
      this.tutorialContents[this.idxTutorial - 1].active = false;
      this.tutorialContents[this.idxTutorial].active = true;
    }
  }
}
