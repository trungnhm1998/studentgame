# A.C.E Star Maths Online Student #

This README document whatever steps are necessary to get your application up and running.

## Overview

The port version of Phaser student game for native iOS/Android using Cocos Creator game engine, we mainly using VSCode as Text Editor but you can use anything else you want some suggestion such as WebStorm, Sublime, Atom, etc. But due to native compatibility of VSCode and Typescript which we use for this project for type safe and more readable code. 

## Table of Contents

<!-- toc -->

- [Development environment](#development-environment)
- [Coding convention](#coding-convention)
- [Project architect](#project-architect)
  - [Scene manager](#scene-manager)
  - [Websocket](#websocket)
- [Example](#example)

<!-- tocstop -->

## Development environment

- Cocos Creator v2.1.3 __this will get update__.
- VSCode with Typescript enabled (tslint).
- NodeJS v10.16.3 and above.
- Google Chrome I have tried a few other one like Firefox and Safari but the dev tools is not that great.

### For Android:
- Android NDK r16b and above works just fine.
- Either JDK 1.8.0 or OpenJDK but recommended JDK because I haven't got time to test out OpenJDK.
- Platform tool android-28 and above works just fine.
- Release keystore would be in `tools/android/ace-keystore.jks`.

### For iOS:
- to
- be
- update

## Project architect

### Scene manager

We use Single-Scene Node because Cocos Create Scene feature buggy when change from one screen to another sometimes will cause property of the scene node cannot find [Bug report here](https://discuss.cocos2d-x.org/t/the-second-time-i-load-a-scene-i-lose-reference-to-certain-components/37667/19).

Also we using this for dialog could be use globaly and overlay with multiple scene. With some resuse element. For example `Canvas/top_bar` in `assets/Scene/boost.fire` can be use in every scene with only one instance.

#### Usage

I have implemented `assets\scripts\core\scene-manager.ts` for managing scene with some ease.
```typescript
// boost.ts
import SceneManager from "../core/scene-manager";

@ccclass
export default class Boost extends cc.Component {
    initGlobal() {
      // Export to global facet so we only need to import ACE once for every other important facets
      ACE.sceneManager = SceneManager.getInstance();
    }

    update(dt) {
      ...
      ACE.sceneManager.pushScene("login"); // push new scene name login, make sure the scene prefab exist with the same name such as assets/resources/scenes/login.prefab notice the scene path for load prefab later
      ...
    }
}

// in login.ts
// extends GameScene is must for OOP purpose and abstract methods
@ccclass
export default class Login extends GameScene {
  // abstract methods
  ...
  /**
   * Call when scene leave this will call after onInvisible and onDisable
   */
  onLeave() {
  }

  /**
   * call before opacity = 255 or active = true
   */
  onEnter() {
  }

  /**
   * Call when opacity = 255 usually when finish some effect or just hidden
   */
  onVisible() {
  }

  /**
   * Oposite of onVisible
   */
  onInvisible() {
  }

  /**
   * When clear stack/forceReload the scene
   */
  onCleanup() {
  }
  ...
}
```

### Websocket

For current state only websocket is implemented for the framework, HTTP Request will be added later.
#### usage

For register a listen in every game scene must override `registerEvents()` from GameScene

```typescript
@ccclass
export default class Login extends GameScene {
  // will got called auto on every gameScene onLoad()
  registerEvents() {
    ACE.gameSocket.on(SERVER_EVENT.ASSET_DATA, (data) => {
      ...
    });
  }
}
```

For emit an event you can either create static class such as `assets\scripts\login\login-service.ts`
```typescript
static logout() {
  // for force logout
  if (ACE.sceneManager.currentScene.name != 'login') {
    ACE.sceneManager.pushScene("login");
  }
  ACE.gameSocket.emit(SERVER_EVENT.LOGOUT, {});
}
```
Or simply call `ACE.gameSocket.emit("event-name", passdata);`, rule of thumbs is you should create a method of it if the logic for parameters are over 5 lines of code.

