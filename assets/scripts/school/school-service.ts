import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";
import AccountManager from "../managers/account-manager";
import SchoolManager from "../managers/school-manager";

export enum NotiType {
  NORMAL = "normal",
  IMPROVEMENT = "improvement",
  HOMEWORK = "homework",
  NAPLAN = "naplan",
}

export class NotiObject {
  notificationText: string;
  type: NotiType;

  constructor(text, type) {
    this.notificationText = text;
    this.type = type;
  }
}

export default class SchoolService {
  static enrolments: any;
  static enrolmentId: number;
  static notiText: any;
  static notifications: string[] = [];
  static notificationObjects: NotiObject[] = [];
  static scene: string = "";
  static emotions: string = "";
  static rightAnswer: any = 0;
  static wrongAnswer: any = 0;
  static numberOfQaSubmitted: any = 1;
  static currentNotfication: NotiObject = null;

  static clear() {
    SchoolService.enrolments = {};
    SchoolService.enrolmentId = -1;
    SchoolService.notiText = "";
    SchoolService.notifications = [];
    SchoolService.notificationObjects = [];
    SchoolService.scene = "";
    SchoolService.emotions = "";
    SchoolService.rightAnswer = 0;
    SchoolService.wrongAnswer = 0;
    SchoolService.numberOfQaSubmitted = 1;
    SchoolManager.getInstance().clear();
  }

  static get qaScene(): string {
    switch (SchoolService.scene) {
      case "practice":
        return "practiseCenter";
      case "improvement":
        return "improvement";
      case "homework":
        return "homework";
      case "naplan":
        return "naplan";
      case "focus":
        return "focusCenter";
      case "revision":
        return "revision";
      default:
        return "";
    }
    return "";
  }

  static enrolmentList() {
    AccountManager.getInstance().gettingSchoolData = true;
    ACE.dialogManager.showLoading();
    ACE.gameSocket.emit(SERVER_EVENT.ENROLMENT_LIST, {
    });
  }

  static getNaplanForMathHouse(enrolmentId) {
    ACE.gameSocket.emit(SERVER_EVENT.GET_NAPLAN_FOR_MATH_HOUSE, {
      enrolmentId,
    });
  }

  static getImprovementForMathHouse(enrolmentId) {
    ACE.gameSocket.emit(SERVER_EVENT.GET_IMPROVEMENT_FOR_MATH_HOUSE, {
      enrolmentId,
    });
  }

  static getHomeworkForMathHouse(enrolmentId) {
    ACE.gameSocket.emit(SERVER_EVENT.GET_HOMEWORK_FOR_MATH_HOUSE, {
      enrolmentId,
    });
  }

  static getFocusForMathHouse(enrolmentId) {
    ACE.gameSocket.emit(SERVER_EVENT.GET_FOCUS_FOR_MATH_HOUSE, {
      enrolmentId,
    });
  }

  static getRevisionForMathHouse(enrolmentId) {
    ACE.gameSocket.emit(SERVER_EVENT.GET_REVISION_FOR_MATH_HOUSE, {
      enrolmentId,
    });
  }

  static getAssignmentForMathHouse(enrolmentId) {
    ACE.gameSocket.emit(SERVER_EVENT.GET_ASSIGNMENT_FOR_MATH_HOUSE, {
      enrolmentId,
    });
  }

  static loadTopics(strengthId) {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_TOPIC, {
      strengthId,
    });
  }

  static loadConceptForPlayNow(topicId) {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CONCEPT_FOR_PLAYNOW, {
      topicId,
    });
  }

  static loadConceptFirstPage(topicId) {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CONCEPT_FIRST_PAGE, {
      topicId,
    });
  }

  static loadConceptNextPage(topicId) {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CONCEPT_NEXT_PAGE, {
      topicId,
    });
  }

  static loadConceptPreviousPage(topicId) {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_CONCEPT_PREVIOUS_PAGE, {
      topicId,
    });
  }

  static submitAnswerPractise(answer) {
    ACE.gameSocket.emit(SERVER_EVENT.SUBMIT_ANSWER_PRACTISE, {
      answer,
    });
  }

  static loadImprovement() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_IMPROVEMENT, {});
  }

  static loadImprovementFirstPage() {
    ACE.gameSocket.emit(SERVER_EVENT.IMPROVEMENT_FIRST_PAGE, {
    });
  }

  static loadImprovementNextPage() {
    ACE.gameSocket.emit(SERVER_EVENT.IMPROVEMENT_NEXT_PAGE, {});
  }

  static loadImprovementPreviousPage() {
    ACE.gameSocket.emit(SERVER_EVENT.IMPROVEMENT_PREVIOUS_PAGE, {});
  }

  static loadHomework() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_HOMEWORK, {
    });
  }

  static loadNaplan() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_NAPLAN, {
    });
  }

  static loadFocusCenter() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_FOCUS_CENTER, {});
  }

  static loadFocusFirstPage() {
    ACE.gameSocket.emit(SERVER_EVENT.FOCUS_FIRST_PAGE, {
    });
  }

  static loadFocusNextPage() {
    ACE.gameSocket.emit(SERVER_EVENT.FOCUS_NEXT_PAGE, {});
  }

  static loadFocusPreviousPage() {
    ACE.gameSocket.emit(SERVER_EVENT.FOCUS_PREVIOUS_PAGE, {});
  }

  static loadRevision() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_REVISION, {
    });
  }

  static loadAssignment() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_ASSIGNMENT, {
    });
  }
}
