const { ccclass } = cc._decorator;

@ccclass
export default class StudentRoomItemGroupL1 {
  ID: number;
  Name: string;
  Description: string;
  RoomID: number;
  UrlImage: string;
}
