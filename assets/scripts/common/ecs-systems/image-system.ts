import System from "../../core/ecs/system";
import Entity from "../../core/ecs/entity";
import ImageComponent, { IImageComponent } from "../ecs-components/image-component";
import PositionComponent, { IPositionComponent } from "../ecs-components/position-component";
import EntityManager from "../../core/ecs/entity-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ImageSystem extends System {
  _filter = ["PositionComponent", "ImageComponent"];

  onEntitiesUpdate(entity: Entity) {
    // cc.log("ImageSystem", entity._uuidv4);
    const { keyImage, linkImage, scale } = entity.getECSComponent(ImageComponent)._data as IImageComponent;
    const { position, size, anchor } = entity.getECSComponent(PositionComponent)._data as IPositionComponent;
    cc.loader.load(`http://starmathsonline.s3.amazonaws.com/${linkImage}`, (error, texture) => {
      let spriteNode = new cc.Node();
      spriteNode.width = size.width != 0 ? size.width : 50;
      spriteNode.height = size.height != 0 ? size.height : 50;
      const sprite = spriteNode.addComponent(cc.Sprite);
      sprite.spriteFrame = new cc.SpriteFrame(texture);
      spriteNode.anchorX = anchor.x;
      spriteNode.anchorY = anchor.y == 0 ? 1 : anchor.y;
      if (scale) {
        spriteNode.width *= scale;
        spriteNode.height *= scale;
      }
      entity.node.width = spriteNode.width;
      entity.node.height = spriteNode.height;
      entity.node.addChild(spriteNode, 5, "sprite-node");
      entity.node.sortAllChildren();
    });
  }
}
