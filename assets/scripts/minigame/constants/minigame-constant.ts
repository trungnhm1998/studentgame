enum MiniGameType {
  PET_TAPPING,
  AMAZING_ADDITION,
  SUPPER_SUBTRACTION,
  MAGNIFICIENT_MULTIPLICATION,
  DYNAMIC_DIVISIONS,
  SUPER_MIX,
}

enum OperatorType {
  ADD,
  SUB,
  MULTI,
  DIV,
}

enum PositionObject {
  UP,
  DOWN,
}

const MathsServerAPI = {
  LOAD_MINIMAP: "monkeyMathsLoadMiniMap",
  SAVE_MONKEY_MATHS: "monkeyMathsGameOver",
  CLAIM_REWARD: "todoDefine",
};

const MiniGameTypeKey = {
  PET_TAPPING: "PetTapping",
  AMAZING_ADDITION: "AmazingAddition",
  SUPPER_SUBTRACTION: "SuperSubtraction",
  MAGNIFICIENT_MULTIPLICATION: "MagnificientMultiplication",
  DYNAMIC_DIVISIONS: "DynamicDivisions",
  SUPER_MIX: "SuperMix",
};

export { MiniGameType, OperatorType, PositionObject, MathsServerAPI, MiniGameTypeKey };
