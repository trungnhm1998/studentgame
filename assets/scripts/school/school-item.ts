import NodeUtils from "../core/utils/node-utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SchoolItem extends cc.Component {
  bellNode: cc.Node = null; // the notification bell
  hoverBackground: cc.Node = null;
  hoverSpriteFrame: cc.SpriteFrame = null; // assessment special case
  spriteNode: cc.Node = null;
  disabledSpriteFrame: cc.SpriteFrame = null;
  normalSpriteFrame: cc.SpriteFrame = null;

  isActive = false;
  isInside = false;

  callback: Function = null;

  onLoad() {
    this.bellNode = NodeUtils.findNodeByName(this.node, 'bell');
    this.hoverBackground = NodeUtils.findNodeByName(this.node, 'hover');
    this.spriteNode = NodeUtils.findNodeByName(this.node, 'sprite');
  }

  init(normalSpriteFrame: cc.SpriteFrame, disabledSpriteFrame: cc.SpriteFrame, isAssessment = false) {
    this.normalSpriteFrame = normalSpriteFrame;
    this.disabledSpriteFrame = disabledSpriteFrame;
    this.setNotiState(true);
    this.node.on(cc.Node.EventType.TOUCH_START, () => {
      this.isInside = true;
      if (this.hoverSpriteFrame && isAssessment) {
        this.spriteNode.getComponent(cc.Sprite).spriteFrame = this.hoverSpriteFrame;
      }
    })

    this.node.on(cc.Node.EventType.TOUCH_END, () => {
      if (this.isInside) {
        this.isInside = false;
        cc.log('Touch inside');
        if (this.callback && this.isActive) {
          this.spriteNode.getComponent(cc.Sprite).spriteFrame = this.normalSpriteFrame;
          this.callback(this.node.name);
        }
      }
    })

    this.node.on(cc.Node.EventType.TOUCH_CANCEL, () => {
      this.isInside = false;
      if (this.isActive)
        this.spriteNode.getComponent(cc.Sprite).spriteFrame = this.normalSpriteFrame;
    })

    this.node.on(cc.Node.EventType.MOUSE_ENTER, () => {
      this.isInside = true;
      if (this.hoverSpriteFrame && isAssessment) {
        this.spriteNode.getComponent(cc.Sprite).spriteFrame = this.hoverSpriteFrame;
      }
      this.hoverBackground.active = true;
    })

    this.node.on(cc.Node.EventType.MOUSE_LEAVE, () => {
      this.isInside = false;
      if (this.isActive)
        this.spriteNode.getComponent(cc.Sprite).spriteFrame = this.normalSpriteFrame;
      this.hoverBackground.active = false;
    })
  }

  setState(isActive = true) {
    this.isActive = isActive;
    if (!this.spriteNode) {
      cc.log('SchoolItem::setState sprite node not found');
      return;
    }

    let spriteComp = this.spriteNode.getComponent(cc.Sprite);
    if (isActive) {
      spriteComp.spriteFrame = this.normalSpriteFrame;
    } else {
      spriteComp.spriteFrame = this.disabledSpriteFrame;

    }
    // this.node.active = isActive;
    // base on isActive will show hide sprite
  }

  setNotiState(isShow = false) {
    this.bellNode.active = isShow;
    this.bellNode.stopAllActions();
    if (!isShow) return;
    this.bellNode.runAction(cc.sequence(
      cc.fadeIn(0.5),
      cc.fadeOut(0.5).easing(cc.easeCubicActionInOut()),
      cc.delayTime(0.5)
    ).repeatForever())
  }
}