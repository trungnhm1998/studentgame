import { IHomework } from "./../models/homework-model";
import { ITopic } from "./../school/topic";
export default class SchoolManager {
  static instance: SchoolManager = null;
  static getInstance() {
    if (!this.instance) {
      this.instance = new SchoolManager();
    }
    return this.instance;
  }

  listTopics: ITopic[];
  topicId: any;
  listConcepts: any;
  qaTemplateId: any;
  focusGroupQATemplateID: any;
  PractiseQATemplateID: any;
  homeworkData: IHomework;
  improvementData: any;
  naplanData: any;
  focusData: any;
  revisionData: any;
  assignmentData: any;
  roomName: any;
  isFirstPage: boolean = false;

  clear() {
    this.assignmentData = null;
    this.improvementData = null;
    this.homeworkData = null;
    this.naplanData = null;
    this.focusData = null;
    this.revisionData = null;
  }
}
