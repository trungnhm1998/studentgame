import { AssetData } from "./interfaces/server-response";

/**
 * This class is project dependence only
 */

const DIRECTORY_NAME = "_assets";

export default class ResourceManager {
  static instance: ResourceManager;
  static getInstance(): ResourceManager {
    if (!ResourceManager.instance) {
      ResourceManager.instance = new ResourceManager();
    }
    return ResourceManager.instance;
  }
  loadedImages: any;
  storagePath: string;
  assetsManager: any;
  assets: AssetData[] = [];

  constructor() {
    if (CC_JSB && cc.sys.isNative) {
      this.storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : "/") + DIRECTORY_NAME);
      cc.log("this.storagePath", this.storagePath);
      this.assetsManager = new jsb.AssetsManager("", this.storagePath);
      const test: cc.Texture2D = null;
    }
  }

  saveStorage(key: string, data: any) {
    cc.sys.localStorage.setItem(key, JSON.stringify(data));
  }

  getStorage(key: string): any {
    const data = JSON.parse(cc.sys.localStorage.getItem(key));
    return data;
  }

  removeStorage(key: string) {
    cc.sys.localStorage.removeItem(key);
  }

  setRemoteImage(sprite: cc.Sprite, imageUrl: string) {
    return new Promise((resolve, reject) => {
      if (this.loadedImages[imageUrl]) {
        sprite.spriteFrame = this.loadedImages[imageUrl];
        resolve();
      } else if (cc.sys.isBrowser) {
        cc.loader.load(imageUrl, (error, texture) => {
          if (typeof texture == "string" || texture == null) {
            // TODO: To implement
            // load image from base64 for small image such as avatar
          }
        });
      }
    });
  }

  // interfaces for overload
  loadRemoteRes(resources: string | string[] | { uuid?: string, url?: string, type?: string }, progressCallback: (completedCount: number, totalCount: number, item: any) => void, completeCallback: Function | null): void;
  loadRemoteRes(resources: (string | string[] | { uuid?: string; url?: string; type?: string; }), completeCallback: Function | null): void;

  loadRemoteRes(resources: string | string[] | { uuid?: string, url?: string, type?: string }, progressCallback?: (completedCount: number, totalCount: number, item: any) => void, completeCallback?: Function | null) {
    this.webCallback(resources, progressCallback, completeCallback);
    // if (cc.sys.isNative) {
    //   // Windows, mobile
    //   // TODO: Implement nativeCallback to cache save to disk
    //   // this.nativeCallback(resources, progressCallback, completeCallback);
    // } else {
    //   // is web browser
    //   this.webCallback(resources, progressCallback, completeCallback);
    // }
  }

  /**
   * for wrap and save assets to disk and load from disk
   * @param resources
   * @param progressCallback
   * @param completeCallback (completedCount: number, totalCount: number, item: any) => void
   */
  private webCallback(resources: string | string[] | { uuid?: string, url?: string, type?: string }, progressCallback: Function, completeCallback: Function | null) {
    if (progressCallback && completeCallback) {
      cc.loader.load(resources, progressCallback.bind(this), (err, res) => this.cacheLoadWrapper(err, res, completeCallback.bind(this)));
    } else {
      cc.loader.load(resources, (err, res) => this.cacheLoadWrapper(err, res, progressCallback.bind(this)));
    }
  }

  /**
   * for wrap and save assets to disk and load from disk
   * @param resources
   * @param progressCallback
   * @param completeCallback (completedCount: number, totalCount: number, item: any) => void
   */
  private nativeCallback(resources: string | string[] | { uuid?: string, url?: string, type?: string }, progressCallback: Function, completeCallback: Function | null) {
    if (progressCallback && completeCallback) {
      // get from disk
      // should be a promise

      // not found
      cc.loader.load(resources, progressCallback.bind(this), (error, res) => {
        if (error) { return; }
        // save to disk
        this.saveResToDisk(res);
        completeCallback(error, res);
      });
    } else {
      cc.loader.load(resources, (error, res) => {
        if (error) { return; }
        // save to disk
        this.saveResToDisk(res);
        progressCallback(error, res);
      });
    }
  }

  private cacheLoadWrapper(error, res, callback: Function) {
    // cache here

    if (callback) {
      callback(error, res);
    }
  }

  private saveResToDisk(res) {
    // cc.log(res.nativeUrl)
    // cc.log(res._native);
    cc.log("Success load resource on native platform, going to save to disk.");
    // cc.loader.Downloader
  }
}
