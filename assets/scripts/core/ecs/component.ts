const { ccclass, property } = cc._decorator;

export interface IComponent {
  name?: string;
}

@ccclass
export default class Component extends cc.Component {
  _c_name: string = "";
  _data: IComponent;
  /**
   * @optional
   * init component with data
   * @param data Should be a object data to associate with the member
   */
  init<T extends IComponent>(data: T) {
    this._data = data;
  }

  getData<T extends IComponent>(): T {
    return this._data as T;
  }
}
