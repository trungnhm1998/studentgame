import Component, { IComponent } from "../../core/ecs/component";
import Entity from "../../core/ecs/entity";

const { ccclass, property } = cc._decorator;

export interface IDragComponent extends IComponent {
  touchId?: number;
  originalParent?: cc.Node;
  dragLayer?: cc.Node;
  originalPosition?: cc.Vec2;
  dropzone?: Entity; // should be a drop zone entity
  type: string;
  value: string;
  multiple: boolean;
  toggle: boolean;
  name: string;
  resultMode: string;
}

@ccclass
export default class DragComponent extends Component {
  _c_name: string = "DragComponent";
  _data: IDragComponent;
}
