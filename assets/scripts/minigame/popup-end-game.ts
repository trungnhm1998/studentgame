import { ACE } from "../core/global-info";
import MinigameController from "./minigame_controller";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupEndGame extends cc.Component {
  @property(cc.Node)
  backgroundWinSprite: cc.Node = null;
  @property(cc.Node)
  backgroundLoseSprite: cc.Node = null;
  @property(cc.Animation)
  animOpenPopup: cc.Animation = null;
  @property(cc.Label)
  levelLabelLose: cc.Label = null;
  @property(cc.Label)
  stageLabelLose: cc.Label = null;
  @property(cc.Label)
  levelLabelWin: cc.Label = null;
  @property(cc.Label)
  stageLabelWin: cc.Label = null;
  @property(cc.Label)
  distanceTravelledLabel: cc.Label = null;
  @property(cc.Label)
  targetDistanceLabel: cc.Label = null;
  @property(cc.Label)
  cointCollectedLabel: cc.Label = null;
  @property(cc.Label)
  correctedLabel: cc.Label = null;
  @property(cc.Label)
  incorrectLabel: cc.Label = null;

  @property(cc.Node)
  goHomeNode: cc.Node = null;
  @property(cc.Node)
  tryAgainNode: cc.Node = null;
  @property(cc.Node)
  nextStageNode: cc.Node = null;
  @property(sp.Skeleton)
  dataSprite: sp.Skeleton = null;

  _stringAnim: string = "";

  onEnable() {
    this.dataSprite.setAnimation(0, "Idle", true);
  }

  showEndGame(isWin: boolean,
              travelled: number, target: number,
              coin: number, corrected: number,
              incorrect: number) {

    this.backgroundWinSprite.active = isWin;
    this.backgroundLoseSprite.active = !isWin;
    this.levelLabelWin.node.getParent().active = isWin;
    this.levelLabelLose.node.getParent().active = !isWin;

    if (isWin) {
      this.distanceTravelledLabel.node.parent.active = false;
      this.tryAgainNode.active = false;
      this.nextStageNode.active = true;
      this._stringAnim = "Win";

    } else {
      this.distanceTravelledLabel.node.parent.active = true;
      this.tryAgainNode.active = true;
      this.nextStageNode.active = false;
      this._stringAnim = "Lose";
    }
    this.levelLabelWin.string = "Level: " + MinigameController.getInstance().getLevelSelected();
    this.stageLabelWin.string = "Stage: " + (MinigameController.getInstance().getStageSelected() + 1);
    this.levelLabelLose.string = "Level: " + MinigameController.getInstance().getLevelSelected();
    this.stageLabelLose.string = "Stage: " + (MinigameController.getInstance().getStageSelected() + 1);
    this.distanceTravelledLabel.string = "" + travelled.toString() + "";
    this.targetDistanceLabel.string = "" + target.toString() + "";
    this.cointCollectedLabel.string = "" + coin.toString();
    this.correctedLabel.string = "" + corrected.toString();
    this.incorrectLabel.string = "" + incorrect.toString();

    this.node.active = true;
    this.animOpenPopup.play();
  }

  update(dt) {
    if (this.dataSprite._isOnLoadCalled && this._stringAnim != "") {
      this.dataSprite.setAnimation(0, this._stringAnim, true);
      this._stringAnim = "";
    }
  }

  backBtnOnClicked() {
    ACE.sceneManager.pushScene("select-level");
  }

  tryAgainBtnOnClicked() {
    ACE.sceneManager.pushScene("game-play");
  }

  nexStageBtnOnClicked() {
    // next stage
    MinigameController.getInstance().nextStage();
    ACE.sceneManager.pushScene("game-play");
  }

}
