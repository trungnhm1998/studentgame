import {PositionObject} from "../constants/minigame-constant";
import GPController from "../gameplay-controller";
import IngameLogic from "./ingame-logic";
import { MAX_SPEED } from "./ingame-logic";
import { Config } from "../../config";
import ItemObject from "./item-object";

const {ccclass, property} = cc._decorator;

const NUMBER_QUESTION_CHECK_SPEED = 4; // correct is 4 , will edit later

export const PET_ANIM = {
  NONE : "none",
  FLYING_DOWN : "Flying_Down",
  FLYING_HIT : "Flying_Hit",
  FLYING_PRE_DOWN: "Flying_Pre_Down",
  FLYING_PRE_UP : "Flying_Pre_Up",
  FLYING_UP : "Flying_Up",
  IDLE: "Idle",
  FLYING_IDLE: "Idle_Flying",
  FLYING_FAST: "Idle_Flying_Fast",
  LOSE : "Lose",
  WIN : "Win",
};

const NUMBER_DIFFICULT_FAST = 3;

@ccclass
export default class PlayerBee extends cc.Component {

  @property(cc.CircleCollider)
  collider: cc.CircleCollider = null;

  @property(sp.Skeleton)
  dataSprite: sp.Skeleton = null;

  @property
  posY_Up = 0;

  @property
  posY_Down = 0;

  heightCenterY = 0;

  postionStatus: PositionObject = PositionObject.UP;

  currentPosY = 0;

  _countDownQuestionPass = 0;
  _countDownCorrect = 0;

  wasColliderObstacle = false;

  needPauseMoving = false;
  private _countDownPauseEffect = 0;

  reset() {
    // anim
    this.SetState(PET_ANIM.IDLE);

    // position init
    this.currentPosY = this.posY_Up;
    this.node.setPosition(this.node.position.x, this.currentPosY);
    this.postionStatus = PositionObject.UP;

    // question
    this._countDownQuestionPass = 0;
    this._countDownCorrect = 0;
    this._countDownPauseEffect = 0;
  }

  onLoad() {
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
  }

  CallBackFunc(test: any) {
    cc.log("target.dataSprite.animation :---------------------- " + test.dataSprite.name);
  }

  onEnable() {
    this.SetState(PET_ANIM.IDLE);
  }

  start() {
    // Open the collision manager, without this part statement you will not detect any collision.
    cc.director.getCollisionManager().enabled = true;

    // cc.director.getCollisionManager().enabledDebugDraw = true;

    // Calculate:
    const canvasSize = cc.view.getVisibleSize();
    cc.log("width: " + canvasSize.width + "\nheight: " + canvasSize.height,
    );

    // Init Pos of Bee start
    this.currentPosY = this.posY_Up;
    this.node.setPosition(this.node.position.x, this.currentPosY);
  }

  update(dt) {
    if (this.needPauseMoving) {
      this._countDownPauseEffect ++;

      if (this._countDownPauseEffect > 30) {
        this._countDownPauseEffect = 0;
        this.needPauseMoving = false;
      }
    }
  }

  onCollisionEnter(other, self) {
    if (IngameLogic.getInstance().isPause) { return; }

    cc.log("++++++++++++++++++++++++ HAVE ENTER Collision with:  " + other);
    const node: cc.Node = other.node;
    if (node) {
      cc.log("we can get the node :::::::: " + node.name);
      if (node.name == "Coin"
        || node.name == "Shield"
        || node.name == "Blood"
        || node.name == "Attack"
      ) {
        if (node.name == "Coin") {
          const itemObject = node.getComponent(ItemObject);

          if (itemObject.AnimationCollection != null) {
              // cc.error("Get And Show Animation .... ");
              itemObject.AnimationCollection.active = true;
              itemObject.AnimationCollection.getComponent(cc.Animation).play();
              itemObject.AnimationCollection.getComponent(cc.AudioSource).play();
          }

          const score = IngameLogic.getInstance().score + 10;
          IngameLogic.getInstance().UpdateScore(score);
          IngameLogic.getInstance().coinCollected++;
        }

        if (node.name == "Blood") {
          const live = IngameLogic.getInstance().live + 1;
          IngameLogic.getInstance().UpdateLive(live);
        }
        // node.active = false;
      }

      if (node.name == "obstacle1") {
        // Pause moving ...
        this.needPauseMoving = true;
        // Animation
        this.dataSprite.setAnimation(0, PET_ANIM.FLYING_HIT, false);
        if (IngameLogic.getInstance().difficult < NUMBER_DIFFICULT_FAST) {
            this.dataSprite.addAnimation(0, PET_ANIM.FLYING_IDLE, true);
        } else {
            this.dataSprite.addAnimation(0, PET_ANIM.FLYING_FAST, true);
        }
        // logic
        this.wasColliderObstacle = true;
        node.active = false;
        IngameLogic.getInstance().answerIncorrect++;
        const live = IngameLogic.getInstance().live - 1;
        IngameLogic.getInstance().UpdateLive(live);

        // When max speed, just need incorrect => level speed down
        if (IngameLogic.getInstance().difficult >= MAX_SPEED - 1) {
          IngameLogic.getInstance().difficult--;
          IngameLogic.getInstance().UpdateLevelupDifficult();
          this._countDownCorrect = 0;
          this._countDownQuestionPass = 0;
        }
      }

      if (node.name == "gatePass1") {
        // hide obstacle
        IngameLogic.getInstance().obtacle1.node.active = false;
        GPController.getInstance().spawnQuestion();

        // General logic
        IngameLogic.getInstance().updateLogicObject(this.postionStatus);
      }
      if (node.name == "gatePass0") {
        if (!this.wasColliderObstacle) {
          // Update question correct
          IngameLogic.getInstance().answerCorrected++;
          this._countDownCorrect++;

          // Add Score when pass:
          const score = IngameLogic.getInstance().score + 10;
          IngameLogic.getInstance().UpdateScore(score);

          // Add extra time ...
          IngameLogic.getInstance().timer -= 1.5 * 1000; // 1.5s
        }

        if (IngameLogic.getInstance().difficult < MAX_SPEED - 1) {
          // checking question pass and process
          if (this._countDownQuestionPass < NUMBER_QUESTION_CHECK_SPEED - 1) {
            this._countDownQuestionPass ++;
            cc.log("Sau : " + this._countDownQuestionPass + " cau hoi");
          } else {
            const Condition75percent = 3;
            const Condition50percent = 2;
            if (this._countDownCorrect > Condition75percent) {
              this.increaseSpeed(true);
            } else if (this._countDownCorrect <= Condition50percent) {
              this.increaseSpeed(false);
            }
            this._countDownCorrect = 0;
            this._countDownQuestionPass = 0;
          }
        }
      }

      if (node.name == "gateStart1") {
        this.wasColliderObstacle = false;
        // Update question
        IngameLogic.getInstance().obtacle1.updateQuestion();
        IngameLogic.getInstance().obtacle1.node.active = true;
      }
    }
  }

  UpdateLevelWin() {
    // IngameLogic.getInstance()._isPause = true;
    // Animation PEt
    this.SetState(PET_ANIM.IDLE);
    IngameLogic.getInstance().GameOver(true);
  }

  UpdateLevelLose() {
    // IngameLogic.getInstance()._isPause = true;
    // Animation PEt
    this.SetState(PET_ANIM.LOSE);
    IngameLogic.getInstance().GameOver(false);
  }

  onKeyDown(event) {
    if (Config.debug) {
      if (IngameLogic.getInstance().isPause) { return; }

      const macro = cc.macro;

      switch (event.keyCode) {
        case macro.KEY.w:
        case macro.KEY.up:
          cc.log("Bee up ");
          this.onSetMovingUp();
          break;
        case macro.KEY.s:
        case macro.KEY.down:
          cc.log("Bee Down");
          this.onSetMovingDown();
          break;
        case macro.KEY.r:
          cc.log("Bee Down");
          IngameLogic.getInstance().startLevel();
          break;
        case macro.KEY.a:
          cc.log("SPEED DOWN ....");
          this.increaseSpeed(false);
          break;
        case macro.KEY.d:
          cc.log("SPEED UP ....");
          this.increaseSpeed(true);
          break;
        case macro.KEY.y:
          this.UpdateLevelWin();
          break;
        case macro.KEY.l:
          IngameLogic.getInstance().targetTime = 3 * 1000;
          // this.UpdateLevelLose();
          break;
      }
    }
  }

  increaseSpeed(increase: boolean) {
    if (increase) {
      if (IngameLogic.getInstance().difficult < 3) {
        IngameLogic.getInstance().difficult++;
        cc.log("Tang toc do: ++ " + IngameLogic.getInstance().difficult);
      }
    } else {
      IngameLogic.getInstance().difficult--;
      if (IngameLogic.getInstance().difficult <= 0) { IngameLogic.getInstance().difficult = 0; }
      cc.log("Giam toc do: -------------- " + IngameLogic.getInstance().difficult);
    }

    if (IngameLogic.getInstance().difficult > NUMBER_DIFFICULT_FAST) {
      this.dataSprite.setAnimation(0, PET_ANIM.FLYING_FAST, true);
    } else {
      this.dataSprite.setAnimation(0, PET_ANIM.FLYING_IDLE, true);
    }

    IngameLogic.getInstance().UpdateLevelupDifficult();
  }

  doActionAnswerCorrect() {
    if (IngameLogic.getInstance().isPause) { return; }

    if (this.postionStatus == PositionObject.UP) {
      this.onSetMovingDown();
    } else {
      this.onSetMovingUp();
    }
  }

  onSetMovingUp() {
    if (this.postionStatus == PositionObject.UP) { return; }
    this.currentPosY = this.posY_Up;
    // this.node.setPosition(this.node.position.x, this.currentPosY);
    this.postionStatus = PositionObject.UP;
    // Animation
    this.dataSprite.setAnimation(0, PET_ANIM.FLYING_PRE_UP, false);
    this.dataSprite.addAnimation(0, PET_ANIM.FLYING_UP, true);

    const moveto = new cc.Vec2(this.node.position.x, this.currentPosY);

    const finish = cc.callFunc(this.CallBackMovingFinish, this);
    const seq = cc.sequence(cc.moveTo(0.5, moveto , 0), finish);
    this.node.runAction(seq);
  }

  onSetMovingDown() {
    if (this.postionStatus == PositionObject.DOWN) { return; }
    this.currentPosY = this.posY_Down;
    // this.node.setPosition(this.node.position.x, this.currentPosY);
    this.postionStatus = PositionObject.DOWN;
    // Animation
    this.dataSprite.setAnimation(0, PET_ANIM.FLYING_PRE_DOWN, false);
    this.dataSprite.addAnimation(0, PET_ANIM.FLYING_DOWN, true);

    const moveto = new cc.Vec2(this.node.position.x, this.currentPosY);

    const finish = cc.callFunc(this.CallBackMovingFinish, this);
    const seq = cc.sequence(cc.moveTo(0.5, moveto , 0), finish);
    this.node.runAction(seq);
  }

  // For Animation
  CallBackMovingFinish() {
      cc.log("Finish .......");
      if (IngameLogic.getInstance().difficult < NUMBER_DIFFICULT_FAST) {
          this.dataSprite.setAnimation(0, PET_ANIM.FLYING_IDLE, true);
      } else {
          this.dataSprite.setAnimation(0, PET_ANIM.FLYING_FAST, true);
      }
  }

  SetState(state: string) {
    switch (state) {
      case PET_ANIM.FLYING_IDLE:
        if (IngameLogic.getInstance().difficult > NUMBER_DIFFICULT_FAST) {
          this.dataSprite.setAnimation(0, PET_ANIM.FLYING_FAST, true);
        } else {
          this.dataSprite.setAnimation(0, PET_ANIM.FLYING_IDLE, true);
        }
        break;
      case PET_ANIM.IDLE:
        this.dataSprite.setAnimation(0, PET_ANIM.IDLE, true);
        break;
      case PET_ANIM.LOSE:
        this.dataSprite.setAnimation(0, PET_ANIM.LOSE, true);
        break;
    }
  }
}
