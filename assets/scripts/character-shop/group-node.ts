import GroupShop from "./group-shop";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GroupNode extends cc.Component {
  @property(cc.Label)
  groupName: cc.Label = null;
  @property(cc.Sprite)
  groupIcon: cc.Sprite = null;
  @property(cc.Node)
  groupBackground: cc.Node = null;
  @property(cc.Node)
  textColor: cc.Node = null;

  @property
  backgroundNormalColor: cc.Color = cc.Color.GRAY;
  @property
  backgroundActiveColor: cc.Color = cc.Color.CYAN;
  @property
  textNormalColor: cc.Color = cc.Color.BLACK;
  @property
  textActiveColor: cc.Color = cc.Color.WHITE;
  _groupID: number = 0;
  _groupShop: GroupShop;
  _isGroupActived: boolean;

  initNode(
    nodeInfo: string,
    iconSF: cc.SpriteFrame,
    groupID: number,
    groupShop,
  ) {
    this.setGroupState(false);
    this.groupName.string = nodeInfo;
    this.groupIcon.spriteFrame = iconSF;
    this._groupID = groupID;
    this._groupShop = groupShop;
    // this.registerHoverEvents();
  }

  setGroupState(isActive: boolean) {
    this._isGroupActived = isActive;
    if (!isActive) {
      this.textColor.color = this.textNormalColor;
      this.groupBackground.color = this.backgroundNormalColor;
      this.groupBackground.active = false;
      this.groupIcon.enabled = false;
    } else {
      this.textColor.color = this.textActiveColor;
      this.groupBackground.color = this.backgroundActiveColor;
      this.groupBackground.active = true;
      this.groupIcon.enabled = true;
    }
  }

  onActionSelectEvent() {
    // todo call select event.
    this.setGroupState(true);
    this._groupShop.groupItemOnSellected(this._groupID);
  }

  registerHoverEvents() {
    var self = this;
    this.node.on(
      cc.Node.EventType.MOUSE_ENTER,
      event => {
        if(!self._isGroupActived){
          self.groupBackground.color = self.backgroundActiveColor;
          self.textColor.color = self.textActiveColor;
          self.groupBackground.active = true;
        }
      },
      this
    );

    this.node.on(
      cc.Node.EventType.MOUSE_LEAVE,
      event => {
        {
          if(!self._isGroupActived){
            self.groupBackground.color = self.backgroundNormalColor;
            self.textColor.color = self.textNormalColor;
            self.groupBackground.active = false;
          }
        }
      },
      this
    );
  }
}
