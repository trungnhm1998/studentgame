import QaNode from "./qa-node";
import { IDesign } from "../models/qa";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QaRect extends QaNode {
  onLoad() {

  }

  init(qaDesign: IDesign) {
    super.init(qaDesign);
  }
}