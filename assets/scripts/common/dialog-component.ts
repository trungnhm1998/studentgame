import { GAME_EVENT } from "../core/constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DialogComponent extends cc.Component {
  dynamic: boolean = false;
  onLoad() {}

  onShow() {}

  onHide() {}

  closeDialog(cleanup = false) {
    if (this.node.active) {
      this.node.active = false;
      this.node.emit(GAME_EVENT.CLOSE_DIALOG);
      this.onHide();
    }

    if (cleanup) {
      this.node.removeFromParent();
    }
  }
}
