const { ccclass, property } = cc._decorator;

@ccclass
export default class AnimationGreenArrow extends cc.Component {
  @property([cc.SpriteFrame])
  arrayIcon: cc.SpriteFrame[] = [];
}
