import { ACE } from "../../core/global-info";
import { IFriend } from "../../models/friend";
import AccountManager from "../../managers/account-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Friend extends cc.Component {
  @property(cc.Button)
  sendGiftButton: cc.Button = null;

  @property(cc.Button)
  visitHouseButton: cc.Button = null;

  @property(cc.Label)
  nameLabel: cc.Label = null;

  iFriend: IFriend;

  sendGiftCallback: (friend: IFriend) => any = null;
  visitHouseCallback: (friend: IFriend) => any = null;

  init(iFriend: IFriend, sendGiftCallback: (friend: IFriend) => any, visitHouseCallback: (friend: IFriend) => any) {
    this.iFriend = iFriend;
    this.sendGiftCallback = sendGiftCallback;
    this.visitHouseCallback = visitHouseCallback;
    this.nameLabel.string = ACE.string.limitNameChar(this.iFriend.FullName);
    if (this.iFriend.isSentGift) {
      this.disableSendGift();
    }
  }

  sendGiftPressed() {
    if (this.sendGiftCallback) {
      if (AccountManager.getInstance().numberGift > 0) {
        this.disableSendGift();
      }
      this.sendGiftCallback(this.iFriend);
    }
  }

  visitHousePressed() {
    if (this.visitHouseCallback) {
      // TODO: Pass agrs
      this.visitHouseCallback(this.iFriend);
    }
  }

  disableSendGift() {
    this.sendGiftButton.interactable = false;
  }

  enableSendGift() {
    this.sendGiftButton.interactable = true;
  }
}
