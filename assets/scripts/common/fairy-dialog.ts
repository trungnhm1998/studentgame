import { Random } from "../core/utils/random-utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class FairyDialog extends cc.Component {
  @property(cc.Label)
  message: cc.Label = null;

  @property(cc.Button)
  nextButton: cc.Button = null;

  @property(cc.Float)
  showMessageInterval: number = 0.05;

  _messages: string[] = [];
  private _finishCallback: Function;
  private _messagesLength: number = -1;
  private _messagesIndex: number = -1;
  private _displayingMessage: string = "";
  private _messageIndex: number = -1;
  private stopAction = false;
  private _readingAction: cc.ActionInterval;

  onLoad() {
    this._readingAction = this.readingAction();
  }

  public set messages(messages: string[]) {
    this._displayingMessage = "";
    this._messageIndex = -1;
    this._messagesIndex = -1;
    this._messagesLength = messages.length;
    this._messages = messages;
  }

  update(dt) {
    if (this.stopAction) {
      this.stopAction = false;
      this.node.stopAction(this._readingAction);
      if (this._messagesIndex == this._messages.length - 1) {
        this.hide(true, 2.5);
      }
    }
  }

  hide(withAnim = false, timeout = 0) {
    this.messages = [];
    if (!withAnim) {
      this.node.active = false;
    } else {
      this.node.runAction(cc.sequence(
        cc.delayTime(timeout),
        cc.fadeOut(0.2),
        cc.callFunc(() => {
          this.node.active = false;
        }),
      ));
    }
  }

  show(messages?: string[]) {
    this.node.stopAllActions();
    this.node.active = true;
    this.node.opacity = 255;
    if (messages) {
      this._messages = messages;
      this.nextMessage();
    }
  }

  readingAction(): cc.ActionInterval {
    return cc.repeatForever(cc.sequence(
      cc.callFunc(() => {
        if (this._messageIndex < this._displayingMessage.length) {
          this.message.string = this._displayingMessage.substring(0, this._messageIndex);
          this._messageIndex++;
        } else {
          this.stopAction = true;
        }
      }),
      cc.delayTime(this.showMessageInterval),
    ));
  }

  onFinishTutorial() {
    if (this._finishCallback) {
      this._finishCallback();
    }
    this.hide(true);
  }

  nextMessage() {
    if (this._messageIndex != -1 && this._messageIndex < this._displayingMessage.length) {
      this.stopAction = true;
      this.message.string = this._displayingMessage;
      this._messageIndex = this._displayingMessage.length;
      return;
    }
    this._messageIndex = 0;
    this._messagesIndex++;
    this._displayingMessage = this._messages[this._messagesIndex];
    if (!this._displayingMessage) {
      this.onFinishTutorial();
      return;
    }
    this.node.runAction(this._readingAction);
  }
}
