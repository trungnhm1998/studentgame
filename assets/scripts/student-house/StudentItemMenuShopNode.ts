import StudentRoomItemGroupL1 from "./Model/StudentRoomItemGroupL1";
import StudentHouseManager from "./StudentHouseManager";
import StudentShopManager from "./StudentRoomShopManage";
const { ccclass, property } = cc._decorator;

@ccclass
export default class StudentItemMenuShopNode extends cc.Component {
  @property(cc.Node)
  public BG: cc.Node = null;

  @property(cc.Node)
  public label: cc.Node = null;

  public studentRoomItemGroupL1: StudentRoomItemGroupL1 = null;

  init(item: StudentRoomItemGroupL1) {
    if (item) {
      this.studentRoomItemGroupL1 = item;
      this.label.getComponent(cc.Label).string = item.Name;
    }
  }

  buttonAction(event, actionName) {
    switch (actionName) {
      case "selectTab":
        StudentShopManager.getInstance().getTabFollowMenuItem(this.studentRoomItemGroupL1.ID);
        StudentHouseManager.getInstance().ShopSelectTab(this.studentRoomItemGroupL1.ID)
        break;
    }
  }
}
