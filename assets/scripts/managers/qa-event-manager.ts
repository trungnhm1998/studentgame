export default class QaEventManager {
  static _instance: QaEventManager = null;

  // using constant/enum instead of string
  currentEvent: string = '';
  currentTarget: cc.Node = null;

  static getInstance(): QaEventManager {
    if (this._instance == null)
      this._instance = new QaEventManager();

    return this._instance;
  }
}