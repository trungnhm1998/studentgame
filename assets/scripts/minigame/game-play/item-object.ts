import { PositionObject } from "../constants/minigame-constant";
import IngameLogic from "./ingame-logic";
const { ccclass, property } = cc._decorator;

enum ItemType {
  POWERUP_ATTACK = 0,
  POWERUP_BLOOD = 1,
  POWERUP_SHIELD = 2,
  POWERUP_COIN = 3,
}

@ccclass
export default class ItemObject extends cc.Component {
  @property({ type: cc.Enum(ItemType) })
  itemType: ItemType = ItemType.POWERUP_COIN;

  @property(cc.Node)
  AnimationCollection: cc.Node = null;

  postionStatus: PositionObject = PositionObject.UP;

  start() {
    cc.director.getCollisionManager().enabled = true;
  }

  updateLogicObject(postionStatus: PositionObject) {
    this.postionStatus = postionStatus;
    let posY = 0;
    if (postionStatus == PositionObject.UP) {
      posY = IngameLogic.getInstance().player.posY_Up;
    } else {
      posY = IngameLogic.getInstance().player.posY_Down;
    }
    this.node.setPosition(this.node.position.x, posY);
  }
}
