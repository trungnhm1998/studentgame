import Component, { IComponent } from "../../core/ecs/component";

const { ccclass, property } = cc._decorator;

export interface IDropComponent extends IComponent {
  type?: string;
  value?: string;
  multiple?: boolean;
  toggle?: boolean;
  name?: string;
  resultMode?: string;
  touchId?: number;
  size: cc.Size;
}

@ccclass
export default class DropComponent extends Component {
  _c_name: string = "DropComponent";
  _data: IDropComponent;
}
