import DialogComponent from "../common/dialog-component";
import { ACE } from "../core/global-info";
import { NoticeDialogType } from "./notice-dialog";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadingDialog extends DialogComponent {
  @property(cc.Label)
  progress: cc.Label = null;

  @property(cc.Label)
  text: cc.Label = null;

  private onTimeout: any;
  private timeout: any;
  onLoad() {

  }

  onShow() {
    super.onShow();
    this.progress.node.active = false;
    this.text.node.active = false;
    this.onTimeout = null;
  }

  setProgress(progress: number) {
    this.progress.node.active = true;
    this.progress.string = "" + Math.floor(progress) + "%";
  }

  setText(text: string) {
    this.text.node.active = true;
    this.text.string = text;
  }

  setTimeoutCallback(onTimeout: any = null, timeout: any) {
    this.node.stopAllActions();
    if (onTimeout != null) {
      this.onTimeout = onTimeout;
    }
    this.timeout = timeout;

    if (this.timeout) {
      this.node.runAction(
        cc.sequence(
          cc.delayTime(this.timeout),
          cc.callFunc(() => {
            this.closeDialog();
            if (this.onTimeout != null) {
              this.onTimeout();
            } else {
              ACE.dialogManager.showNoticeDialog("Something wrong with the internet connection\nplease try again later." ,  NoticeDialogType.OK);
            }
          }),
        ),
      );
    }
  }

  closeDialog() {
    this.timeout = null;
    this.onTimeout = null;
    super.closeDialog();
  }
}
