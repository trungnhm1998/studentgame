import { IDesign } from "../models/qa";
import QaNode from "./qa-node";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QaLabel extends QaNode {
  @property(cc.Label)
  label: cc.Label = null;

  onLoad() {

  }

  init(qaDesign: IDesign) {
    super.init(qaDesign);
    const { objPrivate } = qaDesign;
    const { style, text } = objPrivate;
    const { font, fontWeight, fontSize, lineHeight, lineSpacing, backgroundColor, fill, align, boundsAlignH, boundsAlignV, stroke, strokeThickness, wordWrap, wordWrapWidth, maxLines } = style;
    cc.loader.loadRes(`fonts/${font}`, (err, loadedFont: cc.TTFFont) => {
      this.label.font = loadedFont;
    })
    this.label.fontSize = fontSize;
    this.label.string = text;
    this.label.lineHeight = fontSize;
  }
}