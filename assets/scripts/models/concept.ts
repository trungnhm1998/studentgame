export default interface IConcept {
  ID:                   number;
  Name:                 string;
  Description:          string;
  TopicID:              number;
  TopicDescription:     string;
  StrengthDescription:  string;
  PractiseQATemplateID: number;
  ConceptLevel:         number;
  NumberOfQa:           number;
  NumberBlock:          number;
  NumberCorrect:        number;
  NumberInCorrect:      number;
  Percentage:           number;
  SPI:                  number;
  TimeSpentQa:          number;
  TimeSpent:            number;
  Coins:                number;
  Stars:                number;
  ProgressStar:         number;
}