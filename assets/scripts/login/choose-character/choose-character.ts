import CharacterComponent, { IWearingGroup, ICharacterItem } from "./../../common/character-component";
import GameScene from "../../common/game-scene";
import { ACE } from "../../core/global-info";
import { SERVER_EVENT, DIALOG_TYPE } from "../../core/constant";
import { NoticeDialogType } from "../../dialogs/notice-dialog";
import AccountManager from "../../managers/account-manager";
import Boy from "../../common/boy";
import Girl from "../../common/girl";
import LoginService from "../login-service";
import DialogComponent from "../../common/dialog-component";
import NodeUtils from "../../core/utils/node-utils";
import AudioManager from "../../core/audio-manager";

const { ccclass, property } = cc._decorator;

const enum MenuType {
  CLOTHES = "clothes",
  SHOES = "shoes",
  HAIR = "hair",
  EYES = "face",
  SKIN = "skin",
}

const enum SceneState {
  TUTORIAL,
  GENDER,
  LOOKS,
}

@ccclass
export default class ChooseCharacter extends GameScene {

  @property(cc.Node)
  background: cc.Node = null;

  @property(cc.Node)
  boyOption: cc.Node = null;

  @property(cc.Node)
  girlOption: cc.Node = null;

  @property(cc.Node)
  genderBanner: cc.Node = null;

  @property(cc.Node)
  genderBannerEndPos: cc.Node = null;

  @property(cc.Node)
  boy: cc.Node = null;

  @property(cc.Node)
  girl: cc.Node = null;

  @property(cc.Node)
  accessoriesMenu: cc.Node = null;

  @property(cc.Node)
  looksMenu: cc.Node = null;

  @property(cc.Node)
  back: cc.Node = null;

  @property(cc.Node)
  confirmGender: cc.Node = null;

  @property(cc.Node)
  confirmCharacter: cc.Node = null;

  @property(cc.Node)
  confirmButtonEndPos: cc.Node = null;

  @property(cc.Node)
  showdownLayer: cc.Node = null;

  @property(CharacterComponent)
  character: CharacterComponent = null;

  @property(sp.Skeleton)
  ufo: sp.Skeleton = null;

  @property([cc.Node])
  ufoParticles: cc.Node[] = [];

  @property([cc.Node])
  choosingEffect: cc.Node[] = [];

  _hasShowOutfit = false;
  _currentState: SceneState;
  _boySprite: cc.Sprite = null;
  _girlSprite: cc.Sprite = null;
  _selectedGender: number = -1;
  _selectedGenderNode: cc.Node = null;

  onLoad() {
    super.onLoad();
    this._boySprite = this.boyOption.getComponent(cc.Sprite);
    this._girlSprite = this.girlOption.getComponent(cc.Sprite);
    const boyComponent = this.boy.getComponent(CharacterComponent);
    const girlComponent = this.girl.getComponent(CharacterComponent);
    boyComponent._onCharacterShow = () => {
      this.updateCharacterShadown(boyComponent);
    };
    girlComponent._onCharacterShow = () => {
      this.updateCharacterShadown(girlComponent);
    };
  }

  onResize() {
    super.onResize();
    this.genderBannerEndPos.getComponent(cc.Widget).updateAlignment();
    this.confirmButtonEndPos.getComponent(cc.Widget).updateAlignment();
  }

  onEnter() {
    ACE.dialogManager.showLoading();
    ACE.gameSocket.emit(SERVER_EVENT.GET_BASIC_BOY_WEARING_GROUP); // old API for rendering order got from server
    this.showdownLayer.active = false;
    this.showdownLayer.opacity = 0;
    this.ufoParticles.forEach((particle) => {
      particle.active = false;
    });
    this.boyOption.setScale(1);
    this.girlOption.setScale(1);
    this.boy.setScale(1);
    this.girl.setScale(1);
    this.boyOption.x = -560;
    this.girlOption.x = 560;
    this.boyOption.runAction(this.breathAction());
    this.girlOption.runAction(this.breathAction());
    this.accessoriesMenu.x = -cc.winSize.width;
    this.looksMenu.x = cc.winSize.width;
    this.confirmCharacter.y = -cc.winSize.height;
    this.genderBanner.y = this.genderBannerEndPos.y;
    this._currentState = SceneState.GENDER;
    this.boyOption.setScale(1);
    this.girlOption.setScale(1);

    this.confirmGender.y = this.confirmButtonEndPos.y;
    this.confirmGender.active = false;
    this.confirmCharacter.active = false;
    this.boy.active = false;
    this.girl.active = false;
    this.boy.x = -160;
    this.girl.x = 160;

    AudioManager.getInstance().playAudio("npc_appearing");
    AudioManager.getInstance().playAudio("star2");
    ACE.fairyDialog.show(["Before starting on your adventure, can you tell me a bit about yourself??"]);
  }

  breathAction() {
    return cc.repeatForever(cc.sequence(
      cc.scaleTo(0.6, 1.1).easing(cc.easeInOut(1.0)),
      cc.scaleTo(0.6, 1.0).easing(cc.easeInOut(1.0)),
    ));
  }

  onConfirmGender() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    if (this._selectedGender == -1) { return; }
    if (this._selectedGender == 1) {
      this._selectedGenderNode = this.boy;
      // boy
      this.girl.active = false;
    } else {
      this._selectedGenderNode = this.girl;
      this.boy.active = false;
    }

    this.moveNode(this.boyOption, -cc.winSize.width, 1, 0.3);
    this.moveNode(this.genderBanner, cc.winSize.height, 1, 0.3, null, false);
    this.moveNode(this.confirmGender, -cc.winSize.height, 1, 0.3, null, false);
    this.moveNode(this.girlOption, cc.winSize.width, 1, 0.3, () => {
      this.moveNodeIn(this.accessoriesMenu, -550);
      this.moveNodeIn(this.looksMenu, 550);
      this.moveNodeIn(this.confirmCharacter, this.confirmButtonEndPos.y, 0.3, false);
      this._currentState = SceneState.LOOKS;
      if (!this._hasShowOutfit) {
        this._hasShowOutfit = true;
        AudioManager.getInstance().playAudio("npc_appearing");
        AudioManager.getInstance().playAudio("star3");
        ACE.fairyDialog.show(["Great! Now let's choose how you look!!"]);
      }
    });

    ACE.gameSocket.emit(SERVER_EVENT.SELECT_GENDER, {
      gender: this._selectedGender + "",
    });
  }

  registerEvents() {
    this.wrapperEvent(SERVER_EVENT.GET_BASIC_PRODUCTS, (data) => {
      // for item selection
    });
    this.wrapperEvent(SERVER_EVENT.GET_BASIC_USING_ITEMS, (data: { boy: ICharacterItem[], girl: ICharacterItem[] }) => {
      this.boy.active = true;
      this.girl.active = true;

      const boyComponent = this.boy.getComponent(CharacterComponent);
      const girlComponent = this.girl.getComponent(CharacterComponent);
      boyComponent.setRenderItems(data.boy);
      girlComponent.setRenderItems(data.girl);
      ACE.dialogManager.hideLoading();
    });
    this.wrapperEvent(SERVER_EVENT.SELECT_GENDER, (data) => {
    });
    this.wrapperEvent(SERVER_EVENT.SELECT_NEXT, (data: { boy: ICharacterItem[], girl: ICharacterItem[] }) => {
      this._selectedGenderNode.getComponent(CharacterComponent).setRenderItems(this._selectedGender == 1 ? data.boy : data.girl);
    });
    this.wrapperEvent(SERVER_EVENT.SELECT_PREVIOUS, (data: { boy: ICharacterItem[], girl: ICharacterItem[] }) => {
      this._selectedGenderNode.getComponent(CharacterComponent).setRenderItems(this._selectedGender == 1 ? data.boy : data.girl);
    });
    this.wrapperEvent(SERVER_EVENT.GET_SELECTING_GENDER, (data) => {
    });

    this.wrapperEvent(SERVER_EVENT.GET_BASIC_GIRL_WEARING_GROUP, (data: { isSmall: boolean, group: IWearingGroup[] }) => {
      const girlComponent = this.girl.getComponent(Girl);
      // girlComponent.isSmall = data.isSmall;
      girlComponent.setRenderOrder(data.group);
      ACE.gameSocket.emit(SERVER_EVENT.GET_BASIC_USING_ITEMS); // default character item
    });

    this.wrapperEvent(SERVER_EVENT.GET_BASIC_BOY_WEARING_GROUP, (data: { isSmall: boolean, group: IWearingGroup[] }) => {
      const boyComponent = this.boy.getComponent(Boy);
      // boyComponent.isSmall = data.isSmall;
      boyComponent.setRenderOrder(data.group);
      ACE.gameSocket.emit(SERVER_EVENT.GET_BASIC_GIRL_WEARING_GROUP); // same GET_BASIC_BOY_WEARING_GROUP
    });

    this.wrapperEvent(SERVER_EVENT.FORCE_LOGOUT, (data) => {
      ACE.dialogManager.getDialog(DIALOG_TYPE.WELCOME_TUTORIAL).getComponent(DialogComponent).closeDialog();
    });
  }

  wrapperEvent(event, callback, isOff = false) {
    if (isOff) {
      ACE.gameSocket.off(event);
    } else {
      ACE.gameSocket.on(event, (data) => {
        if (ACE.sceneManager.currentScene == this) {
          callback(data);
        }
      });
    }
  }

  /**
   *
   * @param event EventTouch
   * @param gender
   */
  onGenderSelected(event, gender: number) {
    AudioManager.getInstance().playAudio("ITEM_CLICK_SOUND");
    this.boyOption.stopAllActions();
    this.girlOption.stopAllActions();
    this.boyOption.setScale(1);
    this.girlOption.setScale(1);
    this.confirmGender.active = true;
    this._selectedGender = gender;
    this.setupOptionAnim();
  }

  updateCharacterShadown(characterComp: CharacterComponent) {
    const shadowNode = NodeUtils.findNodeByName(characterComp.node, "_shadow");
    const nodeName = [
      "Skin (female, small)",
      "Skin (male, small)",
      "Skin (female, large)",
      "Skin (male, large)",
    ];
    let shoesNode: cc.Node;
    for (let i = 0; i < nodeName.length; i++) {
      const name = nodeName[i];
      shoesNode = characterComp.node.getChildByName(name);
      if (shoesNode) {
        break;
      }
    }
    NodeUtils.setNodePositionAtTarget(shadowNode, shoesNode);
    // shadowNode.setPosition(NodeUtils.getLocalPosition(shadowNode, shoesNode));
    shadowNode.y -= ((shoesNode.height / 2) - (shadowNode.height / (AccountManager.getInstance().profile.yearGrade <= 4 ? 1.2 : 2)));
  }

  setupOptionAnim() {
    if (this._selectedGender == 1) {
      // male
      this.boyOption.setScale(1.2);
      this.girlOption.setScale(1);
      this.boyOption.runAction(this.breathAction());
      this.moveNode(this.boy, 0, 1);
      this.moveNode(this.girl);
    } else {
      // female == 0
      this.boyOption.setScale(1);
      this.girlOption.setScale(1.2);
      this.girlOption.runAction(this.breathAction());
      this.moveNode(this.girl, 0, 1);
      this.moveNode(this.boy, -180); // move by offset but to the left
    }
  }

  moveNode(node: cc.Node, offset: number = 180, scale = 0.3, duration = 0.2, callback?, isHorizontal = true) {
    node.stopAllActions();
    const nodePos = node.getPosition();
    node.runAction(
      cc.sequence(
        cc.spawn(
          cc.moveTo(duration, cc.v2(isHorizontal ? offset : nodePos.x, isHorizontal ? nodePos.y : offset)),
          cc.scaleTo(duration, scale),
        ),
        cc.callFunc(() => {
          if (callback) {
            callback();
          }
        }),
      ),
    );
  }

  moveNodeIn(node: cc.Node, offset = 580, duration = 0.2, isHorizontal = true) {
    if (node.position.x == offset) { return; }
    const localPos = node.parent.convertToNodeSpaceAR(node.parent.convertToWorldSpaceAR(cc.v2(isHorizontal ? offset : node.position.x, isHorizontal ? node.position.y : offset)));
    node.active = true;
    node.stopAllActions();
    node.runAction(
      cc.moveTo(duration, localPos),
    );
  }

  backToSelectGender() {
    this.moveNode(this.accessoriesMenu, -cc.winSize.width, 1, 0.3);
    this.moveNode(this.confirmCharacter, -cc.winSize.height, 1, 0.3, null, false);

    cc.log("this.genderBannerEndPos.y", this.genderBannerEndPos.y);
    this.moveNode(this.looksMenu, cc.winSize.width, 1, 0.3, () => {
      this.moveNodeIn(this.genderBanner, this.genderBannerEndPos.y, 0.3, false);
      this.moveNodeIn(this.confirmGender, this.confirmButtonEndPos.y, 0.3, false);
      this.moveNodeIn(this.boyOption, -560);
      this.moveNodeIn(this.girlOption, 560);
      this.girl.active = true;
      this.boy.active = true;
      this._currentState = SceneState.GENDER;
      this._selectedGenderNode = null;
    });
  }

  onBackPressed() {
    NodeUtils.preventMultipleClick(this.back.getComponent(cc.Button));
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    if (this._currentState == SceneState.GENDER) {
      // Should clear stack because user want to login?
      ACE.dialogManager.showNoticeDialog("Do you want to exit right?", NoticeDialogType.YES_NO, () => {
        // show pop up confirm here
        LoginService.logout();
        // ACE.sceneManager.pushScene('login', {}, true);
      });
    } else if (this._currentState == SceneState.LOOKS) {
      this.backToSelectGender();
    }
  }

  nextCharacterItem(event: cc.Event.EventTouch, type: MenuType) {
    AudioManager.getInstance().playAudio("CHOOSE_ITEM_SOUND");
    ACE.gameSocket.emit(SERVER_EVENT.SELECT_NEXT, {
      gender: `${this._selectedGender}`,
      type,
    });
  }

  previousCharacterItem(event, type: MenuType) {
    AudioManager.getInstance().playAudio("CHOOSE_ITEM_SOUND");
    ACE.gameSocket.emit(SERVER_EVENT.SELECT_PREVIOUS, {
      gender: `${this._selectedGender}`,
      type,
    });
  }

  onConfirmSelection() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.showdownLayer.active = true;
    this.showdownLayer.opacity = 0;
    this.character.node.opacity = 0;
    const renderOrder = this._selectedGender == 1 ? this.boy.getComponent(CharacterComponent)._renderOrder : this.girl.getComponent(CharacterComponent)._renderOrder;
    const renderItems = this._selectedGender == 1 ? this.boy.getComponent(CharacterComponent)._characterItems : this.girl.getComponent(CharacterComponent)._characterItems;
    const fadeOut = cc.fadeOut(0.5);
    this.boy.runAction(fadeOut);
    this.girl.runAction(fadeOut);
    this.node.runAction(cc.sequence(
      cc.delayTime(1.45),
      cc.callFunc(() => {
        this.ufoParticles.forEach((particle) => {
          particle.active = true;
        });
      }),
      cc.delayTime(0.15),
      cc.callFunc(() => {
        this.choosingEffect.forEach((node) => {
          node.active = true;
        })
      }),
      cc.delayTime(0.30),
      cc.callFunc(() => {
        AudioManager.getInstance().playAudio("character_appearing");
        this.character.node.runAction(cc.fadeIn(0.2));
      }),
    ));
    this.showdownLayer.runAction(cc.sequence(
      cc.callFunc(() => {
        this.character.setRenderOrder(renderOrder);
        this.character.setRenderItems(renderItems);
      }),
      cc.fadeIn(0.5),
      cc.delayTime(5),
      cc.callFunc(() => {
        ACE.sceneManager.pushScene("choose-pet", { renderOrder, renderItems }, false, true);
      })
    ));
    AccountManager.getInstance().firstLoginGender = this._selectedGender == 1;
  }
}
