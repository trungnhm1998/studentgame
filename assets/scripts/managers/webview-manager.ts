import AccountManager from "./account-manager";
import { Config } from "../config";
import Boost from "../common/boost";
import { ACE } from "../core/global-info";
import SchoolService from "../school/school-service";

export const WEBVIEW_WAIT_TIMEOUT = 30; // 30s

export default class WebviewManager {
  static _instance: WebviewManager;
  public static getInstance(): WebviewManager {
    if (this._instance == null || this._instance == undefined) {
      this._instance = new WebviewManager();
    }
    return this._instance;
  }

  isWebviewHidden: boolean = true;
  webviewPreloaded: boolean = false;
  waitInterval: number = 0;
  openingWebview: boolean = false;

  constructor() {
    this.isWebviewHidden = true;
    this.webviewPreloaded = false;
    this.waitInterval = 0;
    this.openingWebview = false;
  }

  /**
   * TODO: To remove after implement QA rendering native side
   * @param stateName [practiseCenter|improvement|homework|naplan|focusCenter|revision|assignment]
   * will create url string http://ctest.starmathsonline.com.au/?token=[accessToken]&=[stateName]&pid=0&cid=[conceptId]
   */
  openQa(stateName: string, conceptId: Number = -1) {
    const username = AccountManager.getInstance().userName;
    const accessToken = AccountManager.getInstance().accessToken;
    const url = `${Config.qaClient}/?token=${accessToken}&state=${stateName}${conceptId != -1 ? `&cid=${conceptId}` : ""}&username=${username}`;
    // if (cc.sys.isBrowser) {
    //   location.href = url + `&return-url=${location.href}`;
    // } else if (cc.sys.isNative) {
    //   Boost.getInstance()._componentSelf.openWebview(url);
    // }
    this.webviewPreloaded = false;
    this.waitInterval = 0;
    this.openingWebview = true;
    Boost._instance._componentSelf.openWebview(url);
  }

  closeWebview(data: any) {
    cc.log("onCloseWebview", data);
    const accessToken = AccountManager.getInstance().accessToken;
    const preloadUrl = `${Config.qaClient}/?token=${accessToken}`;
    if (AccountManager.getInstance().accessToken) {
      if (AccountManager.getInstance().accessToken == data.accessToken) {
        cc.log("TODO::CloseWebview here");
        if (data.value == "school") {
          ACE.sceneManager.pushScene("school");
        }
        if (data.value == "improvement") {
          if (ACE.sceneManager.currentScene.name == "math-room") {
            ACE.sceneManager.popScene("math-room");
          }
          SchoolService.loadImprovement();
        }
        if (data.value == "focusCenter") {
          if (ACE.sceneManager.currentScene.name == "math-room") {
            ACE.sceneManager.popScene("math-room");
          }
          SchoolService.loadFocusFirstPage();
        }
        Boost._instance._componentSelf.closeWebview();
      }
    }
  }

  preloadWebview() {
    if (this.webviewPreloaded) {
      return;
    }
    cc.log("WebviewManager::preloadWebview");
    this.webviewPreloaded = true;
    const accessToken = AccountManager.getInstance().accessToken;
    const username = AccountManager.getInstance().userName;
    const url = `${Config.qaClient}/?token=${accessToken}&username=${username}`;
    cc.log("WebviewManager::preloadWebview::url", url);
    const boostComp = Boost._instance._componentSelf;
    boostComp.qaWebviewContainer.active = true;
    boostComp.qaWebview.url = url;
    boostComp.hideWebview();
  }
}
