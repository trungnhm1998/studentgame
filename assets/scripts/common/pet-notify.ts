import { PetId } from "../models/profile";
import AccountManager from "../managers/account-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PetNotify extends cc.Component {
  @property(cc.Button)
  notifyConfirm: cc.Button = null;

  @property(cc.Label)
  notifyMessage: cc.Label = null;

  @property(cc.Sprite)
  petSprite: cc.Sprite = null;

  @property(cc.Sprite)
  animPetSprite: cc.Sprite = null; // for school purpose and some animation

  @property([cc.SpriteFrame])
  petSpriteFrames: cc.SpriteFrame[] = [];

  onLoad() {
    this.setPet(AccountManager.getInstance().profile.petId); // get pet server
    // by default hide the notify confirm and anim pet sprite button
    this.notifyConfirm.node.active = false;
    this.animPetSprite.node.active = false;
  }

  setPet(petId: PetId) {
    this.petSprite.spriteFrame = this.petSpriteFrames[petId];
    this.animPetSprite.spriteFrame = this.petSpriteFrames[petId];
    this.notifyMessage.node.parent.y = this.petSprite.node.height - 50; // offset 50
  }

  setNotifyMessage(message: string) {
    this.notifyMessage.string = message;
  }

  playPetBouncing() {
    this.petSprite.enabled = false;
    this.animPetSprite.enabled = true;

    this.animPetSprite.node.stopAllActions();
    this.animPetSprite.node.runAction(cc.repeatForever(
      cc.sequence(
        cc.moveTo(1, cc.v2(this.animPetSprite.node.x, this.animPetSprite.node.y + 100)).easing(cc.easeOut(3.0)),
        cc.moveTo(1, cc.v2(this.animPetSprite.node.x, this.animPetSprite.node.y - 100))
      )
    ))
  }

  flip() {
    this.petSprite.node.scaleX *= -1;
    this.notifyMessage.node.scaleX *= -1;
    this.animPetSprite.node.scaleX *= -1;
  }

  stopPetBouncing() {
    this.animPetSprite.node.stopAllActions();
    this.petSprite.enabled = true;
    this.animPetSprite.enabled = false;
  }
}