enum Graphic {
  DEFAULT = 0,
  IMAGE = 1,
  RECT,
  TEXT,
  INPUT,
}

const Server = {
  URL: "http://starmathsonline.s3.amazonaws.com/",
};

enum ItemState {
  AVAILIBLE_BUY,
  AVAILIBLE_EQUIP,
  EQUIPPED,
}

enum Gender {
  BOY,
  GIRL,
}
enum ActionType {
  DRAG = "drag",
  DROP = "drop",
  LIST = "list",
  LIST_CHILD = "list_child",
  CLICK = "click",
}

const Size = {
  width: 800,
  height: 450,
};

export { Graphic, ActionType, Server, Size, ItemState, Gender };
