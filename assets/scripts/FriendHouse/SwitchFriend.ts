import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";
import FriendHouseService from "./FriendHouseService";
import StudentHouseManager from "../student-house/StudentHouseManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SwitchFriend extends cc.Component {
  @property(cc.Node)
  btnNextNode: cc.Node = null;
  @property(cc.Node)
  btnPreNode: cc.Node = null;
  @property(cc.Label)
  friendNameLable: cc.Label = null;

  start() {
    ACE.gameSocket.on(SERVER_EVENT.GET_CURRENT_FRIEND, (data) =>
      this.onCurrentFriendCallback(data),
    );
  }

  onCurrentFriendCallback(data) {
    if (data != null) {
      cc.log("Get Current Friend: ", data);
      this.btnNextNode.active = data.canNext;
      this.btnPreNode.active = data.canPre;
      this.friendNameLable.string = data.info.FullName;
      StudentHouseManager.getInstance().idStudent = data.info.ID;
      StudentHouseManager.getInstance().showCharacter(true);
    }
  }

  // button on clicked
  nextFriendOnClicked() {
    // todo call api
    FriendHouseService.nextHouseFriend();
    StudentHouseManager.getInstance().onEnableLoading();
  }

  previousFriendOnClicked() {
    // todo call api
    FriendHouseService.preHouseFriend();
    StudentHouseManager.getInstance().onEnableLoading();
  }
}
