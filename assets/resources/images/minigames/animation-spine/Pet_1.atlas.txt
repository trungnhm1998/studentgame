
Pet_1.png
size: 695,520
format: RGBA8888
filter: Linear,Linear
repeat: none
Pet_1_FX_Flying
  rotate: true
  xy: 458, 266
  size: 154, 142
  orig: 200, 200
  offset: 27, 38
  index: -1
csp
  rotate: true
  xy: 97, 1
  size: 60, 106
  orig: 81, 107
  offset: 14, 0
  index: -1
cstr
  rotate: false
  xy: 527, 1
  size: 51, 80
  orig: 69, 80
  offset: 0, 0
  index: -1
ctp
  rotate: true
  xy: 429, 5
  size: 66, 97
  orig: 88, 107
  offset: 17, 2
  index: -1
cttr
  rotate: false
  xy: 458, 421
  size: 65, 98
  orig: 81, 122
  offset: 13, 18
  index: -1
dau
  rotate: true
  xy: 78, 257
  size: 262, 261
  orig: 267, 262
  offset: 0, 1
  index: -1
dau_p1
  rotate: true
  xy: 204, 1
  size: 60, 94
  orig: 60, 94
  offset: 0, 0
  index: -1
duoi
  rotate: true
  xy: 579, 3
  size: 78, 112
  orig: 78, 112
  offset: 0, 0
  index: -1
la1
  rotate: false
  xy: 299, 1
  size: 77, 68
  orig: 77, 68
  offset: 0, 0
  index: -1
la2
  rotate: true
  xy: 601, 173
  size: 61, 64
  orig: 61, 64
  offset: 0, 0
  index: -1
la3
  rotate: true
  xy: 601, 285
  size: 62, 81
  orig: 62, 81
  offset: 0, 0
  index: -1
la4
  rotate: true
  xy: 601, 235
  size: 49, 81
  orig: 49, 81
  offset: 0, 0
  index: -1
la5
  rotate: false
  xy: 527, 211
  size: 73, 54
  orig: 73, 54
  offset: 0, 0
  index: -1
lagiua
  rotate: false
  xy: 500, 82
  size: 99, 128
  orig: 99, 128
  offset: 0, 0
  index: -1
lap1
  rotate: false
  xy: 340, 259
  size: 117, 260
  orig: 133, 260
  offset: 0, 0
  index: -1
lap2
  rotate: true
  xy: 1, 8
  size: 242, 95
  orig: 242, 98
  offset: 0, 1
  index: -1
latr1
  rotate: false
  xy: 1, 251
  size: 76, 268
  orig: 76, 268
  offset: 0, 0
  index: -1
latr2
  rotate: true
  xy: 212, 70
  size: 186, 144
  orig: 186, 144
  offset: 0, 0
  index: -1
mat_buon
  rotate: true
  xy: 441, 80
  size: 178, 58
  orig: 178, 58
  offset: 0, 0
  index: -1
mat_nham
  rotate: true
  xy: 357, 72
  size: 186, 83
  orig: 186, 83
  offset: 0, 0
  index: -1
mat_vui
  rotate: false
  xy: 527, 460
  size: 166, 59
  orig: 166, 59
  offset: 0, 0
  index: -1
matp
  rotate: true
  xy: 601, 348
  size: 111, 89
  orig: 111, 89
  offset: 0, 0
  index: -1
mattr
  rotate: false
  xy: 377, 1
  size: 51, 70
  orig: 51, 70
  offset: 0, 0
  index: -1
mieng_buon
  rotate: true
  xy: 524, 421
  size: 38, 15
  orig: 38, 15
  offset: 0, 0
  index: -1
mieng_dong
  rotate: true
  xy: 666, 191
  size: 43, 28
  orig: 43, 28
  offset: 0, 0
  index: -1
mieng_mo
  rotate: true
  xy: 666, 149
  size: 41, 28
  orig: 41, 28
  offset: 0, 0
  index: -1
mieng_rong
  rotate: false
  xy: 540, 428
  size: 48, 31
  orig: 48, 31
  offset: 0, 0
  index: -1
than
  rotate: true
  xy: 97, 62
  size: 194, 114
  orig: 207, 155
  offset: 13, 0
  index: -1
