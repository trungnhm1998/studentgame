import DialogComponent from "../common/dialog-component";

const { ccclass, property } = cc._decorator;

export enum NoticeDialogType {
  NONE = -1, // only close button
  YES_NO, // yes no button
  OK // ok button
}

@ccclass
export default class NoticeDialog extends DialogComponent {
  @property(cc.Button)
  okButton: cc.Button = null;

  @property(cc.Button)
  yesButton: cc.Button = null;

  @property(cc.Button)
  noButton: cc.Button = null;

  @property(cc.Label)
  text: cc.Label = null;

  _timeout: number = -1;
  yesCallback: Function = () => { };
  noCallback: Function = () => { };
  okCallback: Function = () => { };

  _type: NoticeDialogType = NoticeDialogType.NONE;

  onLoad() {

  }

  onShow() {
    super.onShow();
    this.okButton.node.active = false;
    this.yesButton.node.active = false;
    this.noButton.node.active = false;
    this.type = NoticeDialogType.NONE;
  }

  public set timeout(timeout: number) {
    if (timeout > 0) {
      this._timeout = timeout;
      this.scheduleOnce(() => {
        this.closeDialog();
      }, this._timeout);
    }
  }

  public set type(type: NoticeDialogType) {
    switch (type) {
      case NoticeDialogType.NONE:
        break;
      case NoticeDialogType.YES_NO:
        this.yesButton.node.active = true;
        this.noButton.node.active = true;
        break;
      case NoticeDialogType.OK:
        this.okButton.node.active = true;
        break;
    }
  }

  onYesPressed() {
    if (this.yesCallback) {
      this.yesCallback();
    }
    this.closeDialog();
  }

  onNoPressed() {
    if (this.noCallback) {
      this.noCallback();
    }
    this.closeDialog();
  }

  onOkPressed() {
    if (this.okCallback) {
      this.okCallback();
    }
    this.closeDialog();
  }

  onClosePressed() {
    this.closeDialog();
  }
}