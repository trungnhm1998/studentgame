import json
import os

f = []
path ="/Users/starios/projects/cocoscreator/assets/resources/images"
platform = 'web'
for (dirpath, dirnames, filenames) in os.walk(path):
  for file in filenames:
    if '.meta' in file:
      # read the json the check if the file is sprite
      file_path = str(dirpath + "/" + file)
      with open(file_path) as json_file:
        data = json.load(json_file)
        # print(data)
        if 'type' in data and data['type'] == 'sprite':
          if data['width'] >= 200 and data['height'] >= 200:
            if platform not in data['platformSettings']:
              data['platformSettings'][platform] = {}
            if 'formats' not in data['platformSettings'][platform]:
              data['platformSettings'][platform]['formats'] = []
              data['platformSettings'][platform]['formats'].append({
                'name': 'webp',
                'quality': 100
              })
              print("Optimized: " + file_path)
              with open(file_path, 'w') as json_file:
                json.dump(data, json_file)
            else:
              print(str("Skip file: " + file_path))
