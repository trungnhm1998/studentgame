import { Random } from "../utils/random-utils";
import Component, { IComponent } from "./component";
import EntityManager from "./entity-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Entity extends cc.Component {
  _uuidv4: string;
  _components: Map<string, Component>;
  _hasInit = false;
  onLoad() {
    this.init();
  }

  init() {
    if (!this._hasInit || !this._uuidv4) {
      this._hasInit = true;
      this._components = new Map();
      this._uuidv4 = new Random().uuidv4();
      for (const component of this.getComponents(Component)) {
        this._components.set(component._c_name, component);
      }
      EntityManager.getInstance().createEntity(this);
    }
  }

  has(componentName: string) {
    return this._components.has(componentName);
  }

  getECSComponent<T extends Component>(type: (new() => T ) | string): T {
    if (typeof type == "string") {
      const compName: string = type;
      return this._components.get(compName) as T;
    } else {
      for (const component of this.getComponents(Component)) {
        if (component instanceof type) {
          return component as T;
        }
      }
    }
  }

  addECSComponent<T extends Component>(type: new() => T, data?: IComponent): T {
    if (this._components.has(type.name)) {
      this.node.removeComponent(type.name);
      this._components.delete(type.name);
    }
    const component = this.node.addComponent(type);
    this._components.set(component._c_name, component);
    // cc.log("Add ECS component to entity", type.name, component);
    component.init(data);
    EntityManager.getInstance().onEntityComponentUpdate(this);
    return component;
  }

  removeECSComponent<T extends Component>(type: new() => T): void {
    // cc.log("Remove ECS component to entity", type.name);
    this.node.removeComponent(type);
    if (this._components.has(type.name)) {
      this.node.removeComponent(type.name);
      this._components.delete(type.name);
    }
    EntityManager.getInstance().onEntityComponentUpdate(this);
  }
}
