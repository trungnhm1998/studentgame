import ClientProtocol from "./ClientProtocol";

export default class WebSocketProtocol implements ClientProtocol {
  _socket: WebSocket = null;
  _eventCallbacks: { [eventName: string]: Function[] } = {};
  connect(url: string, onCloseCallback: Function, onConnectCallback: Function): Promise<any> {
    return new Promise((resolve, reject) => {
      const initListener = () => {
        this._socket.onopen = (event: MessageEvent) => {
          cc.log("WebSocketProtocol::onopen", event);
        };

        this._socket.onmessage = (event: MessageEvent) => {
          cc.log("WebSocketProtocol::onmessage", event);
          const dataObj = JSON.parse(event.data);

          const data = dataObj.data;
          if (data && data.error) {
            if (data.expired) {
              // clean up access token here
            }
            const callbackEvents = this._eventCallbacks["error"];
            if (callbackEvents) {
              // the callback for this event exist
              for (const callbackEvent of callbackEvents) {
                callbackEvent(data);
              }
            }
          } else {
            const callbackEvents = this._eventCallbacks[dataObj.event];
            if (callbackEvents) {
              // the callback for this event exist
              for (let i = 0; i < callbackEvents.length; i++) {
                const callback = callbackEvents[i];
                callback(data);
              }
            }
          }
        };

        this._socket.onerror = (event: MessageEvent) => {
          cc.log("Something wrong with the client socket", event);
        };

        this._socket.onclose = (event: CloseEvent) => {
          cc.log("WebSocketProtocol::onclose", event);
          onCloseCallback(event);
        };

        this.on("connect", () => {
          onConnectCallback();
          resolve();
        });
      }
      cc.loader.loadRes("cacert", (error, resources) => {
        this._socket = new (WebSocket as any)(url, [], resources.nativeUrl);
        cc.log("cacert loaded", resources, this._socket);
        initListener();
      });
    });
  }

  on(eventName: string, callback: Function): void {
    if (!this._eventCallbacks[eventName]) {
      this._eventCallbacks[eventName] = []
    }
    this._eventCallbacks[eventName].push(callback);
  }

  off(eventName: string, fn?: Function): void {
    if (!fn) {
      this._eventCallbacks[eventName] = []
    } else {
      this._eventCallbacks[eventName].splice(this._eventCallbacks[eventName].indexOf(fn));
    }
  }

  emit(eventName: string, args: any): void {
    if (this._socket.readyState == 1) {
      this._socket.send(JSON.stringify({ event: eventName, data: args }));
    }
  }

  loadEvents() {

  }
}