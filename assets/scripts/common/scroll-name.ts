const { ccclass, property } = cc._decorator;

@ccclass
export default class ScrollName extends cc.Component {
  @property(cc.Label)
  label: cc.Label = null;

  @property(cc.Float)
  waitInterval: Number = 10;

  @property(cc.Float)
  moveSpeed: number = 5.0;

  private _interval = 0.0;
  private _willScroll = false;
  private _labelWidth = 0;
  private _isReachOffset = false;
  private _parentWidth = 0;

  onEnable() {
    if (this.label.string.length >= 10) {
      this._willScroll = true;
      this._labelWidth = this.node.width;
      this._parentWidth = this.node.parent.width;
    }
  }

  update(dt) {
    if (this._willScroll) {
      if (this._interval >= this.waitInterval) {
        if (this.label.node.x + this._labelWidth >= -(this._parentWidth / 2)) {
          this.node.x -= this.moveSpeed * dt;
          if (this._isReachOffset) {
            if (this.node.x <= -(this._parentWidth / 2)) {
              this.node.x = -(this._parentWidth / 2);
              this._interval = 0;
              this._isReachOffset = false;
            }
          }
        } else {
          this.label.node.x = this._labelWidth / 2;
          this._isReachOffset = true;
        }
      } else {
        this._interval += dt;
      }
    }
  }
}
