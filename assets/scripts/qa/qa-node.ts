import { IDesign, IAction, Rect, Image, ConfigState } from "../models/qa";
import QARenderer from "./qa-renderer";
import { Server } from "../constants/qa.constants";

const { ccclass, property } = cc._decorator;

export enum NodeState {
  NONE = 'none',
  INCORRECT = 'incorrect',
  CORRECT = 'correct',
  SELECTED = 'selected'
}

@ccclass
export default class QaNode extends cc.Component {
  @property(cc.Sprite)
  configImageSprite: cc.Sprite = null;

  @property(cc.Graphics)
  rectNode: cc.Graphics = null;

  _NodeBorderState: NodeState = NodeState.NONE;
  _QAObject: IDesign;
  isSelect: boolean = false;
  _onClickCallback: Function = () => { };

  touchedInside: boolean = false;

  onLoad() {
  }

  init(QAObject: IDesign) {
    this._QAObject = QAObject;
    this.updateUI();
  }

  updateUI() {
    const { x, y, h, w, objPrivate } = this._QAObject;
    const { anchorX, anchorY } = objPrivate
    this.node.x = x ? x : 0;
    this.node.y = y ? y * -1 : 0;
    this.node.width = w ? w : 50;
    this.node.height = h ? h : 50;

    this.node.anchorX = anchorX != null && anchorX != undefined ? anchorX : 0.5;
    this.node.anchorY = anchorY != null && anchorY != undefined ? anchorY : 0.5;
    if (this.node.anchorY == 0) {
      this.node.anchorY = 1;
    }
  }

  setNodeState(state: NodeState) {
    let stateConfig: ConfigState = QARenderer.getInstance()._configs.states[state];
    this.rectNode.clear();
    // state none
    if (!stateConfig) {
      return;
    }
    const { rect, image } = stateConfig;
    const offset = 1.3; // scale factor of the QA renderer
    const worldSpacePos = this.node.parent.convertToWorldSpaceAR(this.node.getPosition());
    const { x, y, width, height } = this.node;
    let finalX = (worldSpacePos.x - (this.node.width * offset) / 2) - 7;
    let finalY = (worldSpacePos.y - (this.node.height * offset) / 2) - 7;
    let finalWidth = this.node.width * offset + 14;
    let finalHeight = this.node.height * offset + 14;
    if (rect) {
      this.rectNode.lineWidth = rect.stroke;
      this.rectNode.strokeColor = new cc.Color().fromHEX('#' + ('00000' + (6737151 | 0).toString(16)).substr(-6));
      this.rectNode.roundRect(finalX, finalY, finalWidth, finalHeight, rect.radius);
      this.rectNode.stroke();
    }

    // state incorrect/correct
    if (image) {
      // translate position
      let imgPosConfig: string = (stateConfig.image.position || "");
      let splittedConfig: string[] = imgPosConfig.match(/\b(left|top|right|bottom|center|middle)\b/g);
      for (let i = 0; i < splittedConfig.length; i++) {
        const config = splittedConfig[i];
        switch (config) {
          case "left":
            break;
          case "right":
            finalX += finalWidth;
            break;
          case "top":
            break;
          case "bottom":
            finalY += finalHeight;
            break;
          case "middle":
            finalY += finalHeight / 2;
            break;
          case "center":
            finalX -= finalWidth / 2;
            break;
        }
      }
      cc.loader.load(`${Server.URL}${image.link}`, (err, texture) => {
        if (err) {
          cc.log(err);
          return;
        }
        let convertedPos = this.node.parent.convertToNodeSpaceAR(cc.v2(finalX, finalY));
        cc.log('node position', this.node.getPosition(), cc.v2(convertedPos.x - this.node.x, convertedPos.y - this.node.y));
        this.configImageSprite.node.setPosition(cc.v2(convertedPos.x - this.node.x, this.node.y - convertedPos.y));
        this.configImageSprite.spriteFrame = new cc.SpriteFrame(texture);
        var imageWidth = this.configImageSprite.node.width;
        var imageHeight = this.configImageSprite.node.height;
        var imageRatio = imageWidth / imageHeight;
        if (stateConfig.image.scale) {
          var minSize = Math.min(
            stateConfig.image.scale * imageWidth,
            stateConfig.image.scale * imageWidth
          );
          if (imageWidth > imageHeight) {
            imageWidth = minSize;
            imageHeight = imageWidth / imageRatio;
          } else {
            imageHeight = minSize;
            imageWidth = imageHeight * imageRatio;
          }
          this.configImageSprite.node.width = imageWidth;
          this.configImageSprite.node.height = imageHeight;
        }
        if (stateConfig.image.anchor) {
          this.configImageSprite.node.setAnchorPoint(
            stateConfig.image.anchor[0],
            stateConfig.image.anchor[1]);
        }
      })
    }
  }

  select() {
    this.isSelect = true;
    const { objPrivate } = this._QAObject;
    const { action } = objPrivate;
    if (objPrivate && action && action.value) {
      this.setNodeState(NodeState.SELECTED);
      this._onClickCallback(action.value);
    }
  }

  deselect() {
    this.setNodeState(NodeState.NONE);
    this.isSelect = false;
  }

  getActionName(): string {
    if (!this.hasAction()) return;
    return this._QAObject.objPrivate.action.name;
  }

  getActionData(): IAction {
    if (!this.hasAction()) return;
    return this._QAObject.objPrivate.action;
  }

  hasAction() {
    const { objPrivate } = this._QAObject;
    const { action } = objPrivate;
    return objPrivate && action && (action.value || action.mode)
  }

  /**
   * some action qa node in the same group(same name and value) so we add it the the some node group
   * @param node to add to the group
   */
  addGroupItem(qaNode: QaNode) {
    if (!qaNode.hasAction()) return;
    if (!qaNode.getActionData().value != this.getActionData().value || qaNode.getActionData().name != this.getActionData().name) return;
    qaNode.node.parent = this.node.getChildByName('ui');
  }

  addClickEvent(onClickCallback) {
    this.node.on(cc.Node.EventType.TOUCH_START, (evt: cc.Touch) => {
      this.touchedInside = true;
    })

    this.node.on(cc.Node.EventType.TOUCH_END, (evt: cc.Touch) => {
      if (this.touchedInside) {
        cc.log('TOUCHED');
        this.touchedInside = false;
        if (!this.isSelect) {
          if (onClickCallback) {
            this._onClickCallback = onClickCallback;
            this.select();
          }
        } else {
          this.deselect();
        }
      }
    })
  }
}