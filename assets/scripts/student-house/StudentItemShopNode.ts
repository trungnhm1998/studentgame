import StudentItemInShop from "./Model/StudentItemInShop";
import StudentHouseManager from "./StudentHouseManager";
import StudentRoomShopManage from "./StudentRoomShopManage";
import BGItemShop from "./BGItemShop";
import { RESOURCE_PATH } from "../constants/resource-constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class StudentItemShopNode extends cc.Component {

  static previousNodeTouch: StudentItemShopNode = null;
  @property(cc.Node)
  public BG: cc.Node = null;

  @property(cc.Sprite)
  public icon: cc.Sprite = null;

  @property(cc.Node)
  public equip: cc.Node = null;

  @property(cc.Node)
  public starBtn: cc.Node = null;

  @property(cc.Node)
  public coinBtn: cc.Node = null;

  @property(cc.Node)
  public hideBtn: cc.Node = null;

  @property(cc.Node)
  public useBtn: cc.Node = null;

  studentItemInShop: StudentItemInShop = null;

  _listObjectsNeedShowDetailItem: cc.Node[] = [];

  initProp(item: StudentItemInShop) {
    let self = null;
    this.studentItemInShop = item;
    if (this.studentItemInShop) {
      self = this;
    }

    // Update random Color of BG
    let BgSprite = this.BG.getComponent(BGItemShop).pickRandomlySpriteFrame();
    this.BG.getComponent(cc.Sprite).spriteFrame = BgSprite;

    if (this.studentItemInShop.IsLocked == 0) {
      if (this.studentItemInShop.IsUse == 1) {
        this.equip.active = true;
      }
    } else { // Locked => show money & icon is dark
      this.starBtn.getComponentInChildren(cc.Label).string = "" + this.studentItemInShop.Stars;
      this.coinBtn.getComponentInChildren(cc.Label).string = "" + this.studentItemInShop.Coins;
    }

    cc.loader.load(
      RESOURCE_PATH.DATA_ASSETS_SERVER + this.studentItemInShop.UrlIcon,
      (err, texture) => {
        if (err) {
          cc.error(err.message || err);
          return;
        }

        let iconSp = new cc.SpriteFrame(texture);
        self.icon.spriteFrame = iconSp;

        if (self.studentItemInShop.IsLocked == 1) {
          self.icon.node.color = cc.Color.GRAY;
        } else {
          self.icon.node.color = cc.Color.WHITE;
        }
      });
  }

  start() {
    this._listObjectsNeedShowDetailItem = [
      this.BG,
      this.starBtn,
      this.coinBtn,
      this.hideBtn,
      this.useBtn,
    ];

    this.registerEventShowDetail();
  }

  // when mouse on this object, make show detail of item
  registerEventShowDetail() {
    let self = this;
    this._listObjectsNeedShowDetailItem.forEach((obj) => {
      if (obj) { obj.on(
        cc.Node.EventType.TOUCH_START, (callback) => {
          if (StudentItemShopNode.previousNodeTouch == null) {
            StudentItemShopNode.previousNodeTouch = self;
            self.showDetail();
          } else if (StudentItemShopNode.previousNodeTouch == self) {
            // do nothing
          } else {
            StudentItemShopNode.previousNodeTouch.hideDetail();
            StudentItemShopNode.previousNodeTouch = self;
            self.showDetail();
          }

        }, this, false,
      );
      }
    });
  }

  showDetail() {
    if (this.studentItemInShop.IsLocked == 0) {

      if (this.studentItemInShop.IsUse == 1) {
        this.equip.active = true;
        this.hideBtn.active = true;
      } else {
        this.useBtn.active = true;
      }
    } else { // Locked => show money & icon is dark
      this.starBtn.active = true;
      this.coinBtn.active = true;
    }
  }

  hideDetail() {
    this.hideBtn.active = false;
    this.useBtn.active = false;
    this.starBtn.active = false;
    this.coinBtn.active = false;
  }

  buyItem(event, action) {
    let isBuyWithCoin = true;
    let item = this.studentItemInShop;
    if (action == "star") {
      isBuyWithCoin = false;
    } else {
      isBuyWithCoin = true;
    }
    StudentHouseManager.getInstance().popupConfirmScript.showPopup(
      "Are you sure want to buy item " +
      "?",
      () => {
        // Get correct id
        let id = StudentRoomShopManage.getInstance().getIndexItemFollowID(item.ID);
        if (id != -1) {
          StudentHouseManager.getInstance().BuyRoomItem("" + id, isBuyWithCoin ? "0" : "1", "" + item.RoomItemGroupL1ID);
        }
      },
    );
  }

  UseItem() {
    let item = this.studentItemInShop;
    let id = StudentRoomShopManage.getInstance().getIndexItemFollowID(item.ID);
    if (id != -1) {
      StudentHouseManager.getInstance().UseRoomItem("" + id, "" + item.RoomItemGroupL1ID);
    }
  }

  TryItem() {
    let item = this.studentItemInShop;
    let id = StudentRoomShopManage.getInstance().getIndexItemFollowID(item.ID);
    if (id != -1) {
      StudentHouseManager.getInstance().TryRoomItem("" + id, "" + item.RoomItemGroupL1ID);
    }
  }

  HideItem() {
    let item = this.studentItemInShop;
    let id = StudentRoomShopManage.getInstance().getIndexItemFollowID(item.ID);
    if (id != -1) {
      StudentHouseManager.getInstance().HideRoomItem("" + id, "" + item.RoomItemGroupL1ID);
    }
  }
}
