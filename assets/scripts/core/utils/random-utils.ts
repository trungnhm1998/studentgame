export class Random {
  integer(from: number, to: number) {
    return Math.floor(Math.random() * (to - from + 1)) + from;
  }

  fromList<T>(list: T[]) {
    const randIndex = this.integer(0, list.length - 1);
    return list[randIndex];
  }

  uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
    // tslint:disable-next-line: no-bitwise
    const r = Math.random() * 16 | 0, v = c == "x" ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
}
