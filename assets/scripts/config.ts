import { ModelUtils } from "./core/utils/model-utils";
import { ACE } from "./core/global-info";

export let Config = {
  qaClient: "https://qaclient.starmathsonline.com.au",
  qaServer: "https://qaserver.starmathsonline.com.au/",
  gameSocket: "wss://qajs.starmathsonline.com.au",
  socketIO: "https://serverjs.starmathsonline.com.au/",
  buildVer: "",
  // using 1280*720 for better adaptation
  designWidth: 1280, // 1920,
  designHeight: 720, // 1080
  debug: false,
  checkUpdateInterval: 7200,
  loaded: false,
};

const firebaseConfigUrl = {
  web_debug: "https://au-student-game.firebaseio.com/configs/web/debug.json",
  web_production: "https://au-student-game.firebaseio.com/configs/web/production.json",
  android_debug: "https://au-student-game.firebaseio.com/configs/android/debug.json",
  android_production: "https://au-student-game.firebaseio.com/configs/android/production.json",
  ios_debug: "https://au-student-game.firebaseio.com/configs/ios/debug.json",
  ios_production: "https://au-student-game.firebaseio.com/configs/ios/production.json",
  localhost: "/config/localhost.json", // web/android/ios?
};

function getFirebaseConfig(enviromentType: string): Promise<any> {
  if (!enviromentType && !firebaseConfigUrl[enviromentType]) {
    return new Promise((resolve, reject) => {
      cc.log("Enviroment type not exist");
      reject();
    });
  }
  const url = firebaseConfigUrl[enviromentType];
  cc.log("getFirebaseConfig::url", url);
  return ACE.http.get(url, {});
}

function getConfigFile(): string {
  switch (cc.sys.platform) {
    case cc.sys.ANDROID:
      return `android${CC_DEBUG ? "_debug" : ""}`;
    case cc.sys.IPHONE:
    case cc.sys.IPAD:
      return `ios${CC_DEBUG ? "_debug" : ""}`;
    default:
      break;
  }
  return `web${CC_DEBUG ? "_debug" : ""}`;
}

export function loadProjectConfig(): Promise<any> {
  let configPrefix = getConfigFile();
  cc.log("Loading game config", cc.sys.platform, configPrefix);
  return new Promise((resolve, reject) => {
    cc.loader.loadRes(`configs/${configPrefix}`, (err, resource: cc.JsonAsset) => {
      cc.log("loaded config", resource.json)
      if (resource && resource.json) {
        // ModelUtils.merge(Config, resource.json);
        getFirebaseConfig(resource.json.env).then((data) => {
          cc.log("getFirebaseConfig::Success::", data);
          if (data) {
            cc.log(resource.json.env, data);
            ModelUtils.merge(Config, data);
          }
          Config.loaded = true;
          resolve();
        }).catch((error) => {
          cc.log("getFirebaseConfig::Failed", error);
          Config.loaded = true;
          resolve();
        });
      } else {
        Config.loaded = true;
        resolve();
      }
    });
  });
}
