import ClientProtocol from "./ClientProtocol";

interface IServerResponse {
  args: any;
  error: string;
  dismiss: boolean;
  expired: boolean;
  data: any;
}

export default class SocketIOProtocol implements ClientProtocol {
  _socket = null;
  _eventCallbacks: { [eventName: string]: Function[] } = {};
  accessToken: string = "";
  connect(url: string, onCloseCallback: Function, onConnectCallback: Function): Promise<any> {
    this._socket = null;
    return new Promise((resolve, reject) => {
      let connectError;
      this._socket = io.connect(url, {
        reconnection: false
      });
      // try {
      // } catch (error) {
      //   connectError = error;
      //   onCloseCallback(error);
      // }
      this._socket.on("connect", () => {
        onConnectCallback();
        resolve();
      });
      this._socket.on("error", (error) => {
        cc.log("SocketIOProtocol::error", error);
        onCloseCallback(error);
      });
      this._socket.on("disconnect", (error) => {
        cc.log("SocketIOProtocol::disconnect", error);
        reject(null);
      });
      this._socket.on("connect_failed", (error) => {
        cc.log("SocketIOProtocol::connect_failed", error);
        onCloseCallback(error);
      });
      this._socket.on("connect_error", (error) => {
        cc.log("SocketIOProtocol::connect_error", error);
        onCloseCallback(error);
      });
    });
  }

  on(eventName: string, callback: Function): void {
    if (!this._eventCallbacks[eventName]) {
      this._eventCallbacks[eventName] = []
    }
    this._eventCallbacks[eventName].push(callback);

    this._socket.on(eventName, (data: IServerResponse) => {
      cc.log("SocketIOProtocol::on " + eventName + " - ",  data);
      this._eventCallbacks[eventName].forEach((cb) => {
        if (data.args) {
          cb(data.args);
        } else {
          cb(data);
        }
      });
    });
  }

  off(eventName: string, fn?: Function): void {
    this._socket.off(eventName, fn);
    if (!fn) {
      this._eventCallbacks[eventName] = []
    } else {
      this._eventCallbacks[eventName].splice(this._eventCallbacks[eventName].indexOf(fn));
    }
  }

  emit(eventName: string, args: any): void {
    let accessToken = this.accessToken ? this.accessToken : args ? args.accessToken : "";
    this._socket.emit(eventName, { args, accessToken }, (data: IServerResponse) => {
      if (data.error) {
        cc.log("SocketIOProtocol::emit::error: ", data.error);
        this._eventCallbacks["error"].forEach((cb) => {
          cb(data);
        });
      } else {
        if (!data.dismiss) {
          cc.log("SocketIOProtocol::on" + eventName + " - ", data);
          this._eventCallbacks[eventName].forEach((cb) => {
            cb(data.data);
          });
        }
      }
    });
  }
}