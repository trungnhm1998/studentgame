import NoticeDialog, { NoticeDialogType } from "./../dialogs/notice-dialog";
import NodeUtils from "./utils/node-utils";
import { DIALOG_TYPE, GAME_EVENT } from "./constant";
import { DialogType } from "./interfaces/dialog-type";
import DialogComponent from "../common/dialog-component";
import LoadingDialog from "../dialogs/loading-dialog";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DialogManager extends cc.Component {

  static _instance: DialogManager;

  static getInstance(): DialogManager {
    if (!this._instance) {
      this._instance = new DialogManager();
    }
    return this._instance;
  }
  @property([cc.Prefab])
  dialogs: cc.Prefab[] = [];

  @property(cc.Node)
  dynamicBlock: cc.Node = null;

  @property(cc.Node)
  noticeBlock: cc.Node = null;

  @property(cc.Node)
  mainBlock: cc.Node = null;

  dialogScale = 1;
  dialogStacks: cc.Node[] = [];

  constructor() {
    super();
    DialogManager._instance = this;
  }

  // for custom instance?
  init(instance: DialogManager) {
    DialogManager._instance = instance;
  }

  onResize() {
    const designRatio = 1280 / 720;
    const ratio = Math.min(1, designRatio / (cc.winSize.width / cc.winSize.height));
    this.dialogScale = ratio;
    for (const dlg of this.mainBlock.children) {
      if (dlg.active) {
        dlg.scale = this.dialogScale;
        this.resizeMask(dlg);
      }
    }
    for (const dlg of this.noticeBlock.children) {
      if (dlg.active) {
        dlg.scale = this.dialogScale;
        this.resizeMask(dlg);
      }
    }
    for (const dlg of this.dynamicBlock.children) {
      if (dlg.active) {
        dlg.scale = this.dialogScale;
        this.resizeMask(dlg);
      }
    }
  }

  closeDialog(dialogType: DialogType) {
    for (const dialog of this.mainBlock.children) {
      if (dialog.name == dialogType.name) {
        const dialogComponent: DialogComponent = dialog.getComponent(DialogComponent);
        if (dialogComponent) {
          dialogComponent.closeDialog();
        }
        break;
      }
    }
    for (const dialog of this.noticeBlock.children) {
      if (dialog.name == dialogType.name) {
        const dialogComponent: DialogComponent = dialog.getComponent(DialogComponent);
        if (dialogComponent) {
          dialogComponent.closeDialog();
        }
        break;
      }
    }
    for (const dialog of this.dynamicBlock.children) {
      if (dialog.name == dialogType.name) {
        const dialogComponent: DialogComponent = dialog.getComponent(DialogComponent);
        if (dialogComponent) {
          dialogComponent.closeDialog();
        }
        break;
      }
    }
  }

  resizeMask(targetNode?: cc.Node) {
    const node = targetNode ? targetNode : this.node;
    const mask = NodeUtils.findNodeByName(node, "mask");
    if (mask) {
      mask.width = 5 * cc.winSize.width / this.dialogScale;
      mask.height = 5 * (cc.winSize.height + 100) / this.dialogScale;
    }
  }

  /**
   * get but wont create
   * @param dialogType
   * @param component
   */
  getDialogComponentByType<T>(dialogType: DialogType, component: new () => T): T {
    let dlg = NodeUtils.findNodeByName(this.mainBlock, dialogType.name);
    if (!dlg) {
      dlg = this.createDialog(dialogType);
    }
    dlg.active = true;
    dlg.getComponent(DialogComponent).onShow();
    return dlg.getComponent(component);
  }

  /**
   * get and create if not exist
   * @param dialogType
   */
  getDialog(dialogType: DialogType): cc.Node {
    let dialog = NodeUtils.findNodeByName(this.mainBlock, dialogType.name);
    if (dialog) {
      return dialog;
    }

    for (let i = 0; i < this.dialogs.length; i++) {
      const dialogPrefab = this.dialogs[i];
      if (dialogPrefab.name == dialogType.name) {
        dialog = cc.instantiate(dialogPrefab);
        this.mainBlock.addChild(dialog);
        dialog.active = true;
        this.dialogStacks.push(dialog);
        return dialog;
      }
    }
  }

  /**
   * for simple dialog that don't need params or any thing special when init such as loading has timeout
   * and notice has type
   * @param dialogType
   * @param component
   */
  showDialog<T>(dialogType: DialogType, component: new () => T): T {
    const dialog = this.loadDialog(dialogType, component);
    return dialog;
  }

  showLoading(onTimeout?: any, timeout: number = 30) {
    const loadingDialog = this.loadDialog(DIALOG_TYPE.LOADING, LoadingDialog);
    if (onTimeout || timeout) {
      loadingDialog.setTimeoutCallback(onTimeout, timeout);
    }
    return loadingDialog;
  }

  hideLoading() {
    this.closeDialog(DIALOG_TYPE.LOADING);
  }

  showNoticeDialog(text: string, type: NoticeDialogType, yesCallback: () => any = null, noCallback: () => any = null, okCallback: () => any = null, timeout = -1) {
    const noticeDialog = this.loadDialog(DIALOG_TYPE.NOTICE, NoticeDialog);
    noticeDialog.timeout = timeout;
    noticeDialog.text.string = text;
    noticeDialog.type = type;
    noticeDialog.yesCallback = yesCallback;
    noticeDialog.noCallback = noCallback;
    noticeDialog.okCallback = okCallback;
    return noticeDialog;
  }

  private createDialog(dialogType: DialogType, dialogPrefab?: cc.Prefab): cc.Node {
    let dialog: cc.Node = null;
    let dialogComponent: DialogComponent = null;
    if (dialogPrefab) {
      dialog = cc.instantiate(dialogPrefab);
      dialog.active = true;
      dialogComponent = dialog.getComponent(DialogComponent);
      dialogComponent.dynamic = true; // Does I need this?
      dialog.scale = this.dialogScale;
    } else {
      dialog = this.getDialog(dialogType);
      dialogComponent = dialog.getComponent(DialogComponent);
      dialog.active = true;
      dialog.scale = this.dialogScale;
      const dialogIndex = this.dialogStacks.indexOf(dialog);

      // put the dialog on top of other for data
      // maybe a bad logic here
      // TODO: Review and update
      // if (dialogIndex >= 0) {
      //   this.dialogStacks.splice(dialogIndex, 1);
      // }
      // this.dialogStacks.push(dialog);

      for (let i = 0; i < this.dialogStacks.length; i++) {
        const stackDialog = this.dialogStacks[i];
        stackDialog.zIndex = i;
      }
      dialog.zIndex = dialog.parent.childrenCount; // put on top for UI
      this.mainBlock.sortAllChildren(); // update zindex
      dialog.targetOff(dialog);
      dialog.on(GAME_EVENT.CLOSE_DIALOG, () => {
        dialog.removeFromParent();
        this.dialogStacks.splice(this.dialogStacks.indexOf(dialog));
      }, dialog);
    }
    this.onResize();
    dialogComponent.onShow();
    return dialog;
  }

  private loadDialog<T>(dialogType: DialogType, component: new () => T): T {
    let comp = this.getDialogComponentByType(dialogType, component);
    if (!comp) {
      const dialog = this.createDialog(dialogType);
      comp = dialog.getComponent(component); // faster then get
      this.resizeMask(dialog);
    }
    return comp;
  }
}
