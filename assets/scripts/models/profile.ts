export default interface IProfile {
  active?: boolean;
  classId?: number;
  coins?: number;
  enrolmentId?: number;
  expireDate?: string;
  firstname?: string;
  gender?: boolean;
  hearts?: number;
  id?: number;
  isPremium?: boolean;
  lastname?: string;
  middlename?: string;
  parentId?: number;
  petHouseBgId?: number;
  petId?: PetId;
  schoolId?: number;
  stars?: number;
  studentPetCollectionId?: number;
  teacherId?: number;
  userId?: string; // "0a5a3f12-8e0e-4723-9b59-354936c8e4ee"
  yearGrade?: number;
  newCoinStar?: {
    Coins: number,
    Stars: number,
  };
}

export enum PetId {
  Draco = 1,
  Haya,
  Moru,
  Shoo,
  Umic,
}

export interface IItemChanged {
  stars?: ICoins;
  coins?: ICoins;
  practiseBlock?: IPractiseBlock;
}

export interface ICoins {
  value: number;
  changes: number;
  reason: string;
}

export interface IPractiseBlock {
  value: IValue;
  reason: string;
}

export interface IValue {
  NumberInCorrect: number;
  NumberCorrect: number;
}
