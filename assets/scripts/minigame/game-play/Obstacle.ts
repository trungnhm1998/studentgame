import { PositionObject } from "../constants/minigame-constant";
import IngameLogic from "./ingame-logic";
import GPController from "../gameplay-controller";
import MathQuestion from "../math-logic/stage/math-question";
const { ccclass, property } = cc._decorator;

@ccclass
export default class Obstacle extends cc.Component {
  @property(cc.Node)
  bat: cc.Node = null;

  @property(cc.Node)
  tree: cc.Node = null;

  @property(cc.CircleCollider)
  collider: cc.CircleCollider = null;

  @property(cc.Label)
  BatQuestionLabel: cc.Label = null;

  @property(cc.Label)
  TreeQuestionLabel: cc.Label = null;

  postionStatus: PositionObject = PositionObject.DOWN;

  // @property(sp.Skeleton)
  // batSkeleton : sp.Skeleton = null;

  // board: any = null;

  start() {
    // Open the collision manager, without this part statement you will not detect any collision.
    cc.director.getCollisionManager().enabled = true;

    // cc.director.getCollisionManager().enabledDebugDraw = true;
  }

  onEnable() {
    // this.board = this.batSkeleton.findBone("board");
  }

  updateLogicObject(postionStatus: PositionObject) {
    this.postionStatus = postionStatus;
    if (postionStatus == PositionObject.UP) {
      this.tree.active = false;
      this.bat.active = true;
    } else {
      this.bat.active = false;
      this.tree.active = true;
    }

    cc.log("Obtacle Update Position ..... to : " + this.postionStatus);
    let posY = IngameLogic.getInstance().player.currentPosY;
    this.node.setPosition(this.node.position.x, posY);
  }

  updateQuestion() {
    if (GPController.getInstance().getMathQuestionCurrent() != null) {
      let question: MathQuestion = GPController.getInstance().getMathQuestionCurrent();
      let operationStr = GPController.getInstance().getOperator();

      this.BatQuestionLabel.string = question.firstNumber + " " + operationStr
        + " " + question.secondNumber;
      this.TreeQuestionLabel.string = question.firstNumber + " " + operationStr
        + " " + question.secondNumber;
    }
  }

  // update(dt){

  // if(this.board && this.bat.active)
  // {
  //     // cc.log("board ............................ " + this.board.worldX + " : " + this.board.worldY);
  //     this.BatQuestionLabel.node.setPosition(this.BatQuestionLabel.node.position.x, this.board.worldY)
  // }

  // }
}
