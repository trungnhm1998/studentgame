import Component, { IComponent } from "../../core/ecs/component";

const { ccclass, property } = cc._decorator;

export interface IDebugComponent extends IComponent {
  d_name: string;
  id: number;
}

@ccclass
export default class DebugComponent extends Component {
  _c_name: string = "DebugComponent";
  _data: IDebugComponent;
}
