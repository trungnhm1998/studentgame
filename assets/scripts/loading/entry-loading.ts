import Loading from "./loading";
import HotUpdate, { HotUpdateCode } from "./hot-update";

const { ccclass, property } = cc._decorator;

@ccclass
export default class EntryLoading extends cc.Component {
  @property(cc.Label)
  info: cc.Label = null;

  @property(cc.ProgressBar)
  progressBar: cc.ProgressBar = null;

  _loadingComponent: Loading = null;
  _hotUpdateComponent: HotUpdate = null;
  onLoad() {
    cc.game.addPersistRootNode(this.node);
    this._hotUpdateComponent = this.getComponent(HotUpdate);
    this._loadingComponent = this.getComponent(Loading);
    this._hotUpdateComponent.init().then((code: HotUpdateCode) => {
      if (code == HotUpdateCode.ERROR) {
        cc.log("EntryLoading::HotUpdate:error");
      }
      this._loadingComponent.init();
    });
  }
}
