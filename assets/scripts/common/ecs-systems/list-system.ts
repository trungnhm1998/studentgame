import System from "../../core/ecs/system";
import Entity from "../../core/ecs/entity";
import ListComponent from "../ecs-components/list-component";
import ListItemComponent from "../ecs-components/list-item-component";
import NodeUtils from "../../core/utils/node-utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ListSystem extends System {
  _filter = ["ListComponent"];

  @property(cc.SpriteFrame)
  debugSpriteFrame: cc.SpriteFrame = null;

  _listMap: Map<string, cc.Layout>;

  get listMap(): Map<string, cc.Layout> {
    return this._listMap;
  }

  onLoad() {
    super.onLoad();
    this._listMap = new Map();
    this.node.on("update-list-item", (entity: Entity) => {
      const { itemRef, name } = entity.getECSComponent(ListItemComponent)._data;
      const layout = this._listMap.get(itemRef.parent);
      if (layout) {
        entity.node.parent = layout.node;
        entity.node.zIndex = itemRef.index;
        entity.node.name = name;
        layout.node.sortAllChildren();
        layout.updateLayout();
      }
    });
  }

  addDebugDraw(entity: Entity) {
    const { x, y, anchor } = entity.getECSComponent(ListComponent)._data;
    entity.node.color = new cc.Color().fromHEX("#FF7D44");
    entity.node.addComponent(cc.Sprite).spriteFrame = this.debugSpriteFrame;
    // entity.node.width = 50;
    entity.node.height = 30;
    entity.node.anchorX = anchor.x;
    entity.node.anchorY = anchor.y == 0 ? 1 : anchor.y;
    // entity.node.opacity = 100;
    // debugNode.setPosition(cc.v2(x, y));

    // entity.node.addChild(debugNode, 7, "debug-list-node");
    // entity.node.sortAllChildren();
  }

  getLayoutType(type: string): cc.Layout.Type {
    switch (type) {
      case "horizontal":
        return cc.Layout.Type.HORIZONTAL;
    }
  }

  addLayoutComponent(entity: Entity) {
    const { dir, name } = entity.getECSComponent(ListComponent)._data;
    let layout = entity.addComponent(cc.Layout);
    layout.type = this.getLayoutType(dir);
    layout.resizeMode = cc.Layout.ResizeMode.CONTAINER; // TODO: should check more carefully here
    this._listMap.set(name, layout);
  }

  onEntitiesUpdate(entity: Entity) {
    const { x, y, anchor} = entity.getECSComponent(ListComponent)._data;
    this.addLayoutComponent(entity);
    this.addDebugDraw(entity);
    entity.node.anchorX = anchor.x;
    entity.node.anchorY = anchor.y == 0 ? 1 : anchor.y;
    entity.node.setPosition(cc.v2(x, y >= 0 ? -y : y));
  }
}
