import System from "../../core/ecs/system";
import Entity from "../../core/ecs/entity";
import DropComponent from "../ecs-components/drop-component";
import DragSystem from "./drag-system";
import ImageComponent from "../ecs-components/image-component";
import PositionComponent from "../ecs-components/position-component";
import NodeUtils from "../../core/utils/node-utils";
import DragComponent from "../ecs-components/drag-component";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DropSystem extends System {
  _filter = ["DropComponent"];

  @property(cc.SpriteFrame)
  texture: cc.SpriteFrame = null;
  onLoad() {
    super.onLoad();
    cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_END, (event: cc.Event.EventTouch) => {
      // cc.log("touch end at ", event.getLocation(), DragSystem.pointerDrag);
      const dragEntity = DragSystem.pointerDrag;
      if (dragEntity) {
        const intersectedEntity = this.findLargestIntersectDropArea(dragEntity);
        const dragComp = dragEntity.getECSComponent(DragComponent)._data;
        // reset dropzone value for the drag entity before set it
        if (intersectedEntity) {
          NodeUtils.setNodePositionAtTarget(dragEntity.node, intersectedEntity.node);
          dragEntity.node.x += intersectedEntity.node.width / 2;
          dragEntity.node.y -= intersectedEntity.node.height / 2;
          dragComp.dropzone = intersectedEntity;
          // cc.log(`[${intersectedEntity.getECSComponent(DropComponent)._data.name}>>${dragComp.value}]`);
          DragSystem.pointerDrag = null;
        } else {
          dragEntity.node.setPosition(dragComp.originalPosition);
          DragSystem.pointerDrag = null;
        }
      }
    });
  }

  onEntitiesUpdate(entity: Entity) {
    const { size } = entity.getECSComponent(DropComponent)._data;
    entity.node.width = size.width;
    entity.node.height = size.height;
    // this.addDebugLayer(entity);
  }

  addDebugLayer(entity: Entity) {
    const { size } = entity.getECSComponent(DropComponent)._data;
    const debugNode = new cc.Node();
    // debugNode.color = new cc.Color().fromHEX("#450FF0");
    debugNode.addComponent(cc.Sprite).spriteFrame = this.texture;
    debugNode.width = size.width;
    debugNode.height = size.height;
    debugNode.anchorX = 0;
    debugNode.anchorY = 1;
    debugNode.opacity = 100;
    // debugNode.setPosition(position);

    entity.node.addChild(debugNode, 7, "debug-drop-node");
    entity.node.sortAllChildren();
  }

  findLargestIntersectDropArea(dragEntity: Entity) {
    let largestIntersection: cc.Rect = new cc.Rect();
    let largestEntityIntersected: Entity = null;
    const dragEntityBoundingBox = dragEntity.node.getBoundingBoxToWorld();
    this._entities.forEach((dropZoneEntity) => {
      const dropZoneEntityBoundingBox = dropZoneEntity.node.getBoundingBoxToWorld();
      if (dropZoneEntityBoundingBox.intersects(dragEntityBoundingBox)) {
        const intersection = new cc.Rect();
        dropZoneEntityBoundingBox.intersection(intersection, dragEntityBoundingBox);
        if (intersection.width >= largestIntersection.width && intersection.height >= largestIntersection.height) {
          largestIntersection = intersection;
          largestEntityIntersected = dropZoneEntity;
        }
      }
    });
    return largestEntityIntersected;
  }
}
