import GameScene from "../common/game-scene";
import StartCoundown from "./start-coundown";
import MinigamePopup from "./minigame-popup";
import IngameBottomBar from "./ingame-bottombar";
import PopupEndGame from "./popup-end-game";
import IngameTopbar from "./ingame-topbar";
import MathGenerater from "./math-logic/math-generater";
import MinigameController from "./minigame_controller";
import MathQuestion from "./math-logic/stage/math-question";
import IngameLogic from "./game-play/ingame-logic";
import MinigameService from "./minigame-service";
import GameSaveData from "./game-save-data";
import { ACE } from "../core/global-info";
import PopupClaimReward from "./popup-claim-reward";
import { OperatorType } from "./constants/minigame-constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GamePlayController extends GameScene {

  get questionCorrectCount(): number {
    return this._questionCorrectCount;
  }

  set questionCorrectCount(value: number) {
    this._questionCorrectCount = value;
  }

  get questionCount(): number {
    return this._questionCount;
  }

  set questionCount(value: number) {
    this._questionCount = value;
  }

  // singleton
  static _instance: GamePlayController = null;
  static getInstance() {
    if (GamePlayController._instance == null) { }
    return GamePlayController._instance;
  }

  @property(IngameTopbar)
  minigameTopBar: IngameTopbar = null;
  @property(IngameBottomBar)
  minigameBottomBar: IngameBottomBar = null;
  @property(PopupEndGame)
  minigameEndgamePopup: PopupEndGame = null;
  @property(StartCoundown)
  startCounDown: StartCoundown = null;

  @property(MinigamePopup)
  miniGamePopup: MinigamePopup = null;
  @property(PopupClaimReward)
  claimPopup: PopupClaimReward = null;

  timeForNextQuestion: number = 0;
  _questionCount: number = 0;
  _gamePlayTime: number = 0;
  _timeOfPreQuestion: number = 0;
  _mathQuestionCurrent: MathQuestion = null;
  _questionCorrectCount: number = 0;

  onLoad() {
    super.onLoad();
    if (GamePlayController._instance == null) {
      GamePlayController._instance = this;
    }
  }

  start() {
  }

  onResize() {
    super.onResize();
  }

  registerEvents() {
  }

  onEnable() {
    this.minigameTopBar.node.active = false;
    this.minigameBottomBar.node.active = false;
    this.minigameEndgamePopup.node.active = false;
    this.startCounDown.node.active = true;
    this.claimPopup.node.active = false;
    this.questionCount = 0;
    this._timeOfPreQuestion = -1;
    this.startNewGame();
  }

  startNewGame() {
    this.startCounDown.startCoundown(() => this.onStartGame());
    MathGenerater.getInstance().initGenerateForGameTurn(
      MinigameController.getInstance().getMiniGameType(),
    );
  }

  onStartGame() {
    this.updateTopBarInfo();
    this.minigameTopBar.node.active = true;
    this.minigameBottomBar.node.active = true;
    this._timeOfPreQuestion = this._gamePlayTime + this.timeForNextQuestion;
    IngameLogic.getInstance().startLevel();
  }

  updateTopBarInfo() {
    this.minigameTopBar.updateDistanceTravelled(0);
    this.minigameTopBar.updateCoinCollected(0);
    this.minigameTopBar.updateTargetDistance(this.getDistanceToClear());
    this.minigameTopBar.updateTimeRemaining(0);
    this.minigameTopBar.updateQuestions(0);
  }

  startCameraShakeEffect() {
    this.node.getComponent(cc.Animation).play();
  }

  update(dt) {
    this._gamePlayTime += dt;
  }

  getCurrentSpeed(difficult: number): number {
    return MathGenerater.getInstance().getSpeed(
      MinigameController.getInstance().getStageSelected(),
      MinigameController.getInstance().getLevelSelected(),
      difficult,
    );
  }

  spawnQuestion() {
    // if (this._questionCount % 10 == 0 && this._questionCount > 0) {
    //   IngameLogic.getInstance().player.UpdateLevelWin();
    // } else {
    //   IngameLogic.getInstance().currentQuestion++;
    // }
    if (IngameLogic.getInstance().answerCorrected >= 10 && this._questionCount > 0) {
      IngameLogic.getInstance().player.UpdateLevelWin();
    }
    this.minigameBottomBar.onResetForNewQuestion();
    this._timeOfPreQuestion = this._gamePlayTime + this.timeForNextQuestion;
    this._mathQuestionCurrent = MathGenerater.getInstance().getMathQuestion(
      MinigameController.getInstance().getStageSelected(),
      MinigameController.getInstance().getLevelSelected(),
    );
    this.questionCount++;
  }

  getOperator(): string {
    let operationStr = "";
    switch (this._mathQuestionCurrent.operatorType) {
      case OperatorType.ADD:
        operationStr = "+";
        break;
      case OperatorType.SUB:
        operationStr = "-";
        break;
      case OperatorType.MULTI:
        operationStr = "x";
        break;
      case OperatorType.DIV:
        operationStr = "÷";
        break;
    }
    this.minigameBottomBar.generateAnswer();
    return operationStr;
  }

  getMathQuestionCurrent() {
    return this._mathQuestionCurrent;
  }

  onAnswerQuestion(result: number) {
    if (result == this._mathQuestionCurrent.getMathResult()
      && !this._mathQuestionCurrent.isQuestionAnswered
    ) {
      this._mathQuestionCurrent.isQuestionAnswered = true;
      IngameLogic.getInstance().player.doActionAnswerCorrect();
    }
  }

  showEndGame(isWin: boolean,
              travelled: number, target: number,
              coin: number, corrected: number,
              incorrect: number, difficult: number, score: number) {

    let level = MinigameController.getInstance().getLevelSelected();
    let stage = MinigameController.getInstance().getStageSelected();
    if (isWin) {
      if (stage >= 2) {
        level++;
        stage = 0;
        cc.log("Level up: ", level);
      } else {
        stage++;
        cc.log("Stage up: ", stage);
      }

      let gameData: GameSaveData = new GameSaveData(
        MinigameController.getInstance().getMiniGameType(),
        level,
        stage,
        difficult,
        score,
        travelled,
        corrected,
        incorrect,
      );
      MinigameService.saveMiniGameData(gameData);

      if (MinigameController.getInstance().getLevelSelected() == 6
        && MinigameController.getInstance().getStageSelected() == 2
      ) {
        this.claimPopup.showCongratulations();
      } else {
        this.minigameEndgamePopup.showEndGame(isWin, travelled, target, coin, corrected, incorrect);
      }

    } else {
      this.minigameEndgamePopup.showEndGame(isWin, travelled, target, coin, corrected, incorrect);
    }
  }

  getTopBar() {
    return this.minigameTopBar;
  }

  getTotalTimeForLevel() {
    let timeForLevel = MathGenerater.getInstance().getTotalTimeForLevel(
      MinigameController.getInstance().getStageSelected(),
      MinigameController.getInstance().getLevelSelected());
    return timeForLevel;
  }

  getDistanceToClear() {
    let targetForLevel = MathGenerater.getInstance().getDistanceToClear(
      MinigameController.getInstance().getStageSelected(),
      MinigameController.getInstance().getLevelSelected());

    return targetForLevel;
  }

  getVelocityFollowDifficult(difficult: number) {
    return MathGenerater.getInstance().getVelocityFollowDifficult(
      MinigameController.getInstance().getStageSelected(),
      MinigameController.getInstance().getLevelSelected(), difficult);
  }

  openConfirmToExitGame() {
    IngameLogic.getInstance().isPause = true;
    this.miniGamePopup.show("Exit the game",
      "Are you sure you want to exit the game?",
      () => {
        ACE.sceneManager.pushScene("select-level");
      },
      () => {
        IngameLogic.getInstance().isPause = false;
      },
    );
  }
}
