import NodeUtils from "../core/utils/node-utils";
import { ACE } from "../core/global-info";
import { NoticeDialogType } from "../dialogs/notice-dialog";

const { ccclass, property } = cc._decorator;

export enum HotUpdateCode {
  NONE = 0,
  SUCCESS = 1,
  ERROR = 2,
  NOT_NATIVE = 3,
  UP_TO_DATE,
}

const LOCAL_PATH = "hot-update-assets";

@ccclass
export default class HotUpdate extends cc.Component {
  static _instance: HotUpdate = null;
  @property(cc.Label)
  infoLabel: cc.Label = null;

  @property(cc.ProgressBar)
  progressBar: cc.ProgressBar = null;

  @property({ type: cc.Asset })
  iosManifest: cc.Asset = null;

  @property({ type: cc.Asset })
  androidManifest: cc.Asset = null;

  @property({ type: cc.Asset })
  localUrl: cc.Asset = null;

  _storagePath: string = "";
  _assetManager: jsb.AssetManager = null;
  _updating: boolean = false;
  _canRetry: boolean = false;
  _updateListener: any = null;
  _failCount: number = - 1;
  _hotUpdateCode: HotUpdateCode;
  _manifestUrl: cc.Asset = null;
  _ErrorUpdateTimeout = 15000;
  _hasErrorTime: number = -1;
  _hasErrorUpdating = false;
  _needRestart = false;
  _returnUpdateCallback: (code: HotUpdateCode, error?: string) => any = null;
  // static _instance: HotUpdate = null;

  onLoad() {
    HotUpdate._instance = this;
  }

  initAssetManager(paramMananifest?) {
    this._hotUpdateCode = HotUpdateCode.NONE;
    if (!cc.sys.isNative) {
      return;
    }
    // this.retryButton.interactable = false;
    // HotUpdate._instance = this;

    // get path
    this._storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : "/") + LOCAL_PATH);
    cc.log("storage path for remote asset: " + this._storagePath);
    this._manifestUrl = paramMananifest;
    if (!paramMananifest) {
      if (!CC_DEBUG) {
        this._manifestUrl = cc.sys.os === cc.sys.OS_ANDROID ? this.androidManifest : this.iosManifest;
      } else {
        this._manifestUrl = this.localUrl;
      }
    }
    if (this._manifestUrl == undefined) {
      cc.log("HotUpdate::iniAssetsManager::Manifest is null");
      // this._returnUpdateCallback(HotUpdateCode.ERROR, "Manifest is null");
      return this._assetManager;
    }
    let url = this._manifestUrl.nativeUrl;
    let manifest = JSON.parse(this._manifestUrl._$nativeAsset);
    const {
      packageUrl,
      remoteManifestUrl,
      remoteVersionUrl,
      version,
    } = manifest;
    cc.log("manifestInfos @@@@");
    cc.log(url);
    cc.log(packageUrl);
    cc.log(remoteVersionUrl);
    cc.log(remoteManifestUrl);
    cc.log(version);
    cc.log("manifestInfos end @@@@");
    // md5 security
    cc.log("md5Pipe", cc.loader.md5Pipe);
    if (cc.loader.md5Pipe) {
      url = cc.loader.md5Pipe.transformURL(url);
    }
    this._assetManager = new jsb.AssetsManager(url, this._storagePath, this.versionCompare.bind(this));

    // Setup the verification callback, but we don't have md5 check function yet, so only print some message
    // Return true if the verification passed, otherwise return false
    this._assetManager.setVerifyCallback(this.onAssetManagerVerify.bind(this));

    cc.log(this._assetManager);
    cc.log("HotUpdate::onLoad::assetManager read");
    // this.setInfoString("We are ready!");

    if (cc.sys.os === cc.sys.OS_ANDROID) {
      // Some Android device may slow down the download process when concurrent tasks is too much.
      // The value may not be accurate, please do more test and find what's most suitable for your game.
      this._assetManager.setMaxConcurrentTask(2);
      cc.log("HotUpdate::Android::Max concurrent tasks count have been limited to 2");
      // this.setInfoString("Max concurrent tasks count have been limited to 2");
    }

    // this.fileProgressLabel.string = `${0}`;
    // this.byteProgressLabel.string = `${0}`;
    return this._assetManager;
  }

  init(paramManifest?, forceNewAssetManager = false): Promise<HotUpdateCode> {
    cc.log("HotUpdate::init::getCurrentSceneName()", NodeUtils.getCurrentSceneName());
    if (!this._assetManager || forceNewAssetManager) {
      this.initAssetManager(paramManifest)
    }
    return new Promise((resolve, reject) => {
      cc.log(`HotUpdate::init cc.sys.isNative[${cc.sys.isNative}]`);
      this._returnUpdateCallback = (code: HotUpdateCode, error?) => {
        this._updating = false;
        cc.log(`HotUpdate::_returnUpdateCallback code[${code}] updating[${this._updating}]`, error);
        resolve(code);
      };
      if (this._assetManager == null) {
        this._returnUpdateCallback(HotUpdateCode.ERROR, "Manifest is null");
      }
      if (cc.sys.isNative) {
        cc.log("HotUpdate::init::onPressedCheckUpdate");
        this.onPressedCheckUpdate();
      } else {
        cc.log("HotUpdate::init::HotUpdateCode.NOT_NATIVE");
        this._returnUpdateCallback(HotUpdateCode.NOT_NATIVE);
      }
    });
  }

  onPressedCheckUpdate() {
    cc.log("HotUpdate::onPressedCheckUpdate", this._assetManager.getState());
    if (this._updating) {
      this.setInfoString("Updating new magics...");
      return;
    }
    if (this._assetManager.getState() === jsb.AssetsManager.State.UNINITED) {
      // Resolve md5 url
      let url = this._manifestUrl.nativeUrl;
      if (cc.loader.md5Pipe) {
        url = cc.loader.md5Pipe.transformURL(url);
      }
      this._assetManager.loadLocalManifest(url);
    }
    if (!this._assetManager.getLocalManifest() || !this._assetManager.getLocalManifest().isLoaded()) {
      this.setInfoString("Failed to load local manifest ...");
      return;
    }
    this._assetManager.setEventCallback(this.onCheckUpdate.bind(this));

    cc.log("HotUpdate::onPressedCheckUpdate::checkUpdate()");
    this._assetManager.checkUpdate();
    this._updating = true;
  }

  onPressedUpdate() {
    cc.log("HotUpdate::onPressedUpdate()");
    // this.retryButton.interactable = false;
    if (this._assetManager && !this._updating) {
      this._assetManager.setEventCallback(this.onUpdate.bind(this));

      // assetManager hasn't init
      if (this._assetManager.getState() === jsb.AssetsManager.State.UNINITED) {
        // Resolve md5 url
        let url = this._manifestUrl.nativeUrl;
        // md5 security
        if (cc.loader.md5Pipe) {
          url = cc.loader.md5Pipe.transformURL(url);
        }
        this._assetManager.loadLocalManifest(url);
      }

      this._failCount = 0;
      this._assetManager.update();
      // this.updateButton.interactable = false;
      this._updating = true;
    }
  }

  onPressedRetry() {
    if (!this._updating && this._canRetry) {
      // this.retryButton.interactable = false;
      this._canRetry = false;

      this.setInfoString("Hang in there!");
      this._assetManager.downloadFailedAssets();
    }
  }

  onUpdate(event) {
    // cc.log("onUpdate callback", event);
    this.setInfoString("We are updating...");
    this._needRestart = false;
    let failed = false;
    switch (event.getEventCode()) {
      case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
        cc.log("No local manifest file found, hot update skipped.");
        // this.setInfoString("No local manifest file found, hot update skipped.");
        failed = true;
        break;
      case jsb.EventAssetsManager.UPDATE_PROGRESSION:
        this.progressBar.progress = event.getDownloadedFiles() / event.getTotalFiles();
        // this.byteProgressLabel.string = `${event.getPercent()}`;
        // this.fileProgressLabel.string = `${event.getPercentByFile()}`;

        // this.fileLabel.string = event.getDownloadedFiles() + " / " + event.getTotalFiles();
        // this.byteLabel.string = event.getDownloadedBytes() + " / " + event.getTotalBytes();

        let msg = event.getMessage();
        if (msg) {
          // this.setInfoString("Updated file: " + msg);
          cc.log(event.getPercent() / 100 + "% : " + msg);
        }
        break;
      case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
      case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
        cc.log("@@@@@@@@@@ UPDATE::ERROR @@@@@@@@@@");
        cc.log("Fail to download manifest file, hot update skipped.");
        // this.setInfoString("Fail to download manifest file, hot update skipped.");
        failed = true;
        break;
      case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
        this.setInfoString("Gets Ready.");
        failed = true;
        break;
      case jsb.EventAssetsManager.UPDATE_FINISHED:
        cc.log("Update finished. " + event.getMessage());
        this.setInfoString("We going to restart the game for you(If something wrong kindly help us).");
        this._needRestart = true;
        break;
      case jsb.EventAssetsManager.UPDATE_FAILED:
        cc.log("Update failed. " + event.getMessage());
        // this.setInfoString("Update failed. " + event.getMessage());
        // this.retryButton.interactable = true;
        this._updating = false;
        this._canRetry = true;
        break;
      case jsb.EventAssetsManager.ERROR_UPDATING:
        cc.log("HotUpdate::onUpdate::jsb.EventAssetsManager.ERROR_UPDATING");
        if (!this._hasErrorUpdating) {
          this._hasErrorUpdating = true;
          this._hasErrorTime = new Date().getTime();
        }

        if (this._hasErrorUpdating && this._hasErrorTime != -1) {
          let currentTimeout = new Date().getTime() - this._hasErrorTime;
          if (currentTimeout >= this._ErrorUpdateTimeout) {
            failed = true;
            this._canRetry = false;
            this._hasErrorTime = -1;
            this._hasErrorUpdating = false;
            cc.log("HotUpdate::onUpdate::Abort updating");
          }
        }
        cc.log("Asset update error: " + event.getAssetId() + ", " + event.getMessage());
        // this.setInfoString("Asset update error: " + event.getAssetId() + ", " + event.getMessage());
        break;
      case jsb.EventAssetsManager.ERROR_DECOMPRESS:
        this.setInfoString(event.getMessage());
        break;
      default:
        break;
    }

    if (failed) {
      this._assetManager.setEventCallback(null);
      this._updateListener = null;
      this._updating = false;
      if (!this._canRetry) {
        this._returnUpdateCallback(HotUpdateCode.ERROR, "Something wrong when trying to update.");
      }
    }

    if (this._needRestart) {
      this._assetManager.setEventCallback(null);
      this._updateListener = null;
      // Prepend the manifest's search path
      let searchPaths = jsb.fileUtils.getSearchPaths();
      let newPaths = this._assetManager.getLocalManifest().getSearchPaths();
      cc.log(JSON.stringify(newPaths));
      Array.prototype.unshift.apply(searchPaths, newPaths);
      // This value will be retrieved and appended to the default search path during game startup,
      // please refer to samples/js-tests/main.js for detailed usage.
      // !!! Re-add the search paths in main.js is very important, otherwise, new scripts won't take effect.
      cc.sys.localStorage.setItem("HotUpdateSearchPaths", JSON.stringify(searchPaths));
      jsb.fileUtils.setSearchPaths(searchPaths);

      if (NodeUtils.getCurrentSceneName() != "loading") {
        this.showRestartDialog();
      } else {
        cc.audioEngine.stopAll();
        cc.game.restart();
      }
      this._returnUpdateCallback(HotUpdateCode.SUCCESS);
    }

    if (this._canRetry) {
      this.onPressedRetry();
    }
  }

  showRestartDialog() {
    if (!this._needRestart) {
      return;
    }
    ACE.dialogManager.showNoticeDialog(
      "We have updated your game.\nWant to restart now for the new content?",
      NoticeDialogType.YES_NO,
      () => {
        this._needRestart = false;
        cc.audioEngine.stopAll();
        cc.game.restart();
      },
      () => {
        cc.log("User rejected");
      });
  }

  onAssetManagerVerify(path: string, asset: any) {
    // When asset is compressed, we don't need to check its md5, because zip file have been deleted.
    let compressed = asset.compressed;
    // Retrieve the correct md5 value.
    let expectedMD5 = asset.md5;
    // asset.path is relative path and path is absolute.
    let relativePath = asset.path;
    // The size of asset file, but this value could be absent.
    let size = asset.size;
    if (compressed) {
      this.setInfoString("Verification passed : " + relativePath + " - size: " + size);
      return true;
    } else {
      this.setInfoString("Verification passed : " + relativePath + " (" + expectedMD5 + ")" + " - size: " + size);
      return true;
    }
  }

  onCheckUpdate(event) {
    cc.log("HotUpdate::onPressedCheckUpdate");
    cc.log("Code: " + event.getEventCode(), jsb.EventAssetsManager);
    let needUpdate = false;
    let isUpToDate = false;
    let hasCheckIssue = true;
    switch (event.getEventCode()) {
      case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
        cc.log("No local manifest file found, hot update skipped.");
        // this.setInfoString("No local manifest file found, hot update skipped.");
        break;
      case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
      case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
        cc.log("@@@@@@@@@@ CHECK_UPDATE::ERROR @@@@@@@@@@");
        cc.log("Fail to download manifest file, hot update skipped.");
        // this.setInfoString("Fail to download manifest file, hot update skipped.");
        break;
      case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
        cc.log("Already up to date with the latest remote version.");
        hasCheckIssue = false;
        isUpToDate = true;
        // this.setInfoString("Already up to date with the latest remote version.");
        break;
      case jsb.EventAssetsManager.NEW_VERSION_FOUND:
        this.setInfoString("We've found something new for you.");
        hasCheckIssue = false;
        needUpdate = true;
        // this.checkButton.interactable = false;
        // this.fileProgressLabel.string = `${0}`;
        // this.byteProgressLabel.string = `${0}`;
        break;
      default:
        return;
    }

    this._assetManager.setEventCallback(null);
    // this._checkListener = null;
    this._updating = false;

    if (needUpdate) {
      this.onPressedUpdate();
    }

    if (isUpToDate && this._needRestart) {
      this.showRestartDialog();
    }

    if (isUpToDate && !needUpdate) {
      this._updating = false;
      this._returnUpdateCallback(HotUpdateCode.UP_TO_DATE);
    }

    if (hasCheckIssue) {
      this._returnUpdateCallback(HotUpdateCode.ERROR, "Something wrong while check for update.");
    }
  }

  /**
   * Setup your own version compare handler, versionA and B is versions in string
   * if the return value greater than 0, versionA is greater than B,
   * if the return value equals 0, versionA equals to B,
   * if the return value smaller than 0, versionA is smaller than B.
   * @param versionA
   * @param versionB
   */
  versionCompare(versionA: string, versionB: string) {
    cc.log("JS Custom Version Compare: version A is " + versionA + ", version B is " + versionB);
    let vA = versionA.split(".");
    let vB = versionB.split(".");
    for (let i = 0; i < vA.length; ++i) {
      let a = parseInt(vA[i]);
      let b = parseInt(vB[i] || "0");
      if (a === b) {
        continue;
      } else {
        return a - b;
      }
    }
    if (vB.length > vA.length) {
      return -1;
    } else {
      return 0;
    }
  }

  setInfoString(info: string) {
    let currentSceneName = NodeUtils.getCurrentSceneName();
    if (currentSceneName == "loading") {
      this.infoLabel.string = info;
    } else {
      cc.log("HotUpdate::SetInfoString", info);
    }
  }

  onDestroy() {
    if (this._updateListener) {
      this._assetManager.setEventCallback(null);
      this._updateListener = null;
    }
  }
}
