import { StringUtils } from "./utils/string-utils";
import SceneManager from "./scene-manager";
import TopBar from "../common/top-bar";
import { HttpClient } from "./http-client";
import { NativeManager } from "../managers/native-manager";
import DialogManager from "./dialog-manager";
import WebviewManager from "../managers/webview-manager";
import ResourceManager from "./resources-manager";
import FairyDialog from "../common/fairy-dialog";
import GameSocket from "./networks/GameSocket";

export interface IME {
  sceneManager?: SceneManager;
  gameSocket?: GameSocket;
  qaRenderSocket?: GameSocket;
  string?: StringUtils;
  topBar?: TopBar;
  http?: HttpClient;
  native?: NativeManager;
  dialogManager?: DialogManager;
  webviewManager?: WebviewManager;
  resManager?: ResourceManager;
  fairyDialog?: FairyDialog;
}

export const ACE: IME = {};
