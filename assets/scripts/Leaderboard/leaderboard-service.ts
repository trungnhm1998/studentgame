import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

export default class LeaderboardService {
  // static loadLeaderboardList(mode : string, time : string){ <----- we dont need to declare type of params
  static loadLeaderboardList(time) {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_LEADERBOARD_LIST, {
      mode: "MathHouse",
      time
    });
  }
}
