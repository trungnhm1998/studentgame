import { STORAGE_KEY } from "./../core/constant";
import { IItemChanged } from "./../models/profile";
const { ccclass, property } = cc._decorator;
import { ACE } from "../core/global-info";
import { SERVER_EVENT, DIALOG_TYPE } from "../core/constant";
import AccountManager from "../managers/account-manager";
import LoginService from "../login/login-service";
import { IServerResponse } from "../core/interfaces/server-response";
import SchoolService from "../school/school-service";
import SchoolManager from "../managers/school-manager";
import { ModelUtils } from "../core/utils/model-utils";
import { Config } from "../config";
import LoadingDialog from "../dialogs/loading-dialog";
import { NoticeDialogType } from "../dialogs/notice-dialog";

@ccclass
export default class GameScene extends cc.Component {
  static baseEventsRegistered = false;

  name: string = "";
  designRatio: number;
  ratio: number;
  uiRatio: number;
  passData: any = {};
  onLoad() {
    this.name = this.node.name;
    // load or register all the events
    this.registerEvents();
  }

  registerEvents(): void {
    if (GameScene.baseEventsRegistered) {
      return;
    }
    GameScene.baseEventsRegistered = true;
    ACE.gameSocket.on(SERVER_EVENT.LOGOUT, (data) => { this.onLogout(data); });
    ACE.gameSocket.on(SERVER_EVENT.FORCE_LOGOUT, (data) => this.onForceLogout(data));
    ACE.gameSocket.on(SERVER_EVENT.CLIENT_LOGOUT, (data) => {
      cc.log("GameScene::CLIENT_LOGOUT", data);
    });
    ACE.gameSocket.on(SERVER_EVENT.ERROR, (data) => this.onError(data));
    ACE.gameSocket.on(SERVER_EVENT.ENROLMENT_LIST, (data) =>
      this.onEnrolmentList(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.GET_HOMEWORK_FOR_MATH_HOUSE, (data) => {
      this.onGetHomeworkForMathHouse(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_IMPROVEMENT_FOR_MATH_HOUSE, (data) => {
      this.onGetImprovementForMathHouse(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_NAPLAN_FOR_MATH_HOUSE, (data) => {
      this.onGetNaplanForMathHouse(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_FOCUS_FOR_MATH_HOUSE, (data) => {
      this.onGetFocusForMathHouse(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_REVISION_FOR_MATH_HOUSE, (data) => {
      this.onGetRevisionForMathHouse(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_ASSIGNMENT_FOR_MATH_HOUSE, (data) => {
      this.onGetAssignmentForMathHouse(data);
    });
    ACE.gameSocket.on(SERVER_EVENT.PROFILE_ITEM_CHANGED, (data: IItemChanged) => {
      if (data) {
        if (data.coins && data.coins.value) {
          ACE.topBar.updateCoin(data.coins.value);
        }
        if (data.stars && data.stars.value) {
          ACE.topBar.updateStar(data.stars.value);
        }
      }
    });
    ACE.gameSocket.on(SERVER_EVENT.STUDENT_COINS_STARS, (data) => {
      cc.log("STUDENT_COINS_STARS", data);
      ACE.topBar.updateCoin(data.Coins);
      ACE.topBar.updateStar(data.Stars);
    });
  }

  onEnrolmentList(result) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(40);
    SchoolService.enrolments = result;
    SchoolService.enrolmentId = result[0].ID;
    SchoolService.getNaplanForMathHouse(SchoolService.enrolmentId);
  }

  onGetNaplanForMathHouse(naplanData: any) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(50);
    const manager = SchoolManager.getInstance();
    manager.naplanData = naplanData;
    if (!manager.naplanData) {
      manager.naplanData = {};
      manager.naplanData.state = "disabled";
    } else if (manager.naplanData.NumberQATemplate == 0) {
      manager.naplanData.state = "disabled";
    } else if (manager.naplanData.Finished) {
      manager.naplanData.state = "replay";
    } else if (manager.naplanData.NumberQaAttempt || manager.naplanData.Duration < manager.naplanData.Time) {
      manager.naplanData.state = "continue";
    } else {
      manager.naplanData.state = "start";
    }
    SchoolService.getImprovementForMathHouse(SchoolService.enrolmentId); // calling the next one
  }

  onGetAssignmentForMathHouse(assignmentData: any) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(95);
    const manager = SchoolManager.getInstance();
    manager.assignmentData = assignmentData;
    if (!manager.assignmentData) {
      manager.assignmentData = {};
      manager.assignmentData.state = "disabled";
    } else if (manager.assignmentData.NumberQATemplate == 0) {
      manager.assignmentData.state = "disabled";
    } else if (manager.assignmentData.Finished) {
      manager.assignmentData.state = "replay";
    } else if (manager.assignmentData.NumberQaAttempt) {
      manager.assignmentData.state = "continue";
    } else {
      manager.assignmentData.state = "start";
    }
    if (!ACE.webviewManager.openingWebview) {
      ACE.dialogManager.hideLoading();
    }
    AccountManager.getInstance().gettingSchoolData = false;
  }

  onGetImprovementForMathHouse(improvementData: any) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(60);
    const manager = SchoolManager.getInstance();
    manager.improvementData = improvementData;
    if (!manager.improvementData) {
      manager.improvementData = {};
      manager.improvementData.state = "disabled";
    } else {
      manager.improvementData.state = "start";
    }
    SchoolService.getFocusForMathHouse(SchoolService.enrolmentId);
  }

  onGetHomeworkForMathHouse(homeworkData: any) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(80);
    const manager = SchoolManager.getInstance();
    manager.homeworkData = homeworkData;
    // if manager.homeworkData == undefined
    if (!manager.homeworkData) {
      manager.homeworkData = {};
      manager.homeworkData.state = "disabled";
    } else if (manager.homeworkData.NumberQATemplate == 0) {
      manager.homeworkData.state = "disabled";
    } else if (manager.homeworkData.Finished) {
      manager.homeworkData.state = "replay";
    } else if (manager.homeworkData.NumberQaAttempt) {
      manager.homeworkData.state = "continue";
    } else {
      manager.homeworkData.state = "start";
    }
    SchoolService.getRevisionForMathHouse(SchoolService.enrolmentId);
  }

  onGetFocusForMathHouse(focusData: any) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(70);
    const manager = SchoolManager.getInstance();
    manager.focusData = focusData;
    if (!manager.focusData) {
      manager.focusData = {};
      manager.focusData.state = "disabled";
    } else {
      manager.focusData.state = "start";
    }
    SchoolService.getHomeworkForMathHouse(SchoolService.enrolmentId);
  }

  onGetRevisionForMathHouse(revisionData: any) {
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(90);
    const manager = SchoolManager.getInstance();
    manager.revisionData = revisionData;
    if (!manager.revisionData) {
      manager.revisionData = {};
      manager.revisionData.state = "disabled";
    } else if (manager.revisionData.NumberQATemplate == 0) {
      manager.revisionData.state = "disabled";
    } else if (manager.revisionData.Finished) {
      manager.revisionData.state = "replay";
    } else if (manager.revisionData.NumberQaAttempt) {
      manager.revisionData.state = "continue";
    } else {
      manager.revisionData.state = "start";
    }
    SchoolService.getAssignmentForMathHouse(SchoolService.enrolmentId);
  }

  onLogout(data: any) {
    ACE.gameSocket.emit(SERVER_EVENT.CLIENT_LOGOUT, {});
  }

  onError(data: IServerResponse) {
    cc.log("Server send error", data.error);
    if (data.expired) {
      AccountManager.getInstance().accessToken = null;
    }
  }

  // #region abstract method
  /**
   * Call when scene leave this will call after onInvisible and onDisable
   */
  onLeave() {
  }

  /**
   * call before opacity = 255 or active = true
   */
  onEnter() {
    ACE.resManager.saveStorage(STORAGE_KEY.LAST_ENTER_STATE, { lastEnterState: this.name });
  }

  /**
   * Call when opacity = 255 usually when finish some effect or just hidden
   */
  onVisible() {
  }

  /**
   * Oposite of onVisible
   */
  onInvisible() {
  }

  /**
   * When clear stack/forceReload the scene
   */
  onCleanup() {
  }

  onForceLogout(data: any) {
    if (data.username == AccountManager.getInstance().userName) {
      LoginService.logout();
      ACE.dialogManager.showNoticeDialog("Are you logging somewhere else?", NoticeDialogType.OK);
    }
  }

  onResize() {
    this.designRatio = Config.designWidth / Config.designHeight;
    this.ratio = cc.winSize.width / cc.winSize.height;
    this.uiRatio = Math.min(this.designRatio / this.ratio, 1);
    this.node.scale = this.uiRatio;
    cc.log(`GameScene::onResize screen[${cc.winSize.width}-${cc.winSize.height}] designRation[${this.designRatio}] ratio[${this.ratio}] uiRatio[${this.uiRatio}] this.designRatio / this.ratio[${this.designRatio / this.ratio}]`);
  }

  getScaleRatio(): number {
    cc.log(`GameScene::getScaleRatio cc.winSize.height / 720[${cc.winSize.height / Config.designHeight}] cc.winSize.width / 1280 [${cc.winSize.width / Config.designWidth}]`);
    return this.designRatio > this.ratio ? cc.winSize.height / Config.designHeight : cc.winSize.width / Config.designWidth;
    // return this.designRatio / this.ratio;
  }
  // #endregion
}
