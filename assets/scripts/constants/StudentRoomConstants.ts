export const STUDENT_ROOM = {
    //DEFINE StudentRoom
    ID_BED_ROOM: 0,
    ID_LIVING_ROOM: 1,
    ID_KITCHEN_ROOM: 2,
    ID_SHOW_ROOM: 3,

    //define stirng Room
    BED_ROOM: "BedRoom",
    LIVING_ROOM: "LivingRoom",
    SHOW_ROOM: "Showroom",
    KITCHEN_ROOM: "Kitchen",
};

export const MOVE_ROOM = {
    //DEFINE MOVE ROOM
    UP: 0,
    LEFT: 1,
    DOWN: 2,
    RIGHT: 3,
}
