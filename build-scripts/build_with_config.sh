#!/usr/bin/env bash

# CONSTANT GLOBAL
PROJECT_DIR=$(pwd)
COCOS_CREATOR_EXECUTE=/Applications/CocosCreator.app/Contents/MacOS/CocosCreator
# Build props
configPath="$1"

finalBuildFlag="configPath=$configPath"

# rm -r -f $PROJECT_DIR/build

$COCOS_CREATOR_EXECUTE --path $PROJECT_DIR --build "$finalBuildFlag"