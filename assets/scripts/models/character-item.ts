class CharacterItem {
  CharacterProductPresentationID: number = 531;
  CharacterWearingGroupID: number = 21;
  Name: string = "P_L_B_CHAR2_BOY_HAIR_BASIC_BLACK_HAIR";
  Url: string =
    "character/products/large/boy/CHAR2_BOY_HAIR_BASIC_BLACK_HAIR.png";
  index: number = 1;
}

class UsingItems {
  ID: number = 249;
  CharacterProductPresentationID: number = 461;
  CharacterWearingGroupID: number = 53;
  FileNameAssetKey: string = "P_L_G_CHAR2_GIRL_SKIN_RED";
  FileNameAssetData: string = "0";
  isFullSet: boolean = false;
  isFix: boolean = true;
}

class UsingItemsAsset {
  ID: number = 249;
  Name: string = "";
  CharacterProductPresentationID: number = 461;
  Url: string = "character/products/large/girl/CHAR2_GIRL_SKIN_RED.png";
  UrlJson: string = "";
  TypeName: string = "image";
  Width: 0;
  Height: 0;
  CharacterWearingGroupID: 53;
  isFix: boolean = true;
}

export { CharacterItem, UsingItems, UsingItemsAsset };
