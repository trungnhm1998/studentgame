import { ACE } from "../../core/global-info";
import { SERVER_EVENT, DIALOG_TYPE } from "../../core/constant";
import MyClassService from "./my-class-service";
import MapService from "../map-service";
import AccountManager from "../../managers/account-manager";
import Map from "../map";
import { IFriend } from "../../models/friend";
import PetNotify from "../../common/pet-notify";
import Friend from "./friend-component";
import { Random } from "../../core/utils/random-utils";
import StudentHouseManager from "../../student-house/StudentHouseManager";
import LoadingDialog from "../../dialogs/loading-dialog";
import AudioManager from "../../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MyClass extends cc.Component {
  @property(cc.Prefab)
  friendPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  giftAnimationPF: cc.Prefab = null;

  @property(cc.Node)
  woodenPopup: cc.Node = null;

  @property(cc.Node)
  petContainer: cc.Node = null;

  @property(cc.Node)
  whiteBackgroundNode: cc.Node = null;

  @property(cc.Node)
  friendsContainer: cc.Node = null;

  @property(PetNotify)
  petNotifier: PetNotify = null;

  @property(cc.SpriteFrame)
  welcomeBoySpriteFrame: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  welcomeGirlSpriteFrame: cc.SpriteFrame = null;

  @property(cc.Node)
  welcomeNode: cc.Node = null;

  @property(cc.Sprite)
  welcomeSprite: cc.Sprite = null;

  @property(cc.Label)
  welcomeLabel: cc.Label = null;

  @property(cc.Node)
  thanksNode: cc.Node = null;

  @property(cc.Label)
  thanksLabel: cc.Label = null;

  _friendsComponent: Friend[] = [];
  _selectedFriend: Friend = null;

  // Global Variables
  normalEmotions: string[] = [
    "Come on, your friends want you to visit!\nSee how cool their room is!",
    "Excited to send a gift?!\nTry clicking on your friend's name to send an awesome gift!",
    "Wanna visit your friend's house?\nHover over their name and click on the house!",
  ];
  oneSentEmotions: string[] = [
    " just received the gift.\nAnd they LOVED it! 😍😍😍\nThanks for being awesome!",
    " says you are the best!\nSome of your friends are jealous! 🤩🤩🤩",
    " can't stop smiling! 😁😁😁\nEveryone wants your gift!!!",
  ];
  twoSentEmotions: string[] = [
    "Your friends love you!!!\n",
    "Everyone said you are their favourite!\n",
    "Your gifts were awesome sauce!\n",
  ];
  allSentEmotions: string[] = [
    "You are so popular!\nCome back tomorrow to send more gifts.\nNow, let's visit someone's house! 🏡🏡🏡.",
    "You are the gift master! 😎😎😎\nCome back tomorrow and send more awesome gifts!\nDon't forget to visit your friends!",
  ];
  allSentPopupMessage: string[] = [
    "Horray!!!\nWe love you! \n😍😍😍",
    "Woohoo!!!\nYou are so cool! \n😎😎😎",
    "Yippee!!!\nWe loved your gift! \n🤗🤗🤗",
  ];
  welcomePopupMessage: string[] = [
    ", welcome to my home!",
    ", let's go inside!",
    ", always good to see you!",
    ", let's play inside!",
  ];
  friendsNotReceived: IFriend[] = [];
  mapComponent: Map = null;
  friendList: IFriend[] = [];
  myClassPosition: cc.Vec2;
  originalPosition: cc.Vec2;

  onLoad() {
    ACE.gameSocket.on(SERVER_EVENT.SEND_GIFT, (data) => {
    });
    ACE.gameSocket.on(SERVER_EVENT.OPEN_ROOM, (data) => {
      if (ACE.sceneManager.currentScene.name == "map") {
        ACE.sceneManager.pushScene("student-house");
      }
    });
    this.originalPosition = cc.v2(this.woodenPopup.getPosition().x, this.woodenPopup.getPosition().y);
    this.initData();
  }

  onEnable() {
    this.initData();
  }

  initData() {
    this.welcomeNode.active = false;
    this.friendsNotReceived = this.friendList.filter((iFriend: IFriend) => iFriend.isSentGift == 0);

    this.thanksNode.parent.off(cc.Node.EventType.TOUCH_END);
    this.thanksNode.parent.on(cc.Node.EventType.TOUCH_END, () => {
      this.thanksNode.parent.active = false;
    });
    this.thanksNode.parent.active = false;

    const strText = `Welcome ${AccountManager.getInstance().studentFirstName} ${AccountManager.getInstance().studentLastName}! ${new Random().fromList(this.normalEmotions)}`;
    this.petNotifier.setNotifyMessage(strText);
    this.buildFriendList();
  }

  start() {
  }

  show() {
    ACE.topBar.toggleButtonGroup(false);
    ACE.dialogManager.hideLoading();
    this.myClassPosition = this.woodenPopup.parent.convertToNodeSpaceAR(this.mapComponent.myClassIcon.parent.convertToWorldSpaceAR(this.mapComponent.myClassIcon.getPosition()));
    this.node.active = true;
    ACE.topBar.hide();
    const moveinTime = 0.3;

    this.woodenPopup.scaleX = 0.1;
    this.woodenPopup.scaleY = 0.1;
    this.woodenPopup.x = this.myClassPosition.x;
    this.woodenPopup.y = this.myClassPosition.y;
    this.woodenPopup.opacity = 0;
    this.petNotifier.node.x = -cc.winSize.width;

    this.whiteBackgroundNode.opacity = 0;
    this.whiteBackgroundNode.runAction(cc.fadeTo(0.5, 200));

    this.woodenPopup.runAction(cc.sequence(
      cc.spawn(
        cc.fadeTo(moveinTime, 255),
        cc.scaleTo(moveinTime, 1),
        cc.moveTo(moveinTime, cc.v2(this.originalPosition.x, this.originalPosition.y)),
      ),
      cc.callFunc(() => {
        this.setDialogue(3 - AccountManager.getInstance().numberGift, "Your friend");
        this.petNotifier.node.runAction(cc.moveTo(moveinTime, cc.v2(-500, -500)));
      }),
    ));
  }

  hide() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    ACE.topBar.toggleButtonGroup(true);
    const moveoutTime = 0.3;
    this.petNotifier.node.runAction(cc.moveTo(moveoutTime, -cc.winSize.width, 0));
    this.woodenPopup.runAction(cc.sequence(
      cc.spawn(
        cc.moveTo(moveoutTime, this.myClassPosition.x, this.myClassPosition.y),
        cc.scaleTo(moveoutTime, 0.1),
        cc.fadeTo(moveoutTime, 0),
      ),
      cc.callFunc(() => {
        this.whiteBackgroundNode.runAction(cc.sequence(
          cc.fadeTo(moveoutTime, 0),
          cc.callFunc(() => {
            this.node.active = false;
            ACE.topBar.show();
          }),
        ));
      }),
    ));
  }

  buildFriendList() {
    this._friendsComponent = [];
    this.friendsContainer.removeAllChildren();
    for (let i = 0; i < this.friendList.length; i++) {
      const iFriend = this.friendList[i];
      const friendNode = cc.instantiate(this.friendPrefab);
      const friendComponent: Friend = friendNode.getComponent(Friend);
      this.friendsContainer.addChild(friendNode);
      friendComponent.init(iFriend, (friend: IFriend) => this.sendGift(friend), (friend: IFriend) => this.visitHome(friend));
      this._friendsComponent.push(friendComponent);
    }
  }

  setDialogue(count: number, friendName: string) {
    let strText = "";
    switch (count) {
      case 0:
        strText = `Welcome ${AccountManager.getInstance().studentFirstName} ${
          AccountManager.getInstance().studentLastName
          }!.\n${new Random().fromList(this.normalEmotions)}`;
        break;
      case 1: {
        strText = `${friendName} ${new Random().fromList(this.oneSentEmotions)}`;
        break;
      }
      case 2: {
        strText = `${new Random().fromList(this.twoSentEmotions)} ${ACE.string.limitNameChar(new Random().fromList(this.friendsNotReceived).FullName)} wants a gift too!\nSend some 🎁🎁🎁!`;
        break;
      }
      case 3: {
        strText = `${new Random().fromList(this.allSentEmotions)}`;
        break;
      }
      case 4: {
        strText = `${friendName} already received the gift.\n${ACE.string.limitNameChar(new Random().fromList(this.friendsNotReceived).FullName)} is waiting for your gift!`;
        break;
      }
      case 5: {
        strText = `Visit again tomorrow to send more gifts!\nWhy not visit your friends now? 🏡🏡🏡`;
        break;
      }
    }
    this.petNotifier.setNotifyMessage(strText);
  }

  sendGift(iFriend: IFriend) {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND(TEMP)");
    const accountInstance = AccountManager.getInstance();
    if (accountInstance.numberGift > 0) {
      MyClassService.sendGift(iFriend.ID, iFriend.FullName);

      this.friendList.find((friend) => friend == iFriend).isSentGift = 1;
      this.friendsNotReceived = this.friendList.filter((friend) => friend.isSentGift == 0);
      accountInstance.numberGift--;
      this.setDialogue(3 - accountInstance.numberGift, iFriend.FullName);

      const anim = cc.instantiate(this.giftAnimationPF);
      anim.parent = this.node;
      anim.setPosition(0, 0);
      this.btnGiftAnimate(anim);

      const animation = anim.getComponent(cc.Animation).play();
      animation.speed = 2;
      animation.wrapMode = cc.WrapMode.Loop;
      animation.repeatCount = Infinity;

      if (accountInstance.numberGift == 0) {
        this.thanksNode.parent.active = true;
        this.thanksLabel.string = new Random().fromList(this.allSentPopupMessage);
        this.thanksNode.opacity = 0;
        this.thanksNode.scale = 0;
        this.thanksNode.runAction(cc.spawn(
          cc.fadeTo(0.3, 255),
          cc.scaleTo(0.3, 2, 2),
        ));
      }
    } else {
      this.setDialogue(3, iFriend.FullName);
    }
  }

  visitHome(iFriend: IFriend) {
    this._selectedFriend = this._friendsComponent.find((friend) => friend.iFriend.ID == iFriend.ID);
    this.welcomeNode.active = true;
    if (iFriend.Gender) {
      this.welcomeSprite.spriteFrame = this.welcomeBoySpriteFrame;
    } else {
      this.welcomeSprite.spriteFrame = this.welcomeGirlSpriteFrame;
    }
    this.welcomeLabel.string = "Hi " + ACE.string.limitNameChar(AccountManager.getInstance().profile.firstname.toUpperCase()) + new Random().fromList(this.welcomePopupMessage);
  }

  visitFriendHouse() {
    cc.log("MyClassTS::visitFriendHouse friendId", this._selectedFriend.iFriend.ID);
    ACE.dialogManager.showLoading();
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).progress.node.active = false;
    StudentHouseManager.getInstance().isVisitFriendHouse = true;
    StudentHouseManager.getInstance().idStudent = this._selectedFriend.iFriend.ID;
    MapService.openRoom({
      studentId: this._selectedFriend.iFriend.ID,
      isFriend: true,
      roomName: "BedRoom",
    });
    this.closeWelcome();
    this.hide();
  }

  closeWelcome() {
    this.welcomeNode.active = false;
  }

  btnGiftAnimate(btnGift) {
    // Random animation variables
    let randX1 = 0;
    let randY1 = 0;
    let randAngle1 = 0;
    let randScale1 = 0;
    let randTime1 = 0;
    let randX2 = 0;
    let randY2 = 0;
    let randAngle2 = 0;
    let randScale2 = 0;
    let randTime2 = 0;
    let randX3 = 0;
    let randY3 = 0;
    let randAngle3 = 0;
    let randScale3 = 0;
    let randTime3 = 0;
    randX1 = Math.floor(Math.random() * 1000 - 500); // -500, 500
    randY1 = Math.floor(Math.random() * 400 - 200); // -200, 200
    randScale1 = Math.random() * 0.1 + 1.25; // 1.25, 1.35
    randAngle1 = Math.floor(Math.random() * 60 - 30); // -30, 30
    randTime1 = Math.random() * 0.2 + 0.4; // 400,600ms
    randX2 =
      randX1 > 0
        ? randX1 + Math.floor(Math.random() * 200 + 300)
        : randX1 + Math.floor(Math.random() * 150 - 400); // 300,500 : -400,-250
    randY2 =
      randY1 > 0
        ? randY1 + Math.floor(Math.random() * 150 + 200)
        : randY1 + Math.floor(Math.random() * 200 - 300); // 200,350 : -300,-100
    randAngle2 = Math.floor(Math.random() * 30 - 15); // -15, 15
    randScale2 = Math.random() * 0.3 + 0.6; // 0.6, 0.9
    randTime2 = Math.random() * 0.2 + 0.4; // 400, 600ms
    randX3 =
      randX2 > 0
        ? randX2 + Math.floor(Math.random() * 300 + 900)
        : randX2 + Math.floor(Math.random() * 400 - 1300); // 900,1200: -1300,-900
    randY3 =
      randY2 > 0
        ? randY2 + Math.floor(Math.random() * 100 + 300)
        : randY2 + Math.floor(Math.random() * 200 - 500); // 300,400: -500,-300
    randAngle3 = Math.floor(Math.random() * 20 - 10); // -10, 10
    randScale3 = Math.random() * 0.2 + 0.2; // 0.2, 0.4
    randTime3 = Math.random() * 0.1 + 0.45; // 450,550ms

    btnGift.runAction(
      cc.sequence(
        cc.spawn(
          cc.moveTo(randTime1, randX1, randY1),
          cc.scaleTo(randTime1, randScale1),
          cc.rotateTo(randTime1, randAngle1),
        ),
        cc.spawn(
          cc.moveTo(randTime2, randX2, randY2),
          cc.scaleTo(randTime2, randScale2),
          cc.rotateTo(randTime2, randAngle2),
        ),
        cc.spawn(
          cc.moveTo(randTime3, randX3, randY3),
          cc.scaleTo(randTime3, randScale3),
          cc.rotateTo(randTime3, randAngle3),
        ),
      ),
    );
    // emitter.start(false, 4000, 1);
    this.scheduleOnce(() => {
      btnGift.removeFromParent();
    }, randTime1 + randTime2 + randTime3);
  }
}
