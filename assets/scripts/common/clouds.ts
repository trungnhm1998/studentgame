import { Random } from "../core/utils/random-utils";

const { ccclass, property } = cc._decorator;
/**
 * Parent node is an empty node and is a child of the map
 * Parent node is positioned at 0,0 as a child of map [map isn't positioned at 0,0 due to its wiered shape]
 * You can mode clouds to the node as children and clouds.js calculates random positioning
 * and timing for each clouds animations.
 *
 * This script comes handy when there are somany clouds to be initiated. If number of clouds to
 * be implemented are less than 4, postioning them manually will be optimal.
 */

@ccclass
export default class Clounds extends cc.Component {
  @property({
    type: cc.Integer,
    tooltip: 'Maximum distance a cloud can move'
  })
  maxDistance: number = 400;

  @property({
    type: cc.Integer,
    tooltip: 'Minimum distance a cloud has to move'
  })
  minDistance: number = 150;

  @property({
    type: cc.Integer,
    tooltip: 'Minimum speed for a cloud'
  })
  minSpeed: number = 10;

  @property({
    type: cc.Integer,
    tooltip: 'Maximum speed for a cloud'
  })
  maxSpeed: number = 12;

  @property({
    type: cc.Integer,
    tooltip: 'Minimum x value for a cloud'
  })
  minXPos: number = -cc.winSize.width;
  
  @property({
    type: cc.Integer,
    tooltip: 'Maximum x value for a cloud'
  })
  maxXPos: number = cc.winSize.width;

  @property({
    type: cc.Integer,
    tooltip: 'Maximum y value for a cloud'
  })
  maxYPos: number = 1200;

  @property({
    type: cc.Integer,
    tooltip: 'Minimum y value for a cloud'
  })
  minYPos: number = 1050;

  clouds: cc.Node[] = [];

  onLoad() {
    this.clouds = this.node.children;
  }

  start() {
    let x = 0;
    let y = 0;

    this.clouds.forEach(element => {
      let loopInterval = Math.random() * (this.maxSpeed - this.minSpeed) + this.minSpeed;
      let loopDistance = Math.floor(Math.random() * (this.maxDistance - this.minDistance) + this.minDistance);
      let initialDirection = Math.floor(Math.random() * 2);

      x = new Random().integer(this.minXPos, this.maxXPos);
      y = Math.floor(Math.random() * (this.maxYPos - this.minYPos) + this.minYPos);
      element.setPosition(x, y);
      element.runAction(this.cloudMovement(loopInterval, loopDistance, initialDirection));

    });
  }

  cloudMovement(interval, distance, direction) {
    var goLeft = cc.moveBy(interval, cc.v2(-distance, 0)).easing(cc.easeSineInOut());
    var goRight = cc.moveBy(interval, cc.v2(distance, 0)).easing(cc.easeSineInOut());
    return direction == 1 ? cc.repeatForever(cc.sequence(goRight, goLeft)) : cc.repeatForever(cc.sequence(goLeft, goRight));
  }
}
