#!/usr/bin/env bash

# CONSTANT GLOBAL
PROJECT_DIR=$(pwd)
COCOS_CREATOR_EXECUTE=/Applications/CocosCreator.app/Contents/MacOS/CocosCreator
IOS_PROJECT=build/jsb-default/frameworks/runtime-src/proj.ios_mac
# Build props
platform="ios"
startScene="2d2f792f-a40c-49bb-a189-ed176a246e49"
title="ACE_StarMathsOnline"
mergeStartScene="false"
androidStudio="true"
debug="false"
useDebugKeystore="true"
packageName="com.starmathsonline.student"
orientation="{'landscapeLeft': true, 'landscapeRight': true}"
template="default"
md5Cache="false"
encryptJs="true"
xxteaKey="cab41512-5847-4f"
zipCompressJs="true"
excludeScenes="['479df138-4821-465f-aff3-5849939d33bb', '4e219f4e-c994-45ae-9489-5be302c63f23']"
buildPath="$PROJECT_DIR/build"

finalBuildFlag="platform=$platform;startScene=$startScene;title=$title;mergeStartScene=$mergeStartScene;androidStudio=$androidStudio;debug=$debug;useDebugKeystore=$useDebugKeystore;appABIs=$appABIs;orientation=$orientation;template=$template;md5Cache=$md5Cache;encryptJs=$encryptJs;xxteaKey=$xxteaKey;zipCompressJs=$zipCompressJs;buildPath=$buildPath;excludeScenes=$excludeScenes;"

# rm -r -f $PROJECT_DIR/build

$COCOS_CREATOR_EXECUTE --path $PROJECT_DIR --build "$finalBuildFlag"

xcodebuild -project "$PROJECT_DIR/$IOS_PROJECT/$title.xcodeproj" -configuration Release -target "$title-mobile" -arch x86_64 -sdk iphonesimulator CONFIGURATION_BUILD_DIR="/Users/starios/Projects/ACECocosCreator/build/jsb-default/publish/ios" VALID_ARCHS="i386 x86_64"