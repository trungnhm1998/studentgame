export interface IServerResponse {
  args: any;
  error: string;
  dismiss: boolean;
  expired: boolean;
  data: any;
}

export interface AssetData {
  ID:          number;
  Name:        string;
  Description: string;
  Url:         string;
  UrlJson:     string;
  AssetTypeID: number;
  Width:       number;
  Height:      number;
  Frame:       number;
  TypeName:    string;
  GameState:   string;
}
