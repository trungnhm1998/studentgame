import { MiniGameType } from "./constants/minigame-constant";
const { ccclass, property } = cc._decorator;
const PetTappingName = "Pet Tapping";
const AddName = "Amazing Addition";
const SubtractionName = "Supper Subtraction";
const MultiplicationName = "Magnificeient Multiplication";
const DivisionsName = "Dynamic Divisions";
const MixName = "Supper Mix";

@ccclass
export default class MinigameController {
  static _instance: MinigameController;

  public static getInstance() {
    if (this._instance == null) {
      this._instance = new MinigameController();
    }
    return this._instance;
  }
  MinigameController() { }

  _currentGameType: MiniGameType = null;
  _levelSelected: number = null;
  _stageSelected: number = null;

  selectMiniGameType(gameType: MiniGameType) {
    this._currentGameType = gameType;
  }

  getMiniGameType() {
    return this._currentGameType;
  }

  getMiniGameName() {
    switch (this.getMiniGameType()) {
      case MiniGameType.PET_TAPPING:
        return PetTappingName;
      case MiniGameType.AMAZING_ADDITION:
        return AddName;
      case MiniGameType.SUPPER_SUBTRACTION:
        return SubtractionName;
      case MiniGameType.MAGNIFICIENT_MULTIPLICATION:
        return MultiplicationName;
      case MiniGameType.DYNAMIC_DIVISIONS:
        return DivisionsName;
      case MiniGameType.SUPER_MIX:
        return MixName;
    }
    return "";
  }

  //
  selectLevel(level: number) {
    this._levelSelected = level;
  }

  getLevelSelected() {
    return this._levelSelected;
  }

  selectStage(stage: number) {
    this._stageSelected = stage;
  }

  getStageSelected() {
    return this._stageSelected;
  }

  nextStage() {
    if (this._stageSelected >= 2) {
      this._levelSelected++;
      this._stageSelected = 0;
    } else {
      this._stageSelected++;
    }
  }
}
