import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import SelectTopic from "./select-topic";
import { SERVER_EVENT } from "../core/constant";
import IConcept from "../models/concept";
import SchoolService from "./school-service";
import SchoolManager from "../managers/school-manager";

const { ccclass, property } = cc._decorator;

export enum SchoolType {
  Practice = "practiseCenter",
  FocusCenter = "focusCenter",
  Improvement = "improvement",
}

@ccclass
export default class ChooseConcept extends GameScene {
  @property(SelectTopic)
  selectTopic: SelectTopic = null;

  @property([cc.Node])
  roomName: cc.Node[] = [];

  @property(cc.Button)
  chooseConcept: cc.Button = null;

  @property(cc.Node)
  questionAttempt: cc.Node = null;

  onLoad() {
    super.onLoad();
  }

  onResize() {
    this.node.getChildByName("background").scale = this.getScaleRatio();
  }

  onEnter() {
    super.onEnter();
    ACE.topBar.hide();
    switch (this.passData.schoolType) {
      case SchoolType.Practice:
        this.selectTopic.showPractice();
        this.roomName[0].active = true;
        break;
      case SchoolType.Improvement:
        if (this.passData.data !== null) {
          this.selectTopic.renderConcept(this.passData.data, false, SchoolType.Improvement);
        }
        this.roomName[1].active = true;
        break;
      case SchoolType.FocusCenter:
        if (this.passData.data !== null) {
          this.selectTopic.renderConcept(this.passData.data, false, SchoolType.FocusCenter);
        }
        this.roomName[2].active = true;
        break;
      default:
        break;
    }
    this.selectTopic.node.active = true;
    this.chooseConcept.node.active = true;
  }

  registerEvents() {
    ACE.gameSocket.on(SERVER_EVENT.LOAD_CONCEPT_FOR_PLAYNOW, (data) =>
      this.onLoadConceptForPlayNow(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_CONCEPT_FIRST_PAGE, (data) =>
      this.onLoadConcept(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_CONCEPT_NEXT_PAGE, (data) =>
      this.onLoadConceptNextPage(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.LOAD_CONCEPT_PREVIOUS_PAGE, (data) =>
      cc.log(data),
    );
    ACE.gameSocket.on(SERVER_EVENT.FOCUS_NEXT_PAGE, (data) => {
      this.selectTopic.renderConcept(data, false, this.passData.schoolType);
    });
    ACE.gameSocket.on(SERVER_EVENT.FOCUS_PREVIOUS_PAGE, (data) => {
      this.selectTopic.renderConcept(data, false, this.passData.schoolType);
    });
    ACE.gameSocket.on(SERVER_EVENT.IMPROVEMENT_NEXT_PAGE, (data) => {
      this.selectTopic.renderConcept(data, false, this.passData.schoolType);
    });
    ACE.gameSocket.on(SERVER_EVENT.IMPROVEMENT_PREVIOUS_PAGE, (data) => {
      this.selectTopic.renderConcept(data, false, this.passData.schoolType);
    });
  }

  onLoadConceptForPlayNow(data: IConcept) {
    // TODO: Open webview with the concept ID here
    ACE.webviewManager.openQa("practiseCenter", data.ID);
  }

  onLoadConcept(listConcepts: IConcept[]) {
    SchoolManager.getInstance().listConcepts = listConcepts;
    this.selectTopic.renderConcept(listConcepts);
  }

  onLoadConceptNextPage(listConcepts: IConcept[]) {
    // this.renderConcept(listConcepts);
  }

  onLoadConceptPreviousPage(listConcepts: IConcept[]) {
    // this.renderConcept(listConcepts);
  }

  onChooseConceptButtonClicked() {
    if (this.passData.schoolType == "improvement") {
      SchoolService.loadImprovementFirstPage();
    } else if (this.passData.schoolType == "focusCenter") {
      SchoolService.loadFocusFirstPage();
    } else {
      this.selectTopic.showPractice();
    }
    this.selectTopic.node.active = true;
    ACE.topBar.hide();
  }

  onLeave() {
    this.roomName.forEach((child) => {
      child.active = false;
    });
  }
}
