import { ACE } from "../core/global-info";
import GameScene from "../common/game-scene";
import AccountManager from "../managers/account-manager";
import ShowRoomManager from "./ShowRoomManager";
import StudentRoomShopManage from "./StudentRoomShopManage";
import StudentRoom from "./Model/StudentRoom";
import ShopManager from "./StudentRoomShopManage";
import PopupConfirm from "../character-shop/popup-confirm";
import FriendHouseService from "../FriendHouse/FriendHouseService";
import { SERVER_EVENT } from "../core/constant";
import AnimationGreenArrow from "./AnimationGreenArrow";
import { STUDENT_ROOM, MOVE_ROOM } from "../constants/StudentRoomConstants";
import StudentRoomState from "./Model/StudentRoomState";
import { SND } from "./utils/SoundManager";
import SoundManager from "./utils/SoundManager";
import ShowRoomMiniMapManager from "./ShowRoomMiniMapManager";
import { TutorialType } from "../common/top-bar";
import CharacterComponent from "../common/character-component";
import { DIALOG_TYPE } from "./../core/constant";
import LoadingDialog from "../dialogs/loading-dialog";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

const PATH_STUDENT_HOUSE_PREFAB = "prefabs/student-house";

@ccclass
export default class StudentHouseManager extends GameScene {
  public static getInstance(): StudentHouseManager {
    if (this._instance == null) { this._instance = new StudentHouseManager(); }
    return this._instance;
  }

  public static setInstance(object: StudentHouseManager) {
    let isVisitFriendH = false;
    let id = 0;
    if (this._instance != null) {
      isVisitFriendH = this._instance.isVisitFriendHouse;
      id = this._instance.idStudent;
    }
    this._instance = object;
    this._instance.isVisitFriendHouse = isVisitFriendH;
    this._instance.idStudent = id;
  }

  private static _instance: StudentHouseManager = null;
  @property(cc.Node)
  public LoadingScreen: cc.Node = null;
  @property(cc.Node)
  public BedRoom: cc.Node = null;
  @property(cc.Node)
  public BedRoomName: cc.Node = null;
  @property(cc.Node)
  public ShowRoom: cc.Node = null;
  @property(cc.Node)
  public ShowRoomName: cc.Node = null;
  @property(cc.Node)
  public LivingRoom: cc.Node = null;
  @property(cc.Node)
  public LivingRoomName: cc.Node = null;
  @property(cc.Node)
  public Kitchen: cc.Node = null;
  @property(cc.Node)
  public KitchenName: cc.Node = null;
  @property(cc.Node)
  public Shop: cc.Node = null;
  @property(cc.Node)
  public MiniMap: cc.Node = null;
  @property(cc.Node)
  public PopupConfirm: cc.Node = null;
  @property([cc.Node])
  public GreenArrowNodes: cc.Node[] = [];
  @property([cc.Node])
  public DarkArrowNodes: cc.Node[] = [];
  @property(cc.Node)
  public BtnShowMapNode: cc.Node = null;
  @property(cc.Node)
  public BtnShowShopNode: cc.Node = null;
  @property(cc.Node)
  public BtnShowFeedNode: cc.Node = null;
  @property(cc.Node)
  public BtnCleanNode: cc.Node = null;
  @property(cc.Sprite)
  backgroundSprite: cc.Sprite = null;
  @property(cc.Node)
  friendsPlatform: cc.Node = null;

  @property(ShowRoomMiniMapManager)
  ShowRoomMiniMapManager: ShowRoomMiniMapManager = null;

  _SoundManager: SoundManager = null;

  currentRoom = STUDENT_ROOM.BED_ROOM;
  currentRoomObject: cc.Node = this.BedRoom;

  listStudentRoom: StudentRoom[] = [];

  _shopScript: ShopManager = null;
  popupConfirmScript: PopupConfirm = null;

  _pathStudentHousePrefab = PATH_STUDENT_HOUSE_PREFAB;
  _characterNode: cc.Node = null;
  _switchNode: cc.Node = null;

  isVisitFriendHouse: boolean = false;
  idStudent: number = 0;

  _isLoadedHouse: boolean = false;

  _StudentRoomState: StudentRoomState = null; // obj for save status of tutorial of user at StudentRoom

  /**
   * Process for Buying Item in Student Shop
   * Status of Item should save for processing other emit...
   */
  processingDoActionItem = false;
  statusBuyItemIndex = "";
  statusBuyItemMode = "";
  statusBuyItemgrpL1Id = "";

  onLoad() {
    super.onLoad();
    StudentHouseManager.setInstance(this);
  }

  registerEvents() {
    ACE.gameSocket.on(SERVER_EVENT.TRY_ROOM_ITEM, (data) => {
      cc.log(" ITEM WILL UPDATE ...... " + data);
      this.TryToUseItemInRoom(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.UNEQUIP_ROOM_ITEM, (data) => {
      if (this.processingDoActionItem && data.result == 1) {
        this.UpdateItemUsingInRoom(data.usingItem);

        ACE.gameSocket.emit(SERVER_EVENT.ROOM_ITEM_CURRENT_PAGE, {
          grpL1Id: parseInt(this.statusBuyItemgrpL1Id),
        });
      } else if (data.result == 3) {
        this.popupConfirmScript.showPopup(
          "Can't UnEquip This Item", () => {
            cc.log("OK will update function later ....");
          },
          true,
        );
        this.ResetStatusBuyItem();
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.EQUIP_ROOM_ITEM, (data) => {
      if (this.processingDoActionItem) {
        this.UpdateItemUsingInRoom(data.usingItem);

        ACE.gameSocket.emit(SERVER_EVENT.ROOM_ITEM_CURRENT_PAGE, {
          grpL1Id: parseInt(this.statusBuyItemgrpL1Id),
        });
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.ROOM_ITEM_CURRENT_PAGE, (data) => {
      const studentRoomShopManage = this.Shop.getComponent(StudentRoomShopManage);
      studentRoomShopManage.onInitShopItems(data);
      this.ResetStatusBuyItem();
    });

    ACE.gameSocket.on(SERVER_EVENT.BUY_ROOM_ITEM, (data) => {
      if (data == 1) {
        if (this.processingDoActionItem) {
          ACE.gameSocket.emit(SERVER_EVENT.EQUIP_ROOM_ITEM, {
            index: this.statusBuyItemIndex,
            grpL1Id: this.statusBuyItemgrpL1Id,
          });
        }
      } else {
        const text = this.statusBuyItemMode == "1" ? "stars" : "coins";
        this.popupConfirmScript.showPopup(
          "You don\'t have enough " + text + " to by this item!",
          null,
          true,
        );
        this.ResetStatusBuyItem();
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.BUY_ROOM, (data) => {
      if (data == 1) {
        ACE.gameSocket.emit(SERVER_EVENT.GET_CURRENT_ROOM, {});
        this.MiniMap.active = false;
      } else {
        this.popupConfirmScript.showPopup(
          "You don\'t have enough coins to by this room!",
          null,
          true,
        );
      }
    });

    // First | Next | Previous
    ACE.gameSocket.on(SERVER_EVENT.ROOM_ITEM_FIRST_PAGE, (data) => {
      const studentRoomShopManage = this.Shop.getComponent(StudentRoomShopManage);
      studentRoomShopManage.onInitShopItems(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.ROOM_ITEM_NEXT_PAGE, (data) => {
      const studentRoomShopManage = this.Shop.getComponent(StudentRoomShopManage);
      studentRoomShopManage.onInitShopItems(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.ROOM_ITEM_PREVIOUS_PAGE, (data) => {
      const studentRoomShopManage = this.Shop.getComponent(StudentRoomShopManage);
      studentRoomShopManage.onInitShopItems(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_ROOM_ITEM_GROUP_L1, (data) => {
      const studentRoomShopManage = this.Shop.getComponent(StudentRoomShopManage);
      studentRoomShopManage.onInitMenu(data);
      // Get First Item & Emit new Event for get the list item of shop
      ACE.gameSocket.emit(SERVER_EVENT.ROOM_ITEM_FIRST_PAGE, {
        grpL1Id: studentRoomShopManage.listStudentRoomItemGroupL1[0].ID,

      });
    });
    ACE.gameSocket.on(SERVER_EVENT.OPEN_ROOM, (data) => { });

    ACE.gameSocket.on(SERVER_EVENT.GET_CURRENT_ROOM, (data) => {
      // should check data room correct with local -> then will call other api
      ACE.gameSocket.emit(SERVER_EVENT.LOAD_ALL_ROOM_ITEM_GROUP_L1, {});

      ACE.gameSocket.emit(SERVER_EVENT.LOAD_STUDENT_ROOMS, {});
    });

    // For whose house ...
    ACE.gameSocket.on(SERVER_EVENT.GET_WHOSE_HOUSE, (data) => {
      cc.log("GET_WHOSE_HOUSE", data);
      ACE.gameSocket.emit(SERVER_EVENT.LOAD_ALL_ROOM_ITEM_GROUP_L1, {});

      ACE.gameSocket.emit(SERVER_EVENT.LOAD_STUDENT_ROOMS, {});
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_ALL_ROOM_ITEM_GROUP_L1, (data) => {
      const room = this.currentRoomObject.getComponent(ShowRoomManager);
      if (room != null) {
        room.loadAllItemGroupL1(data);
      }

      // After finish -> call API
      ACE.gameSocket.emit(SERVER_EVENT.LOAD_ROOM_ITEM_USING, {});
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_STUDENT_ROOMS, (data) => {
      this.UpdateCurrentRoom(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_ROOM_ITEM_USING, (data) => {
      this.UpdateItemUsingInRoom(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_ROOM_ITEM_GROUP_L2, (data) => { });
    ACE.gameSocket.on(SERVER_EVENT.INIT_ROOM, (data) => {
      cc.log("StudentHouseManager::RegisterEvent::on::initRoom", data);
    });
  }

  onEnter() {
    super.onEnter();
    this.onResize();
    this.friendsPlatform.active = false;
    this.onEnableLoading();
    ACE.topBar.show();
    ACE.topBar.positionButtons();
    this.popupConfirmScript = this.PopupConfirm.getComponent(PopupConfirm);
    this._shopScript = this.Shop.getComponent(StudentRoomShopManage);
    this._SoundManager = this.node.getComponent(SoundManager);
    this._SoundManager.player = this.node.getComponent(cc.AudioSource);
    AudioManager.getInstance().playAudio("MATH_HOUSE_3", true, true);
  }

  onResize() {
    this.backgroundSprite.node.scaleX = cc.winSize.width / this.backgroundSprite.node.width;
    this.backgroundSprite.node.scaleY = cc.winSize.height / this.backgroundSprite.node.height;
  }

  onEnable() {
    if (this.isVisitFriendHouse) {
      cc.log("Visit Friend House");
      this.onInitFriendHouse();
    } else {
      cc.log("Visit My House");
      this.onInitMyHouse();
    }
  }

  onDisable() {
    if (this._characterNode != null) {
      this._characterNode.active = false;
    }
    if (this._switchNode != null) {
      this._switchNode.active = false;
    }
  }

  // ------Logic

  showCharacter(isShow = false) {
    this.friendsPlatform.active = isShow;
    if (isShow) {
      this.friendsPlatform.getComponentInChildren(CharacterComponent).autoUpdate(false, false);
    }
  }

  onLoadingHouseContentFinish() {
    ACE.dialogManager.hideLoading();
    const mapTutorialState = AccountManager.getInstance()._gameStateTutorial.get("studentHouse-new");
    if (!mapTutorialState.isClosed) {
      ACE.topBar.showTutorial(TutorialType.StudentHouse);
    }
    if (this.isVisitFriendHouse) {
      if (this._characterNode != null) {
        this._characterNode.active = true;
      }
      if (this._switchNode != null) {
        this._switchNode.active = true;
      }

      // TODO: Using new character component
      // CharacterService.loadCharacterOfFriendUsing();
      // ACE.gameSocket.emit(SERVER_EVENT.GET_CURRENT_FRIEND);
    }
  }
  onEnableLoading() {
    ACE.dialogManager.showLoading();
    ACE.dialogManager.getDialogComponentByType(DIALOG_TYPE.LOADING, LoadingDialog).setProgress(95);
    if (this.isVisitFriendHouse) {
      if (this._characterNode != null) {
        this._characterNode.active = false;
      }
      if (this._switchNode != null) {
        this._switchNode.active = false;
      }
    }
  }

  // Called when visit friend house
  onInitFriendHouse() {
    this.Shop.active = false;
    this.MiniMap.active = false;

    if (this._switchNode == null) {
      cc.loader.loadRes(
        `${this._pathStudentHousePrefab}/${"friend_switch"}`,
        (error, prefab) => {
          if (error) {
            cc.log("Prefab load failed with error.", error);
            return;
          }
          this._switchNode = cc.instantiate(prefab);
          this._switchNode.parent = this.node;
        },
      );
    }

    // on event listener from socket io o o oo o
    ACE.gameSocket.on(SERVER_EVENT.GET_NEXT_FRIEND, (data) => {
      FriendHouseService.LoadFriendRoom(data.friendID);
      this.SetVisibleRoom("BedRoom", true);
    });
    ACE.gameSocket.on(SERVER_EVENT.GET_PRE_FRIEND, (data) => {
      FriendHouseService.LoadFriendRoom(data.friendID);
      this.SetVisibleRoom("BedRoom", true);
    });

    // Default load friend room selected
    FriendHouseService.LoadFriendRoom();
    this.SetVisibleRoom("BedRoom", true);

    // Hide node dont esixted in friend house
    if (this.BtnShowMapNode != null) {
      this.BtnShowMapNode.active = false;
    }
    if (this.BtnShowShopNode != null) {
      this.BtnShowShopNode.active = false;
    }
    if (this.BtnShowFeedNode != null) {
      this.BtnShowFeedNode.active = true;
    }
    if (this.BtnCleanNode != null) {
      this.BtnCleanNode.active = true;
    }
  }

  // Called when visit my house
  onInitMyHouse() {
    this.friendsPlatform.active = false;
    this.Shop.active = false;
    // Manage Room Code.
    // Set Room ShowRoom ...

    // Step: 1
    ACE.gameSocket.emit(SERVER_EVENT.OPEN_ROOM, { roomName: "BedRoom" });
    this.SetVisibleRoom("BedRoom");

    // Step 2 ( Init Room) : will check we need to call ?
    ACE.gameSocket.emit(SERVER_EVENT.INIT_ROOM, {});

    // Step 3: Check status tutorial ...
    // already done in login state

    // Step 4: Read Info of room -> show current room
    ACE.gameSocket.emit(SERVER_EVENT.GET_CURRENT_ROOM, {});

    // Hide node dont esixted in friend house
    if (this.BtnShowMapNode != null) {
      this.BtnShowMapNode.active = true;
    }
    if (this.BtnShowShopNode != null) {
      this.BtnShowShopNode.active = true;
    }
    if (this.BtnShowFeedNode != null) {
      this.BtnShowFeedNode.active = false;
    }
    if (this.BtnCleanNode != null) {
      this.BtnCleanNode.active = false;
    }
  }

  UpdateCurrentRoom(data) {
    this.listStudentRoom = [];
    // Update Status of StudentRooms
    data.forEach((element) => {
      this.listStudentRoom.push(element);
    });
    this.initMap();
  }

  UpdateItemUsingInRoom(data) {
    this.onEnableLoading();
    const room = this.currentRoomObject.getComponent(ShowRoomManager);
    if (room != null) {
      room.loadRoomItemUsing(data);
    }
  }

  TryToUseItemInRoom(data) {
    cc.log("TryToUseItemInRoom");
    this.onEnableLoading();
    const room = this.currentRoomObject.getComponent(ShowRoomManager);
    if (room != null) {
      room.tryApplyItemInRoom(data);
      cc.log("TEST", data)
    }

    // after finish Load
    this.onLoadingHouseContentFinish();
  }

  initMap() {
    this.ShowRoomMiniMapManager.InitStatusMiniMap(this.listStudentRoom);
    // Check CurrentRoom & show /hide Arrow Function
    // Set All arrow is disable
    this.GreenArrowNodes.forEach((arrow) => {
      arrow.active = false;
    });

    this.DarkArrowNodes.forEach((arrow) => {
      if (arrow != null) {
        arrow.active = false;
      }
    });

    switch (this.currentRoom) {
      case STUDENT_ROOM.BED_ROOM: // 3 dir
        // checkStatus SHOWROOM
        if (this.listStudentRoom[STUDENT_ROOM.ID_SHOW_ROOM].IsLocked == 0) {
          this.SetSpriteArrowHintChangeRoom(MOVE_ROOM.UP, STUDENT_ROOM.ID_SHOW_ROOM);
          this.GreenArrowNodes[MOVE_ROOM.UP].active = true;
        } else {
          this.DarkArrowNodes[MOVE_ROOM.UP].active = true;
          if (this.isVisitFriendHouse) {
            this.DarkArrowNodes[MOVE_ROOM.UP].children[0].active = false;
          } else {
            this.DarkArrowNodes[MOVE_ROOM.UP].children[0].active = true;
          }
        }

        // checkStatus LIVING_ROOM
        if (this.listStudentRoom[STUDENT_ROOM.ID_LIVING_ROOM].IsLocked == 0) {
          this.SetSpriteArrowHintChangeRoom(MOVE_ROOM.LEFT, STUDENT_ROOM.ID_LIVING_ROOM);
          this.GreenArrowNodes[MOVE_ROOM.LEFT].active = true;
        } else {
          this.DarkArrowNodes[MOVE_ROOM.LEFT].active = true;
          if (this.isVisitFriendHouse) {
            this.DarkArrowNodes[MOVE_ROOM.LEFT].children[0].active = false;
          } else {
            this.DarkArrowNodes[MOVE_ROOM.LEFT].children[0].active = true;
          }
        }

        // checkStatus KITCHEN_ROOM
        if (this.listStudentRoom[STUDENT_ROOM.ID_KITCHEN_ROOM].IsLocked == 0) {
          this.SetSpriteArrowHintChangeRoom(MOVE_ROOM.DOWN, STUDENT_ROOM.ID_KITCHEN_ROOM);
          this.GreenArrowNodes[MOVE_ROOM.DOWN].active = true;
        } else {
          this.DarkArrowNodes[MOVE_ROOM.DOWN].active = true;
          if (this.isVisitFriendHouse) {
            this.DarkArrowNodes[MOVE_ROOM.DOWN].children[0].active = false;
          } else {
            this.DarkArrowNodes[MOVE_ROOM.DOWN].children[0].active = true;
          }
        }

        break;
      case STUDENT_ROOM.SHOW_ROOM:
        this.SetSpriteArrowHintChangeRoom(MOVE_ROOM.DOWN, STUDENT_ROOM.ID_BED_ROOM);
        this.GreenArrowNodes[MOVE_ROOM.DOWN].active = true;
        break;
      case STUDENT_ROOM.LIVING_ROOM:
        this.SetSpriteArrowHintChangeRoom(MOVE_ROOM.RIGHT, STUDENT_ROOM.ID_BED_ROOM);
        this.GreenArrowNodes[MOVE_ROOM.RIGHT].active = true;
        break;
      case STUDENT_ROOM.KITCHEN_ROOM:
        this.SetSpriteArrowHintChangeRoom(MOVE_ROOM.UP, STUDENT_ROOM.ID_BED_ROOM);
        this.GreenArrowNodes[MOVE_ROOM.UP].active = true;
        break;
    }
  }

  SetSpriteArrowHintChangeRoom(arrowType: number, hintRoom: number) {
    const icon = this.GreenArrowNodes[arrowType]
      .getChildByName("animation")
      .getComponent(AnimationGreenArrow).arrayIcon[hintRoom];

    this.GreenArrowNodes[arrowType]
      .getChildByName("animation")
      .getComponent(cc.Sprite).spriteFrame = icon;
  }

  buttonActions(event, data) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    switch (data) {
      // Go to Character Shop
      case "gotoCharacterShop":
        ACE.sceneManager.pushScene("character-shop");
        break;
      // -----------------------
      // Function change ROOM
      case "UP":
        ACE.topBar.toggleButtonGroup(true);
        this.onEnableLoading();
        if (this.currentRoom == STUDENT_ROOM.BED_ROOM) {
          // load ShowRoom
          this.openRoomService("Showroom");
          this.SetVisibleRoom("Showroom");
        } else if (this.currentRoom == STUDENT_ROOM.KITCHEN_ROOM) {
          this.openRoomService("BedRoom");
          this.SetVisibleRoom("BedRoom");
        }
        break;
      case "DOWN":
        ACE.topBar.toggleButtonGroup(true);
        this.onEnableLoading();
        if (this.currentRoom == STUDENT_ROOM.SHOW_ROOM) {
          this.openRoomService("BedRoom");
          this.SetVisibleRoom("BedRoom");
        } else { // current room : BED_ROOM
          this.openRoomService("Kitchen");
          this.SetVisibleRoom("Kitchen");
        }
        break;
      case "LEFT":
        ACE.topBar.toggleButtonGroup(true);
        this.onEnableLoading();
        if (this.currentRoom == STUDENT_ROOM.BED_ROOM) {
          this.openRoomService("LivingRoom");
          this.SetVisibleRoom("LivingRoom");
        }
        break;
      case "RIGHT":
        ACE.topBar.toggleButtonGroup(true);
        this.onEnableLoading();
        if (this.currentRoom == STUDENT_ROOM.LIVING_ROOM) {
          this.openRoomService("BedRoom");
          this.SetVisibleRoom("BedRoom");
        }
        break;
      case "openShop":
        ACE.topBar.toggleButtonGroup(false);
        this._shopScript.currentGroup = 1;
        ACE.gameSocket.emit(SERVER_EVENT.LOAD_ROOM_ITEM_GROUP_L1, {
          grpL2Id: this._shopScript.currentGroup,

        });
        this.Shop.active = true;
        break;
      case "closeShop":
        ACE.topBar.toggleButtonGroup(true);
        this.Shop.active = false;
        break;
      case "openMiniMap":
        this.MiniMap.active = true;
        break;
      case "closeMiniMap":
        this.MiniMap.active = false;
        break;
      case "buyShowRoom":
        this.buyRoom(this.listStudentRoom[STUDENT_ROOM.ID_SHOW_ROOM].ID);
        break;
      case "buyKitchenRoom":
        this.buyRoom(this.listStudentRoom[STUDENT_ROOM.ID_KITCHEN_ROOM].ID);
        break;
      case "buyLivingRoom":
        this.buyRoom(this.listStudentRoom[STUDENT_ROOM.ID_LIVING_ROOM].ID);
        break;
      default:
        cc.log(data);
        break;
    }
  }

  buyRoom(index) {
    const self = this;
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    this.popupConfirmScript.showPopup(
      "Are you sure want to buy this room ? " +
      "?",
      () => {
        self._SoundManager.playSound(SND.SND_EFFECT_CLICK);
        ACE.gameSocket.emit(SERVER_EVENT.BUY_ROOM, {
          index: "" + (index - 1), // sub 1 because ID : 1, 2, 3, 4 but send to server from : 0
          mode: "0",
        });
      },
    );

  }

  openRoomService(room) {
    if (this.isVisitFriendHouse) {
      FriendHouseService.openRoomFriend(this.idStudent, room);
    } else {
      ACE.gameSocket.emit(SERVER_EVENT.OPEN_ROOM, {
        roomName: room,
      });
    }

  }

  SetVisibleRoom(room, isSkipGetCurrentRoom: boolean = false) {
    if (!isSkipGetCurrentRoom) {
      if (this.isVisitFriendHouse) {
        ACE.gameSocket.emit(SERVER_EVENT.GET_WHOSE_HOUSE, {});
      } else {
        ACE.gameSocket.emit(SERVER_EVENT.GET_CURRENT_ROOM, {});
      }
    }

    this.currentRoom = room;

    this.BedRoom.active = false;
    this.ShowRoom.active = false;
    this.LivingRoom.active = false;
    this.Kitchen.active = false;

    this.BedRoomName.active = false;
    this.ShowRoomName.active = false;
    this.LivingRoomName.active = false;
    this.KitchenName.active = false;

    switch (room) {
      case STUDENT_ROOM.BED_ROOM:
        this.currentRoomObject = this.BedRoom;
        this.BedRoomName.active = true;
        break;
      case STUDENT_ROOM.SHOW_ROOM:
        this.currentRoomObject = this.ShowRoom;
        this.ShowRoomName.active = true;
        break;
      case STUDENT_ROOM.LIVING_ROOM:
        this.currentRoomObject = this.LivingRoom;
        this.LivingRoomName.active = true;
        break;
      case STUDENT_ROOM.KITCHEN_ROOM:
        this.currentRoomObject = this.Kitchen;
        this.KitchenName.active = true;
        break;
    }

    // process
    this.Shop.active = false;
    this.currentRoomObject.active = true;
  }

  ShopSelectTab(ID: number) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    ACE.gameSocket.emit(SERVER_EVENT.ROOM_ITEM_FIRST_PAGE, { grpL1Id: ID });
  }

  SelectGroupShop(groupID: number) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_ROOM_ITEM_GROUP_L1, {
      grpL2Id: groupID,
    });
  }

  ResetStatusBuyItem() {
    this.processingDoActionItem = false;
    this.statusBuyItemIndex = "";
    this.statusBuyItemMode = "";
    this.statusBuyItemgrpL1Id = "";
  }

  AddProcessingBuyItem(index: string, mode: string, grpL1Id: string) {
    this.processingDoActionItem = true;
    this.statusBuyItemIndex = index;
    this.statusBuyItemMode = mode;
    this.statusBuyItemgrpL1Id = grpL1Id;
  }

  AddProcessEquiptItem(index: string, grpL1Id: string) {
    this.processingDoActionItem = true;
    this.statusBuyItemIndex = index;
    this.statusBuyItemgrpL1Id = grpL1Id;
  }

  BuyRoomItem(index: string, mode: string, grpL1Id: string) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    this.AddProcessingBuyItem(index, mode, grpL1Id);

    ACE.gameSocket.emit(SERVER_EVENT.BUY_ROOM_ITEM, {
      index,
      mode,
      grpL1Id,
    });
  }

  LoadNextItems(grpL1Id: number) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    ACE.gameSocket.emit(SERVER_EVENT.ROOM_ITEM_NEXT_PAGE, {
      grpL1Id,

    });
  }

  LoadPreviousItems(grpL1Id: number) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    ACE.gameSocket.emit(SERVER_EVENT.ROOM_ITEM_PREVIOUS_PAGE, {
      grpL1Id,

    });
  }

  TryRoomItem(index: string, grpL1Id: string) {
    ACE.gameSocket.emit("tryRoomItem", {
      index,
      grpL1Id,
    });
  }

  UseRoomItem(index: string, grpL1Id: string) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    this.AddProcessEquiptItem(index, grpL1Id);
    ACE.gameSocket.emit(SERVER_EVENT.EQUIP_ROOM_ITEM, {
      index: this.statusBuyItemIndex,
      grpL1Id: this.statusBuyItemgrpL1Id,
    });
  }

  HideRoomItem(index: string, grpL1Id: string) {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    // unequipRoomItem
    this.AddProcessEquiptItem(index, grpL1Id);
    ACE.gameSocket.emit("unequipRoomItem", {
      index: this.statusBuyItemIndex,
      grpL1Id: this.statusBuyItemgrpL1Id,
    });
  }

  // Action MiniMap
  OpenRoom(event, name) {
    ACE.topBar.toggleButtonGroup(true);
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    this.openRoomService(name);
    this.SetVisibleRoom(name);
    this.MiniMap.active = false;
  }

  BtnFeedOnClicked() {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    this.popupConfirmScript.showPopup(
      "Coming Soon!",
      null,
      true,
    );
  }
  BtnCleanOnClicked() {
    this._SoundManager.playSound(SND.SND_EFFECT_CLICK);
    this.popupConfirmScript.showPopup(
      "Coming Soon!",
      null,
      true,
    );
  }
}
