import GameScene from "../common/game-scene";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupConfirm extends GameScene {
  @property(cc.Node)
  btnOkNode: cc.Node = null;
  @property(cc.Node)
  btnApccepNode: cc.Node = null;
  @property(cc.Node)
  btnDenieNode: cc.Node = null;

  @property(cc.Label)
  messageLabel: cc.Label = null;
  @property(cc.Animation)
  animOpenPopup: cc.Animation = null;

  _acceptCallback: () => void;

  start() {}

  showPopup(
    message: string,
    acceptCallback: () => void = null,
    isOkBtn: boolean = false,
  ) {
    if (isOkBtn) {
      this.btnOkNode.active = true;
      this.btnApccepNode.active = false;
      this.btnDenieNode.active = false;
    } else {
      this.btnOkNode.active = false;
      this.btnApccepNode.active = true;
      this.btnDenieNode.active = true;
    }
    this._acceptCallback = acceptCallback;
    this.messageLabel.string = message;
    this.node.active = true;
    this.animOpenPopup.play();
  }

  onBtnDenieOnClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.node.active = false;
  }

  onBtnAppceptOnClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.node.active = false;
    if (this._acceptCallback != null) this._acceptCallback();
  }
}
