export interface IFriend {
  ID:            number;
  FirstName:     string;
  LastName:      string;
  FullName:      string;
  UserName:      string;
  Stars:         number;
  Coins:         number;
  SchoolID:      number;
  ClassID:       number;
  ClassName:     string;
  YearGrade:     number;
  YearGradeName: string;
  EnrolmentID:   number;
  PetId:         number;
  Gender:        boolean;
  isSentGift:    number;
  Oid:           null;
}