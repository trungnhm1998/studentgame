import QaDrag from "./qa-drag";
import QaNode from "./qa-node";

const {ccclass, property} = cc._decorator;

@ccclass
export default class QaDropZone extends cc.Component {
  dragObject: QaDrag = null;
  
  onLoad() {
    // this.node.on(cc.Node.EventType.TOUCH_MOVE, (event) => this.onTouchMove(event));
  }

  onTouchMove(event: cc.Event.EventTouch): any {
    cc.log('QaDropZone::onTouchMove', event);
  }

  setDragObject(dragObject: QaDrag) {
    if (this.dragObject != null) {
      this.removeDragObject();
      this.dragObject = null;
    }
    this.dragObject = dragObject;
    this.dragObject.node.setPosition(this.node.getPosition());
  }

  removeDragObject() {
    this.dragObject.resetDropzone();
    this.dragObject = null;
  }

  getValue(): string {
    if (!this.dragObject) return '';
    let qaNodeComponent: QaNode = this.dragObject.getComponent(QaNode);
    return qaNodeComponent._QAObject.objPrivate.action.value;
  }
}
