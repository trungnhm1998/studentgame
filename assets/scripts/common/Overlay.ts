const {ccclass, property} = cc._decorator;

@ccclass
export default class Overlay extends cc.Component {
  start () {
  }

  fadeOut() {
    var thisNode : cc.Node = this.node;
    var action : cc.Action = cc.fadeOut(1.0);
    thisNode.runAction(action);
  }
}
