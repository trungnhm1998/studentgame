import IngameLogic from "./ingame-logic";

const {ccclass, property} = cc._decorator;

const BASE_SPEED = 74//80;
const BASE_BG_SIZE_W = 1920;
@ccclass
export default class ScrollSceneManager extends cc.Component {
    @property
    isBG = true;

    @property
    speed: number = 6;

    @property
    offsetX: number = -BASE_BG_SIZE_W;

    @property
    OFFSET_PX: number = 0;

    posX = 0;

    @property([cc.Node])
    NodeBGs: cc.Node[] = [];

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    @property
    startPosX: number = 0;

    first = 0;
    last = 0;
    step = 1;

    onEnable() {
        this.step = 0;
        this.first = 0;
        this.last = this.NodeBGs.length - 1;

        if(!this.isBG){
            cc.error("$$$$$$$$$$$$$$$$$$$$$$$$$$$ offsetX : Obstacle :: LAYER"  + this.node.width);
            // this.startPosX = this.node.width/2;
            // this.offsetX = - this.node.width;
            this.node.setPosition(this.startPosX,this.node.position.y);
        }
        else{
            // this.NodeBGs.forEach(item => {
            //     item.setPosition(this.startPosX, item.position.y);
            // });
            
            if(this.NodeBGs.length > 0)
            {
                var width = this.NodeBGs[this.first].width - this.OFFSET_PX;
                this.NodeBGs[this.first].setPosition(this.startPosX - width, this.NodeBGs[this.first].position.y)
                this.NodeBGs[this.first + 1].setPosition(this.startPosX, this.NodeBGs[this.first].position.y)
                this.NodeBGs[this.last].setPosition(this.startPosX + width, this.NodeBGs[this.first].position.y)
            }
        }

        this.posX = this.startPosX;
        this.node.setPosition(this.startPosX,this.node.position.y);
    }

    update (dt) {
        // cc.log("Value change :: " + this.node.position.x
        //     + "\ndt: " + dt    
        // );
        if(IngameLogic.getInstance().player.needPauseMoving)
        {
            // cc.error("current is Pause by Effect by Hit to Obtacle........");
            return;
        }
            

        if(!IngameLogic.getInstance()._isPause)
        {
            if(!this.isBG)
            {
                if(this.posX <= this.offsetX)
                    this.posX = this.startPosX;
            }
            
            var speedDifficult = this.speed * IngameLogic.getInstance().speedFollowDesign;
            this.posX -= dt *  BASE_SPEED * speedDifficult;
            this.node.setPosition(this.posX,this.node.position.y);
            // cc.log(" ******* Each time Moving :::: " + this.node.position.x);
            // this.node.position.x -= dt * this.speed;

            if(this.isBG && this.NodeBGs.length > 0 && this.posX <= - this.NodeBGs[this.first].width* this.step)
            {
                this.step ++;
                var posLast = this.NodeBGs[this.last].position.x + this.NodeBGs[this.last].width - this.OFFSET_PX; //-OFFSET_PX because image have 1px tranparent...
                // cc.log("PostLast ::::::::: ++++++++++++++++++++ " + posLast)
                this.NodeBGs[this.first].setPosition(posLast,this.NodeBGs[this.first].position.y);
                this.last = this.first;
                this.first ++;
                if(this.first > this.NodeBGs.length - 1) this.first = 0;
            }
        }
    }
}
