import Component, { IComponent } from "../../core/ecs/component";

const {ccclass, property} = cc._decorator;

export interface IPositionComponent extends IComponent {
  position: cc.Vec2;
  size: cc.Size;
  anchor: cc.Vec2;
}

@ccclass
export default class PositionComponent extends Component {
  @property(cc.Vec2)
  position: cc.Vec2 = cc.v2();
  _c_name: string = "PositionComponent";
  _data: IPositionComponent;
  onLoad() {
    this._data.position = this.position;
  }
}
