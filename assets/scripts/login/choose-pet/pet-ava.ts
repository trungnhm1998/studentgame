import NodeUtils from "../../core/utils/node-utils";
import ChoosePet from "./choose-pet";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PetAva extends cc.Component {
  @property(cc.SpriteFrame)
  petSprite: cc.SpriteFrame = null;

  @property(cc.Integer)
  petId: number = -1;

  choosePetRef: ChoosePet = null;

  _infoButton: cc.Button = null;
  _spriteComp: cc.Sprite = null;
  _buttonComp: cc.Button = null;
  onLoad() {
    this._spriteComp = this.getComponent(cc.Sprite);
    this._buttonComp = this.addComponent(cc.Button);
    const infoButtonNode = NodeUtils.findNodeByName(this.node, "info_button");
    this._infoButton = infoButtonNode.addComponent(cc.Button);

    // set
    if (this.petSprite) {
      this._spriteComp.spriteFrame = this.petSprite;
    }

    this._buttonComp.transition = cc.Button.Transition.SCALE;
    this._buttonComp.duration = 0.1;
    this._buttonComp.zoomScale = 1.2;

    this._infoButton.transition = cc.Button.Transition.SCALE;
    this._infoButton.duration = 0.1;
    this._infoButton.zoomScale = 1.2;

    // buttons event
    const clickEvent: cc.Component.EventHandler = new cc.Component.EventHandler();
    clickEvent.component = "choose-pet";
    clickEvent.target = this.choosePetRef.node;
    clickEvent.handler = "showPetInfo";
    clickEvent.customEventData = this.petId + "";

    this._buttonComp.clickEvents.push(clickEvent);
    this._infoButton.clickEvents.push(clickEvent);
    NodeUtils.preventMultipleClick(this._buttonComp);
    NodeUtils.preventMultipleClick(this._infoButton);

    // mouse hover effect for web
    this.node.on(cc.Node.EventType.MOUSE_ENTER, () => {
      this.node.runAction(cc.fadeTo(0.1, 200));
    });
    this.node.on(cc.Node.EventType.MOUSE_LEAVE, () => {
      this.node.runAction(cc.fadeTo(0.1, 255));
    });

    this._infoButton.node.on(cc.Node.EventType.MOUSE_ENTER, () => {
      this._infoButton.node.runAction(cc.fadeTo(0.1, 200));
    });
    this._infoButton.node.on(cc.Node.EventType.MOUSE_LEAVE, () => {
      this._infoButton.node.runAction(cc.fadeTo(0.1, 255));
    });
  }

  hideInfo() {
    this._infoButton.node.active = false;
    this.node.off(cc.Node.EventType.MOUSE_ENTER);
    this.node.off(cc.Node.EventType.MOUSE_LEAVE);
    this._infoButton.node.off(cc.Node.EventType.MOUSE_ENTER);
    this._infoButton.node.off(cc.Node.EventType.MOUSE_LEAVE);
  }
}
