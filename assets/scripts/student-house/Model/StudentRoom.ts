const { ccclass } = cc._decorator;

@ccclass
export default class StudentRoom {
  ID: number;
  Name: string;
  Description: string;
  UrlImage: string;
  Stars: number;
  Coins: number;
  UrlLockImage: string;
  UrlInsImage: string;
  IsLocked: number; // 0 & 1
}