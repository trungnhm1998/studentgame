const { ccclass, property } = cc._decorator;

@ccclass
export default class LevelNode extends cc.Component {

  @property(cc.Label)
  numLevelLabel: cc.Label = null;

  _numLevel: number = 0;

  initLevel(numLv: number, onClicked: (level: number) => any, isEnable: boolean = false) {
    this.numLevelLabel.string = numLv.toString();
    this._numLevel = numLv;

    let self = this;
    this.node.on(cc.Node.EventType.TOUCH_END, (event) => {
      onClicked(self._numLevel);
    });
    // if(isEnable){
    //     this.node.color= cc.Color.WHITE;
    // }else{
    //     this.node.color= cc.Color.GRAY;
    // }
  }

  getNumLevel() {
    return this._numLevel;
  }
}
