export class ModelUtils {
  static merge(oldModel: any, newModel: any) {
    const keys = Object.keys(newModel);
    for (const key of keys) {
      if (oldModel[key] && typeof oldModel[key] == "object") {
        ModelUtils.merge(oldModel[key], newModel[key]);
      } else {
        oldModel[key] = newModel[key];
      }
    }
  }

  static exist(model: any) {
    return model != null && model != undefined;
  }
}
