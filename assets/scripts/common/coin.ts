const { ccclass, property } = cc._decorator;

@ccclass
export default class CoinAnimation extends cc.Component {
  _armatureDisplay: dragonBones.ArmatureDisplay = null;

  onLoad() {
    this._armatureDisplay = this.node.getComponent(dragonBones.ArmatureDisplay);
  }

  onEnable() {
    setTimeout(() => {
      this.play();
    }, 200);
  }

  play() {
    this.node.setPosition(0, 540);
    this.node.opacity = 0;
    this.node.runAction(
      cc.sequence(
        cc.spawn(
          cc.fadeTo(0.5, 255),
          cc.moveBy(1, cc.v2(0, -550)).easing(cc.easeBounceOut())
        ),
        cc.spawn(cc.scaleTo(0.5, 0), cc.moveTo(0.5, cc.v2(-810, 450))),
        cc.callFunc(() => {
          cc.log("finished");
          this.node.removeFromParent();
        })
      )
    );
  }
}
