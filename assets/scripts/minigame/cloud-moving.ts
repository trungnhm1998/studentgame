import { Random } from "../core/utils/random-utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CloudMoving extends cc.Component {
  @property
  speed: number = 0;
  @property
  range: number = 0;

  _positionDefault: cc.Vec2 = null;
  _posTarget: number = 10;

  onLoad() {
    this._positionDefault = this.node.position;
  }

  onEnable() {
    this.node.setPosition(this._positionDefault);
    this._posTarget = 10;
    let rand = new Random();
    if (rand.integer(0, 2) == 0) {
      this._posTarget = -10;
    }
  }

  update(dt) {
    this.node.setPosition(
      cc.v2(cc.misc.lerp(this.node.position.x, this.node.position.x + this._posTarget, dt * this.speed),
        this.node.position.y));
    if (this.node.position.x <= this._positionDefault.x - this.range) {
      this._posTarget = 10;
    }
    if (this.node.position.x >= this._positionDefault.x + this.range) {
      this._posTarget = -10;
    }
  }
}
