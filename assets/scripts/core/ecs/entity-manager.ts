import Entity from "./entity";
import SystemManager from "./system-manager";

export default class EntityManager {
  static _instance: EntityManager = null;
  static getInstance(): EntityManager {
    if (EntityManager._instance == null) {
      EntityManager._instance = new EntityManager();
    }
    return EntityManager._instance;
  }

  entities: Map<string, Entity>;

  constructor() {
    this.entities = new Map();
  }

  createEntity(entity: Entity) {
    const uuid = entity._uuidv4;
    if (uuid && !this.entities.has(uuid)) {
      this.entities.set(uuid, entity);
      SystemManager.getInstance().notifyAddEntity(entity);
    }
    // cc.log(`EntityManager::createEntit::uuid[${uuid}] current size[${this.entities.size}]`);
  }

  onEntityComponentUpdate(entity: Entity) {
    SystemManager.getInstance().notifyEntityUpdate(entity);
  }

  alive(entity: Entity) {
    return this.entities.has(entity._uuidv4);
  }

  destroy(entity: Entity | cc.Node) {
    let entityComp: Entity = null;
    if (entity instanceof Entity) {
      entityComp = entity;
      this.entities.delete(entity._uuidv4);
      entity.node.stopAllActions();
      entity.node.destroy(); // for clean up hybrid ECS
    } else {
      entityComp = entity.getComponent(Entity);
      this.entities.delete(entityComp._uuidv4);
      entity.stopAllActions();
      entity.destroy(); // for clean up hybrid ECS
    }
    // cc.log("Remove entity", entityComp._uuidv4, " size " + this.entities.size);
    SystemManager.getInstance().onEntityDelete(entityComp);
  }

}
