import { ItemState } from "../constants/qa.constants";
import ItemShop from "./item-shop";
import ProductItem from "../models/product-item";
import AudioManager from "../core/audio-manager";
import { RESOURCE_PATH } from "../constants/resource-constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemNode extends cc.Component {
  @property(cc.Sprite)
  icon: cc.Sprite = null;
  @property(cc.Node)
  iconNode: cc.Node = null;
  @property(cc.Node)
  itemBackground: cc.Node = null;
  @property(cc.Node)
  equippedPanel: cc.Node = null;
  @property(cc.Node)
  actionBuyBy: cc.Node = null;
  @property(cc.Node)
  actionUse: cc.Node = null;
  @property(cc.Node)
  actionHide: cc.Node = null;
  @property(cc.Node)
  actionBuyByCoin: cc.Node = null;
  @property(cc.Node)
  actionBuyByStar: cc.Node = null;

  @property colorBackgroundSelected: cc.Color = cc.Color.GREEN;
  @property colorBackgroundNomal: cc.Color = cc.Color.BLUE;
  @property colorIconNotUnlocked: cc.Color = cc.Color.GRAY;
  @property colorIconUnlocked: cc.Color = cc.Color.WHITE;

  _idItem: number = 0;
  _itemShop: ItemShop = null;
  _state: ItemState = ItemState.AVAILIBLE_BUY;
  _actionState: cc.Node = null;
  _productItem: ProductItem = null;
  _listObjectsNeedShowDetailItem: cc.Node[] = [];

  start() {
    this._listObjectsNeedShowDetailItem = [
      this.itemBackground,
      this.actionBuyByCoin,
      this.actionBuyByStar,
      this.actionUse,
      this.actionHide,
    ];
  }

  initItem(productItem: ProductItem, idItem: number, itemShop: ItemShop) {
    const self = this;
    cc.loader.load(
      RESOURCE_PATH.DATA_ASSETS_SERVER + productItem.AssetImageUrl,
      (err, texture) => {
        if (err) {
          cc.error(err.message || err);
          return;
        }

        const iconSp = new cc.SpriteFrame(texture);
        self.icon.spriteFrame = iconSp;
      },
    );
    this._productItem = productItem;

    this._idItem = idItem;
    this._itemShop = itemShop;
    if (this._productItem.isEquipped) {
      this.setStateItem(ItemState.EQUIPPED);
    } else if (this._productItem.isOwned) {
      this.setStateItem(ItemState.AVAILIBLE_EQUIP);
    } else {
      this.setStateItem(ItemState.AVAILIBLE_BUY);
    }
  }

  setStateItem(itemState: ItemState) {
    this._state = itemState;
    switch (itemState) {
      case ItemState.AVAILIBLE_BUY:
        {
          this.equippedPanel.active = false;
          if (this._actionState != null) { this._actionState.active = false; }
          this._actionState = this.actionBuyBy;
          this.iconNode.color = this.colorIconNotUnlocked;

          //
          this.actionBuyByCoin.children[0].children[0].getComponent(
            cc.Label,
          ).string = this._productItem.Coins.toString();
          this.actionBuyByStar.children[0].children[0].getComponent(
            cc.Label,
          ).string = this._productItem.Stars.toString();
        }
        break;
      case ItemState.AVAILIBLE_EQUIP:
        {
          this.equippedPanel.active = false;
          if (this._actionState != null) { this._actionState.active = false; }
          this._actionState = this.actionUse;
          this.iconNode.color = this.colorIconUnlocked;
        }
        break;
      case ItemState.EQUIPPED:
        {
          this.equippedPanel.active = true;
          if (this._actionState != null) { this._actionState.active = false; }
          this._actionState = this.actionHide;
          this.iconNode.color = this.colorIconUnlocked;
        }
        break;
    }
  }
  // hover event
  registerHoverEvents() {
    const self = this;

    this._listObjectsNeedShowDetailItem.forEach((obj) => {
      if (obj) { obj.on(
        cc.Node.EventType.MOUSE_ENTER, (callback) => {
          self.showDetail();
        }, this, false,
      );
      }
    });

    this._listObjectsNeedShowDetailItem.forEach((obj) => {
      if (obj) { obj.on(
        cc.Node.EventType.MOUSE_LEAVE, (callback) => {
          self.hideDetail();
        }, this, false,
      );
      }
    });
  }

  registerClickEvents() {
    const self = this;
    this.actionHide.on(cc.Node.EventType.TOUCH_END, (event) => {
      self.actionHideOnClicked();
    });
    this.actionUse.on(cc.Node.EventType.TOUCH_END, (event) => {
      self.actionUseOnClicked();
    });
    this.actionBuyByCoin.on(cc.Node.EventType.TOUCH_END, (event) => {
      self.actionBuyByCoinOnClicked();
    });
    this.actionBuyByStar.on(cc.Node.EventType.TOUCH_END, (event) => {
      self.actionBuyByStarOnClicked();
    });
  }

  showDetail() {
    this._actionState.active = true;
    this.iconNode.color = this.colorIconUnlocked;
  }

  hideDetail() {
    if (this._state == ItemState.AVAILIBLE_BUY) {
      this.iconNode.color = this.colorIconNotUnlocked;
    }
    this._actionState.active = false;
  }

  setHideItem() {
    this.node.active = false;
  }

  setItemUnSelected() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.itemBackground.color = this.colorBackgroundNomal;
    this.hideDetail();
  }

  actionHideOnClicked() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this._itemShop.onItemHideCallback(this._idItem);
  }

  itemOnSelected() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.itemBackground.color = this.colorBackgroundSelected;
    this._itemShop.onItemSelectedCallback(this._idItem);
    this.showDetail();
  }

  actionUseOnClicked() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this._itemShop.onItemUseCallback(this._idItem);
  }

  actionBuyByStarOnClicked() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this._itemShop.onItemBuyCallback(this._idItem, true);
  }
  actionBuyByCoinOnClicked() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this._itemShop.onItemBuyCallback(this._idItem, false);
  }

  getState() {
    return this._state;
  }
}
