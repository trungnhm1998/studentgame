export default interface IPractiseBlock {
  ID:                   number;
  Oid:                  null;
  PractiseQATemplateID: number;
  QATemplateID:         number;
  ConceptLevel:         number;
  NumberOfQa:           number;
  NumberQaAttempt:      number;
  NumberCorrect:        number;
  NumberInCorrect:      number;
  Percentage:           number;
  SPI:                  number;
  TimeSpentQa:          number;
  TimeSpent:            number;
  Coins:                number;
  Stars:                number;
  BonusCoins:           number;
  BonusStars:           number;
  Active:               boolean;
  CreatedOn:            string;
  UpdatedOn:            string;
  Synced:               boolean;
  SyncedOn:             string;
}
