import { ACE } from "../core/global-info";
import AccountManager from "../managers/account-manager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class FairyMessage extends cc.Component {

  @property(cc.Node)
  chatbox: cc.Node = null;

  @property(cc.Label)
  message: cc.Label = null;

  _camera: cc.Camera = null;
  _scale = 1.7;
  _movingDuration = 1.5;

  turnOnChatBoxAnim() {
    this.chatbox.setRotation(15);
    this.chatbox.runAction(
      cc.sequence(
        cc.spawn(
          cc.fadeIn(0.2),
          cc.rotateBy(0.2, 20),
        ),
        cc.rotateBy(0.03, -5),
      ),
    );
  }

  turnOffChatBoxAnim() {
    this.chatbox.runAction(
      cc.sequence(
        cc.rotateBy(0.03, 5),
        cc.spawn(
          cc.fadeOut(0.2),
          cc.rotateBy(0.2, -20),
        ),
      ),
    );
  }

  updateTutorialMessage(message: string) {
    this.message.string = message;
  }
}
