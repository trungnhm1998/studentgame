const { ccclass, property } = cc._decorator;

import { STUDENT_ROOM } from "../constants/StudentRoomConstants";
import StudentHouseManager from "./StudentHouseManager";
import StudentRoom from "./Model/StudentRoom";

@ccclass
export default class ShowRoomMiniMapManager extends cc.Component {

  @property([cc.Node])
  public MapButton: cc.Node[] = [];

  BtnBuyRoom: cc.Node[] = [];

  _StudentRoom: StudentRoom[] = [];

  onEnable() {
    let currentRoom = StudentHouseManager.getInstance().currentRoom;
    let currentRoomID = this.ConvertStringRoomToID(currentRoom);
    this.BtnBuyRoom = [];
    this.MapButton.forEach((button, id) => {
      // Logic for set active room
      if (id == currentRoomID) {
        button.getChildByName("Mark").active = true;
      } else {
        button.getChildByName("Mark").active = false;
      }

      // Logic for update lock / unlock
      this.BtnBuyRoom.push(button.getChildByName("buy"));
      if (this._StudentRoom[id].IsLocked) {
        this.BtnBuyRoom[id].active = true;
        this.BtnBuyRoom[id].getChildByName("price").getComponent(cc.Label).string = "" + this._StudentRoom[id].Coins; // Coins
        button.getComponent(cc.Button).interactable = false;
      } else {
        this.BtnBuyRoom[id].active = false;
        button.getComponent(cc.Button).interactable = true;
      }

    });
  }

  InitStatusMiniMap(arrayStudentRoom: StudentRoom[]) {
    this._StudentRoom = arrayStudentRoom;
    if (arrayStudentRoom != null && arrayStudentRoom.length > 0) {
      arrayStudentRoom.forEach((studentRoom, ID) => {
        cc.log("############################### --------------");
        cc.log("Room Name : " + studentRoom.Name + " ID: " + ID + "isLocked: " + studentRoom.IsLocked);
      });
    }
  }

  ConvertStringRoomToID(roomName: string): number {
    let res = -1;
    switch (roomName) {
      case STUDENT_ROOM.BED_ROOM:
        res = STUDENT_ROOM.ID_BED_ROOM;
        break;
      case STUDENT_ROOM.KITCHEN_ROOM:
        res = STUDENT_ROOM.ID_KITCHEN_ROOM;
        break;
      case STUDENT_ROOM.LIVING_ROOM:
        res = STUDENT_ROOM.ID_LIVING_ROOM;
        break;
      case STUDENT_ROOM.SHOW_ROOM:
        res = STUDENT_ROOM.ID_SHOW_ROOM;
        break;
      default:
        break;
    }
    return res;
  }

  buyRoom(event, data) {
    cc.log("BUY ROOM ::: " + data);
    let id = this.ConvertStringRoomToID(data);
    StudentHouseManager.getInstance().buyRoom(this._StudentRoom[id].ID);
  }
}
