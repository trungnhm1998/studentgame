import SchoolManager from "../managers/school-manager";
import SchoolItem from "./school-item";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SchoolItems extends cc.Component {
  @property(cc.Node)
  revision: cc.Node = null;

  @property(cc.Node)
  practice: cc.Node = null;

  @property(cc.Node)
  practiceLarge: cc.Node = null;

  @property(cc.Node)
  naplan: cc.Node = null;

  @property(cc.Node)
  naplanLarge: cc.Node = null;

  @property(cc.Node)
  improvement: cc.Node = null;

  @property(cc.Node)
  improvementLarge: cc.Node = null;

  @property(cc.Node)
  homework: cc.Node = null;

  @property(cc.Node)
  homeworkLarge: cc.Node = null;

  @property(cc.Node)
  focus: cc.Node = null;

  @property(cc.Node)
  focusLarge: cc.Node = null;

  @property(cc.Node)
  assessment: cc.Node = null;

  @property(cc.Node)
  assessmentContainer: cc.Node = null;

  @property(cc.SpriteAtlas)
  schoolAtlas: cc.SpriteAtlas = null;

  itemClickCallback: Function = null;

  onLoad() {
  }

  addItemEvent(item: cc.Node, isAssessment = false, isLarge = false) {
    // TODO: Add SchoolItem component here
    let schoolItem = item.getComponent(SchoolItem);
    if (!schoolItem) {
      schoolItem = item.addComponent(SchoolItem);
    }
    schoolItem.onLoad();
    schoolItem.init(this.schoolAtlas.getSpriteFrame(`${item.name}${isLarge ? '_large' : ''}`), this.schoolAtlas.getSpriteFrame(`${item.name}_hide`), isAssessment);
    if (isAssessment) {
      schoolItem.hoverSpriteFrame = this.schoolAtlas.getSpriteFrame('assessment_hover');
    }
    schoolItem.callback = (name) => this.itemClickCallback(name);

    switch (item.name) {
      case 'improvement':
      case 'homework':
      case 'naplan':
      case 'focus':
      case 'revision':
        item.getComponent(SchoolItem).setNotiState(false);
        break;
    }

    item.active = false;
  }

  onHoverItem() {
    this.addItemEvent(this.revision);
    this.addItemEvent(this.practiceLarge, false, true);
    this.addItemEvent(this.naplan);
    this.addItemEvent(this.improvement);
    this.addItemEvent(this.homework);
    this.addItemEvent(this.focus);
    this.addItemEvent(this.assessment, true);
    // TODO: Will be implement later here
    // this.addItemEvent(this.practice);
    // this.addItemEvent(this.naplanLarge);
    // this.addItemEvent(this.homeworkLarge);
    // this.addItemEvent(this.focusLarge);
  }

  /**
   * Based on the server data will show or hide/disable the item
   * if a item is hide the but exceed the 5 item will replace their position with the revision
   * all the data should be loaded now
   */
  setupItems() {
    this.onHoverItem();
    const { assignmentData, improvementData, homeworkData, naplanData, focusData, revisionData } = SchoolManager.getInstance();
    if (assignmentData && !assignmentData.Finished && assignmentData.state != 'disabled') {
      this.assessmentContainer.active = true;
      this.assessment.active = true;
      this.assessment.getComponent(SchoolItem).setState(true);
    } else {
      this.assessmentContainer.active = false;
    }

    this.practiceLarge.active = true;
    let practiceLargeItem = this.practiceLarge.getComponent(SchoolItem);
    practiceLargeItem.setState(true);

    this.improvement.active = true;
    let improvementItem = this.improvement.getComponent(SchoolItem);
    improvementItem.setState(false);
    if (improvementData.state != 'disabled') {
      improvementItem.setState(true);
    }

    this.homework.active = true;
    let homeworkItem = this.homework.getComponent(SchoolItem);
    homeworkItem.setState(false);
    if (homeworkData.state != 'disabled') {
      homeworkItem.setState(true);
      homeworkItem.setNotiState(false);
      if (homeworkData.Counter < 4) {
        homeworkItem.setNotiState(true)
      }
    }

    this.naplan.active = true;
    let naplanItem = this.naplan.getComponent(SchoolItem);
    naplanItem.setState(false); // by default disable all the item
    if (naplanData.state != 'disabled') {
      naplanItem.setState(true);
      naplanItem.setNotiState(false);
      if (naplanData.Counter < 4) {
        naplanItem.setNotiState(true);
      }
    }

    this.focus.active = true;
    // TODO: update large and small dynamic
    let focusCenter = this.focus.getComponent(SchoolItem);
    focusCenter.setState(false);
    if (focusData.state != 'disabled') {
      focusCenter.setState(true);
      focusCenter.setNotiState(true);
    }

    let revisionItem = this.revision.getComponent(SchoolItem);
    if (revisionData.state != 'disabled') {
      this.revision.active = true;
      revisionItem.setState(true);
      if (naplanData.State == 'disabled') {
        this.naplan.active = false;
        this.revision.setPosition(this.revision.parent.convertToNodeSpaceAR(this.naplan.parent.convertToWorldSpaceAR(this.naplan.getPosition())));
      } else if (homeworkData.state == 'disabled') {
        this.homework.active = false;
        this.revision.setPosition(this.revision.parent.convertToNodeSpaceAR(this.homework.parent.convertToWorldSpaceAR(this.homework.getPosition())));
      } else {
        this.improvement.active = false;
        cc.log(this.improvement.getPosition());
        cc.log('this.improvement.parent.convertToWorldSpaceAR(this.improvement.getPosition())', this.improvement.parent.convertToWorldSpaceAR(this.improvement.getPosition()));
        this.revision.setPosition(this.revision.parent.convertToNodeSpaceAR(this.improvement.parent.convertToWorldSpaceAR(this.improvement.getPosition())));
      }
    } else {
      // special case for revision
      this.revision.active = false;
    }
  }
}
