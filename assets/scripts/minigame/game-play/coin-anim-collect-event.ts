const {ccclass, property} = cc._decorator;

@ccclass
export default class CoinAnimCollectEvent extends cc.Component {
  @property(cc.Node)
  ItemCollect: cc.Node = null;

  onCoinCollectFinish(checkFinish: boolean) {
    // cc.error("Coin Finish Play really .... ");
    if (this.ItemCollect != null) {
      this.ItemCollect.active = false;
    }
  }
}
