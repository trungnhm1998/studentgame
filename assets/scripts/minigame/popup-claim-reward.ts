import GamePlayController from "./gameplay-controller";
import { ACE } from "../core/global-info";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupClaimReward extends cc.Component {

  @property(cc.Node)
  congratulationsNode: cc.Node = null;
  @property(cc.Node)
  yourPrizeNode: cc.Node = null;
  @property(cc.Animation)
  animOpenPopup: cc.Animation = null;
  // lable
  @property(cc.Label)
  totalCoinLabel: cc.Label = null;
  @property(cc.Label)
  totalQuestionLabel: cc.Label = null;
  @property(cc.Label)
  totalCorrectedLabel: cc.Label = null;
  @property(cc.Label)
  totalIncorrectLabel: cc.Label = null;

  // onLoad () {}

  start() {}

  onEnable() {
    this.congratulationsNode.active = true;
    this.yourPrizeNode.active = false;
  }

  showCongratulations() {
    this.congratulationsNode.active = true;
    this.yourPrizeNode.active = false;
    this.node.active = true;
    this.animOpenPopup.play();
  }
  showYourPrize() {
    this.congratulationsNode.active = false;
    this.yourPrizeNode.active = true;
    this.node.active = true;
    this.animOpenPopup.play();
  }

  claimRewardBtnOnClick() {
    this.showYourPrize();
    // GamePlayController.getInstance().openClaimReward();
    // Call socket for claim reward when completed math game
    // MinigameService.claimReward();
  }

  goMapBtnOnClick() {
    ACE.sceneManager.pushScene("select-level");
  }

  returnToMapOnClick() {
    ACE.sceneManager.pushScene("select-level");
  }

  chestBtnOnClick() {

  }
}
