import { ACE } from "../core/global-info";
import { MiniGameType, MathsServerAPI, MiniGameTypeKey } from "./constants/minigame-constant";
import GameSaveData from "./game-save-data";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MinigameService {
  static loadMiniMap(type: MiniGameType) {
    let modeValue = "Add";
    switch (type) {
      case MiniGameType.AMAZING_ADDITION:
        modeValue = MiniGameTypeKey.AMAZING_ADDITION;
        break;
      case MiniGameType.SUPPER_SUBTRACTION:
        modeValue = MiniGameTypeKey.SUPPER_SUBTRACTION;
        break;
      case MiniGameType.MAGNIFICIENT_MULTIPLICATION:
        modeValue = MiniGameTypeKey.MAGNIFICIENT_MULTIPLICATION;
        break;
      case MiniGameType.DYNAMIC_DIVISIONS:
        modeValue = MiniGameTypeKey.DYNAMIC_DIVISIONS;
        break;
      case MiniGameType.SUPER_MIX:
        modeValue = MiniGameTypeKey.SUPER_MIX;
        break;
    }

    ACE.gameSocket.emit(MathsServerAPI.LOAD_MINIMAP, {
      mode: modeValue,
    });
  }

  static saveMiniGameData(gameData: GameSaveData) {
    ACE.gameSocket.emit(MathsServerAPI.SAVE_MONKEY_MATHS, gameData);

    ACE.gameSocket.on(MathsServerAPI.SAVE_MONKEY_MATHS, (data) => {
      cc.log("Save Game Response: ", data);
    }, true);
  }

  static claimReward() {
    ACE.gameSocket.emit(MathsServerAPI.CLAIM_REWARD, {
      // saveData: gameData
    });
  }
}
