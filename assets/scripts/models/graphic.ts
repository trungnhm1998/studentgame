import { Graphic, ACTION_TYPE } from '../constants/qa.constants';
export class Graphic {
  graphicId:  Graphic;
  objPrivate: ObjPrivate;
  x:          number;
  y:          number;
  w:          number;
  h:          number;
  name:       string;
  id:         number;
  scale:      number;
}

// generic
export class ObjPrivate {
  anchorX:       number;
  anchorY:       number;
  click:         any;
  dragDrop:      any;
  multiDragDrop: any;
  action:        Action;
}

export class TextStyle {
  font:            string;
  fontStyle:       string;
  fontVariant:     string;
  fontWeight:      string;
  fontSize:        number;
  lineHeight:      number;
  lineSpacing:     number;
  backgroundColor: null;
  fill:            string;
  align:           string;
  boundsAlignH:    string;
  boundsAlignV:    string;
  stroke:          string;
  strokeThickness: number;
  wordWrap:        boolean;
  wordWrapWidth:   number;
  maxLines:        number;
  tabs:            number;
}

export class ItemRef {
  align:  string;
  parent: string;
  index:  number;
  anchor: string;
  count:  number;
}

export class Action {
  name:    string;
  type:    ACTION_TYPE;
}

export class ListAction extends Action {
  x:       number;
  y:       number;
  anchor:  ActionAnchor;
  dir:     string;
  spacing: number;
}

export class DragAction extends Action {
  value:    string;
  multiple: boolean;
  toggle:   boolean;
}

export class ListChildAction {
  name:    string;
  type:    ACTION_TYPE;
  itemRef: ItemRef;
}

export interface ActionAnchor {
  x: number;
  y: number;
}

export class ImageObjPrivate extends ObjPrivate {
  keyImage:       string;
  linkImage:      string;
}

export class TextObjPrivate extends ObjPrivate {
  style:        TextStyle;
  text:         string;
}

// ----------------------- Parents/Containers -----------------------
// graphicId: 2
// but have action and action.type: 'list'
export class ListObjPrivate extends ObjPrivate {
  centerX:       number;
  centerY:       number;
  lineWidth:     number;
  lineColor:     number;
  radius:        number;
  action:        ListAction;
}

// graphicId: 1
// but have action and action.type: 'drag'
export class DragObjPrivate extends ObjPrivate {
  anchorX:       number;
  anchorY:       number;
  keyImage:      string;
  linkImage:     string;
  action:        DragAction;
}

// TODO: Implement drop zone
// graphicId: 2
// have action and action.type: 'drop

// ----------------------- Childs -----------------------
// graphicId: 1
// but have action and action.itemRef
export class ListChildObjPrivate extends ObjPrivate {
  anchorX:       number;
  anchorY:       number;
  keyImage:      string;
  linkImage:     string;
  action:        ListChildAction;
}

// is a text object
// graphicId: 3
// but have action and action.itemRef
export class DragChildObjPrivate extends TextObjPrivate {
  anchorX:       number;
  anchorY:       number;
  keyImage:      string;
  linkImage:     string;
  action:        DragAction;
}

