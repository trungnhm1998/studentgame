import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

const { ccclass, property } = cc._decorator;

export interface ICharacterItem {
  index: number;
  CharacterProductPresentationID: number;
  CharacterWearingGroupID: number;
  Name: string;
  Url: string;
  isSelected?: boolean;
}

export interface IWearingGroup {
  ID: number;
  Name: string;
  PosX: number;
  PosY: number;
  PosZ: number;
  Gender: boolean;
  CharacterTypeID: number;
}

const IMAGES_DIR_PREFIX = "images";

@ccclass
export default class CharacterComponent extends cc.Component {
  itemOrder: Map<number, cc.Node> = null;
  itemURLs: Map<number, string> = null;
  itemLoadedQueue: any[] = [];

  _isSmall: boolean = true;
  _resPath: string = "";
  _loadWithAnim: boolean = false;
  _renderOrder: IWearingGroup[] = [];
  _characterItems: ICharacterItem[] = [];

  // calbacks
  _onCharacterShow: Function = () => {};

  // TODO: Remove
  public set isSmall(isSmall: string | boolean) {
    this._isSmall = false;
    if (isSmall == "small" || isSmall == true) {
      this.node.y = -30;
      this._isSmall = true;
    } else {
      this.node.y = 0;
    }
  }

  // TODO: Remove because unused
  public get isSmall() {
    return this._isSmall;
  }

  onLoad() {
    ACE.gameSocket.on(SERVER_EVENT.WEARING_GROUP_OF_FRIEND, (data) => {
      if (!this.node.active) {
        return;
      }
      if (data.group) {
        this.setRenderOrder(data.group);
        ACE.gameSocket.emit(SERVER_EVENT.LOAD_FRIEND_CHARACTER_USING);
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_FRIEND_CHARACTER_USING, (data) => {
      if (!this.node.active) {
        return;
      }
      if (data.usingItemsAsset) {
        this.setRenderItems(data.usingItemsAsset);
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.WEARING_GROUP, (data) => {
      if (!this.node.active) {
        return;
      }
      if (data.group) {
        if (data.type) {
          this.isSmall = data.type;
        }
        this.setRenderOrder(data.group);
        ACE.gameSocket.emit(SERVER_EVENT.USING_ITEMS);
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.TRY_ITEM_PRODUCT, (data) => {
      if (!this.node.active) {
        return;
      }
      if (data.tryingItemsAsset) {
        this.setRenderItems(data.tryingItemsAsset);
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.EQUIP_CHARACTER_ITEM, (data) => {
      if (!this.node.active) {
        return;
      }
      if (data.usingItemsAsset) {
        this.setRenderItems(data.usingItemsAsset);
      }
    });

    ACE.gameSocket.on(SERVER_EVENT.USING_ITEMS, (data) => {
      if (!this.node.active) {
        return;
      }
      if (data.usingItemsAsset) {
        this.setRenderItems(data.usingItemsAsset);
      }
    });
  }

  /**
   * using Local api and emit for auto update wearing
   */
  autoUpdate(withAnim = true, isMe = true) {
    this._loadWithAnim = withAnim;
    if (this.node.active) {
      if (isMe) {
        ACE.gameSocket.emit(SERVER_EVENT.WEARING_GROUP);
      } else {
        ACE.gameSocket.emit(SERVER_EVENT.WEARING_GROUP_OF_FRIEND);
      }
    }
  }

  /**
   * Will be deprecate because port version bad practices
   * skin, face, shoe, cloth, hair property might not working properly
   * @param renderOrder rendering order from server
   */
  setRenderOrder(renderOrder: IWearingGroup[]) {
    this._renderOrder = renderOrder;
    // double check data before re create the render order
    if (this.itemOrder) {
      for (let i = 0; i < renderOrder.length; i++) {
        const order = renderOrder[i];
        if (!this.itemOrder.has(order.ID)) {
          // if data different break the loop
          break;
        }
        if (i == renderOrder.length - 1) {
          // not going to execute the code below because data has nothing new
          return;
        }
      }
    }
    this.itemOrder = new Map<number, cc.Node>();
    this.itemURLs = new Map();
    // this.node.removeAllChildren();
    this.node.children.forEach((child) => {
      if (!child.name.startsWith("_")) {
        child.removeFromParent();
      }
    });
    for (let i = 0; i < renderOrder.length; i++) {
      const group = renderOrder[i];
      const itemNode = new cc.Node();
      const spriteComp = itemNode.addComponent(cc.Sprite);
      this.node.addChild(itemNode, i, group.Name);
      this.itemOrder.set(group.ID, itemNode);
    }
  }

  /**
   * Will deprecated or change
   * @param characterItems
   */
  setRenderItems(characterItems: ICharacterItem[]) {
    this._characterItems = characterItems;
    if (this._loadWithAnim) {
      this.node.setScale(0);
    }
    for (const renderNode of this.node.children) {
      if (!renderNode.name.startsWith("_")) {
        renderNode.active = false;
      }
    }
    for (let i = 0; i < characterItems.length; i++) {
      const characterItem = characterItems[i];
      const itemRendererNode = this.itemOrder.get(characterItem.CharacterWearingGroupID);
      if (itemRendererNode == null || itemRendererNode == undefined) {
        // in some cases developer missed to call wearing order so prevent render here
        continue;
      }
      const normalizeFilePath: string = cc.path.dirname(characterItem.Url);
      const finalizeFilePath = `${IMAGES_DIR_PREFIX}/${normalizeFilePath.toLowerCase()}/${cc.path.basename(characterItem.Url, ".png")}`;
      itemRendererNode.active = true;
      if (this.itemURLs.get(characterItem.CharacterWearingGroupID) == finalizeFilePath) {
        cc.log("Skip load res for ", finalizeFilePath);
        continue;
      }
      this.itemURLs.set(characterItem.CharacterWearingGroupID, finalizeFilePath); // prevent multiple load
      this.itemLoadedQueue.push(false);
      cc.loader.loadRes(finalizeFilePath, (error: Error, resource: cc.Texture2D) => {
        if (error) {
          cc.log("CharacterComponent::load item failed with error", error);
          return;
        }
        const spriteComp = itemRendererNode.getComponent(cc.Sprite);
        spriteComp.spriteFrame = new cc.SpriteFrame(resource);
        this.itemLoadedQueue.pop();
        this.showCharacter();
      });
    }
  }

  showCharacter() {
    if (this.itemLoadedQueue.length != 0) { return; }
    this.node.active = true;
    if (this._loadWithAnim) {
      this.node.setScale(0);
      this.node.runAction(cc.scaleTo(0.5, 1));
      this._loadWithAnim = false; // only play the scale action once
    }
    this._onCharacterShow();
  }
}
