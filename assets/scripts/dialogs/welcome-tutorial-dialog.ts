import DialogComponent from "../common/dialog-component";
import { Random } from "../core/utils/random-utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WelcomeTutorialDialog extends DialogComponent {
  @property(cc.Label)
  message: cc.Label = null;

  @property(cc.Button)
  nextButton: cc.Button = null;

  @property(cc.Float)
  showMessageInterval: number = 0.05;

  @property(cc.Boolean)
  preventSkip: boolean = true;

  @property(cc.Sprite)
  eye: cc.Sprite = null;

  @property(cc.Sprite)
  mouth: cc.Sprite = null;

  @property([cc.SpriteFrame])
  eyes: cc.SpriteFrame[] = [];

  @property([cc.SpriteFrame])
  mouths: cc.SpriteFrame[] = [];

  _messages: string[] = [];
  private _finishCallback: Function;
  private _messagesLength: number = -1;
  private _messagesIndex: number = -1;
  private _displayingMessage: string = '';
  private _messageIndex: number = 0; // for display split image
  private stopAction = false;
  private hasSetupProfessorAction = false;
  private eyeClosed = false;
  private _readingAction: cc.ActionInterval;

  onLoad() {
    this._readingAction = this.readingAction();
  }

  onEnable() {
    this.eye.spriteFrame = this.eyes[1];
    this.messages = [
      "Hi there! Welcome to the world of S.T.A.R Maths Online! Hmm, let's see what we have...",
      "Oh! My name is Professor Jupiter. I created this world for you and others to enjoy.",
      "I seem to have lost my glasses...can you tell me what you look like while I find my glasses?"
    ];
    this.nextMessage();
  }

  public set finishCallback(callback: Function) {
    this._finishCallback = callback;
  }

  public set messages(messages: string[]) {
    this._displayingMessage = '';
    this._messageIndex = 0;
    this._messagesIndex = -1;
    this._messagesLength = messages.length;
    this._messages = messages;
  }

  onNextMessage() {
    this.nextMessage();
  }

  update(dt) {
    if (this.stopAction) {
      this.stopAction = false;
      this.node.stopAction(this._readingAction);
    }
  }

  /**
   * this will called when the tutorial read all the message
   */
  onFinishTutorial() {
    if (this._finishCallback) {
      this._finishCallback();
    }
    this.closeDialog();
  }

  readingAction(): cc.ActionInterval {
    return cc.repeatForever(
      cc.sequence(
        cc.callFunc(() => {
          if (this._messageIndex < this._displayingMessage.length) {
            this.message.string = this._displayingMessage.substring(0, this._messageIndex);
            this._messageIndex++;
          } else {
            this.stopAction = true;
          }
        }
        ),
        cc.delayTime(this.showMessageInterval)
      ));
  }

  nextMessage() {
    if (this.preventSkip) {
      if (this._messageIndex < this._displayingMessage.length && !this.stopAction)
        return;
    }
    this._messageIndex = 0;
    this._messagesIndex++;
    this._displayingMessage = this._messages[this._messagesIndex];
    if (!this._displayingMessage) {
      this.onFinishTutorial();
      return;
    }
    this.node.runAction(this._readingAction);

    if (!this.hasSetupProfessorAction) {
      let random = new Random();
      this.hasSetupProfessorAction = true;
      // mouth
      this.node.runAction(cc.repeatForever(
        cc.sequence(
          cc.callFunc(() => {
            this.mouth.spriteFrame = random.fromList(this.mouths);
          }),
          cc.delayTime(0.5)
        )
      ))

      // blinking
      this.node.runAction(cc.repeatForever(
        cc.sequence(
          cc.delayTime(2.5),
          cc.callFunc(() => {
            this.eye.spriteFrame = this.eyes[0];
          }),
          cc.delayTime(0.25),
          cc.callFunc(() => {
            this.eye.spriteFrame = this.eyes[1];
          })
        )
      ))
    }
  }
}
