import { Config } from './../config';
import { HttpClient } from "./../core/http-client";
import { ACE } from "./../core/global-info";
import Boost from '../common/boost';
import WebviewManager from '../managers/webview-manager';
import { NoticeDialogType } from '../dialogs/notice-dialog';
import { NativeManager } from '../managers/native-manager';
const { ccclass, property } = cc._decorator;

const IS_SERVER = false;
const IS_CLIENT = true;

interface PlatformConfig {
  Server: string;
  Client: string;
}

@ccclass
export default class CheckServer extends cc.Component {
  static _instance: CheckServer = null;
  serverIndex = 1;
  http: HttpClient = null;
  clients: string[] = [];
  servers: string[] = [];
  originalConfig: PlatformConfig = null;
  _serverToCheck: string = "";
  _isOriginalServerFreeze = true;
  onLoad() {
    ACE.native = NativeManager.getInstance();
    CheckServer._instance = this;
    cc.game.addPersistRootNode(this.node);
    this.http = new HttpClient();
    this.schedule(this.checkServerFreezingRoutine.bind(this), 30);
    this.getPlatformConfig().then((res) => {
      this._serverToCheck = res.Server;
      // this._serverToCheck = "https://autotest.starmathsonline.com.au";
    }).catch((error) => {
      cc.log("CheckServer::onLoad::getPlatformConfig::error", error);
    })
    // this.checkServerFreezingRoutine();
  }

  async getArrays() {
    this.clients = await this.getBackupArray(IS_CLIENT);
    this.servers = await this.getBackupArray(IS_SERVER);
  }

  async checkServerFreezingRoutine() {
    // if boost not even load yet or firebase config not loaded and connect then return
    if (!Boost._instance || !ACE.gameSocket._connected) {
      return;
    }
    cc.log("checkServerFreezingRoutine");
    // get configs
    this.originalConfig = await this.getPlatformConfig();
    if (this.originalConfig == null) {
      return null;
    }
    cc.log("CheckServer::checkServerFreezingRoutine::this.originalConfig", this.originalConfig);

    // check original first
    // let prioritiesServerToCheck = this.originalConfig.Server;
    // https://serverqaandroid.starmathsonline.com.au/app.js
    // let prioritiesServerToCheck = "https://autotest.starmathsonline.com.au";
    cc.log(`CheckServer::checkServerFreezingRoutine::check if last connected server is freezing [${this._serverToCheck}]`);
    if (await this.isServerFreeze(this._serverToCheck)) {
      if (this._serverToCheck == this.originalConfig.Server) {
        this._isOriginalServerFreeze = true;
      }
      cc.log(`CheckServer::checkServerFreezingRoutine::server ${this._serverToCheck} is freezing`);
      await this.getArrays();
      // original freeze we start to changing to other server here
      // reset to 0
      if (this.serverIndex >= this.servers.length) {
        this.serverIndex = 0;
      }
      for (let i = this.serverIndex; i < this.servers.length; i++) {
        let serverToSwitch: PlatformConfig = {
          Server: this.servers[i],
          Client: this.clients[i],
        };
        let isServerFreeze = await this.isServerFreeze(serverToSwitch.Server);
        if (!isServerFreeze) {
          if (await this.switchServer(serverToSwitch)) {
            this.serverIndex = i;
            cc.log(`CheckServer::checkServerFreezingRoutine::switchToServer::${serverToSwitch.Server}`);
            break;
          }
        } else {
          cc.log(`CheckServer::switch to ${serverToSwitch.Server} failed because is freezing too`);
        }
      }
    } else {
      cc.log("CheckServer::checkServerFreezingRoutine::Last server is not freezing start to check if original is freezing");
      // not freezing
      // check could we switch back to the original server if we are on something not original
      let { loaded, qaClient, qaServer } = Config;
      if (!loaded) {
        // boost hasn't loaded yet neither the firebase config
        cc.log("CheckServer::checkServerFreezingRoutine::Firebase Config not loaded");
        return null;
      }
      if (qaServer == this.originalConfig.Server) {
        cc.log("CheckServer::checkServerFreezingRoutine::same server skipped");
        return null;
      }
      // check if the server we are on is the same with original?
      if (Config && qaClient != this.originalConfig.Client && qaServer != this.originalConfig.Server) {
        // not the same
        // switch back to original because it's not freezing
        cc.log("CheckServer::checkServerFreezingRoutine::current connected server is not original server");
        if (!await this.isServerFreeze(this.originalConfig.Server) && this._isOriginalServerFreeze) {
          cc.log("CheckServer::checkServerFreezingRoutine::Original server is not freezing");
          this._isOriginalServerFreeze = false;
        }
        if (ACE.webviewManager.isWebviewHidden) {
          cc.log("CheckServer::checkServerFreezingRoutine::webview is hidden force switch back to original server");
          let result = await this.switchBackToOriginalServer(true);
          cc.log(`Switch back original server [${this.originalConfig.Server}] because webview is not open result [${result}]`);
        }
      }
    }
  }

  async switchBackToOriginalServer(skipCheck = false) {
    if (this._isOriginalServerFreeze) {
      return new Error("Original server freeze");
    }
    if (skipCheck || !await this.isServerFreeze(this.originalConfig.Server)) {
      let result = await this.switchServer(this.originalConfig);
      cc.log("switching back to original server result: ", result);
      return true;
    }
    return new Error("switchBackToOriginalServer failed with unknow problem.");
  }

  updateWebviewUrl(oldClientURL: string, newClientURL: string) {
    if (Boost._instance) {
      const { qaWebviewContainer, qaWebview } = Boost._instance;
      if (ACE.webviewManager && !ACE.webviewManager.isWebviewHidden && qaWebview.url != "") {
        let text = "It seems that there is something wrong with the internet connection. Don’t worry, we will reload this page right away!";
        ACE.dialogManager.showNoticeDialog(text, NoticeDialogType.OK, null, null, null, 5);
      }

      if (qaWebviewContainer.active && !ACE.webviewManager.isWebviewHidden) {
        cc.log(`CheckServer::switchServer::qaWebviewis opening update it url from ${qaWebview.url} to ${newClientURL}`);
        let replacedUrl = qaWebview.url.replace(oldClientURL, newClientURL);
        Boost._instance.qaWebview.url = replacedUrl;
        cc.log("current webview url", qaWebview.url);
      } else {
        if (ACE.webviewManager.isWebviewHidden) {
          WebviewManager.getInstance().webviewPreloaded = false;
          WebviewManager.getInstance().preloadWebview();
        }
      }
    }
  }

  async switchServer(configToSwitch: PlatformConfig) {
    // check if qa webview currently open?
    // check boost is loaded?
    let isConnectedToNewServer = false; // failed
    const oldClientURL = Config.qaClient;
    Config.qaClient = configToSwitch.Client;
    Config.qaServer = configToSwitch.Server;
    this.updateWebviewUrl(oldClientURL, configToSwitch.Client);
    try {
      await ACE.qaRenderSocket.connect(configToSwitch.Server);
    } catch (error) {
      cc.log(`switching to server ${configToSwitch.Server} failed with error ${error}`);
      isConnectedToNewServer = false;
      return false;
    }
    if (ACE.qaRenderSocket._connected) {
      Boost._instance.registerQaServerSocketEvents();
      isConnectedToNewServer = true;
    }
    if (isConnectedToNewServer) {
      this._serverToCheck = configToSwitch.Server;
    }
    return isConnectedToNewServer;
  }

  async isServerFreeze(serverUrl: string, timeout = 15) {
    let finalUrl = `${serverUrl}/app.js`.replace("//app.js", "/app.js");
    cc.log(`CheckServer::isServerFreeze::checking if ${finalUrl} is freezing`);
    let response = null;
    try {
      response = await this.timeoutPromise(15, this.http.get(finalUrl, {}, false)) as Response;
    } catch (error) {
      cc.log("CheckServer::isSererFreeze::error", error);
      return true;
    }
    let result = (response && response.status != 200);
    cc.log(`CheckServer::isServerFreeze::response is [${result ? "Freezing" : "NOT FREEZING"}]`, response);
    return result;
  }

  timeoutPromise(ms: number, promise: Promise<Response>) {
    return new Promise((resolve, reject) => {
      this.scheduleOnce(() => {
        reject(new Error("Timeout"));
      }, ms);
      promise.then((res) => {
        resolve(res);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  getBackupArray(isClient = IS_CLIENT): Promise<string[]> {
    return new Promise(async (resolve, reject) => {
      let arrayType = isClient ? "qaClients" : "qaServers";
      let url = `https://au-student-game.firebaseio.com/configs/common/${arrayType}.json`;
      let response: string[] = await this.http.get(url, {});
      if (response && response.length != 0) {
        resolve(response);
      } else {
        reject(null);
      }
    });
  }

  async getPlatformConfig(): Promise<PlatformConfig> {
    let platform = "";
    let isDebug = CC_DEBUG ? "debug" : "production";
    cc.log(`CheckServer::getPlatformConfig::platform android[${cc.sys.OS_ANDROID}:${cc.sys.ANDROID}] ios[${cc.sys.OS_IOS}:${cc.sys.IPAD}]`, cc.sys.platform);
    switch (cc.sys.platform) {
      case cc.sys.ANDROID:
        platform = "android";
        break;
      case cc.sys.IPHONE:
      case cc.sys.IPAD:
        platform = "ios";
        break;
      default:
        platform = "web";
        break;
    }
    let originalClientURL = `https://au-student-game.firebaseio.com/configs/${platform}/${isDebug}/qaClient.json`;
    let originalServerURL = `https://au-student-game.firebaseio.com/configs/${platform}/${isDebug}/qaServer.json`;
    cc.log("CheckServer::getPlatformConfigs", originalClientURL, originalServerURL);
    let server = await this.http.get(originalServerURL, {});
    let client = await this.http.get(originalClientURL, {});
    let config: PlatformConfig = {
      Server: server,
      Client: client,
    };
    if (server && client) {
      return config;
    } else {
      return null;
    }
  }
}
