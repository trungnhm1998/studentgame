const {ccclass, property} = cc._decorator;
 
@ccclass
export default class ActiveAnimation extends cc.Component {
    
    start () {
        this.registerEventMouseEnter();
        this.registerEventMouseLeave();
    }

    registerEventMouseEnter(){
        var self = this;
        this.node.on(
            cc.Node.EventType.TOUCH_START, callback =>{
                // cc.log("On my way .....")
                self.SetActiveAnimation(true);
            },this,false
        )
    }

    registerEventMouseLeave(){
        var self = this;
        this.node.on(
            cc.Node.EventType.TOUCH_CANCEL, callback =>{
                self.SetActiveAnimation(false);
            },this,false
        )

        this.node.on(
            cc.Node.EventType.TOUCH_END, callback =>{
                self.SetActiveAnimation(false);
            },this,false
        )
    }


    SetActiveAnimation(trigger: boolean){
        var animNode = this.node.getChildByName("animation");
        var anim = animNode.getComponent(cc.Animation);

        if(trigger)
        {    
            if(animNode) animNode.active = true;   
            if(anim) anim.play();
        }
        else
        {
            if(anim) anim.stop();
            if(animNode) animNode.active = false;   
        }
    }   
}
