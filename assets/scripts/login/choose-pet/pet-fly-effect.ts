const { ccclass, property } = cc._decorator;

@ccclass
export default class PetFlyEffect extends cc.Component {
  @property(cc.Node)
  particleNode: cc.Node = null;

  @property(cc.Sprite)
  miniPetSprite: cc.Sprite = null;

  _startPos: cc.Vec2 = cc.v2();
  _endPos: cc.Vec2 = cc.v2();

  onLoad() {
    if (CC_JSB) {
      this.particleNode.removeFromParent();
    }
  }

  /**
   * convert world pos to local then update logic for effect
   * @param startPos world pos
   * @param endPos world pos
   */
  playEffect(startPos: cc.Vec2, endPos: cc.Vec2) {
    const localStartPos = this.node.parent.convertToNodeSpaceAR(startPos);
    const localEndPos = this.node.parent.convertToNodeSpaceAR(endPos);
    const bezier = [localStartPos, cc.v2(0, 550), localEndPos];
    this.node.setPosition(localStartPos);
    cc.log(this.node, bezier);
    return new Promise((resolve, reject) => {
      this.node.runAction(cc.sequence(
        // cc.moveTo(0.5, localEndPos).easing(cc.easeOut(0.5)),
        cc.spawn(
          cc.bezierTo(1.0, bezier).easing(cc.easeInOut(3.0)),
          cc.scaleTo(0.5, 1),
        ),
        cc.callFunc(() => {
          resolve();
        }),
      ));
    });
  }
}
