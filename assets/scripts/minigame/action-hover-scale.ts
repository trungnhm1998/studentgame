const { ccclass, property } = cc._decorator;

@ccclass
export default class ActionHoverScale extends cc.Component {
  _scaleDefault: number = 0.0;
  _scaleHover: number = 1.4;

  start() {
    this._scaleDefault = this.node.scale;
    this.registerHoverEvents();
  }

  registerHoverEvents() {
    let self = this;

    this.node.on(
      cc.Node.EventType.MOUSE_ENTER,
      (event) => {
        self.node.scale = self._scaleHover;
      },
      this,
    );
    this.node.on(
      cc.Node.EventType.MOUSE_LEAVE,
      (event) => {
        self.node.scale = self._scaleDefault;
      },
      this,
    );
  }
}
