import { OperatorType } from "../../constants/minigame-constant";

const { ccclass, property } = cc._decorator;

export default class MathQuestion {
  firstNumber: number = null;
  secondNumber: number = null;
  operatorType: OperatorType = null;

  isQuestionAnswered = false;

  constructor(firstN, secondN, operator) {
    this.firstNumber = firstN;
    this.secondNumber = secondN;
    this.operatorType = operator;
    this.isQuestionAnswered = false;
  }

  getMathResult() {
    let result = 0;
    switch (this.operatorType) {
      case OperatorType.ADD:
        result = this.firstNumber + this.secondNumber;
        break;
      case OperatorType.SUB:
        result = this.firstNumber - this.secondNumber;
        break;
      case OperatorType.MULTI:
        result = this.firstNumber * this.secondNumber;
        break;
      case OperatorType.DIV:
        result = this.firstNumber / this.secondNumber;
        break;
    }
    return result;
  }
}