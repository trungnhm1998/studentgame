import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

export default class MapService {
  static openRoom(args) {
    ACE.gameSocket.emit(SERVER_EVENT.OPEN_ROOM, args);
  }

  static loadMessageListAll() {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_MESSAGE_LIST_ALL);
  }
}
