import Entity from "./entity";
import EntityManager from "./entity-manager";
import { Random } from "../utils/random-utils";
import SystemManager from "./system-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default abstract class System extends cc.Component {
  _filter: string[] = [];
  _entities: Map<string, Entity>;
  _uuidv4: string = "";

  onLoad() {
    this._uuidv4 = "s_" + new Random().uuidv4();
    SystemManager.getInstance().addSystem(this);
    this._entities = new Map();
    this.updateEntityRefs();
  }

  updateSingleEntityRef(entity: Entity) {
    let isDirty = false;
    if (this._filter.length == 0) { return; }
    for (let i = 0; i < this._filter.length; i++) {
      const fil = this._filter[i];
      if (!entity.has(fil)) {
        isDirty = true;
        break;
      }
    }
    if (!isDirty && !this._entities.has(entity._uuidv4)) {
      this._entities.set(entity._uuidv4, entity);
      this.onEntitiesUpdate(entity);
    } else {
      // remove the entity when component got removed
      if (this._entities.has(entity._uuidv4) && isDirty) {
        this._entities.delete(entity._uuidv4);
        entity.node.stopAllActions(); // hybrid?
      }
    }
  }

  onEntityDelete(entity: Entity) {
    this._entities.delete(entity._uuidv4);
  }

  updateEntityRefs() {
    const allEntities = EntityManager.getInstance().entities;
    allEntities.forEach((entity) => {
      this.updateSingleEntityRef(entity);
    });
  }

  abstract onEntitiesUpdate(entity: Entity);
}
