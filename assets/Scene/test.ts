import { ACE } from "../scripts/core/global-info";
import { StringUtils } from "../scripts/core/utils/string-utils";
import { IQATemplate } from "./qa-template-interface";
import EntityManager from "../scripts/core/ecs/entity-manager";
import Entity from "../scripts/core/ecs/entity";
import PositionComponent, { IPositionComponent } from "../scripts/common/ecs-components/position-component";
import QARendererV2 from "../scripts/qa/qa-renderer-v2";
import GameSocket from "../scripts/core/networks/GameSocket";
import WebSocketProtocol from "../scripts/core/networks/WebSocketProtocol";

const { ccclass, property } = cc._decorator;

const QA_LIST = [
  // "Click One",
  "Y6.NA.WN.SN.SQUARE.1B",
  "Y5.NA.WN.PPVNS.1B",
  "Y5.MG.AN.IDANGLES.1B",
  "Y4.NA.FD.CF3.1B",
  "Y4.SP.CH.EWA.1B",

  // "Click Many",
  "Y2.MG.3DS.3DV2D3.1B",
  "Y2.MG.3DS.3Dvs2D2.1B",
  "Y2.NA.WN.PSCHOWMUCH.1B",
  "Y3.MG.VC.RTOLC.1B",
  "Y4.NA.MD.DF.1B",

  // "Click One + Click Many",
  "Y3.MG.MA.OWSM.1B",

  // "Click One + Input Number (1)",
  "Y6.MG.LEN.CONV.1B",
  "Y6.MG.LEN.CONV.2B",
  "Y6.MG.LEN.CONV.3B",
  "Y6.MG.LEN.CONV.4B",
  "Y6.MG.LEN.CONV.5B",

  // "Drag and Drop (1)",
  "Y1.MG.PO.FOIA.1B",
  "Y2.NA.PA.NPW.1B",
  "Y3.MG.MA.EST1KG.1B",
  "Y4.MG.3DS.2DVS3D.1B",
  "Y5.NA.FD.FRAC.COMPARE.1B",

  // "Drag and Drop (2)",
  "Y1.MG.2DS.N2DS.1B",
  "Y1.MG.MA.RECMPENB.1B",
  "Y1.MG.TI.RDIDW.1B",
  "Y2.SP.DA.DTG.1B",
  "Y3.MG.AR.LNSM.1B",
  "",
  "",
  "Drag and Drop (3)",
  "Y1.MG.AR.OOA.1B",
  "Y2.MG.TI.ORDTIMEUIU.1B",
  "Y3.MG.VC.COV.1B",
  "Y5.MG.2DS.TRR.1B",
  "Y5.MG.2DS.TRR.1B",

  // "Drag and Drop (4)",
  "Y1.MG.3DS.I3DSDO.1B",
  "Y2.MG.AR.OOA.1B",
  "Y3.MG.2DS.ID2DS.1B",
  "Y4.MG.AN.OAL2S.1B",
  "Y5.NA.FD.DEC.ORDERASC1.1B",

  // "Drag and Drop (Many)",
  "Y1.MG.3DS.3DO.1B",
  "Y2.MG.3DS.SORT3D.1B",
  "Y3.MG.PO.PPOS.1B",
  "Y4.NA.MD.1TO6D.1B",
  "Y5.NA.MD.7TO12D.1B",

  // "Drag and Drop (1) + Input Number (1)",
  "Y5.SP.DA.RDLG.1B",
  "Y6.NA.WN.MIX.PS.1B",

  // "Drag and Drop (1) + Input Number (2)",
  "Y4.MG.TI.RT.1B",
  "Y4.MG.TI.A2D.1B",

  // "Drag and Drop (Many) + Input Number",
  "Y1.MG.LE.MIU.1B",

  // "Input Number (1)",
  "Y1.NA.AS.ADDF.1B",
  "Y2.MG.AR.GRID3.1B",
  "Y3.NA.AS.EQVC.1B",
  "Y4.MG.MA.PMS.1B",
  "Y5.NA.MD.11X.1B",

  // "Input Number (2)",
  "Y6.SP.CH.DP.FRAC.1B",
  "Y5.MG.TI.PS24HR.1B",
  "Y4.NA.FD.RWN.1B",
  "Y3.MG.LE.LCMM2CM.1B",
  "Y2.MG.TI.WTIMED.1B",

  // "Input Number (3)",
  "Y1.NA.AS.ASEN.1B",
  "Y2.NA.MD.DSHARE10.1B",
  "Y3.NA.AS.UAPOA.1B",
  "Y4.NA.FD.D2FC.1B",
  "Y5.MG.3DS.PROPFACE.1B",

  // "Input Number (4)",
  "Y2.NA.AS.INV.1B",
  "Y3.NA.AS.UATCS.1B",
  "Y4.NA.WN.PN.1B",
  "Y5.NA.MD.VM1D.1B",
  "Y5.NA.WN.FACTORS.1B",

  // "Input Number (Many)",
  "Y1.NA.MD.C10.1B",
  "Y2.NA.AS.ADDSPLIT.1B",
  "Y3.NA.AS.APPV2.1B",
  "Y4.MG.2DS.NS.1B",
  "Y5.NA.WN.MULTIP.1B",

  // "Input Number (2) + Drag and Drop (1)",
  "Y2.NA.MD.MPROP.1B",

  // "Input Number (1) + Drag and Drop (1)",
  "Y4.NA.PA.PAOE.1B",
  "Y4.NA.PA.PMOE.1B",
  "Y4.NA.PA.PSOE.1B",

  // "Input Number (2) + Input Letter (1)",
  "Y1.NA.WN.READ.1B",
  "Y4.MG.TI.RC.1B",

  // "Input Letter (1)",
  "Y4.NA.WN.N2W10000.1B",
  "Y5.MG.3DS.C3DAND2D.1B",
  "Y6.MG.TI.READ.DIG.1B",
  "Y1.NA.WN.N2W.1B",
  "Y2.MG.3DS.3DOBJ.1B",

  // "Drag and Drop (2) + Input Letter (2) + Input Number (2)",
  "Y6.MG.PO.MAPS.1B",
];

@ccclass
export default class TestScene extends cc.Component {
  @property
  _qaIndex: number = 0;

  @property
  set qaIndex(source: number) {
    if (source >= 0 && source < QA_LIST.length) {
      this._qaIndex = source;
      this.loadQaTemplate(QA_LIST[this._qaIndex]);
    }
  }

  get qaIndex(): number {
    return this._qaIndex;
  }

  @property(cc.Node)
  qaBoard: cc.Node = null;

  @property(QARendererV2)
  render: QARendererV2 = null;

  @property(cc.JsonAsset)
  localTemplate: cc.JsonAsset = null;

  _qaInputValue: cc.EditBox = null;
  _network: GameSocket = null;
  _keydown = false;

  onLoad() {
    EntityManager.getInstance();
    ACE.string = new StringUtils();
    this._network = new GameSocket(new WebSocketProtocol());
    this._network.connect("wss://qajs.starmathsonline.com.au").then(() => {
      if (this.localTemplate) {
        this.render.renderQA(this.localTemplate.json);
      } else {
        this.loadQaTemplate("Y1.MG.PO.FOIA.1B");
      }
    });
    // cc.log(QA_LIST[this.qaIndex]);
    this._qaInputValue = this.node.getComponentInChildren(cc.EditBox);
    // this._qaInputValue.string = "Y6.MG.PO.MAPS.1B"; // default
    // tap next and right on screen
    // this.node.on(cc.Node.EventType.TOUCH_START, this.onTouch.bind(this));
    this._network.on("loadQaTemplate", (data) => this.onQaTemplateLoaded(data));
    // DEBUG
    this.node.on(cc.Node.EventType.MOUSE_WHEEL, (event: cc.Event.EventMouse) => {
      const scrollData = event.getScrollY();
      if (scrollData > 0) {
        this.qaBoard.scaleX += 0.1;
        this.qaBoard.scaleY += 0.1;
      } else {
        this.qaBoard.scaleX -= 0.1;
        this.qaBoard.scaleY -= 0.1;
      }
    });
    this.node.on(cc.Node.EventType.MOUSE_DOWN, (event: cc.Event.EventMouse) => {
      if (event.getButton() == cc.Event.EventMouse.BUTTON_RIGHT) {
        this.qaBoard.setScale(1);
        this.qaBoard.setPosition(0, 0);
      }
    });
    this.node.on(cc.Node.EventType.MOUSE_MOVE, (event: cc.Event.EventMouse) => {
      if (this._keydown) {
        this.qaBoard.x += event.getDeltaX();
        this.qaBoard.y += event.getDeltaY();
      }
    });
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, (event: cc.Event.EventKeyboard) => {
      if (event.keyCode == cc.macro.KEY.space) {
        this._keydown = true;
      }
    }, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, (event: cc.Event.EventKeyboard) => {
      if (event.keyCode == cc.macro.KEY.space) {
        this._keydown = false;
      }
    }, this);
  }

  // Working Default Y6.MG.PO.MAPS.1B
  onQaTemplateLoaded(data: IQATemplate) {
    this._qaInputValue.string = data.Name;
    cc.log("loadQaTemplate", data);
    this.render.renderQA(data);
  }

  onTouch(event: cc.Event.EventTouch) {
    cc.log(event.getLocationX());
    if (event.getLocationX() >= cc.winSize.width / 2) {
      this.qaIndex++;
    } else {
      this.qaIndex--;
    }
    cc.log(this.qaIndex);
  }

  onUpdateQaPressed() {
    const qaInput = this._qaInputValue.string;
    cc.log(Number.parseInt(qaInput));
    const numberInput = Number.parseInt(qaInput);
    // try to parse number it
    if (!isNaN(numberInput)) {
      this.qaIndex = numberInput;
    } else {
      // input string load using string
      this.loadQaTemplate(qaInput);
    }
  }

  loadQaTemplate(id: string) {
    if (!this._network._connected) {
      return;
    }
    const userId = new Date().getTime();
    this._network.emit("loadQaTemplate", {
      id,
      userId,
      workspace: "workspace",
    });
  }
}
