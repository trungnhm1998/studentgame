import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import SchoolService from "./school-service";
import SchoolManager from "../managers/school-manager";
import AccountManager from "../managers/account-manager";
import SpeechBubble from "./speechBubble";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QuestionsRoom extends GameScene {
  @property([cc.Node])
  roomName: cc.Node[] = [];

  @property(cc.Button)
  chooseConcept: cc.Button = null;

  @property(cc.Node)
  questionAttempt: cc.Node = null;

  @property(cc.Node)
  timer: cc.Node = null;

  @property(SpeechBubble)
  emotionText: SpeechBubble = null;

  onLoad() {
    super.onLoad();
  }

  onResize() {
    this.node.getChildByName("background").scale = this.getScaleRatio();
  }

  onEnter() {
    super.onEnter();
    ACE.topBar.show();
    ACE.topBar.positionButtons();
    cc.log("Room:", SchoolManager.getInstance().roomName);
    this.onIdentifyRoom();
    this.emotionText.inSchool = false;
  }

  onVisible() {
    super.onVisible();
    if (!ACE.webviewManager.openingWebview && !AccountManager.getInstance().gettingSchoolData) {
      ACE.dialogManager.hideLoading();
    }
  }

  registerEvents() {
  }

  update() {
    this.setupNotiSpeechText();
  }

  onIdentifyRoom() {
    let room = SchoolManager.getInstance().roomName;
    switch (room) {
      case "revision":
        this.roomName[5].active = true;
        this.questionAttempt.active = true;
        break;
      case "practice":
        this.chooseConcept.node.active = true;
        this.roomName[0].active = true;
        this.questionAttempt.active = false;
        break;
      case "naplan":
        this.questionAttempt.active = true;
        this.roomName[4].active = true;
        this.timer.active = true;
        break;
      case "improvement":
        this.chooseConcept.node.active = true;
        this.roomName[2].active = true;
        this.questionAttempt.active = true;
        break;
      case "homework":
        this.questionAttempt.active = true;
        this.roomName[1].active = true;
        break;
      case "focus":
        this.chooseConcept.node.active = true;
        this.roomName[3].active = true;
        this.questionAttempt.active = true;
        break;
      case "assessment":
        this.questionAttempt.active = true;
        break;
    }
  }

  setupNotiSpeechText() {
    let strText = "";
    const studentName = `${AccountManager.getInstance().studentFirstName} ${
      AccountManager.getInstance().studentLastName
      }`;
    strText = `Do you know the answer for this?`;
    SchoolService.notiText = strText;
  }

  onLeave() {
    this.chooseConcept.node.active = false;
    this.timer.active = false;
    this.roomName[0].active = false;
    this.roomName[1].active = false;
    this.roomName[2].active = false;
    this.roomName[3].active = false;
    this.roomName[4].active = false;
    this.roomName[5].active = false;
  }
}
