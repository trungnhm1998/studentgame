import { ACE } from "../core/global-info";
import Map from "./map";
import FairyMessage from "./fairy-tutorial-messages";
import NodeUtils from "../core/utils/node-utils";
import LoginService from "../login/login-service";
import AccountManager from "../managers/account-manager";

const { ccclass, property } = cc._decorator;

enum House {
  school = 0,
  myClass,
  mailBox,
  myHouse,
  shop,
  game,
}

@ccclass
export default class MapTutorialNew extends cc.Component {
  @property(cc.Node)
  middle: cc.Node = null;

  @property(Map)
  map: Map = null;

  @property(cc.String)
  gameState: string = "";

  @property(cc.Button)
  cancelTutorial: cc.Button = null;

  @property(cc.Button)
  quickViewTutorial: cc.Button = null;

  @property(FairyMessage)
  fairy: FairyMessage = null;

  _currentHouse: number = -1;

  showTopBar(show = false) {
    const mapTutorialState = AccountManager.getInstance()._gameStateTutorial.get("map");
    if (show) {
      this.cancelTutorial.node.active = false;
      this.quickViewTutorial.node.active = false;
      this.fairy.node.active = false;
      ACE.topBar.show();
    } else {
      if (!mapTutorialState.isClosed) {
        this.cancelTutorial.node.active = false;
      } else {
        this.cancelTutorial.node.active = true;
      }
      this.fairy.node.active = true;
      ACE.topBar.hide();
    }
  }

  turnOffTutorial() {
    this.cancelTutorial.node.active = false;
    this.fairy.turnOffChatBoxAnim();
    let animationComp = this.node.getComponent(cc.Animation);
    animationComp.stop();
    animationComp.play("clean-map-tutorial");
    this.fairy.node.active = false;
    ACE.topBar.show();
    this.quickViewTutorial.node.active = false;
    // While fairy move to the next building, "middle" will have no children that make an error at line 72. This condition is to prevent that bug.
    if (this.middle.children[0] == undefined) {
      this.middle = this.middle;
    } else {
      this.middle.children[0].parent = this.middle.parent;
    }
    this.middle.parent.sortAllChildren();
    this._currentHouse = -1;
  }

  next() {
    let animationComp = this.node.getComponent(cc.Animation);
    this.fairy.turnOffChatBoxAnim();
    switch (this._currentHouse) {
      case 0:
        animationComp.play("map-tutorial", 6);
        break;
      case 1:
        animationComp.play("map-tutorial", 12);
        break;
      case 2:
        animationComp.play("map-tutorial", 18);
        break;
      case 3:
        animationComp.play("map-tutorial", 24);
        break;
      case 4:
        animationComp.play("map-tutorial", 30);
        break;
      case 5:
        animationComp.play("map-tutorial", 36);
        break;
    }
    NodeUtils.preventMultipleClick(this.quickViewTutorial, 1);
  }

  swap(house: number) {
    if (this._currentHouse != house) {
      this._currentHouse = house;
      this.map._mapIcons[this._currentHouse].parent = this.middle;

      this.fairy.turnOnChatBoxAnim();
      let message = "The school is where you will learn and earn shiny coins!";
      switch (house) {
        case 0:
          this.quickViewTutorial.node.active = true;
          message = "The school is where you will learn and earn shiny coins!";
          break;
        case 1:
          message = "Don't forget that all your awesome friends are here too!";
          break;
        case 2:
          message = "Remember to check your mailbox to receive amazing presents!";
          break;
        case 3:
          message = "This is your house, so fill your room with cool things!";
          break;
        case 4:
          message = "The shop rules! Buy all your clothes and fun stuff here!";
          break;
        case 5:
          message = "And lucky last, a super fun place for amazing games only!";
          LoginService.saveTutorial(this.gameState);
          break;
      }
      this.fairy.updateTutorialMessage(message);
    } else {
      this.middle.children[0].parent = this.middle.parent;
      this.fairy.turnOffChatBoxAnim();
    }
    this.middle.parent.sortAllChildren();
  }
}
