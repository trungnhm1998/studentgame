import { ACE } from "../core/global-info";
import { Config } from "../config";
import { Random } from "../core/utils/random-utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class FairyLayer extends cc.Component {
  @property(cc.Node)
  cloudContainer: cc.Node = null;

  @property(cc.Node)
  npc: cc.Node = null;

  @property(cc.Node)
  mask: cc.Node = null;

  onEnable() {
    for (let i = 0; i < this.cloudContainer.childrenCount; i++) {
      const cloud = this.cloudContainer.children[i];
      let offset = new Random().integer(-80, 80);
      cloud.runAction(cc.repeatForever(cc.sequence(
        cc.moveBy(2.5, cc.v2(offset, 0)).easing(cc.easeSineInOut()),
        cc.moveBy(2.5, cc.v2(offset * -1, 0)).easing(cc.easeSineInOut()),
      )));
    }
  }

  runEffect(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.node.runAction(cc.sequence(
        cc.callFunc(() => {
          ACE.topBar.hide();
        }),
        cc.fadeIn(0.5),
        cc.delayTime(8 ),
        // cc.fadeOut(0.5),
        cc.callFunc(() => {
          this.npc.runAction(cc.fadeOut(0.2));
          this.mask.runAction(cc.fadeOut(1.5));
          this.runCloudsEffect();
        }),
        cc.delayTime(2.0),
        cc.callFunc(() => {
          this.node.removeFromParent();
          resolve();
        }),
      ));
    });
  }

  runCloudsEffect() {
    this.cloudContainer.children.forEach((cloud) => {
      cloud.stopAllActions();
      let direction = new Random().fromList([1, -1]);
      let widgetComp = cloud.getComponent(cc.Widget);
      if (widgetComp) {
        if (widgetComp.isAlignLeft) {
          direction = -1;
        } else if (widgetComp.isAlignRight) {
          direction = 1;
        }
        widgetComp.enabled = false;
      }
      cloud.runAction(cc.spawn(
        cc.moveBy(1.5, cc.v2(500 * direction, 0)),
        cc.fadeOut(1.5),
      ));
    });
    // if cloud has widget check left or right to move with correct position
    // else randomly move the cloud to either left or right
  }
}
