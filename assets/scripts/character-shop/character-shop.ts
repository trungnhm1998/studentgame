import { Gender } from "../constants/qa.constants";
import GroupShop from "./group-shop";
import ItemShop from "./item-shop";
import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import AccountManager from "../managers/account-manager";
import { TutorialType } from "../common/top-bar";
import CharacterComponent from "../common/character-component";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CharacterShop extends GameScene {
  @property(cc.Node)
  loadingNode: cc.Node = null;
  @property(GroupShop)
  groupShop: GroupShop = null;
  @property(ItemShop)
  itemShop: ItemShop = null;
  @property(CharacterComponent)
  character: CharacterComponent = null;
  @property(cc.Sprite)
  backgroundSprite: cc.Sprite = null;

  _isDataInited: boolean = false;

  onLoad() {
    super.onLoad();
  }

  registerEvents() {
  }

  onEnter() {
    super.onEnter();
    this.onResize();
    ACE.topBar.show();
    ACE.topBar.positionButtons();
    this.character.autoUpdate(false);
    const mapTutorialState = AccountManager.getInstance()._gameStateTutorial.get("characterShop-new");
    if (!mapTutorialState.isClosed) {
      ACE.topBar.showTutorial(TutorialType.Shop);
    }
    AudioManager.getInstance().playAudio("Bg_music2_v2", true, true);
  }

  onResize() {
    this.backgroundSprite.node.scaleX = cc.winSize.width / this.backgroundSprite.node.width;
    this.backgroundSprite.node.scaleY = cc.winSize.height / this.backgroundSprite.node.height;
  }

  onEnable() {
    cc.log("start::character shop", ACE.sceneManager);
    let gender = Gender.GIRL;
    if (AccountManager.getInstance().profile.gender) {
      gender = Gender.BOY;
    }
    this.groupShop.initGroupShop(gender);
    this.itemShop.onChangeGroupItem(0);
    ACE.topBar.init();
    ACE.gameSocket.emit("loadStudentGameStateTutorial", {
      gameState: "characterShop-new",
    });
  }

  update(dt) { }

  onDisable() { }
}
