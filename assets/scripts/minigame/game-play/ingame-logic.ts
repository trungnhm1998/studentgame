import { PositionObject } from "../constants/minigame-constant";
import Obstacle from "./Obstacle";
import PlayerBee from "./PlayerBee";
import { PET_ANIM } from "./PlayerBee";
import ItemObject from "./item-object";
import ScrollSceneManager from "./ScrollSceneManager";
import GamePlayController from "../gameplay-controller";
import MinigameController from "../minigame_controller";

const { ccclass, property } = cc._decorator;
export const TOTAL_QUESTION_IN_LEVEL = 20;
export const TOTAL_STAGE_IN_LEVEL = 3;
export const MAX_SPEED = 5;
export const BASE_DISTANCE = 6; // 10 question / min
export const BASE_HEIGHT = 1080;

@ccclass
export default class IngameLogic extends cc.Component {
  // property
  get currentDistance(): number {
    return this._currentDistance;
  }
  set currentDistance(value: number) {
    this._currentDistance = value;
    if (this.distanceTravelledOnChanged != null) {
      this.distanceTravelledOnChanged(this._currentDistance);
    }
  }

  get targetDistance(): number {
    return this._targetDistance;
  }
  set targetDistance(value: number) {
    this._targetDistance = value;
    if (this.targetDistanceOnChanged != null) {
      this.targetDistanceOnChanged(this._targetDistance);
    }
  }

  get timeRemining(): number {
    return this._timeRemining;
  }
  set timeRemining(value: number) {
    this._timeRemining = value;
    if (this.timeRemainingOnChanged != null) {
      this.timeRemainingOnChanged(this._timeRemining);
    }
  }

  get coinCollected(): number {
    return this._coinCollected;
  }
  set coinCollected(value: number) {
    this._coinCollected = value;
    if (this.coinCollectedOnChanged != null) {
      this.coinCollectedOnChanged(this._coinCollected);
    }
  }

  get currentQuestion(): number {
    return this._currentQuestion;
  }
  set currentQuestion(value: number) {
    this._currentQuestion = value;
    if (this.questionAnsweredOnChanged != null) {
      this.questionAnsweredOnChanged(this._currentQuestion);
    }
  }

  get answerCorrected(): number {
    return this._answerCorrected;
  }
  set answerCorrected(value: number) {
    this._answerCorrected = value;
    if (this.answerCorrecteOnChanged != null) {
      this.answerCorrecteOnChanged(this._answerCorrected);
    }
  }

  get answerIncorrect(): number {
    return this._answerInCorrect;
  }
  set answerIncorrect(value: number) {
    this._answerInCorrect = value;
  }
  get isPause(): boolean {
    return this._isPause;
  }
  set isPause(value: boolean) {
    if (value) {
      this.player.SetState(PET_ANIM.IDLE);
    } else {
      this.player.SetState(PET_ANIM.FLYING_IDLE);
    }

    this._isPause = value;
  }
  static getInstance() {
    if (this._instance == null) {
      this._instance = new IngameLogic();
    }
    return this._instance;
  }

  // singleton
  private static _instance: IngameLogic = null;

  // for debug:
  @property
  USE_QUICK_PLAY = false;

  // variables

  @property(cc.Node)
  BG1: cc.Node = null;

  @property(cc.Node)
  BG2: cc.Node = null;

  @property(ScrollSceneManager)
  scrollBG: ScrollSceneManager = null;

  @property(ScrollSceneManager)
  scrollObjects: ScrollSceneManager = null;

  @property(cc.Node)
  mainPlayer: cc.Node = null;

  @property(cc.Node)
  NodeObtacle1: cc.Node = null;

  @property(cc.Node)
  InitPosItems1: cc.Node = null;

  @property(ItemObject)
  itemCoin: ItemObject = null;

  @property([cc.Node])
  listPosItems1: cc.Node[] = [];

  listItems: ItemObject[] = [];

  player: PlayerBee = null;
  obtacle1: Obstacle = null;
  obtacle2: Obstacle = null;

  // varable get from map
  currentLevel = 1; // will get from selecl level map
  currentTypeOfGame = 0; // maybe 0: + , 1: -, 2: *, 3: /, 4: mix, will check with Luc to get it later
  targetTime: number = 24 * 1000; // 24s

  // variables should reset
  currentStage = 0; // 3 Stage in each level. ( TOTAL_STAGE_IN_LEVEL)
  _currentQuestion = 0; // have total 20 question in 1 level (TOTAL_QUESTION_IN_LEVEL)
  difficult = 0; // it is difficult in stage : 5 mode .
  _currentDistance = 0;
  timer = 0;

  // variable need to show to screen.
  _timeRemining = 0;
  // luc.tc add
  _coinCollected = 0;
  _targetDistance = 0;
  _answerCorrected = 0;
  _answerInCorrect = 0;
  _velocityFollowDifficult = 6; // 6m/s

  // event
  distanceTravelledOnChanged: (value: number) => any;
  targetDistanceOnChanged: (value: number) => any;
  timeRemainingOnChanged: (value: number) => any;
  coinCollectedOnChanged: (value: number) => any;
  questionAnsweredOnChanged: (value: number) => any;
  answerCorrecteOnChanged: (value: number) => any;

  // end add

  _isPause = true;

  score = 0;
  live = 3;
  speedFollowDesign = 1;

  /*
  CallBack maybe no need, will check later
  */

  // CallBack for update in UI
  scoreCB: (score: number) => any;
  liveCB: (score: number) => any;

  // Resize only for this node, because Canvas Fit with width, but GamePlay request fit width Height..
  onResize() {
    const deviceSize = cc.view.getFrameSize();

    cc.error("$$$$$$$$$$$$$$$$$$$$$$$$$$$ deviceSize.width ::: " + deviceSize.width
      + "\ndeviceSize.height " + deviceSize.height
      + "\ncc.winSize.width : " + cc.winSize.width
      + "\ncc.winSize.height : " + cc.winSize.height,

    );
    let scale = cc.winSize.height > BASE_HEIGHT ? cc.winSize.height / BASE_HEIGHT : BASE_HEIGHT / cc.winSize.height;
    this.node.scale = scale;
  }

  onLoad() {

  }

  onEnable() {
    this.onResize();
    IngameLogic._instance = this;

    // check for enable BG1 / BG 2
    this.currentLevel = MinigameController.getInstance().getLevelSelected();
    if (this.currentLevel < 5) {
      this.BG1.active = true;
      this.BG2.active = false;
    } else {
      this.BG1.active = false;
      this.BG2.active = true;
    }

    // reset main player
    this.player = this.mainPlayer.getComponent(PlayerBee);
    // this.player.onSetMovingUp();
    this.player.reset();

    // first time...
    GamePlayController.getInstance().spawnQuestion();
    this.obtacle1 = this.NodeObtacle1.getComponent(Obstacle);
    this.obtacle1.updateQuestion();

    this.listPosItems1.forEach((item) => {
      item.active = false;
    });

    if (!this.USE_QUICK_PLAY) {
      // Start will pause, waiting count number ...
      this._isPause = true;
    } else {
      this._isPause = false;
    }
    // this.scoreCallback = this.UpdateScore
  }

  resetGame() {
    this.currentStage = 0;
    this.currentQuestion = 0;
    this.difficult = 0;
    this.currentDistance = 0;
    this.timer = 0;
    this.coinCollected = 0;
    this.score = 0;
    this.live = 3;
    this.speedFollowDesign = 1;
    this.timeRemining = this.targetTime = Math.floor(GamePlayController.getInstance().getTotalTimeForLevel()) * 1000;
    this.targetDistance = Math.floor(GamePlayController.getInstance().getDistanceToClear());
    this.answerCorrected = 0;
    this.answerIncorrect = 0;
  }

  startNewStage() {
    this.difficult = 0;
    this.currentDistance = 0;
    this.timer = 0;
    this.currentQuestion = 0;
    this.answerCorrected = 0;
    this.answerIncorrect = 0;
    this.timeRemining = this.targetTime = Math.floor(GamePlayController.getInstance().getTotalTimeForLevel()) * 1000;
    this.targetDistance = Math.floor(GamePlayController.getInstance().getDistanceToClear());
  }

  startLevel() {
    this.currentStage = 0;
    this.currentQuestion = 0;
    this.difficult = 0;
    this.currentDistance = 0;
    this.timer = 0;
    this.coinCollected = 0;
    this.score = 0;
    this.live = 3;
    this.speedFollowDesign = 1;
    this.timeRemining = this.targetTime = Math.floor(GamePlayController.getInstance().getTotalTimeForLevel()) * 1000;
    this.targetDistance = Math.floor(GamePlayController.getInstance().getDistanceToClear());
    this.answerCorrected = 0;
    this.answerIncorrect = 0;

    // init all postion & type of object here
    this.updateLogicObject(PositionObject.UP);
    this.obtacle1.updateQuestion();
    this.player.SetState(PET_ANIM.FLYING_IDLE);
    // this.InitObject();

    this._isPause = false;
    this.UpdateLevelupDifficult();
    this.schedule(this.Coundown, 1, cc.macro.REPEAT_FOREVER, 0);
  }

  InitObject() {
    // reset
    this.listItems.forEach((item) => {
      item.node.removeFromParent();
    });
    this.listItems = [];

    for (let index = 0; index < 3; index++) {
      let item = this.GetRandomlyItem(this.InitPosItems1);
      if (item) {
        cc.log("...................");
        item.node.setPosition(this.listPosItems1[index].position);
        this.listItems.push(item);
      }
    }
  }

  GetRandomlyItem(parentNode: cc.Node): ItemObject {
    let item: ItemObject = null;
    let node = cc.instantiate(this.itemCoin.node);
    node.parent = parentNode;
    node.active = true;
    item = node.getComponent(ItemObject);
    return item;
  }

  updateLogicObject(postionStatus: PositionObject) {
    let oppositePosition = PositionObject.DOWN;
    if (postionStatus == PositionObject.DOWN) {
      oppositePosition = PositionObject.UP;
    }

    // obtacle is same side when init with player
    this.obtacle1.updateLogicObject(postionStatus);

    // all items will be oppositePos with player
    this.InitObject();
    this.listItems.forEach((item) => {
      item.updateLogicObject(oppositePosition);
    });
  }

  Coundown(dt) {
    if (!this._isPause) {
      if (this.targetTime - this.timer <= 500) {
        this.timeRemining = 0;
        // cc.error("WHY LOSE ????")
        // so cau / tong so cau 10
        // if answer over 60%
        if (this.answerCorrected / 10 >= 0.6) {
          this.player.UpdateLevelWin();
          return;
        }
        this.player.UpdateLevelLose();
        return;
      }

      this.timer += 1000;
      this.timeRemining = this.targetTime - this.timer;
      this.currentDistance += this._velocityFollowDifficult;
      if (this.targetDistance <= 0) {
        this.targetDistance = Math.floor(GamePlayController.getInstance().getDistanceToClear());
      }
    }
  }

  // target: number, coin: number, corrected: number, incorrect: number)
  GameOver(isWin: boolean) {
    cc.log("GAME OVER ++++++++++++++++++++++++++++++++ ");
    this._isPause = true;

    GamePlayController.getInstance().showEndGame(isWin,
      this.currentDistance, this.targetDistance,
      this.coinCollected, this.answerCorrected,
      this.answerIncorrect, this.difficult, this.score,
    );
  }

  UpdateLevelupDifficult() {
    // do effect animation ...

    // calc speed follow design
    this.speedFollowDesign = GamePlayController.getInstance().getCurrentSpeed(IngameLogic.getInstance().difficult);
    cc.log("speed Follow Design ------------------------------------- :: " + this.speedFollowDesign);

    this._velocityFollowDifficult = Math.floor(GamePlayController.getInstance().getVelocityFollowDifficult(this.difficult));
  }

  UpdateScore(score: number) {
    this.score = score;
    if (this.scoreCB != null) { this.scoreCB(score); }
  }

  UpdateLive(live: number) {
    this.live = live;
    if (this.liveCB != null) { this.liveCB(live); }
  }
}
