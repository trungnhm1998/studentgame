import { Random } from "./../../core/utils/random-utils";
const { ccclass, property } = cc._decorator;

@ccclass
export default class RevealEggEffect extends cc.Component {
  @property([cc.SpriteFrame])
  borkenEgg: cc.SpriteFrame[] = [];

  @property([cc.Node])
  brokenEggNode: cc.Node[] = [];

  @property(cc.Animation)
  brokenAnim: cc.Animation = null;

  @property(cc.Node)
  hollow: cc.Node = null;

  @property(cc.Node)
  petGroup: cc.Node = null;

  @property(cc.Sprite)
  pet: cc.Sprite = null;

  @property(cc.Sprite)
  eggSprite: cc.Sprite = null;

  @property(cc.ParticleSystem)
  particle: cc.ParticleSystem = null;

  _animComp: cc.Animation = null;
  _petData: any = {
    1: "draco",
    2: "haya",
    3: "moru",
    4: "shoo",
    5: "umic",
  };
  _petId = -1;
  onLoad() {
    this._animComp = this.getComponent(cc.Animation);
  }

  playEffect() {
    return new Promise((resolve, reject) => {
      if (!CC_JSB) {
        this.particle.node.active = true;
      } else {
        this.particle.node.removeFromParent();
      }
      this._animComp.play("egg-shake");
      this.node.runAction(cc.sequence(
        cc.spawn(
          cc.moveTo(3.5, cc.v2(0, 0)),
          cc.rotateTo(3.5, -10),
          cc.scaleTo(3.5, 2),
        ),
        cc.callFunc(() => {
          this.playBroken().then(() => {
            resolve();
          });
        })
      ));
    })
  }

  playBroken() {
    return new Promise((resolve, reject) => {
      this.node.runAction(cc.sequence(
        cc.callFunc(() => {
          this.node.setScale(1);
          this.eggSprite.spriteFrame = this.borkenEgg[0];
        }),
        cc.delayTime(1),
        cc.callFunc(() => {
          this.eggSprite.spriteFrame = this.borkenEgg[1];
        }),
        cc.delayTime(1),
        cc.callFunc(() => {
          this.eggSprite.spriteFrame = this.borkenEgg[2];
        }),
        cc.delayTime(2),
        cc.spawn(
          cc.callFunc(() => {
            this._animComp.stop();
            this.eggSprite.node.active = false;
            this.brokenAnim.node.active = true;
            this.brokenAnim.play();
          }),
          cc.callFunc(() => {
            this.playFlyPieces();
          })
        ),
        cc.callFunc(() => {
          this.node.angle = 0;
          cc.loader.loadRes(`common/pets/${this._petData[this._petId]}`, cc.SpriteFrame, (err, spriteFrame) => {
            this.particle.node.active = false;
            this.pet.spriteFrame = spriteFrame;
            this.petGroup.active = true;
            this.hollow.runAction(cc.rotateBy(5, 360).repeatForever());
            this.pet.node.runAction(cc.scaleTo(1, 1.5));
            resolve();
          });
        })
      ));
    })
    // this.node.runAction(cc.)
  }

  playFlyPieces() {
    this.brokenEggNode.forEach((eggNode) => { eggNode.active = true; });
    this.brokenEggNode[0].runAction(cc.spawn(
      cc.moveBy(0.4, cc.v2(new Random().integer(-50, 50), -80)),
      cc.fadeOut(0.8)
    ));
    this.brokenEggNode[1].runAction(cc.spawn(
      cc.moveBy(0.4, cc.v2(-80, new Random().integer(-50, 50))),
      cc.fadeOut(0.8)
    ));
    this.brokenEggNode[2].runAction(cc.spawn(
      cc.moveBy(0.4, cc.v2(80, new Random().integer(-50, 50))),
      cc.fadeOut(0.8)
    ));
  }
}
