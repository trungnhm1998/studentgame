import { ACE } from "../core/global-info";
import { SERVER_EVENT, STORAGE_KEY } from "../core/constant";
import AccountManager from "../managers/account-manager";
import SchoolService from "../school/school-service";

export default class LoginService {
  static logout() {
    LoginService.updateToken(null);
    AccountManager.getInstance().logout();
    SchoolService.clear();
    if (cc.sys.isBrowser) {
      ACE.resManager.removeStorage(STORAGE_KEY.ACCESS_TOKEN);
      ACE.resManager.removeStorage(STORAGE_KEY.REMEMBER_INFO);
      location.reload();
    }
    // for force logout
    if (ACE.sceneManager.currentScene.name != "login") {
      ACE.sceneManager.pushScene("login");
    }
  }

  static login(username, password) {
    ACE.gameSocket.emit(SERVER_EVENT.LOGIN, {
      username,
      password
    });
  }

  static updateToken(accessToken) {
    ACE.gameSocket.emit("token", { accessToken });
  }

  static clearToken() {
    ACE.gameSocket.emit("token", null);
  }

  static profile(userName) {
    ACE.gameSocket.emit(SERVER_EVENT.PROFILE, {
      userName
    });
  }

  static checkCharacterExists(userName) {
    ACE.gameSocket.emit(SERVER_EVENT.CHECK_CHARACTER_EXISTS, {
      accessToken: AccountManager.getInstance().accessToken
    });
  }

  static loadTutorial(gameState) {
    ACE.gameSocket.emit(SERVER_EVENT.LOAD_STUDENT_GAME_TUTORIAL, {
      gameState
    });
  }

  static saveTutorial(gameState: string, step = 0, isClosed = true) {
    AccountManager.getInstance()._gameStateTutorial.get(gameState).isClosed = true;
    ACE.gameSocket.emit(SERVER_EVENT.SAVE_STUDENT_GAME_TUTORIAL, {
      gameState,
      step,
      isClosed
    });
  }

  static getStudentCoinsStars() {
    ACE.gameSocket.emit(SERVER_EVENT.STUDENT_COINS_STARS, {});
  }

  static loadAssetsData() {
    ACE.gameSocket.emit(SERVER_EVENT.ASSET_DATA, {});
  }
}
