const { ccclass, property } = cc._decorator;

export default class SpeedLevel {
  speeds: number[];
  constructor(level1, level2, level3, level4, level5) {
    this.speeds = [level1, level2, level3, level4, level5];
  }
}