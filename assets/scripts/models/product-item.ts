const { ccclass, property } = cc._decorator;

@ccclass
export default class ProductItem {
  ID: number = 545;
  Name: string = "Black hair";
  Code: string = "GIRL_ICON_HAIR_BASIC_BLACK_HAIR";
  Coins: number = 50;
  Stars: number = 3;
  FileNameAssetData: string = "0";
  FileNameAssetKey: string = "I_L_G_GIRL_ICON_HAIR_BASIC_BLACK_HAIR";
  AssetImageUrl: string =
    "character-shop/shop_tag_icon_girl_popup_3_5_5_01_0004_Layer-6.png";
  AssetJsonUrl: string = "";
  isOwned: boolean = false;
  isEquipped: boolean = false;
}
