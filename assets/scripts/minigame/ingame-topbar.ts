import IngameLogic from "./game-play/ingame-logic";
import GamePlayController from "./gameplay-controller";

const { ccclass, property } = cc._decorator;

@ccclass
export default class IngameTopbar extends cc.Component {
  @property(cc.Label)
  timeRemainingLabel: cc.Label = null;
  @property(cc.Label)
  coinCollectedLabel: cc.Label = null;
  @property(cc.Label)
  questionsAnseredLabel: cc.Label = null;

  @property(cc.ProgressBar)
  distanceProgressBar: cc.ProgressBar = null;
  @property(cc.Node)
  starProgressNode: cc.Node = null;

  _progressTarget: number = 0;
  _numQuest: number = 0;

  start() {
    IngameLogic.getInstance().coinCollectedOnChanged = (value) => this.updateCoinCollected(value);
    IngameLogic.getInstance().distanceTravelledOnChanged = (value) => this.updateDistanceTravelled(value);
    IngameLogic.getInstance().targetDistanceOnChanged = (value) => this.updateTargetDistance(value);
    IngameLogic.getInstance().timeRemainingOnChanged = (value) => this.updateTimeRemaining(value);
    IngameLogic.getInstance().answerCorrecteOnChanged = (value) => this.updateQuestionsCorrected(value);
    IngameLogic.getInstance().questionAnsweredOnChanged = (value) => this.updateQuestions(value);
  }

  onEnable() {
    this.distanceProgressBar.progress = 0;
    this.starProgressNode.active = false;
    this.questionsAnseredLabel.string = 0 + "/10";
  }

  backBtnOnClicked() {
    GamePlayController.getInstance().openConfirmToExitGame();
  }

  updateDistanceTravelled(distance: number) {
    if (this._progressTarget > 0) {
      this.distanceProgressBar.progress = distance / this._progressTarget;
      cc.log(":::DISTANCE:::", distance, this.distanceProgressBar.progress, this._progressTarget);
    }
    if (this._progressTarget > 0
      && IngameLogic.getInstance().answerCorrected >= 6
      && this.starProgressNode.active == false) {
      this.starProgressNode.active = true;
    }
  }

  updateTargetDistance(distance: number) {
    this._progressTarget = distance * (100.0 / 100.0);
  }

  updateTimeRemaining(timeRemaining: number) {
    let timeS = timeRemaining / 1000;
    let strTime = "";
    let minus = Math.floor(timeS / 60);
    strTime = minus + "";
    if (minus < 10) {
      strTime = "0" + minus;
    }
    strTime += ":";
    let second = Math.floor(timeS % 60);
    if (second < 10) {
      strTime += "0";
    }
    strTime += second;
    this.timeRemainingLabel.string = strTime;
  }

  updateCoinCollected(coin: number) {
    this.coinCollectedLabel.string = coin.toString();
  }

  updateQuestions(question: number) {
    this._numQuest = question;
  }

  updateQuestionsCorrected(question: number) {
    cc.log("OnQuestion Correct");
    this.questionsAnseredLabel.string = question + "/10";
  }
}
