export const RESOURCE_PATH = {
  CHARACTER_SHOP_PRODUCE_ITEM_BOY: "images/CharacterShop/Products/Large/Boy",
  CHARACTER_SHOP_PRODUCE_ITEM_GIRL: "images/CharacterShop/Products/Large/Girl",
  CHARACTER_SHOP_PRODUCE_ITEM_ICON_GIRL:
    "images/CharacterShop/Products/Icon/Girl",
  CHARACTER_SHOP_PRODUCE_ITEM_ICON_BOY:
    "images/CharacterShop/Products/Icon/Boy",
  CHARACTER_SHOP_PRODUCE_ITEM_ICON_SKIN:
    "images/CharacterShop/Products/Icon/Skin",
  BOY_CHARACTER_SET_MID_PATH: "CHAR2_BOY_",
  GIRL_CHARACTER_SET_MID_PATH: "CHAR2_GIRL_",
  BOY_CHARACTER_ICON_MID_PATH: "BOY_ICON_",
  GIRL_CHARACTER_ICON_MID_PATH: "GIRL_ICON_",

  DATA_ASSETS_SERVER: "https://gameasset.s3.amazonaws.com/"
};
