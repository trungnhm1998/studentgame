const { ccclass, property } = cc._decorator;

@ccclass
export default class CharacterInfo {
  CharacterTypeID: number = 3;
  CharacterTypeName: string = "GirlNormal";
  Gender: boolean = false;
  ID: number = 9;
  Name: "GirlNormal";
}
