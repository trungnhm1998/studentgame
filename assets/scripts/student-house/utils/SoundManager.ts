const { ccclass, property } = cc._decorator;

export const SND = {
  SND_EFFECT_CLICK: 0,
  SND_EFFECT_HOVER: 1
}

@ccclass
export default class SoundManager extends cc.Component {

    @property({type:cc.AudioClip})
    snd_effect_click : cc.AudioClip = null;

    @property({type:cc.AudioClip})
    snd_effect_hover : cc.AudioClip = null;

  @property(cc.AudioSource)
  player: cc.AudioSource = null;

  SoundArrays: cc.AudioClip[] = [];

  onLoad() {
    this.SoundArrays.push(this.snd_effect_click);
    this.SoundArrays.push(this.snd_effect_hover);
  }

  // update (dt) {}
  playSound(ID: number) {
    this.player.clip = this.SoundArrays[ID];
    this.player.play();
  }
}
