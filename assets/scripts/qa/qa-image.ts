import QaNode from "./qa-node";
import { IDesign } from "../models/qa";
import { Server } from "../constants/qa.constants";
import QARenderer from "./qa-renderer";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QaImage extends QaNode {
  @property(cc.Sprite)
  sprite: cc.Sprite = null;

  onLoad() {

  }

  init(qaDesign: IDesign) {
    super.init(qaDesign);
    const { objPrivate, w, h, x, y } = qaDesign;
    const { linkImage, anchorX, anchorY } = objPrivate;
    // if the image already load before use that instead
    let imageLink = `${Server.URL}${linkImage}`;
    cc.loader.load(imageLink, (err, texture) => {
      if (!err) {
        this.sprite.spriteFrame = new cc.SpriteFrame(texture);
        // if the design have size, reset it here
        this.updateProps();
      } else {
        cc.log(`Failed to load texture using imageUrl [${linkImage}] with error [${err}]`);
      }
    })
  }

  updateProps() {
    const { objPrivate, w, h, x, y } = this._QAObject;
    const { linkImage, anchorX, anchorY, keyImage } = objPrivate;
    // cc.log(`QAImage::updateProps %c[${keyImage}] with anchor[${anchorX}-${anchorY}] position[${x}-${y}] size[${w}-${h}]`, 'color: red');
    this.node.anchorX = this.sprite.node.anchorX = anchorX === undefined && anchorX === null ? 0.5 : anchorX;
    this.node.anchorY = this.sprite.node.anchorY = anchorY === undefined && anchorY === null ? 0.5 : anchorY;
    if (this.node.anchorY == 0) {
      this.node.anchorY = this.sprite.node.anchorY = 1;
    }
    
    this.node.width = this.sprite.node.width = w ? w : this.sprite.node.width;
    this.node.height = this.sprite.node.height = h ? h : this.sprite.node.height;
    // this.border.width = this.node.width * 1.05 * QARenderer.getInstance().scaleRatio;
    // this.border.height = this.node.height * 1.25 * QARenderer.getInstance().scaleRatio;
    // cc.log(`this.node.anchor [${this.node.anchorX}-${this.node.anchorY}] this.sprite.node.anchor[${this.sprite.node.anchorX}-${this.sprite.node.anchorY}] this.sprite.node.size[${this.sprite.node.width}-${this.sprite.node.height}]`);
  }
}