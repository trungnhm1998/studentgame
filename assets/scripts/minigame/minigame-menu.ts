import { ACE } from "../core/global-info";
import GameScene from "../common/game-scene";
import MinigameController from "./minigame_controller";
import { MiniGameType } from "./constants/minigame-constant";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MiniGameMenu extends GameScene {
  onLoad() {
    super.onLoad();
  }

  registerEvents() {
  }

  onEnter() {
    super.onEnter();
    super.onResize();
    ACE.topBar.show();
    ACE.topBar.positionButtons();
    AudioManager.getInstance().playAudio("Bg_music3_v2", true, true);
  }

  actionOnClicked(event, data) {
    let gameType = MiniGameType.PET_TAPPING;
    switch (data) {
      case "PET_TAPPING":
        gameType = MiniGameType.PET_TAPPING;
        // todo implement
        break;
      case "ADDITION":
        gameType = MiniGameType.AMAZING_ADDITION;
        // todo implement
        break;
      case "MULTIPLICATION":
        gameType = MiniGameType.MAGNIFICIENT_MULTIPLICATION;
        // todo implement
        break;
      case "SUBTRACTION":
        gameType = MiniGameType.SUPPER_SUBTRACTION;
        // todo implement
        break;
      case "DIVISION":
        gameType = MiniGameType.DYNAMIC_DIVISIONS;
        // todo implement
        break;
      case "MIX":
        gameType = MiniGameType.SUPER_MIX;
        // todo implement
        break;

    }
    MinigameController.getInstance().selectMiniGameType(gameType);
    if (gameType != MiniGameType.PET_TAPPING) {
      ACE.sceneManager.pushScene("select-level");
    } else {
      // goto pet taping mini game
      // ...
    }
  }

}
