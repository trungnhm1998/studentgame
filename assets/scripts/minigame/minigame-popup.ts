
const {ccclass, property} = cc._decorator;

@ccclass
export default class MinigamePopup extends cc.Component {
    @property(cc.Label)
    titleLabel: cc.Label = null;
    @property(cc.Label)
    messageLabel: cc.Label = null;
    @property(cc.Animation)
    animOpenPopup: cc.Animation = null;

    _acceptCallback: () => void;
    _closeCallback: () => void;

  start() {}

  show(
    title: string = "",
    message: string = "",
    acceptCallback: () => void = null,
    closeCallback: () => void = null,
  ) {

    this._acceptCallback = acceptCallback;
    this._closeCallback = closeCallback;
    this.titleLabel.string = title;
    this.messageLabel.string = message;
    this.node.active = true;
    this.animOpenPopup.play();
  }

  onBtnCloseOnClick() {
    this.node.active = false;
    if (this._closeCallback != null) { this._closeCallback(); }
  }

  onBtnOkOnClick() {
    this.node.active = false;
    if (this._acceptCallback != null) { this._acceptCallback(); }
  }
}
