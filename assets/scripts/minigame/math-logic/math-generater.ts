import BaseStage from "./stage/base-stage";
import AdditionStage from "./stage/addition-stage";
import { OperatorType, MiniGameType } from "../constants/minigame-constant";
import SubtractionStage from "./stage/subtraction-stage";
import MultiplicationStage from "./stage/multiplication-stage";
import DivisionStage from "./stage/division-stage";
import MathQuestion from "./stage/math-question";
import { Random } from "../../core/utils/random-utils";
import { BASE_DISTANCE } from "../game-play/ingame-logic";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MathGenerater {
  static _instance: MathGenerater = null;
  static getInstance() {
    if (MathGenerater._instance == null) {
      MathGenerater._instance = new MathGenerater();
    }
    return MathGenerater._instance;
  }

  _gamestages: BaseStage[] = null;
  _gameType: MiniGameType = null;

  initGenerateForGameTurn(gameType: MiniGameType) {
    this._gameType = gameType;
    this._gamestages = new Array<BaseStage>();
    switch (gameType) {
      case MiniGameType.AMAZING_ADDITION:
        this._gamestages.push(new AdditionStage());
        break;
      case MiniGameType.SUPPER_SUBTRACTION:
        this._gamestages.push(new SubtractionStage());
        break;
      case MiniGameType.MAGNIFICIENT_MULTIPLICATION:
        this._gamestages.push(new MultiplicationStage());
        break;
      case MiniGameType.DYNAMIC_DIVISIONS:
        this._gamestages.push(new DivisionStage());
        break;
      case MiniGameType.SUPER_MIX:
        this._gamestages.push(new AdditionStage());
        this._gamestages.push(new SubtractionStage());
        this._gamestages.push(new MultiplicationStage());
        this._gamestages.push(new DivisionStage());
        break;
    }
  }

  // quilv add to get Speed for 1 level - follow stage

  getSpeed(stage: number, level: number, difficult: number): number {
    let speed = 1;
    let gamestageForQuestion = this._gamestages[0];
    let SpeedLevel = gamestageForQuestion.getSpeedLevel((level - 1) * 3 + stage);
    speed = SpeedLevel.speeds[difficult] / 10; // will use hard code is 10 ... (start min is 10)

    return speed;
  }

  getMathQuestion(stage, level) {

    let gamestageForQuestion = this._gamestages[0];
    // mix case
    if (this._gameType == MiniGameType.SUPER_MIX) {
      let idxRand = new Random().integer(0, this._gamestages.length - 1);
      gamestageForQuestion = this._gamestages[idxRand];
      // cc.log("Math question:: ", "Rand operator for question");
    }
    // get range
    let stageRange = gamestageForQuestion.getStateRanges((level - 1) * 3 + stage);
    let rand = new Random();
    let operatorType: OperatorType = null;
    let firstN;
    let secondN;
    // Rand question in range
    cc.log("Math question:: ", "Stage: " + stage + " Stage range: " + stageRange.min + ":" + stageRange.max);
    if (gamestageForQuestion instanceof AdditionStage) {
      operatorType = OperatorType.ADD;
      firstN = rand.integer(stageRange.min, stageRange.max);
      secondN = rand.integer(stageRange.min, stageRange.max);
    } else if (gamestageForQuestion instanceof SubtractionStage) {
      operatorType = OperatorType.SUB;
      firstN = rand.integer(stageRange.min, stageRange.max);
      secondN = rand.integer(stageRange.min, firstN);
    } else if (gamestageForQuestion instanceof MultiplicationStage) {
      operatorType = OperatorType.MULTI;
      firstN = stageRange.min;
      secondN = rand.integer(stageRange.max, stageRange.upto);
    } else if (gamestageForQuestion instanceof DivisionStage) {
      operatorType = OperatorType.DIV;
      secondN = rand.integer(stageRange.min, stageRange.max);
      let answer = rand.integer(1, stageRange.upto);
      firstN = answer * secondN;
    } else {
      operatorType = null;
    }
    //
    return new MathQuestion(firstN, secondN, operatorType);
  }
  // GetTotalTime need for level
  getTotalTimeForLevel(stage, level) {
    let res = 0;
    for (let i = 0; i < 5; i++) {
      res = res + this.getTargetTimeFollowDifficult(stage, level, i);
    }
    return res;
  }

  // Get Distance Target
  getDistanceToClear(stage, level) {
    let res = 0;
    // d1 + 0.75(d2) + 0.75(d3) + 0.75(d4) + 0.75(d5)

    for (let i = 0; i < 5; i++) {
      cc.log("V _____________________________ : " + this.getVelocityFollowDifficult(stage, level, i));
      if (i == 0) {
        res = res + this.getVelocityFollowDifficult(stage, level, i) * this.getTargetTimeFollowDifficult(stage, level, i);
      } else {
        res = res + this.getVelocityFollowDifficult(stage, level, i) * this.getTargetTimeFollowDifficult(stage, level, i) * 0.75;
      }
    }
    return res;
  }

  // GetTimeFollowDesign
  getTargetTimeFollowDifficult(stage, level, difficult) {
    let t = 0;
    let gamestageForQuestion = this._gamestages[0];
    let SpeedLevel = gamestageForQuestion.getSpeedLevel((level - 1) * 3 + stage); // get array of question per min ( 10 question/ 1 min | 15 question/ 1 min ...)
    cc.log("getTargetTimeFollowDifficult::SpeedLevel", SpeedLevel, stage, difficult, level);

    t = 2 * (60 / SpeedLevel.speeds[difficult]);

    return t;
  }

  // Get Velocity for 1 level with specific : stage & difficult
  /*
      10 question ---> 6m/s
      15 question ---> 12m/s
      20 question ---> 24m/s
      ---
      each 5 question : 6x2
      ---
      ex:
      n = 1
      15 question : 10 + 5*n ---> 6 * 2^n
      12 question : n = 2/5 .
  */
  getVelocityFollowDifficult(stage, level, difficult) {
    // cc.log("getVelocityFollowDifficult :::: ++++++++++++++++ " + stage + " | " + level + " | " + difficult)
    let v = 1;
    let gamestageForQuestion = this._gamestages[0];
    let SpeedLevel = gamestageForQuestion.getSpeedLevel((level - 1) * 3 + stage); // get array of question per min ( 10 question/ 1 min | 15 question/ 1 min ...)
    let n = (SpeedLevel.speeds[difficult] - 10) / 5;
    v = Math.pow(2, n) * BASE_DISTANCE;
    return v;
  }
}
