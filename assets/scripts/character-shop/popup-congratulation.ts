import GameScene from "../common/game-scene";
import AudioManager from "../core/audio-manager";
import { RESOURCE_PATH } from "../constants/resource-constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupCongratulation extends GameScene {
  @property(cc.Label)
  message: cc.Label = null;
  @property(cc.Sprite)
  iconItem: cc.Sprite = null;

  _acceptCallback: () => void;

  start() {}

  onShowPopup(message: string, lastUrl: string, acceptCallback: () => void) {
    this.message.string = message;

    this._acceptCallback = acceptCallback;
    this.node.active = true;
    var self = this;
    cc.loader.load(RESOURCE_PATH.DATA_ASSETS_SERVER + lastUrl, function(
      err,
      texture
    ) {
      if (err) {
        cc.error(err.message || err);
        return;
      }
      let icon = new cc.SpriteFrame(texture);
      self.iconItem.spriteFrame = icon;
    });
  }

  btnUseOnClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.node.active = false;
    this._acceptCallback();
  }
  btnOkOnClick() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.node.active = false;
  }
}
