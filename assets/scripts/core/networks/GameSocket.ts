import ClientProtocol from "./ClientProtocol";

export default class GameSocket {
  _clientProtocol: ClientProtocol = null;
  _reconnectAttempts: number = 0;
  _connected: boolean = false;
  // _reconnect: boolean = false;
  _url: string = "";
  _reconnectTimeout: number = 5000; // 5 sec
  _maxReconnectAttempt: number = 5;

  _reconnectTimeoutObject = null;
  
  constructor(clientProtocol: ClientProtocol) {
    this._clientProtocol = clientProtocol;
  }

  connect(url: string, onConnectedCallback?: Function): Promise<any> {
    this._connected = false;
    this._reconnectAttempts = 0;
    clearInterval(this._reconnectTimeoutObject);
    this._url = url;
    return new Promise(async (resolve, reject) => {
      if (this._connected) {
        resolve();
        return;
      }
      if (this._clientProtocol == null) {
        reject("Protocol is null.");
        return;
      }
      try {
        await this._clientProtocol.connect(this._url, this.onProtocolClose.bind(this), this.onProtocolConnect.bind(this));
      } catch (error) {
        if (error) {
          cc.log("GameSocket::connect::error", error);
        }
      }
      if (this._clientProtocol._socket != null) {
        cc.log(`GameSocket::connect::protocol init successfully to url ${this._url}`);
        if (onConnectedCallback) {
          onConnectedCallback();
        }
        resolve();
      }
    })
  }

  on(eventName: string, callback: Function) {
    this._clientProtocol.on(eventName, callback);
  }

  emit(eventName: string, args: any = {}): void {
    this._clientProtocol.emit(eventName, args);
  }

  off(eventName: string) {
    this._clientProtocol.off(eventName);
  }

  protected onProtocolConnect() {
    this._connected = true;
    this._reconnectAttempts = 0;
    clearInterval(this._reconnectTimeoutObject);
  }

  protected onProtocolClose(error) {
    cc.log("GameSocket::onProtocolClose", error);
    if (this._reconnectTimeoutObject) {
      clearInterval(this._reconnectTimeoutObject);
    }
    if (this._reconnectAttempts < this._maxReconnectAttempt && error) {
      this._reconnectAttempts++;
      if (!this._connected) {
        this._reconnectTimeoutObject = setTimeout(() => {
          cc.log("GameSocket::onProtocolCLose::Trying to reconnect");
          this._clientProtocol.connect(this._url, this.onProtocolClose.bind(this), this.onProtocolClose.bind(this));
        }, this._reconnectTimeout);
      }
    }
  }
}