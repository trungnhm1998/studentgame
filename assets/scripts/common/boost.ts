import { STORAGE_KEY } from "./../core/constant";
import { ACE } from "../core/global-info";
import { StringUtils } from "../core/utils/string-utils";
import SceneManager from "../core/scene-manager";
import NodeUtils from "../core/utils/node-utils";
import TopBar from "./top-bar";
import { HttpClient } from "../core/http-client";
import { NativeManager } from "../managers/native-manager";
import { loadProjectConfig, Config } from "../config";
import { SERVER_EVENT } from "../core/constant";
import WebviewManager, { WEBVIEW_WAIT_TIMEOUT } from "../managers/webview-manager";
import SchoolService from "../school/school-service";
import AccountManager from "../managers/account-manager";
import DialogManager from "../core/dialog-manager";
import LoginService from "../login/login-service";
import { NoticeDialogType } from "../dialogs/notice-dialog";
import ResourceManager from "../core/resources-manager";
import FairyDialog from "./fairy-dialog";
import AudioManager from "../core/audio-manager";
import GameSocket from "../core/networks/GameSocket";
import WebSocketProtocol from "../core/networks/WebSocketProtocol";
import SocketIOProtocol from "../core/networks/SocketIOProtocol";
import ClientProtocol from "../core/networks/ClientProtocol";
import CheckServer from "../check-server/check-server";
const { ccclass, property } = cc._decorator;

@ccclass
export default class Boost extends cc.Component {
  static _instance: Boost = null;
  originalWebviewSize: cc.Size;
  iosInterval: NodeJS.Timeout;

  @property(cc.Node)
  sceneNodeContainer: cc.Node = null;

  @property(cc.Label)
  info: cc.Label = null;

  @property(cc.Node)
  topBar: cc.Node = null;

  // begin TODO: to be remove after implement QA rendering natively
  @property(cc.WebView)
  qaWebview: cc.WebView = null;

  @property(cc.Node)
  qaWebviewContainer: cc.Node = null;
  // end TODO: to be remove after implement QA rendering natively

  @property(cc.Node)
  dialogContainer: cc.Node = null;

  @property(FairyDialog)
  fairyDialog: FairyDialog = null;

  @property(cc.Prefab)
  dialogManager: cc.Prefab = null;

  isLoadedLogin: boolean = false;
  hasNotValidated: boolean = false;
  _componentSelf: Boost = null;

  onLoad() {
    Boost._instance = this;
    Boost._instance._componentSelf = this;
    cc.game.setFrameRate(59);
    this.pauseResumeHandler();
    this.initGlobal();
  }

  initGlobal() {
    ACE.string = new StringUtils();
    let protocol: ClientProtocol = null;
    if (cc.sys.isBrowser) {
      protocol = new SocketIOProtocol();
    } else {
      protocol = new WebSocketProtocol();
    }
    ACE.gameSocket = new GameSocket(protocol);
    ACE.sceneManager = SceneManager.getInstance();
    ACE.sceneManager.sceneNodeContainer = this.sceneNodeContainer;
    const dialogManagerInstance = cc.instantiate(this.dialogManager);
    this.dialogContainer.addChild(dialogManagerInstance);
    ACE.dialogManager = dialogManagerInstance.getComponent(DialogManager);
    ACE.http = new HttpClient();
    ACE.native = NativeManager.getInstance();
    ACE.topBar = this.topBar.getComponent(TopBar);
    ACE.topBar.hide();
    ACE.webviewManager = WebviewManager.getInstance();
    ACE.resManager = ResourceManager.getInstance();
    ACE.fairyDialog = this.fairyDialog;
    ACE.fairyDialog.hide();

    ACE.qaRenderSocket = new GameSocket(new SocketIOProtocol());
  }

  start() {
    try {
      loadProjectConfig().then(() => {
        this.bootstrapGame();
      }).catch((error) => {
        cc.log("Boost::start::loadProjectConfig failed with error.", error);
      });
    } catch (e) {
      cc.log("Boost::start::boostrapGame failed with error.", e);
    }
  }

  bootstrapGame() {
    cc.log("Boost::bootstrapGame");
    cc.view.setResizeCallback(() => this.onResize());
    this.onResize();
    this.info.string = "Checking your internet connection...";
    ACE.qaRenderSocket.connect(Config.qaServer).then(() => {
      cc.log("Boost::bootstrapGame::qaREnderSocket.Connect()");
      this.registerQaServerSocketEvents();
    });
    ACE.gameSocket.connect(cc.sys.isBrowser ? Config.socketIO : Config.gameSocket).then(() => {
      this.registerGameSocketEvents();
    });
  }

  registerQaServerSocketEvents() {
    ACE.qaRenderSocket.on(SERVER_EVENT.CLOSE_WEBVIEW, (data) => this.onCloseWebview(data));
  }

  registerGameSocketEvents() {
    this.info.string = "Let the journey begin!";
    ACE.gameSocket.on(SERVER_EVENT.CLOSE_WEBVIEW, (data) => this.onCloseWebview(data));
    ACE.gameSocket.on(SERVER_EVENT.ERROR, (data) => this.onError(data));
    ACE.gameSocket.on(SERVER_EVENT.VALIDATE_TOKEN, (data) => this.autoLoginIfNeeded(data));
    const lastLoginAccessToken = ACE.resManager.getStorage(STORAGE_KEY.ACCESS_TOKEN);
    if (lastLoginAccessToken) {
      ACE.gameSocket.emit(SERVER_EVENT.VALIDATE_TOKEN, { accessToken: lastLoginAccessToken.accessToken });
    } else {
      ACE.sceneManager.pushScene("login");
    }
  }

  onResize() {
    ACE.dialogManager.node.width = this.sceneNodeContainer.width = cc.winSize.width;
    ACE.dialogManager.node.height = this.sceneNodeContainer.height = cc.winSize.height;
    const currentScene = ACE.sceneManager.currentScene;
    if (currentScene) {
      if (currentScene) {
        currentScene.onResize();
        cc.log(`qaView size [${this.qaWebview.node.width}-${this.qaWebview.node.height}]`);
        const designRatio = Config.designWidth / Config.designHeight;
        const ratio = cc.winSize.width / cc.winSize.height;
        const uiRatio = Math.min(designRatio / ratio, 1);
        if (!ACE.webviewManager.isWebviewHidden) {
          this.qaWebview.node.width = 1920 * uiRatio;
          this.qaWebview.node.height = 1080 * uiRatio;
        }
      }
    }
    NodeUtils.updateWidgetAlignment(this.node);
    ACE.dialogManager.onResize();

    if (!cc.sys.isNative) {
      if (window.innerWidth < window.innerHeight) {
        this.showMobileUrlBar();
      }
    }
  }

  showMobileUrlBar() {
    let i = 0;
    if (this.iosInterval) {
      clearInterval(this.iosInterval);
    }
    this.iosInterval = setInterval(() => {
      document.body.scrollTop = 0;
      // clear timeout after 250 ms and reset scrollTop to 0
      if (i++ > 25) {
        clearInterval(this.iosInterval);
      }
    }, 20);
  }

  update(dt) {
    // only change to next scene when connect to server success
    if (ACE.webviewManager.openingWebview && ACE.webviewManager.waitInterval <= WEBVIEW_WAIT_TIMEOUT) {
      ACE.webviewManager.waitInterval += dt;
    } else if (ACE.webviewManager.waitInterval > WEBVIEW_WAIT_TIMEOUT) {
      ACE.webviewManager.openingWebview = false;
      ACE.webviewManager.waitInterval = 0;
      this.closeWebview();
      ACE.dialogManager.hideLoading();
      ACE.dialogManager.showNoticeDialog("It seems that there is an issue with the internet connection.\nPlease try again later.", NoticeDialogType.OK);
    }
  }

  autoLoginIfNeeded(data) {
    cc.log("autoLoginIfNeeded", data);
    const lastState = ACE.resManager.getStorage(STORAGE_KEY.LAST_ENTER_STATE);
    const accessTokenStorage = ACE.resManager.getStorage(STORAGE_KEY.ACCESS_TOKEN);
    const passData = lastState != null && accessTokenStorage != null ? {
      lastState: lastState.lastEnterState,
      ...accessTokenStorage,
    } : {};
    ACE.dialogManager.showLoading();
    SceneManager.getInstance().pushScene("login", passData);
  }

  loadResources(): Promise<void> {
    return new Promise((resolve, reject) => {
      Promise.all([]).then(() => {
        cc.log("loaded scenes");
        resolve();
      });
    });
  }

  onWebviewEvent(sender, event: cc.WebView.EventType) {
    if (ACE.webviewManager.webviewPreloaded) {
      return;
    }
    cc.log("Boost::onWebviewEvent", event);
    if (event == cc.WebView.EventType.LOADING) {
      this.hideWebview();
      ACE.dialogManager.showLoading();
    } else if (event == cc.WebView.EventType.LOADED) {
      cc.log("onWebviewEvent webview loaded");
    }
  }

  hideWebview(skipCheckSwitchServer = true) {
    const hide = () => {
      ACE.webviewManager.isWebviewHidden = true;
      this.originalWebviewSize = cc.size(this.qaWebview.node.width, this.qaWebview.node.height);
      this.qaWebview.node.width = 1;
      this.qaWebview.node.height = 1;
      this.qaWebviewContainer.opacity = 0;

      // set position
      this.qaWebview.node.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
    };
    if (!skipCheckSwitchServer) {
      CheckServer._instance.switchBackToOriginalServer().then(() => {
        hide();
      }).catch((error) => {
        cc.log("Boost::hideWebView", error);
        hide();
      });
    } else {
      hide();
    }
  }

  showWebview() {
    ACE.webviewManager.isWebviewHidden = false;
    this.qaWebviewContainer.runAction(cc.fadeIn(0.5));
    const designRatio = Config.designWidth / Config.designHeight;
    const ratio = cc.winSize.width / cc.winSize.height;
    const uiRatio = Math.min(designRatio / ratio, 1);
    this.qaWebview.node.setPosition(0, 0);
    this.qaWebview.node.width = 1920 * uiRatio;
    this.qaWebview.node.height = 1080 * uiRatio;
    this.qaWebviewContainer.opacity = 255;
  }

  openWebview(url: string) {
    cc.log("Boost::openWebview with url", url);
    this.qaWebviewContainer.active = true;
    this.qaWebviewContainer.opacity = 0;
    this.qaWebview.url = url;
  }

  closeWebview() {
    cc.log("Boost::closeWebview");
    // this.qaWebview.url = "";
    this.qaWebviewContainer.active = false;
  }

  onCloseWebview(data) {
    if (data && data.accessToken != AccountManager.getInstance().accessToken) { return; }
    if (data.value != "loadCompleted") {
      // update data after close webview
      SchoolService.enrolmentList();
      ACE.webviewManager.closeWebview(data);
      // if user press no button on webview
      // SchoolService.loadImprovement();
      // SchoolService.loadFocusFirstPage();
    } else {
      ACE.webviewManager.openingWebview = false;
      ACE.dialogManager.hideLoading();
      this.showWebview();
    }
    LoginService.getStudentCoinsStars();
  }

  onError(data) {
    cc.log("onError", data);
    const { error } = data;
    if ((error == "Access Denied" || error == "invalid signature" || error == "jwt expired") && !this.hasNotValidated) {
      ACE.resManager.removeStorage(STORAGE_KEY.ACCESS_TOKEN);
      AccountManager.getInstance().logout();
      SceneManager.getInstance().pushScene("login");
    }
  }

  pauseResumeHandler() {
    cc.game.on(cc.game.EVENT_HIDE, this.onPause.bind(this));
    cc.game.on(cc.game.EVENT_SHOW, this.onResume.bind(this));
  }

  onPause() {
    cc.log("Game Pause");
  }

  onResume() {
    cc.log("Game Resume");
    cc.audioEngine.resumeAll();
  }
}
