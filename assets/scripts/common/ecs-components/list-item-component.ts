import Component, { IComponent } from "../../core/ecs/component";

const { ccclass, property } = cc._decorator;

export interface IListItemComponent extends IComponent {
  name: string;
  itemRef: ItemRef;
}

export interface ItemRef {
  align: string;
  parent: string;
  index: number;
  anchor: string;
  count: number;
}

export interface Anchor {
  x: number;
  y: number;
}

@ccclass
export default class ListItemComponent extends Component {
  _c_name: string = "ListItemComponent";
  _data: IListItemComponent;
}
