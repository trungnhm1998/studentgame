import StateRange from "./state-range";
import SpeedLevel from "./speed-level";
const { ccclass, property } = cc._decorator;
export default class BaseStage {
  public stageRanges: StateRange[];
  public stageSpeeds: SpeedLevel[];

  // GDD Maths Runner formula missing other stage => hardcode for all stages/all levels
  public timeOfGame = [24, 16, 12, 9.6, 8];
  public distanceTravelled = [6, 12, 24, 48, 96];

  public getStateRanges(index) {
    return this.stageRanges[index];
  }

  public getSpeedLevel(index) {
    return this.stageSpeeds[index];
  }

  public getDistanceToClear() {
    let distanceTravelledPerStage = 0;
    for (let idx = 0; idx < this.timeOfGame.length; idx++) {
      distanceTravelledPerStage += this.timeOfGame[idx] * this.distanceTravelled[idx];
    }
    return distanceTravelledPerStage * 0.6;
  }
}
