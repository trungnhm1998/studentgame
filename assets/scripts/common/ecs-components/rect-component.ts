
import Component, { IComponent } from "../../core/ecs/component";

const { ccclass, property } = cc._decorator;

export interface IRectComponent extends IComponent {
}

@ccclass
export default class RectComponent extends Component {
  _c_name: string = "RectComponent";
  _data: IRectComponent;
}
