import { ACE } from "../../core/global-info";
import { SERVER_EVENT } from "../../core/constant";
import { MailboxMessage } from "../../models/mailbox-message";
import Message from "./message";
import { Random } from "../../core/utils/random-utils";
import MailboxService from "./mailbox-service";
import NodeUtils from "../../core/utils/node-utils";
import AudioManager from "../../core/audio-manager";

const { ccclass, property } = cc._decorator;

const GiftAnimName = "gift_anim";

@ccclass
export default class MailBox extends cc.Component {
  @property(cc.Prefab)
  messagePrefab: cc.Prefab = null;

  @property(cc.Node)
  messageListContent: cc.Node = null;

  @property(cc.Label)
  fromLabel: cc.Label = null;

  @property(cc.Label)
  subjectLabel: cc.Label = null;

  @property(cc.Label)
  bodyMessage: cc.Label = null;

  @property(cc.Label)
  giftValue: cc.Label = null;

  @property(cc.Sprite)
  giftSprite: cc.Sprite = null;

  @property(cc.Node)
  plusSign: cc.Node = null;

  @property(cc.SpriteFrame)
  starSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  coinSF: cc.SpriteFrame = null;

  @property(cc.Node)
  giftChestNode: cc.Node = null;

  @property(cc.Button)
  claimBox: cc.Button = null;

  @property(cc.Button)
  claimGiftButton: cc.Button = null;

  @property(cc.Node)
  giftSparkNode: cc.Node = null;

  @property(cc.SpriteAtlas)
  giftBoxAtlas: cc.SpriteAtlas = null;

  @property(cc.Animation)
  gifAnim: cc.Animation = null;

  @property(cc.Prefab)
  coinPrefab: cc.Prefab = null;

  @property(cc.Node)
  empty: cc.Node = null;

  giftsRemain: number = 0;
  savedRemain: number = 0;
  unreadRemain: number = 0; // To keep count of unread messages
  boxes: cc.Node[] = []; // to save front end ui for each messages
  curent: number = 0; // curent message in front end
  nameCharLimit: number = 14; // to limit name length
  normalEmotions = [
    " has sent you a gift 🎉🎉🎉.\nClick on the box to open it 🎁🎁🎁",
    "'s gift has arrived 🎊🎊🎊\nOpen the box and claim it 🎁🎁🎁",
    " has sent this cool gift ✨✨✨\nCheck out the box 🎁🎁🎁",
  ];
  collectedEmotions = [
    "You have already claimed the gift 🎈🎈🎈\nSend more gifts to friends 😍😍😍",
    "This gift was opened and claimed already ✨✨✨ \nTry opening a different one 🎁🎁🎁",
  ];
  thanksEmotions = [
    " would like to thank you for your gift!\nIt was awesome 😍😍😍",
    " has recieved your gift and said it was a cool gift 🤩🤩🤩",
    " was jumping up with happiness after recieving your gift!\nKeep up awesomness ✨✨✨",
    " got your gift and said you are his best buddy!\nBuddies care and share 🎉🎉🎉",
  ];

  messageList: MailboxMessage[] = [];
  messageComponentDic: Map<string, Message> = new Map();
  selectedMessage: Message = null;
  claimBoxAnimComponent: cc.Animation;

  onLoad() {
    this.claimBoxAnimComponent = this.claimBox.getComponent(cc.Animation);
    this.registerEvents();
  }

  registerEvents() {
    // ACE.gameSocket.on(SERVER_EVENT.LOAD_MESSAGE_LIST_ALL, data => { }); //added in map
    ACE.gameSocket.on(SERVER_EVENT.RECEIVE_GIFT, (data) => {
      ACE.topBar.updateStar(data[0].Stars);
      ACE.topBar.updateCoin(data[0].Coins);
    });
    ACE.gameSocket.on(SERVER_EVENT.RECEIVE_GIFT_ALL, (data) => {
      cc.find("TopBar")
        .getComponent("TopBar")
        .updateStar(data[0].Stars)
        .updateCoin(data[0].Coins);
    });
    ACE.gameSocket.on(SERVER_EVENT.DELETE_MESSAGE, (data) => { });
    ACE.gameSocket.on(SERVER_EVENT.SET_MESSAGE_SAVED, (data) => { });
    ACE.gameSocket.on(SERVER_EVENT.GET_MESSAGE_BY_ID, (data) => { });
  }

  onEnable() {
    // TODO: Do not call rebuild ui every time enable like this
  }

  hide() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    this.node.active = false;
    ACE.topBar.toggleButtonGroup(true);
  }

  show() {
    this.node.active = true;
    this.buildMessageList();
    ACE.topBar.toggleButtonGroup(false);
  }

  buildMessageList() {
    this.messageListContent.removeAllChildren();
    this.hideBody();
    this.empty.active = false;
    if (this.messageList.length == 0) {
      this.empty.active = true;
      ACE.dialogManager.hideLoading();
      return;
    }
    this.messageComponentDic = new Map();
    for (let i = 0; i < this.messageList.length; i++) {
      const message = this.messageList[i];
      const messageNode = cc.instantiate(this.messagePrefab);
      const messageComponent = messageNode.getComponent(Message);
      messageComponent.setupData(message, (oId) => this.onSelectMessage(oId));
      this.messageListContent.addChild(messageNode);
      this.messageComponentDic.set(message.Oid, messageComponent);
    }
    this.messageListContent.getComponent(cc.Layout).updateLayout();
    this.messageListContent.sortAllChildren();
    cc.log("Mailbox::buildMessageList", this.messageListContent.childrenCount);
    this.onSelectMessage(this.messageList[0].Oid);
    ACE.dialogManager.hideLoading();
  }

  // user that don't have any mail
  hideBody() {
    NodeUtils.findNodeByName(this.node, "message-header").active = false;
    NodeUtils.findNodeByName(this.node, "body-content").active = false;
  }

  showBody() {
    NodeUtils.findNodeByName(this.node, "message-header").active = true;
    NodeUtils.findNodeByName(this.node, "body-content").active = true;
  }

  /**
   * setup UI, text, body message when select a message
   * @param oId object id of the message user select
   */
  onSelectMessage(oId: string) {
    this.showBody();
    if (this.selectedMessage && oId == this.selectedMessage._message.Oid) { return; }
    if (this.selectedMessage != null) {
      this.selectedMessage.deselect();
    }
    this.selectedMessage = this.messageComponentDic.get(oId);
    this.selectedMessage.select();

    const { Type, MessageGroup, IsReceive, Oid, MessageEmojiUrl } = this.selectedMessage._message;
    this.subjectLabel.string = "Thanks!"; // default message, doesn't need default case for switch case below
    if (MessageGroup == 1) {
      switch (Type) {
        case 0:
          this.bodyMessage.string = `${this.selectedMessage.getSenderName()}${new Random().fromList(this.thanksEmotions)}`;
          break;
        case 1:
        case 2:
          this.bodyMessage.string = `${this.selectedMessage.getSenderName()}${new Random().fromList(this.normalEmotions)}`;
          this.subjectLabel.string = "Gift";
          break;
      }
    } else {
      this.subjectLabel.string = "Message!"; // hardcode
      this.bodyMessage.string = this.selectedMessage._message.Message ? this.selectedMessage._message.Message : "";
      // if click on a message that messageGroup == 2 => read
      if (Type == 0 && !IsReceive) {
        MailboxService.receiveGift(Oid);
        this.selectedMessage.isReadMessage(true);
      }
    }
    if (Type == 0 && !IsReceive) {
      MailboxService.receiveGift(Oid);
      this.selectedMessage.isReadMessage(true);
    }
    this.fromLabel.string = this.selectedMessage.getSenderName();

    // setup UI for coin/stars gift value and icon
    this.plusSign.active = true;
    if (Type == 1) {
      const color = cc.color(255, 119, 36);
      this.plusSign.color = color;
      this.giftSprite.spriteFrame = this.coinSF;
      this.giftValue.node.getComponent(cc.LabelOutline).color = color;
      this.setupOpenAnim();
    } else if (Type == 2) {
      const color = cc.color(0, 173, 235);
      this.plusSign.color = color;
      this.giftSprite.spriteFrame = this.starSF;
      this.giftValue.node.getComponent(cc.LabelOutline).color = color;
      this.setupOpenAnim(false);
    } else {
      this.plusSign.active = false;
    }

    // coins/stars gift value
    this.giftValue.string = this.selectedMessage.getGiftValue() + "";

    this.setupGiftChestAnim();
    this.setupGifAnim(MessageEmojiUrl);
  }

  setupOpenAnim(isCoin = true) {
    this.claimBox.getComponent(cc.Sprite).spriteFrame = this.giftBoxAtlas.getSpriteFrame("boxClosed");
    this.claimBoxAnimComponent.pause();
    const temp: cc.SpriteFrame[] = [
      this.giftBoxAtlas.getSpriteFrame("boxClosed"),
      isCoin ? this.giftBoxAtlas.getSpriteFrame("boxCoinHalfOpen") : this.giftBoxAtlas.getSpriteFrame("boxStarHalfOpen"),
      isCoin ? this.giftBoxAtlas.getSpriteFrame("boxCoinOpen") : this.giftBoxAtlas.getSpriteFrame("boxStarOpen"),
      this.giftBoxAtlas.getSpriteFrame("boxOpen"),
    ];
    const clip = cc.AnimationClip.createWithSpriteFrames(temp as [cc.SpriteFrame], 5);
    clip.name = GiftAnimName;
    clip.wrapMode = cc.WrapMode.Normal;

    this.claimBoxAnimComponent.addClip(clip);
  }

  setupGifAnim(url: string) {
    this.gifAnim.node.active = false;
    if (url == "") {
      return;
    }
    this.gifAnim.node.active = true;
    cc.loader.loadRes(`images/map/mailbox/${url}`.replace(".png", ""), cc.SpriteAtlas, (error, atlas: cc.SpriteAtlas) => {
      if (error) {
        cc.log(error);
      } else {
        const clip = cc.AnimationClip.createWithSpriteFrames(atlas.getSpriteFrames(), 5);
        clip.name = "gif";
        clip.wrapMode = cc.WrapMode.Loop;
        this.gifAnim.addClip(clip);
        this.gifAnim.play("gif");
        this.gifAnim.node.x = 0;

        if (this.selectedMessage._message.Type == 1) {
          this.gifAnim.node.x = -350;
        }
      }
    });
  }

  setupGiftChestAnim() {
    // run action for chest
    this.claimBox.getComponent(cc.Sprite).spriteFrame = this.giftBoxAtlas.getSpriteFrame("boxClosed");
    this.claimBoxAnimComponent.pause(GiftAnimName);
    this.giftChestNode.stopAllActions();
    this.claimBox.node.stopAllActions();
    this.giftSparkNode.stopAllActions();
    this.giftChestNode.active = false;
    this.claimBox.node.active = false;
    this.claimGiftButton.node.active = false;
    this.giftSparkNode.active = false;
    this.claimGiftButton.interactable = false;
    this.claimBox.interactable = false;
    this.claimBox.node.y = -235; // reset position
    const { IsReceive, MessageGroup, Type } = this.selectedMessage._message;
    if ((MessageGroup == 2 || MessageGroup == 1) && Type == 0) { return; }
    if (!IsReceive) {
      this.giftChestNode.active = true;
      this.giftChestNode.setPosition(0, 125);
      this.giftChestNode.setScale(0);
      this.giftChestNode.opacity = 255;
      this.giftChestNode.runAction(cc.sequence(
        cc.spawn(
          cc.scaleTo(0.6, 1),
          cc.moveTo(0.6, cc.v2(500, 0)).easing(cc.easeCircleActionOut()),
        ),
        cc.moveTo(0.6, cc.v2(0, -125)).easing(cc.easeCircleActionIn()),
        cc.fadeTo(0, 0),
        cc.callFunc(() => {
          this.claimBox.interactable = true;
          this.claimGiftButton.interactable = true;
          this.giftSparkNode.active = true;
          this.claimBox.node.active = true;
          this.claimGiftButton.node.active = true;
          this.giftSparkNode.runAction(cc.sequence(
            cc.delayTime(0.3),
            cc.rotateBy(0, 150),
          ).repeatForever());
          this.claimBox.node.runAction(cc.sequence(
            cc.moveBy(0.5, cc.v2(0, 20)).easing(cc.easeBackInOut()),
            cc.moveBy(0.5, cc.v2(0, -20)).easing(cc.easeBackInOut()),
          ).repeatForever());
        }),
      ));
    } else {
      // show opened empty chest
      const { Type } = this.selectedMessage._message;
      if (Type == 1 || Type == 2) {
        this.claimBox.node.active = true;
        this.claimBox.getComponent(cc.Sprite).spriteFrame = this.giftBoxAtlas.getSpriteFrame("boxOpen");
      }
    }
  }

  onClaimGift() {
    this.claimGiftButton.interactable = false;
    this.claimBox.interactable = false;
    this.claimGiftButton.node.active = false;
    this.giftSparkNode.active = false;
    this.claimBox.node.stopAllActions();
    this.claimBoxAnimComponent.play(GiftAnimName);

    // play coin anim
    const coin = cc.instantiate(this.coinPrefab);
    this.node.addChild(coin);

    MailboxService.receiveGift(this.selectedMessage._message.Oid);
    this.selectedMessage._message.IsReceive = true;
    this.selectedMessage.isReadMessage(true);
  }
}
