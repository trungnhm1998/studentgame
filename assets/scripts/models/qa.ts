import { Graphic } from "../constants/qa.constants";

export interface IQA {
  ID:                    number;
  YearGrade:             number;
  YearName:              string;
  StrengthID:            number;
  StrengthName:          string;
  StrengthDescription:   string;
  TopicID:               number;
  TopicName:             string;
  TopicDescription:      string;
  QATemplateID:          number;
  QATemplateName:        string;
  QATemplateDescription: string;
  QaName:                string;
  Name:                  string;
  QDescription:          string;
  ADescription:          string;
  Stars:                 number;
  Coins:                 number;
  Type:                  number;
  TypeName:              string;
  AnswerType:            number;
  AnswerTypeName:        string;
  StatusName:            string;
  Version:               number;
  lastQuestion:          boolean;
  version:               number;
  design:                IDesign[];
  answers:               string[];
  answerShapes:          any[];
  explains:              Explain[];
}

export interface IDesign {
  version?:    number;
  qaName?:     string;
  qaType?:     number;
  configure?:  string;
  incorrect?:  ConfigState;
  correct?:    ConfigState;
  selected?:   ConfigState;
  graphicId?:  Graphic;
  objPrivate?: ObjPrivate;
  x?:          number;
  y?:          number;
  w?:          number;
  h?:          number;
  name?:       string;
  id?:         number;
}

export interface ConfigState {
  image: Image;
  rect:  Rect;
}

export interface Image {
  link:     string;
  key:      string;
  position: string;
  anchor:   number[];
  scale:    number;
}

export interface Rect {
  radius: number;
  stroke: number;
  border: number;
}

export interface ObjPrivate {
  keyImage?:     string;
  linkImage?:    string;
  preload?:      boolean;
  Click:         Click;
  DragDrop:      Click;
  multiDragDrop: Click;
  anchorX?:      number;
  anchorY?:      number;
  style?:        Style;
  text?:         string;
  centerX?:      number;
  centerY?:      number;
  lineWidth?:    number;
  lineColor?:    number;
  radius?:       number;
  action?:       IAction;
}

export interface Click {
}

export interface IAction {
  name:            string;
  type?:           string;
  value?:          any;
  x?:              number;
  y?:              number;
  anchor?:         Anchor;
  dir?:            string;
  spacing?:        number;
  itemRef?:        ItemRef;
  multiple?:       boolean;
  toggle?:         boolean;
  keyboard?:       string;
  dataType?:       string;
  maxLength?:      number;
  resultPosition?: string;
  tabOrder?:       number;
  fontSize?:       number;
  mode?:           string;
  scale?:          number;
}

export interface Anchor {
  x: number;
  y: number;
}

export interface ItemRef {
  align:  string;
  parent: string;
  index:  number;
  anchor: string;
  count:  number;
}

export interface Style {
  font:            string;
  fontStyle:       string;
  fontVariant:     string;
  fontWeight:      string;
  fontSize:        number;
  lineHeight:      number;
  lineSpacing:     number;
  backgroundColor: null;
  fill:            string;
  align:           string;
  boundsAlignH:    string;
  boundsAlignV:    string;
  stroke:          string;
  strokeThickness: number;
  wordWrap:        boolean;
  wordWrapWidth:   number;
  maxLines:        number;
  tabs:            number;
  width?:          number;
  height?:         number;
  textcolor?:      string;
  fontFamily?:     string;
  textAlign?:      string;
  borderRadius?:   number;
  borderStyle?:    string;
  borderWidth?:    number;
  borderColor?:    string;
  hidden?:         boolean;
  maxLength?:      number;
  autofocus?:      boolean;
}

export interface Explain {
  html: string;
}
