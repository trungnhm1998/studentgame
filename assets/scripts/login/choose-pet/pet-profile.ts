export interface petProfile {
    name: string,
    species: string,
    eats: string,
    ability: string,
    personality: string
}