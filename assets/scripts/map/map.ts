import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";
import MapService from "./map-service";
import MyClassService from "./my-class/my-class-service";
import StudentHouseManager from "../student-house/StudentHouseManager";
import MyClass from "./my-class/my-class";
import AccountManager from "../managers/account-manager";
import MailBox from "./mailbox/mailbox";
import { MailboxMessage } from "../models/mailbox-message";
import { TutorialType } from "../common/top-bar";
import SchoolManager from "../managers/school-manager";
import { NoticeDialogType } from "../dialogs/notice-dialog";
import NodeUtils from "../core/utils/node-utils";
import AudioManager from "../core/audio-manager";
import FairyLayer from "./fairy-layer";
import CharacterComponent from "../common/character-component";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Map extends GameScene {
  @property(cc.Node)
  mapIcons: cc.Node = null;

  @property(cc.Prefab)
  myClassPF: cc.Prefab = null;

  @property(cc.Prefab)
  mailBoxPF: cc.Prefab = null;

  @property(cc.Node)
  dialogContainer: cc.Node = null;

  @property(cc.Node)
  myClassIcon: cc.Node = null;

  @property(cc.Prefab)
  fairyPrefab: cc.Prefab = null;

  @property(CharacterComponent)
  characterPreload: CharacterComponent = null;

  @property(cc.Animation)
  tutorialAnim: cc.Animation = null;

  myClassDialog: cc.Node = null;
  mailboxDialog: cc.Node = null;
  fairyLayer: FairyLayer = null;
  // characterPreload: CharacterComponent = null;

  // Global Variables
  _mapIcons: cc.Node[];
  profile = null;

  onLoad() {
    super.onLoad();
    this._mapIcons = this.mapIcons.children;
    for (let i = 0; i < this._mapIcons.length; i++) {
      const mapIcon = this._mapIcons[i];
      mapIcon.zIndex = i;
    }

    this._mapIcons[0].parent.sortAllChildren();
  }

  onEnter() {
    super.onEnter();
    this.characterPreload.autoUpdate(false);
    let urls = ["scenes/student-house", "scenes/character-shop", "scenes/minigame-menu", "scenes/game-play"];
    cc.loader.loadResArray(urls, (err, res) => {
      cc.log("@@@@@@ Loaded the remain builings @@@@@@", err, res);
    });
    ACE.webviewManager.preloadWebview();
    ACE.topBar.mapCallback = () => {
      this.tutorialAnim.play();
    };
    MyClassService.getNumberGifts();
    const mapTutorialState = AccountManager.getInstance()._gameStateTutorial.get("map");
    if (!mapTutorialState.isClosed) {
      ACE.topBar.hide();
      AudioManager.getInstance().playAudio("npc_appearing");
      AudioManager.getInstance().playAudio("star7");
      this.fairyLayer = cc.instantiate(this.fairyPrefab).getComponent(FairyLayer);
      this.node.addChild(this.fairyLayer.node);
      this.fairyLayer.runEffect().then(() => {
        ACE.topBar.showTutorial(TutorialType.Map, () => this.showAssisgnmentPopup());
        this.tutorialAnim.play();
      }).then(() => {
        ACE.topBar.show();
        ACE.topBar.positionButtons();
      });
    } else {
      this.showAssisgnmentPopup();
    }
    ACE.topBar.show();
    ACE.topBar.positionButtons();
    ["intro", "choose-character", "choose-pet"].forEach((sceneName) => {
      ACE.sceneManager.popScene(sceneName);
    });
    AudioManager.getInstance().playAudio("map", true, true);
  }

  onResize() {
    super.onResize();
  }

  registerEvents() {
    ACE.gameSocket.on(SERVER_EVENT.SAVE_STUDENT_GAME_TUTORIAL, (data) => {
      cc.log(data);
    });

    ACE.gameSocket.on(SERVER_EVENT.GET_NUMBER_OF_GIFTS, (data) => {
      AccountManager.getInstance().numberGift = data;
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_FRIEND_LIST, (data) => {
      AccountManager.getInstance().friendList = data;
      this.showMyClassDialog();
    });

    ACE.gameSocket.on(SERVER_EVENT.LOAD_MESSAGE_LIST_ALL, (data) => this.onLoadMessageListAll(data));
  }

  arrowTween() {
    // tween action for arrows
    const arrowUp = cc.moveBy(0.3, cc.v2(0, 20)).easing(cc.easeSineInOut());
    const arrowDown = cc.moveBy(0.3, cc.v2(0, -20)).easing(cc.easeSineInOut());
    return cc.repeatForever(cc.sequence(arrowUp, arrowDown));
  }

  showAssisgnmentPopup() {
    if (AccountManager.getInstance().justLogin) {
      ACE.topBar.init();
      const { assignmentData } = SchoolManager.getInstance();
      if (assignmentData && assignmentData.NumberQaAttempt < assignmentData.NumberOfQa && AccountManager.getInstance().showAssessment) {
        AccountManager.getInstance().showAssessment = false;
        ACE.dialogManager.showNoticeDialog(
          "You have been give a new assessment challenge\nLet's go there complete it!",
          NoticeDialogType.YES_NO,
          () => ACE.webviewManager.openQa("assignment"),
        );
      }
    }
  }

  buttonActions(event: cc.Event, data) {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    cc.log(event);
    // it takes custom event data from button component of each buttons in the map and nava bar and does the stuff
    NodeUtils.preventMultipleClick(event.target.getComponent(cc.Button));
    switch (data) {
      case "myClass":
        ACE.dialogManager.showLoading();
        MyClassService.loadFriendList();
        break;
      case "school":
        ACE.dialogManager.showLoading();
        ACE.sceneManager.pushScene("school");
        break;
      case "shop":
        ACE.dialogManager.showLoading();
        ACE.sceneManager.pushScene("character-shop");
        break;
      case "myHouse":
        ACE.dialogManager.showLoading();
        StudentHouseManager.getInstance().isVisitFriendHouse = false;
        ACE.sceneManager.pushScene("student-house");
        break;
      case "mailBox":
        ACE.dialogManager.showLoading();
        this.mailBox();
        break;
      case "game":
        ACE.sceneManager.pushScene("minigame-menu");
        break;
      default:
        cc.log(data);
        break;
    }
  }

  showMyClassDialog() {
    if (AccountManager.getInstance().friendList != null) {
      // Instantiating myclass and passing the data
      if (this.myClassDialog == null) {
        this.myClassDialog = cc.instantiate(this.myClassPF);
        const myClassComponent: MyClass = this.myClassDialog.getComponent(MyClass);
        myClassComponent.mapComponent = this;
        this.dialogContainer.addChild(this.myClassDialog);
      }
      this.myClassDialog.setPosition(0, 0);
      this.myClassDialog.getComponent(MyClass).friendList = AccountManager.getInstance().friendList;
      this.myClassDialog.getComponent(MyClass).initData();
      this.myClassDialog.getComponent(MyClass).show();
    }
  }

  mailBox() {
    MapService.loadMessageListAll();
  }

  onLoadMessageListAll(data: MailboxMessage[]) {
    cc.log("Map::onLoadMessageListAll::data", data);
    if (this.mailboxDialog == null) {
      this.mailboxDialog = cc.instantiate(this.mailBoxPF);
      this.dialogContainer.addChild(this.mailboxDialog);
    }
    this.mailboxDialog.getComponent(MailBox).messageList = data;
    this.mailboxDialog.getComponent(MailBox).show();
  }
}
