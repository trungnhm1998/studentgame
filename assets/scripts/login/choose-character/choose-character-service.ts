import { ACE } from "../../core/global-info";
import { SERVER_EVENT } from "../../core/constant";

export default class ChooseCharacterService {
  static getBasicProducts() {
    ACE.gameSocket.emit(SERVER_EVENT.GET_BASIC_PRODUCTS, {});
  }

  static getBasicUsingItems() {
    ACE.gameSocket.emit(SERVER_EVENT.GET_BASIC_USING_ITEMS, {});
  }

  static selectGender(gender) {
    ACE.gameSocket.emit(SERVER_EVENT.SELECT_GENDER, {
      gender
    });
  }

  static selectNext(gender, type) {
    ACE.gameSocket.emit(SERVER_EVENT.SELECT_NEXT, {
      gender,
      type
    });
  }

  static selectPrev(gender, type) {
    ACE.gameSocket.emit(SERVER_EVENT.SELECT_PREVIOUS, {
      gender,
      type
    });
  }

  static getSelectingGender() {
    ACE.gameSocket.emit(SERVER_EVENT.GET_SELECTING_GENDER, {});
  }
}
