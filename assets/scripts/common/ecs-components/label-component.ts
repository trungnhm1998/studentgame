import Component, { IComponent } from "../../core/ecs/component";

const { ccclass, property } = cc._decorator;

export interface ILabelComponent extends IComponent {
  font: string;
  fontStyle: string;
  fontVariant: string;
  fontWeight: string;
  fontSize: number;
  lineHeight: number;
  lineSpacing: number;
  backgroundColor: null;
  fill: string;
  align: string;
  boundsAlignH: string;
  boundsAlignV: string;
  stroke: string;
  strokeThickness: number;
  wordWrap: boolean;
  wordWrapWidth: number;
  maxLines: number;
  tabs: number;
  text: string;
  name: string;
}

@ccclass
export default class LabelComponent extends Component {
  _c_name: string = "LabelComponent";
  _data: ILabelComponent;
  onLoad() {
  }
}
