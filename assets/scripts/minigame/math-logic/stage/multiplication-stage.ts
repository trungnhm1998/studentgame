
import StateRange from "./state-range";
import BaseStage from "./base-stage";
import SpeedLevel from "./speed-level";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MultiplicationStage extends BaseStage {
  constructor() {
    super();
    this.stageRanges = new Array<StateRange>();
    this.stageRanges = [
      // 1
      new StateRange(1, 3, 10),
      new StateRange(2, 4, 10),
      new StateRange(3, 5, 10),
      // 2
      new StateRange(1, 4, 10),
      new StateRange(2, 5, 10),
      new StateRange(3, 6, 10),
      // 3
      new StateRange(1, 7, 10),
      new StateRange(3, 8, 10),
      new StateRange(5, 9, 10),
      // 4
      new StateRange(2, 8, 12),
      new StateRange(4, 9, 12),
      new StateRange(6, 10, 12),
      // 5
      new StateRange(1, 10, 12),
      new StateRange(3, 12, 12),
      new StateRange(6, 14, 12),
      // 6
      new StateRange(1, 12, 14),
      new StateRange(3, 12, 14),
      new StateRange(6, 14, 14),
    ];

    BaseStage.prototype.stageSpeeds = [
      // 1
      new SpeedLevel(10, 12.5, 15, 17.5, 20),
      new SpeedLevel(10, 12.5, 15, 17.5, 20),
      new SpeedLevel(10, 12.5, 15, 17.5, 20),
      // 2
      new SpeedLevel(12, 14, 16, 18, 20),
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(10, 12, 14, 16, 18),
      // 3
      new SpeedLevel(12, 14, 16, 18, 20),
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(10, 12, 14, 16, 18),
      // 4
      new SpeedLevel(12, 14, 16, 18, 20),
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(10, 12, 14, 16, 18),
      // 5
      new SpeedLevel(10, 12, 14, 16, 20),
      new SpeedLevel(10, 12, 14, 16, 20),
      new SpeedLevel(10, 12, 14, 16, 20),
      // 6
      new SpeedLevel(12, 14, 16, 18, 20),
      new SpeedLevel(15, 17.5, 20, 22.5, 25),
      new SpeedLevel(20, 22.5, 25, 27.5, 30),
    ];
  }
}
