import { ACE } from "../core/global-info";
import GameScene from "../common/game-scene";
import SchoolService from "./school-service";
import SchoolManager from "../managers/school-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class QaAttempts extends GameScene {
  @property(cc.Label)
  numberOfQaSubmitted: cc.Label = null;

  @property(cc.SpriteAtlas)
  selectTopicAtlas: cc.SpriteAtlas = null;

  start() {
    this.updateQaAttempts();
  }

  updateQaAttempts() {
    if (ACE.sceneManager.currentScene.name == "practice") {
      this.numberOfQaSubmitted.string = `${SchoolService.numberOfQaSubmitted}`;
    } else if (ACE.sceneManager.currentScene.name == "assignment") {
      this.numberOfQaSubmitted.string = `${SchoolService.numberOfQaSubmitted}/8`;
    } else if (ACE.sceneManager.currentScene.name == "revision") {
      this.numberOfQaSubmitted.string = `${SchoolService.numberOfQaSubmitted}/10`;
    } else if (ACE.sceneManager.currentScene.name == "homework") {
      this.numberOfQaSubmitted.string = `${SchoolService.numberOfQaSubmitted}/20`;
    } else if (ACE.sceneManager.currentScene.name == "naplan") {
      this.numberOfQaSubmitted.string = `${SchoolService.numberOfQaSubmitted}/40`;
    } else if (ACE.sceneManager.currentScene.name == "focus") {
      this.numberOfQaSubmitted.string = `${SchoolService.numberOfQaSubmitted}/20`;
    } else if (ACE.sceneManager.currentScene.name == "improvement") {
      this.numberOfQaSubmitted.string = `${SchoolService.numberOfQaSubmitted}/20`;
    }
  }

  update() {
    this.updateQaAttempts();
  }
}
