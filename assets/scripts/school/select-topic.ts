import { ACE } from "../core/global-info";
import SchoolManager from "../managers/school-manager";
import Topic, { ITopic } from "./topic";
import ConceptComponent from "./concepts";
import IConcept from "../models/concept";
import SchoolService from "./school-service";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SelectTopic extends cc.Component {
  @property(cc.Prefab)
  topicPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  conceptPrefab: cc.Prefab = null;

  @property(cc.SpriteAtlas)
  selectTopicAtlas: cc.SpriteAtlas = null;

  @property(cc.Node)
  topicContainer: cc.Node = null;

  @property(cc.Node)
  conceptLayer: cc.Node = null;

  @property(cc.Node)
  conceptContainer: cc.Node = null;

  @property(cc.Node)
  nextConceptButton: cc.Node = null;

  @property(cc.Node)
  previousConceptButton: cc.Node = null;

  @property(cc.Label)
  conceptNameLabel: cc.Label = null;

  @property(cc.Node)
  backButton: cc.Node = null;

  chooseTopic: ITopic;
  roomName: any;
  count: number = 1;
  listConceptLastPage: number = 0;

  renderTopic() {
    const listTopics = SchoolManager.getInstance().listTopics;
    if (!listTopics || listTopics.length == 0) {
      return;
    }
    this.topicContainer.removeAllChildren();
    this.conceptContainer.removeAllChildren();
    for (let i = 0; i < listTopics.length; i++) {
      const topicData: ITopic = listTopics[i];
      // create new node based on topic data
      const topicNode = cc.instantiate(this.topicPrefab);
      // active the node for it to render
      // get sprite from atlast with correct id from topicSpriteId
      // update sprite frame for the topic prefab object
      topicNode.getComponent(cc.Sprite).spriteFrame = this.selectTopicAtlas.getSpriteFrame(`${topicData.TopicID}`);

      const topicComponent: Topic = topicNode.getComponent(Topic);
      topicComponent.setData(topicData);
      topicComponent.onPlayCallback = () =>
        this.onPlayPressed(topicData.TopicID);
      topicComponent.onAdvancedCallback = () => {
        this.chooseTopic = topicData;
        this.onAdvancedPressed(topicData.TopicID);
        // Singleton to save TopicId
        SchoolManager.getInstance().topicId = topicData.TopicID;
      };
      topicNode.active = true;
      this.topicContainer.addChild(topicNode);
      this.topicContainer.getComponent(cc.Layout).updateLayout();
    }
  }

  renderConcept(listConcepts: IConcept[], canBack = true, state = "practiseCenter") {
    this.roomName = state;
    let listAllConcepts = SchoolManager.getInstance().listConcepts;
    if (listAllConcepts.length > 9) {
      this.nextConceptButton.active = true;
    }
    this.topicContainer.active = false;
    this.conceptLayer.active = true;
    if (this.chooseTopic) {
      this.conceptNameLabel.node.active = true;
      this.conceptNameLabel.string = this.chooseTopic.TopicDescription;
    } else {
      this.conceptNameLabel.string = "List of Concepts";
      this.backButton.active = canBack;
    }

    this.conceptContainer.destroyAllChildren();
    this.listConceptLastPage = listConcepts.length;
    for (let index = 0; index < listConcepts.length; index++) {
      const concept = listConcepts[index];
      const conceptNode = cc.instantiate(this.conceptPrefab);
      conceptNode.active = true;
      const conceptComponent: ConceptComponent = conceptNode.getComponent(ConceptComponent);
      conceptComponent.loadConcept(concept);
      this.conceptContainer.addChild(conceptNode);
      conceptComponent.onConceptCallback = () => {
        ACE.webviewManager.openQa(state, conceptComponent.data.ID);
      };
    }
    this.conceptContainer.getComponent(cc.Layout).updateLayout();

    this.renderTurnPageButtons();
  }

  renderTurnPageButtons() {
    if (this.count == 1) {
      this.previousConceptButton.active = false;
    }

    if (SchoolManager.getInstance().listConcepts.length == (9 * (this.count - 1) + this.listConceptLastPage)) {
      this.nextConceptButton.active = false;
    }
  }

  onPlayPressed(topicId: number) {
    SchoolService.loadConceptForPlayNow(topicId);
  }

  onAdvancedPressed(topicId: number) {
    SchoolService.loadConceptFirstPage(topicId);
  }

  onNextConceptPressed(topicId) {
    this.previousConceptButton.active = true;
    this.count++;
    if (this.roomName == "practiseCenter") {
      SchoolService.loadConceptNextPage(topicId);
    } else if (this.roomName == "improvement") {
      SchoolService.loadImprovementNextPage();
    } else {
      SchoolService.loadFocusNextPage();
    }
  }

  onPreviousConceptPressed(topicId) {
    this.count--;
    if (this.roomName == "practiseCenter") {
      SchoolService.loadConceptPreviousPage(topicId);
    } else if (this.roomName == "improvement") {
      SchoolService.loadImprovementPreviousPage();
    } else {
      SchoolService.loadFocusPreviousPage();
    }
  }

  onClosePressed() {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    ACE.topBar.show();
    ACE.topBar.positionButtons();
    this.chooseTopic = undefined;
    this.previousConceptButton.active = false;
    this.nextConceptButton.active = false;
    this.node.active = false;
  }

  showPractice() {
    this.topicContainer.active = true;
    this.conceptLayer.active = false;
    this.renderTopic();
  }

  conceptBack() {
    this.showPractice();
  }
}
