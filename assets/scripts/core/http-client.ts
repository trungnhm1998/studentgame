import { ACE } from "./global-info";

export function base64ArrayBuffer(arrayBuffer) {
  let base64 = "";
  const encodings = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  const bytes = new Uint8Array(arrayBuffer);
  const byteLength = bytes.byteLength;
  const byteRemainder = byteLength % 3;
  const mainLength = byteLength - byteRemainder;

  let a;
  let b;
  let c;
  let d;
  let chunk;

  // Main loop deals with bytes in chunks of 3
  for (let i = 0; i < mainLength; i += 3) {
    // Combine the three bytes into a single integer
    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

    // Use bitmasks to extract 6-bit segments from the triplet
    a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
    b = (chunk & 258048) >> 12; // 258048   = (2^6 - 1) << 12
    c = (chunk & 4032) >> 6; // 4032     = (2^6 - 1) << 6
    d = chunk & 63;        // 63       = 2^6 - 1

    // Convert the raw binary segments to the appropriate ASCII encoding
    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
  }

  // Deal with the remaining bytes and padding
  if (byteRemainder === 1) {
    chunk = bytes[mainLength];

    a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

    // Set the 4 least significant bits to zero
    b = (chunk & 3) << 4; // 3   = 2^2 - 1

    base64 += `${encodings[a]}${encodings[b]}==`;
  } else if (byteRemainder === 2) {
    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

    a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
    b = (chunk & 1008) >> 4; // 1008  = (2^6 - 1) << 4

    // Set the 2 least significant bits to zero
    c = (chunk & 15) << 2; // 15    = 2^4 - 1

    base64 += `${encodings[a]}${encodings[b]}${encodings[c]}=`;
  }

  return base64;
}

export interface HttpOptions {
  headers?: any;
}

export class HttpClient {
  constructor() {
  }

  openURL(url, target = "_blank") {
    if (cc.sys.isNative) {
      cc.sys.openURL(url);
    } else {
      ( window as any).open(url, target);
    }
  }

  imageURLToBase64(url, options?: HttpOptions) {
    return new Promise<any>((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open("GET", url, true);

      // If specified, responseType must be empty string or "text"
      xhr.responseType = "arraybuffer";

      xhr.onload = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            ACE.native.debug("HttpClient::Get response for: " + url);
            let arrayBuffer = xhr.response;
            // let str = String.fromCharCode.apply(null, new Uint8Array(arrayBuffer));
            let base64Image = base64ArrayBuffer(arrayBuffer);
            resolve(base64Image);
          }
        }
      };

      ACE.native.debug("HttpClient::Get request " + url);
      xhr.send(null);
    });
  }

  /**
   * JSON only
   * @param url
   * @param options
   */
  get(url, options?: HttpOptions, isJson = true) {
    return new Promise<any>((resolve, reject) => {
      if (cc.sys.isNative) {
        let request = new XMLHttpRequest();
        request.open("GET", url, true);
        if (isJson) {
          request.setRequestHeader("Accept", "application/json");
          request.setRequestHeader("Content-Type", "application/json");
        }

        // if (options && options.headers) {
        //   for (let headerKey of Object.keys(options.headers)) {
        //     let headerValue = options.headers[headerKey];
        //     request.setRequestHeader(headerKey, headerValue);
        //   }
        // }

        request.onload = (evt) => {
          ACE.native.debug("HttpClient::Get response: " + JSON.stringify(request));
          try {
            let json_response = isJson ? JSON.parse(request.responseText) : request;
            resolve(json_response);
          } catch {
            cc.log("cant parse JSON");
            reject(new Error("Can't parse JSON"));
          }
        };

        request.onerror = (evt) => {
          let error = JSON.stringify(request);
          ACE.native.debug("HttpClient::Get error: " + JSON.stringify(request));
          reject(error);
        };

        ACE.native.debug("HttpClient::Get request " + url);
        request.send(null);
      } else {
        // let data = {
        //   headers: {
        //   },
        // };
        // if (options && options.headers) {
        //   let keys = Object.keys(options.headers);
        //   for (let headerKey of keys) {
        //     let headerValue = options.headers[headerKey];
        //     data[headerKey] = headerValue;
        //   }
        // }
        fetch(url)
          .then((response) => resolve(isJson ? response.json() : response))
          .catch((error) => resolve(error));
      }

    });
  }

  post(url, data, options?: HttpOptions) {
    return new Promise<any>((resolve, reject) => {
      let request = new XMLHttpRequest();
      request.open("POST", url, true);
      request.setRequestHeader("Accept", "application/json");
      request.setRequestHeader("Content-Type", "application/json");

      if (options && options.headers) {
        for (let headerKey of Object.keys(options.headers)) {
          let headerValue = options.headers[headerKey];
          request.setRequestHeader(headerKey, headerValue);
        }
      }

      request.onload = (evt) => {
        ACE.native.debug("HttpClient::Post response: " + request.responseText);
        let json_response = JSON.parse(request.responseText);
        resolve(json_response);

      };

      request.onerror = (evt) => {
        ACE.native.debug("HttpClient::Post error: " + JSON.stringify(request));
        resolve(request.statusText);
      };

      ACE.native.debug("HttpClient::Post request " + url + " " + JSON.stringify(data));
      request.send(JSON.stringify(data));
    });
  }

  getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    let regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    let results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }
}
