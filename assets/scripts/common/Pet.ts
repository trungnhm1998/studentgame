import AccountManager from "../managers/account-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Pet extends cc.Component {
  @property
  petIndex: number = 0;

  @property({
    visible: false,
  })
  petIDList: string[] = null;

  onEnable() {
    const petIndexForExistedChracter = AccountManager.getInstance().petId;
    this.petIDList = ["draco", "haya", "moru", "shoo", "umic"];
    this.changePet(this.petIDList[petIndexForExistedChracter - 1]);
  }

  changePet(petName: string) {
    cc.loader.loadRes(`common/pets/${petName}`, cc.SpriteFrame, (err, newFrame: cc.SpriteFrame) => {
      this.node.getComponent(cc.Sprite).spriteFrame = newFrame;
    });

    setTimeout(() => {
      this.petTween();
    }, 200);
  }

  /**
   *
   * @param value 0 | 1 | 2 | 3 | 4
   */
  setPetIndex(value: number) {
    this.petIndex = value;
    this.changePet(this.petIDList[this.petIndex]);
  }

  /**
   * @returns 0 | 1 | 2 | 3 | 4
   */
  getPetIndex() {
    return this.petIndex;
  }

  nextPet() {
    if (this.petIndex == this.petIDList.length - 1) {
      this.petIndex = 0;
      this.changePet(this.petIDList[this.petIndex]);
      // for some reason, after the first cycle, Draco height becomes weird
      // and this messes with the auto position function.
      if (this.node.children.length > 0) {
        this.node.children[0].getComponent("speechBubble").updatePosition(0, 162);
      }
    } else {
      this.petIndex++;
      this.changePet(this.petIDList[this.petIndex]);
      if (this.node.children.length > 0) {
        this.node.children[0].getComponent("speechBubble").updatePosition();
      }
    }
  }

  previousPet() {
    if (this.petIndex == 0) {
      this.petIndex = this.petIDList.length - 1;
      this.changePet(this.petIDList[this.petIndex]);
      // for some reason, after the first cycle, Draco height becomes weird
      // and this messes with the update position function.
      if (this.node.children.length > 0) {
        this.node.children[0].getComponent("speechBubble").updatePosition(0, 162);
      }
    } else {
      this.petIndex--;
      this.changePet(this.petIDList[this.petIndex]);
      if (this.node.children.length > 0) {
        this.node.children[0].getComponent("speechBubble").updatePosition();
      }
    }
  }

  petTween() {
    this.node.setPosition(0, 0);
    this.node.stopAllActions();
    const petUp = cc.moveBy(1, cc.v2(0, 20)).easing(cc.easeSineInOut());
    const petDown = cc.moveBy(1, cc.v2(0, -20)).easing(cc.easeSineInOut());
    this.node.runAction(cc.repeatForever(cc.sequence(petUp, petDown)));
  }
}
