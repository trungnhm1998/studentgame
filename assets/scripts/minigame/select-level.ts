import GameScene from "../common/game-scene";
import { ACE } from "../core/global-info";
import MinigameController from "./minigame_controller";
import MinigamePopup from "./minigame-popup";
import { MathsServerAPI } from "./constants/minigame-constant";
import MinigameService from "./minigame-service";
import GamePlayController from "./gameplay-controller";
import { Random } from "../core/utils/random-utils";

const { ccclass, property } = cc._decorator;
const MAX_LEVEL = 18;
@ccclass
export default class SelectLevel extends GameScene {

  @property(cc.Node)
  levelParentNode: cc.Node = null;
  @property(cc.ScrollView)
  levelScrollView: cc.ScrollView = null;
  @property(cc.Prefab)
  arrowLevelSelectingPrefab: cc.Prefab = null;
  @property(MinigamePopup)
  popupStart: MinigamePopup = null;
  @property(cc.Node)
  loadingNode: cc.Node = null;
  @property(cc.SpriteFrame)
  cloudLevelActiveNormal: cc.SpriteFrame = null;
  @property(cc.SpriteFrame)
  cloudLevelActiveSpecific: cc.SpriteFrame = null;
  @property(cc.SpriteFrame)
  cloudLevelInactive: cc.SpriteFrame = null;
  @property(cc.Node)
  mapFullLevel: cc.Node = null;

  _levelSelecting: number = null;
  _levelSelectingNode: cc.Node = null;
  _arrowSelectingNode: cc.Node = null;

  onLoad() {
    super.onLoad();
    ACE.gameSocket.on(MathsServerAPI.LOAD_MINIMAP, (data) => this.initLevel(data));
  }

  onEnter() {
    // can't regist this stage
    super.onResize();
  }

  onEnable() {

    ACE.topBar.init();
    ACE.topBar.show();

    // todo get it from server
    this.loadingNode.active = true;
    // emit api
    MinigameService.loadMiniMap(MinigameController.getInstance().getMiniGameType());

    /// fake loading api
    // this.schedule(function() {
    //     this.initLevel(3);
    // },0, 0, 3);
  }

  initLevel(data) {
    cc.log("SelectLevel:  ", data);
    // {"Oid":"1F71827F-DDB6-476A-B30F-7601067BF4E1",
    // "StudentID":30091,
    // "Mode":"AmazingAddition",
    // "Stage":0,"Level":1,"Difficulty":1,
    // "Score":120,"Distance":750,"Correct":14,
    // "Incorrect":11,"IsBest":false,
    // "CreatedOn":"2019-12-14T17:58:24.510Z","UpdatedOn":"2019-12-14T17:58:24.510Z"}
    if (data == null) {
      this._levelSelecting = 1;
      // cc.log("GameData Level Nullllll: ", data);
    } else {
      // let rand = new Random();
      // this._levelSelecting = data.Level; //rand.integer(2, 6);//
      // cc.log("GameData Level ! null =>: ", data);
      // luc.tc fixed for parse data incorect, because response server changed ... :(
      if (data.Level == null) {
        this._levelSelecting = 1;
      } else {
        this._levelSelecting = ((data.Level - 1) * 3) + (data.Stage + 1);
        if (this._levelSelecting >= MAX_LEVEL) {
          this._levelSelecting = MAX_LEVEL;
        }
      }
    }
    this.loadingNode.active = false;
    let levelIdx = 1;
    let self = this;
    this.levelParentNode.children.forEach((level) => {
      level.getComponent("level-node").initLevel(
        levelIdx,
        level => self.levelOnSelectedCallback(level),
        levelIdx <= self._levelSelecting);
      if (levelIdx == self._levelSelecting) {
        self._levelSelectingNode = level;
      }
      if (levelIdx <= self._levelSelecting) {
        // enable
        if (levelIdx % 3 == 0) {
          level.getComponent(cc.Sprite).spriteFrame = this.cloudLevelActiveSpecific;
        } else {
          level.getComponent(cc.Sprite).spriteFrame = this.cloudLevelActiveNormal;
        }
      } else {
        // disable
        level.getComponent(cc.Sprite).spriteFrame = this.cloudLevelInactive;
      }

      levelIdx++;
    });

    // Init arrow
    this._arrowSelectingNode = cc.instantiate(this.arrowLevelSelectingPrefab);
    this._arrowSelectingNode.parent = this._levelSelectingNode;
    // Scroll to
    let nodeCompare = this.mapFullLevel.position;
    let levelPos = this._levelSelectingNode.position;
    // cc.log("Select level scroll: ", "AR: " +
    // levelPos.x +  " levelScrollView AR: "+nodeCompare.x);

    let xScroll = Math.abs(levelPos.x - nodeCompare.x - this.node.width / 2) /
      (this.levelScrollView.content.width - this.node.width);
    if (levelPos.x <= this.node.width / 2) {
      xScroll = 0;
    } else if (levelPos.x >= this.levelScrollView.content.width - this.node.width / 2) {
      xScroll = 1;
    }
    this.levelScrollView.scrollTo(new cc.Vec2(xScroll, 0), 3);
  }

  onLeave() {
    cc.log("Select level leave=>>");
    this._arrowSelectingNode.removeFromParent();

  }

  levelOnSelectedCallback(level: number) {
    cc.log("levelOnSelectedCallback: ", "Level:" + (Math.floor((level - 1) / 3) + 1) + " Stage: " + (level - 1) % 3);
    if (level <= this._levelSelecting) {
      MinigameController.getInstance().selectLevel((Math.floor((level - 1) / 3) + 1));
      MinigameController.getInstance().selectStage((level - 1) % 3);
      this.onGotoPlayMinigame();
      // this.popupStart.show(
      //     "Star Game "+ MinigameController.getInstance().getMiniGameName(),
      //     "Level: "+ level,
      //     this.onGotoPlayMinigame
      // ) ;
    } else {
      MinigameController.getInstance().selectLevel(level);
      this.popupStart.show(
        "Opp!",
        "Level " + level + " has not been unlocked!",
      );
    }
  }

  onGotoPlayMinigame() {
    // cc.log("onGotoPlayMinigame");
    ACE.sceneManager.pushScene("game-play");
    ACE.topBar.hide();
  }
}
