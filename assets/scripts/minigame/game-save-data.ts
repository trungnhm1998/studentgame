import { MiniGameType, MiniGameTypeKey } from "./constants/minigame-constant";

const { ccclass, property } = cc._decorator;
export default class GameSaveData {

  Mode: string = "";
  Level: number = 0;
  Stage: number = 0;
  Difficulty: number = 0;
  Score: number = 0;
  Distance: number = 0;
  Correct: number = 0;
  Incorrect: number = 0;

  constructor(type: MiniGameType, level: number,
              stage: number, difficulty: number, score: number,
              distance: number, correct: number, incorrect: number,
  ) {
    let modeValue = "Add";
    switch (type) {
      case MiniGameType.AMAZING_ADDITION:
        modeValue = MiniGameTypeKey.AMAZING_ADDITION;
        break;
      case MiniGameType.SUPPER_SUBTRACTION:
        modeValue = MiniGameTypeKey.SUPPER_SUBTRACTION;
        break;
      case MiniGameType.MAGNIFICIENT_MULTIPLICATION:
        modeValue = MiniGameTypeKey.MAGNIFICIENT_MULTIPLICATION;
        break;
      case MiniGameType.DYNAMIC_DIVISIONS:
        modeValue = MiniGameTypeKey.DYNAMIC_DIVISIONS;
        break;
      case MiniGameType.SUPER_MIX:
        modeValue = MiniGameTypeKey.SUPER_MIX;
        break;
    }

    this.Mode = modeValue;
    this.Level = level;
    this.Stage = stage;
    this.Difficulty = difficulty;
    this.Score = score;
    this.Distance = distance;
    this.Correct = correct;
    this.Incorrect = incorrect;
  }
}
