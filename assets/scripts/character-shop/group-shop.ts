import GroupNode from "./group-node";
import GroupItem from "./model/group-item";
import ItemShop from "./item-shop";
import { Gender } from "../constants/qa.constants";
import AudioManager from "../core/audio-manager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GroupShop extends cc.Component {
  @property(cc.Node)
  groupContents: cc.Node = null;

  @property(cc.Prefab)
  groupNodePref: cc.Prefab = null;

  @property({ type: [GroupItem] })
  groupItems: GroupItem[] = [];

  @property(ItemShop)
  itemShop: ItemShop = null;

  _listGroupNode = new Array<GroupNode>();
  _groupItemActived: number = 0;

  // LIFE-CYCLE CALLBACKS:
  start() {
    // CharacterShopService.loadShopCharacterTags(this.onShopTagResponce);
  }

  onShopTagResponce(data) {
    // cc.log("onShopTagResponce", data);
  }

  initGroupShop(characterGender: Gender) {
    this._groupItemActived = 0;
    this.groupContents.removeAllChildren();
    this._listGroupNode = new Array<GroupNode>();

    let idxIcon = 0;
    if (characterGender == Gender.GIRL) {
      idxIcon = 1;
    }
    let idx = 0;
    for (const entry of this.groupItems) {
      const nodeInst = cc.instantiate(this.groupNodePref);
      nodeInst.parent = this.groupContents;

      nodeInst.setPosition(0, 0);
      const groupObj = nodeInst.getComponent("group-node");
      this._listGroupNode.push(groupObj);
      if (groupObj == null) { cc.log("Group node null !!!!"); }
      groupObj.initNode(entry.itemName, entry.iconSFs[idxIcon], idx, this);
      idx++;
    }

    /// set active first group
    this.schedule(function() {
      this._listGroupNode[this._groupItemActived].setGroupState(true);
    }, 0.3);
  }

  groupItemOnSellected(groupID: number) {
    AudioManager.getInstance().playAudio("BUTTON_CLICK_SOUND");
    // this.listGroupNode[groupID].setState(true);
    this._listGroupNode[this._groupItemActived].setGroupState(false);
    this._groupItemActived = groupID;
    this.itemShop.onChangeGroupItem(groupID);
  }
}
