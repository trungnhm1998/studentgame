const { ccclass, property } = cc._decorator;

@ccclass
export default class LeaderboardModel {
  CountDay: number = 47;
  FirstName: string = "Cuong";
  FullName: string = "Cuong Dang";
  Gender: boolean = false;
  LastName: string = "Dang";
  StudentID: number = 24316;
  TermName: string = "Term 1";
  Value: number = 13850;
  YearName: string = "Year";
  isMe: number = 0;
  isRetail: number = 0;
}
