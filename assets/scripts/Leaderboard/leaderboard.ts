import LeaderboardService from "./leaderboard-service"
import LeaderboardModel from "../models/leaderboard-model";
import { ACE } from "../core/global-info";
import { SERVER_EVENT } from "../core/constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Leaderboard extends cc.Component {
  @property(cc.Prefab)
  leaderboardItemPrefab: cc.Prefab = null;
  @property(cc.Node)
  parentItem: cc.Node = null;
  @property(cc.Node)
  btnTerm: cc.Node = null;
  @property(cc.Node)
  btnYear: cc.Node = null;
  @property(cc.Animation)
  animOpenPopup: cc.Animation = null;

  @property([cc.SpriteFrame])
  spriteBtnDaysLefts: cc.SpriteFrame[] = [];

  _NUM_SCALE_BTN_ACTIVE = 1.1;
  _NUM_SCALE_BTN_INACTIVE = 0.8;

  _listLeaderBoard: LeaderboardModel[] = null;

  onLoad() {  
    this.btnTerm.scale = this._NUM_SCALE_BTN_ACTIVE;
    this.btnYear.scale = this._NUM_SCALE_BTN_INACTIVE;
    ACE.gameSocket.on(SERVER_EVENT.LOAD_LEADERBOARD_LIST_SHOP, data =>
      this.onLoadedLeaderboard(data)
    );
  }

  onLoadedLeaderboard(data) {
    this._listLeaderBoard = new Array<LeaderboardModel>();
    this.parentItem.removeAllChildren();
    this._listLeaderBoard = data;
    var self = this;
    var idx = 1;
    this._listLeaderBoard.forEach(element => {
      let nodeInst = cc.instantiate(self.leaderboardItemPrefab);
      nodeInst.parent = self.parentItem;
      nodeInst.setPosition(0, 0);
      var itemObj = nodeInst.getComponent("leaderboard-item");

      itemObj.initItem(element, idx);
      idx++;
    });
  }

  showPopup() {

    ACE.topBar.hide();
    this.node.active = true;
    this.animOpenPopup.play();
    LeaderboardService.loadLeaderboardList(0);
  }

  termBtnOnClick() {
    this.btnTerm.scale = this._NUM_SCALE_BTN_ACTIVE;
    this.btnYear.scale = this._NUM_SCALE_BTN_INACTIVE;
    LeaderboardService.loadLeaderboardList(0);
  }

  yearBtnOnClick() {
    this.btnTerm.scale = this._NUM_SCALE_BTN_INACTIVE;
    this.btnYear.scale = this._NUM_SCALE_BTN_ACTIVE;
    LeaderboardService.loadLeaderboardList(1);
  }

  closePopupOnClick() {
    ACE.topBar.show();
    this.node.active = false;
  }
}
