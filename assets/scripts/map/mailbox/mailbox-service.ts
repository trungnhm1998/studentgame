import { ACE } from "../../core/global-info";
import { SERVER_EVENT } from "../../core/constant";
import AccountManager from "../../managers/account-manager";
const { ccclass, property } = cc._decorator;

@ccclass
export default class MailboxService {

  static receiveGift(messageOid) {
    ACE.gameSocket.emit(SERVER_EVENT.RECEIVE_GIFT, {
      messageOid
    });
  }

  static receiveGiftAll() {
    ACE.gameSocket.emit(SERVER_EVENT.RECEIVE_GIFT_ALL, { accessToken: AccountManager.getInstance().accessToken });
  }

  // static loadMessageListAll() {
  //     ACE.gameSocket.emit(SERVER_EVENT.LOAD_MESSAGE_LIST_ALL, { accessToken: AccountManager.getInstance().accessToken });
  // }

  static deleteMessage(DeleteAll, ID) {
    // Deletes a single message if DeleteAll = 1
    // If DeleteAll, id can be anything. it will override with student id in backend.
    // If !DeleteAll, id should be messageID.
    ACE.gameSocket.emit(SERVER_EVENT.DELETE_MESSAGE, {
      DeleteAll,
      ID,
      accessToken: AccountManager.getInstance().accessToken
    });
  }

  static setMessageSaved(messageID, isSaved) {
    // isSaved should be 1 or 0
    ACE.gameSocket.emit(SERVER_EVENT.SET_MESSAGE_SAVED, {
      messageID,
      isSaved,
      accessToken: AccountManager.getInstance().accessToken
    });
  }

  static getMessageByID(messageID) {
    ACE.gameSocket.emit(SERVER_EVENT.GET_MESSAGE_BY_ID, {
      messageID,
      accessToken: AccountManager.getInstance().accessToken
    });
  }
}
