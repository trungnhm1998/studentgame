import StateRange from "./state-range";
import BaseStage from "./base-stage";
import SpeedLevel from "./speed-level";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SubtractionStage extends BaseStage {

  constructor() {
    super();
    this.stageRanges = new Array<StateRange>();
    this.stageRanges = [
      // 1
      new StateRange(1, 10),
      new StateRange(5, 15),
      new StateRange(16, 25),
      // 2
      new StateRange(16, 25),
      new StateRange(25, 35),
      new StateRange(35, 45),
      // 3
      new StateRange(36, 45),
      new StateRange(45, 55),
      new StateRange(55, 65),
      // 4
      new StateRange(55, 65),
      new StateRange(65, 75),
      new StateRange(75, 99),
      // 5
      new StateRange(75, 99),
      new StateRange(99, 150),
      new StateRange(151, 200),
      // 6
      new StateRange(151, 200),
      new StateRange(201, 250),
      new StateRange(300, 399),
    ];

    BaseStage.prototype.stageSpeeds = [
      // 1
      new SpeedLevel(10, 15, 20, 25, 30),
      new SpeedLevel(10, 12.5, 15, 17.5, 20),
      new SpeedLevel(10, 12, 14, 16, 18),
      // 2
      new SpeedLevel(12, 14, 16, 18, 20),
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(9, 11, 13, 15, 17),
      // 3
      new SpeedLevel(12, 14, 16, 18, 20),
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(9, 11, 13, 15, 17),
      // 4
      new SpeedLevel(12, 14, 16, 18, 20),
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(8, 10, 12, 14, 16),
      // 5
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(10, 12, 14, 16, 18),
      new SpeedLevel(7, 9, 11, 13, 15),
      // 6
      new SpeedLevel(8, 10, 12, 14, 16),
      new SpeedLevel(7, 9, 11, 13, 15),
      new SpeedLevel(7, 9, 11, 13, 15),
    ];
  }
}
