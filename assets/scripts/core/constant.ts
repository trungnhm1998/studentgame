import { DialogType } from "./interfaces/dialog-type";

export const SERVER_EVENT = {
  CONNECT: "connect",
  ERROR: "error",
  DISCONNECT: "disconnect",
  RECONNECT: "reconnection",
  LOGIN: "login",
  PROFILE: "profile",
  LOGOUT: "logout",
  FORCE_LOGOUT: "forceLogout",
  CLIENT_LOGOUT: "client_logout",
  OPEN_ROOM: "openRoom",
  LOAD_STUDENT_GAME_TUTORIAL: "loadStudentGameStateTutorial",
  SAVE_STUDENT_GAME_TUTORIAL: "saveStudentGameStateTutorial",
  CLOSE_WEBVIEW: "closeWebview",
  VALIDATE_TOKEN: "validateToken",
  SAVE_ACCESS_TOKEN: "saveAccessToken",

  STUDENT_COINS_STARS: "studentCoinsStars",
  SELECT_GENDER: "selectGender",
  SELECT_NEXT: "selectNext",
  SELECT_PREVIOUS: "selectPrevious",
  GET_SELECTING_GENDER: "getSelectingGender",
  GET_FIRST_PET: "getFirstPet",
  ASSET_DATA: "assetData",
  WEARING_GROUP: "wearingGroup",
  NEXT_PET: "selectNextPet",
  PREV_PET: "selectPreviousPet",
  FINISH_SELECTION: "finishSelection",
  FINISH_SELECTION_NEW: "finishSelectionNew",
  GET_NUMBER_OF_GIFTS: "getNumberGift",
  LOAD_FRIEND_LIST: "loadFriendList",
  SEND_GIFT: "sendGift",
  CHECK_CHARACTER_EXISTS: "checkExistCharacter",
  PROFILE_ITEM_CHANGED: "profile.itemChanged",

  GET_BASIC_PRODUCTS: "getBasicProducts",
  GET_BASIC_USING_ITEMS: "getBasicUsingItems",
  GET_BASIC_BOY_WEARING_GROUP: "getBasicBoyWearingGroup",
  GET_BASIC_GIRL_WEARING_GROUP: "getBasicGirlWearingGroup",
  USING_ITEMS: "usingItems",
  // LOAD_STRENGTH: "loadStrength",

  // <------ Begin StudentHouse ------>
  GET_WHOSE_HOUSE: "getWhoseHouse",
  GET_CURRENT_ROOM: "getCurrentRoom",
  LOAD_STUDENT_ROOMS: "loadStudentRooms",
  LOAD_MESSAGE_LIST_ALL: "loadMessageListAll",
  INIT_ROOM: "initRoom",

  // <------ Begin MailBox (Map) ------>
  RECEIVE_GIFT: "receiveGift",
  RECEIVE_GIFT_ALL: "receiveGiftAll",
  DELETE_MESSAGE: "deleteMessage",
  SET_MESSAGE_SAVED: "setMessageSaved",
  GET_MESSAGE_BY_ID: "getMessageByID",

  // <----- Character --->
  LOAD_CHARACTER_BASIC_USING: "getBasicUsingItems",
  LOAD_CHARACTER_USING: "usingItems",
  LOAD_BASIC_BOY_WEARING_GROUP: "getBasicBoyWearingGroup",
  LOAD_BASIC_GIRL_WEARING_GROUP: "getBasicGirlWearingGroup",

  // <------ Begin MathHouse (School) ------>
  LOAD_FOCUS_CENTER: "loadFocusCenter",
  LOAD_IMPROVEMENT: "loadImprovement",
  ENROLMENT_LIST: "enrolmentList",
  GET_HOMEWORK_FOR_MATH_HOUSE: "homeWorkForMathHouse",
  GET_IMPROVEMENT_FOR_MATH_HOUSE: "improvementForMathHouse",
  GET_NAPLAN_FOR_MATH_HOUSE: "naplanForMathHouse",
  GET_FOCUS_FOR_MATH_HOUSE: "focusCenterForMathHouse",
  GET_REVISION_FOR_MATH_HOUSE: "revisionForMathHouse",
  GET_ASSIGNMENT_FOR_MATH_HOUSE: "assignmentForMathHouse",
  LOAD_LEADERBOARD_LIST: "loadLeaderboardList",
  // <------ End MathHouse (School) ------>

  // <------ Begin Practise ------>
  LOAD_TOPIC: "loadTopics",
  LOAD_CONCEPT_FOR_PLAYNOW: "loadConceptForPlayNow",
  LOAD_CONCEPT_FIRST_PAGE: "conceptFirstPage",
  LOAD_CONCEPT_NEXT_PAGE: "conceptNextPage",
  LOAD_CONCEPT_PREVIOUS_PAGE: "conceptPreviousPage",
  GENERATE_PRACTISE_QA: "initPractiseBlock",
  SUBMIT_ANSWER_PRACTISE: "submitAnswerPractiseBlock",
  // <------ End Practise ------>

  // <------ Begin Improvement Centre ------>
  IMPROVEMENT_FIRST_PAGE: "improvementFirstPage",
  IMPROVEMENT_NEXT_PAGE: "improvementNextPage",
  IMPROVEMENT_PREVIOUS_PAGE: "improvementPreviousPage",
  GENERATE_IMPROVEMENT_QA: "initImpQatemplate",
  // <------ End Improvement Centre ------>

  // <------ Begin Homework ------>
  LOAD_HOMEWORK: "homework",
  LOAD_CURRENT_HOMEWORK: "currentQuestionHomework",
  SUBMIT_ANSWER_HOMEWORK: "submitAnswerHomework",
  // <------ End Homework ------>

  // <------ Begin Naplan ------>
  LOAD_NAPLAN: "naplan",
  LOAD_CURRENT_NAPLAN: "currentQuestionNaplan",
  SUBMIT_ANSWER_NAPLAN: "submitAnswerNaplan",
  FINALIZE_NAPLAN: "finalizeNaplan",
  // <------ End Naplan ------>

  // <------ Begin Focus Centre ------>
  FOCUS_FIRST_PAGE: "focusCenterFirstPage",
  FOCUS_NEXT_PAGE: "focusCenterNextPage",
  FOCUS_PREVIOUS_PAGE: "focusCenterPreviousPage",
  GENERATE_FOCUS_QA: "initFocQatemplate",
  // <------ End Focus Centre ------>

  // <------ Begin Revision ------>
  LOAD_REVISION: "revision",
  LOAD_CURRENT_REVISION: "currentQuestionRevision",
  SUBMIT_ANSWER_REVISION: "submitAnswerRevision",
  FINALIZE_REVISION: "finalizeRevision",
  // <------ End Revision ------>

  // <------ Begin Assignment ------>
  LOAD_ASSIGNMENT: "assignment",
  LOAD_CURRENT_ASSIGNMENT: "currentQuestionAssignment",
  SUBMIT_ANSWER_ASSIGNMENT: "submitAnswerAssignment",
  FINALIZE_ASSIGNMENT: "finalizeAssignment",
  // <------ End Assignment ------>

  //#region QA
  CURRENT_QUESTION_PRACTISE_BLOCK: "currentQuestionPractiseBlock",
  // #endrgion

  // <----- Character --->
  CHECK_EXIST_CHARACTER: "checkExistCharacter",

  // <----- Character Shop --->
  LOAD_SHOP_TAG: "shopTags",
  LOAD_CHAR_INFO: "characterInfo",
  LOAD_TOTAL_PAGE: "totalPagesInShop",
  LOAD_CURRENT_PAGE: "shopCurrentPage",
  LOAD_PRE_PAGE: "shopPreviousPage",
  LOAD_NEXT_PAGE: "shopNextPage",
  LOAD_WEARING_GROUP: "wearingGroup",

  TRY_ITEM_PRODUCT: "tryCharacterProduct",
  // LOAD_SHOP_TAG: "saveStudentGameStateTutorial",
  // LOAD_SHOP_TAG: "saveStudentGameStateTutorial",
  EQUIP_CHARACTER_ITEM: "equipCharacterItemInShop",
  HIDE_CHARACTER_ITEM: "unequipCharacterItemShop",
  BUY_ITEM_BY_STAR: "buyCharacterItemByStar",
  BUY_ITEM_BY_COIN: "buyCharacterItemByCoin",
  LOAD_LEADERBOARD_LIST_SHOP: "loadLeaderboardList",

  // Friend student house // open room
  // Done
  LOAD_ALL_ROOM_ITEM_GROUP_L1: "loadAllRoomItemGroupL1",
  GET_CURRENT_FRIEND: "getCurrentFriend",
  LOAD_ROOM_ITEM_USING: "loadRoomItemUsing",
  LOAD_FRIEND_CHARACTER_USING: "usingItemsOfFriend",
  WEARING_GROUP_OF_FRIEND: "wearingGroupOfFriend",
  GET_PRE_FRIEND: "getPreviousFriend",
  GET_NEXT_FRIEND: "getNextFriend",

  // <----- Begin Student House ----->
  TRY_ROOM_ITEM: "tryRoomItem",
  UNEQUIP_ROOM_ITEM: "unenquipRoomItem",
  EQUIP_ROOM_ITEM: "equipRoomItem",
  ROOM_ITEM_CURRENT_PAGE: "roomItemCurrentPage",
  BUY_ROOM: "buyRoom",
  ROOM_ITEM_FIRST_PAGE: "roomItemFirstPage",
  BUY_ROOM_ITEM: "buyRoomItem",
  ROOM_ITEM_NEXT_PAGE: "roomItemNextPage",
  ROOM_ITEM_PREVIOUS_PAGE: "roomItemPreviousPage",
  LOAD_ROOM_ITEM_GROUP_L1: "loadRoomItemGroupL1",
  LOAD_ROOM_ITEM_GROUP_L2: "loadRoomItemGroupL2",
  // <----- End Student House ----->

  // custom bridge server events
  CHANGE_QA_SERVER: "changeQaServer",
};

export const DIALOG_TYPE = {
  LOADING: {
    name: "loading_dialog",
    type: -1,
  },
  NOTICE: {
    name: "star_notice_dialog",
    type: 0,
  },
  WELCOME_TUTORIAL: {
    name: "welcome_tutorial_dialog",
    type: 1,
  },
};

export const GAME_EVENT = {
  CLOSE_DIALOG: "CLOSE_DIALOG",
};

export const STORAGE_KEY = {
  ACCESS_TOKEN: "accessToken",
  REMEMBER_INFO: "rememberInfo",
  LAST_ENTER_STATE: "lastEnterState",
  IS_AUDIO_OFF: "isAudioOff",
  CHARACTER_EXIST: "characterExist",
};
