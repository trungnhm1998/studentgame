import System from "../../core/ecs/system";
import PositionComponent from "../ecs-components/position-component";
import Entity from "../../core/ecs/entity";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PositionSystem extends System {
  _filter = ["PositionComponent"];
  onLoad() {
    super.onLoad();
  }

  onEntitiesUpdate(entity: Entity) {
    // cc.log("Entity size with Position Component", this._entities.size);
    const { position, anchor, size } = entity.getECSComponent<PositionComponent>("PositionComponent")._data;
    // if the current anchor and position is already set
    entity.node.anchorX = anchor.x;
    entity.node.anchorY = anchor.y == 0 ? 1 : anchor.y;
    entity.node.setPosition(cc.v2(position.x, position.y >= 0 ? -position.y : position.y));
    entity.node.width = size.width;
    entity.node.height = size.height;
  }
}
