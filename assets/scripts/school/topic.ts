const { ccclass, property } = cc._decorator;

export interface ITopic {
  NumberBlock: number;
  NumberCorrect: number;
  NumberInCorrect: number;
  NumberOfQa: number;
  Percentage: number;
  ProgressStar: number;
  TopicDescription: string;
  TopicID: number;
  TopicName: string;
  TopicNameDescription: string;
}

@ccclass
export default class Topic extends cc.Component implements ITopic {
  @property(cc.Node)
  topicBack: cc.Node = null;

  @property(cc.Node)
  starContainer: cc.Node = null;

  @property(cc.SpriteFrame)
  noStarSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  wholeStarSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  halfStarSF: cc.SpriteFrame = null;

  // reference to five star in UI
  stars: cc.Node[] = [];

  onPlayCallback: Function = null;
  onAdvancedCallback: Function = null;

  NumberBlock: number;
  NumberCorrect: number;
  NumberInCorrect: number;
  NumberOfQa: number;
  Percentage: number;
  ProgressStar: number;
  TopicDescription: string;
  TopicID: number;
  TopicName: string;
  TopicNameDescription: string;

  onEnable() {
    this.node.getComponent(cc.Sprite).enabled = true;
    this.topicBack.active = false;
  }

  setData(iTopic: ITopic) {
    const {
      NumberBlock,
      NumberCorrect,
      NumberInCorrect,
      NumberOfQa,
      Percentage,
      ProgressStar,
      TopicDescription,
      TopicID,
      TopicName,
      TopicNameDescription
    } = iTopic;
    this.NumberBlock = NumberBlock;
    this.NumberCorrect = NumberCorrect;
    this.NumberInCorrect = NumberInCorrect;
    this.NumberOfQa = NumberOfQa;
    this.Percentage = Percentage;
    this.ProgressStar = ProgressStar;
    this.TopicDescription = TopicDescription;
    this.TopicID = TopicID;
    this.TopicName = TopicName;
    this.TopicNameDescription = TopicNameDescription;
    this.setupStars();
  }

  setupStars() {
    if (this.stars.length == 0 || this.stars.length == 5) {
      this.stars = [];
      for (let i = 0; i < this.starContainer.children.length; i++) {
        const starNode = this.starContainer.children[i];
        starNode.zIndex = i;
        this.stars.push(starNode);
      }
    }
    // setup star based on ProgessStar
    // clean up first
    for (let i = 0; i < this.stars.length; i++) {
      const starNode = this.stars[i];
      starNode.removeAllChildren();
      starNode.getComponent(cc.Sprite).spriteFrame = this.noStarSF;
    }
    // we can either add a sprite as it child or change the spriteframe
    // I'll change the spriteframe
    let starToAdd = this.ProgressStar;
    let index = 0;
    while (starToAdd > 0) {
      // priorites whole star first
      if (starToAdd - 1 >= 0) {
        this.stars[index].getComponent(
          cc.Sprite
        ).spriteFrame = this.wholeStarSF;
        starToAdd -= 1;
        index++;
      } else if (starToAdd - 0.5 >= 0) {
        this.stars[index].getComponent(cc.Sprite).spriteFrame = this.halfStarSF;
        starToAdd -= 0.5;
        index++;
      }
    }
  }

  onTopicPressed() {
    // do flip anim
    this.node.runAction(
      cc.sequence(
        cc.spawn(cc.scaleTo(0.2, 0, 1), cc.skewTo(0.2, 2.5, 0)),
        cc.callFunc(() => {
          this.node.getComponent(cc.Sprite).enabled = false;
          this.topicBack.active = true;
        }),
        cc.spawn(cc.scaleTo(0.2, 1, 1), cc.skewTo(0.2, 0, 0))
      )
    );

    // show play
  }

  onPlayPressed() {
    if (this.onPlayCallback != null) {
      this.onPlayCallback();
    }
  }

  onAdvancedPressed() {
    cc.log("onAdvancedPressed()::topic.ts");
    if (this.onAdvancedCallback != null) {
      this.onAdvancedCallback();
      this.node.getComponent(cc.Sprite).enabled = true;
    }
  }

  onBackMaskPressed() {
    this.node.runAction(
      cc.sequence(
        cc.spawn(cc.scaleTo(0.2, 0, 1), cc.skewTo(0.2, 2.5, 0)),
        cc.callFunc(() => {
          this.node.getComponent(cc.Sprite).enabled = true;
          this.topicBack.active = false;
        }),
        cc.spawn(cc.scaleTo(0.2, 1, 1), cc.skewTo(0.2, 0, 0))
      )
    );
  }
}
