import { MailboxMessage } from "../../models/mailbox-message";
import { ACE } from "../../core/global-info";

const { ccclass, property } = cc._decorator;

export enum FromType {
  Friend = 0,
  Teacher = 1,
  Parent = 2,
  System = 3,
}

@ccclass
export default class Message extends cc.Component {
  @property(cc.Sprite)
  messageIcon: cc.Sprite = null;

  @property(cc.Label)
  fromLabel: cc.Label = null;

  @property(cc.Sprite)
  stampSprite: cc.Sprite = null;

  @property(cc.SpriteFrame)
  claimedSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  readSF: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  selected: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  selectedHover: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  selectedPressed: cc.SpriteFrame = null;

  @property([cc.SpriteFrame])
  activeIcons: cc.SpriteFrame[] = [];

  @property([cc.SpriteFrame])
  idleIcons: cc.SpriteFrame[] = [];

  _giftFrom: FromType;
  _buttonComponent: cc.Button = null;

  _iconSF: cc.SpriteFrame = null;
  _normalSF: cc.SpriteFrame = null;
  _hoverSF: cc.SpriteFrame = null;
  _pressedSF: cc.SpriteFrame = null;

  _message: MailboxMessage;
  isSelected: boolean = false;
  messageCallback: Function = null;

  onLoad() {
    // this.stampSprite.node.active = false;
  }

  setupData(message: MailboxMessage, callback: Function) {
    this.messageCallback = callback;
    this._buttonComponent = this.node.getComponent(cc.Button);

    // TODO: Implement on touch/ on cancel for mobile devices
    // this.node.on(cc.Node.EventType.TOUCH_START, () => {
    //   cc.log('Message::setupData::TouchStart')
    // })
    
    this._normalSF = this._buttonComponent.normalSprite;
    this._hoverSF = this._buttonComponent.hoverSprite;
    this._pressedSF = this._buttonComponent.pressedSprite;
    this._iconSF = this.messageIcon.spriteFrame;

    this._message = message;
    this.fromLabel.string = this.getSenderName();

    if (message.FromStudentID != null) {
      this._giftFrom = FromType.Friend;
    } else if (message.FromParentID) {
      this._giftFrom = FromType.Parent;
    } else if (message.FromTeacherID) {
      this._giftFrom = FromType.Teacher;
    } else {
      this._giftFrom = FromType.System;
    }

    this.messageIcon.spriteFrame = this.idleIcons[this._giftFrom];

    if (message.Coins > 0 || message.Stars > 0) {
      this.stampSprite.spriteFrame = this.claimedSF;
    } else {
      this.stampSprite.spriteFrame = this.readSF;
    }

    this.stampSprite.node.active = message.IsReceive;
  }

  isReadMessage(isRead = false) {
    this.stampSprite.node.active = isRead;
  }

  onMessagePressed() {
    if (this.messageCallback != null) this.messageCallback(this._message.Oid);
  }

  select() {
    if (!this.isSelected) {
      this.isSelected = true;
      this.messageIcon.spriteFrame = this.activeIcons[this._giftFrom];
      this._buttonComponent.normalSprite = this.selected;
      this._buttonComponent.hoverSprite = this.selectedHover;
      this._buttonComponent.pressedSprite = this.selectedPressed;
    }
  }

  deselect() {
    if (this.isSelected) {
      this.isSelected = false;
      this.messageIcon.spriteFrame = this.idleIcons[this._giftFrom];
      this._buttonComponent.normalSprite = this._normalSF;
      this._buttonComponent.hoverSprite = this._hoverSF;
      this._buttonComponent.pressedSprite = this._pressedSF;
    }
  }

  getSenderName(): string {
    if (this._message.FromStudentID) {
      return this._message.FromStudentFirstName;
    } else if (this._message.FromParentID) {
      return this._message.FromParentID;
    } else if (this._message.FromTeacherID) {
      return this._message.FromTeacherFirstName;
    } else {
      return "S.T.A.R Maths Online";
    }
  }

  getGiftValue(): number {
    switch (this._message.Type) {
      case 1:
        return this._message.Coins;
      case 2:
        return this._message.Stars;
      default:
        return 0;
    }
  }
}