export class StringUtils {
  formatDate(time: string|number) {
    const nowDate = new Date(time);
    const strMonth = nowDate.getMonth() < 10 ? ("0" + (nowDate.getMonth() + 1)) : (nowDate.getMonth() + 1);
    const strDate = nowDate.getDate() < 10 ? ("0" + nowDate.getDate()) : (nowDate.getDate());

    return (nowDate.getFullYear() + "-" + strMonth + "-" + strDate + " " + ((nowDate.getHours() < 10) ? ("0" + nowDate.getHours()) : (nowDate.getHours())) + ":" + ((nowDate.getMinutes() < 10) ? ("0" + nowDate.getMinutes()) : (nowDate.getMinutes())) + ":" + ((nowDate.getSeconds() < 10) ? ("0" + nowDate.getSeconds()) : (nowDate.getSeconds())));
  }

  limitNameChar(fullName: string, nameCharLimit = 12) {
    let name = fullName;
    if (fullName.length > nameCharLimit) {
      const temp = fullName.split(" ");
      if (temp.length > 1) {
        if (temp[0].length > nameCharLimit - 2) {
          name =
            temp[0].slice(0, nameCharLimit - 5) +
            "... " +
            temp[1].charAt(0);
        } else {
          name = temp[0] + temp[1].charAt(0);
        }
      } else {
        name = temp[0].slice(0, nameCharLimit - 4) + "...";
      }
    }
    return name;
  }
}
